
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Configuration                         ║
// ║ Description      : Defines a configuration map.          ║
// ║ History:                                                 ║
// ║  - 18/12/2017 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef CONFIGURATION_HPP
#define CONFIGURATION_HPP

namespace org {
  namespace application {
    class Configuration;
  }
}

#include <entity/loggingentity.hpp>
#include <application/configurationparameter.hpp>
#include <types/cstring.hpp>

class org::application::Configuration : public virtual org::entity::LoggingEntity {
  protected:
    std::map <org::types::CString, std::list <ConfigurationParameter *> *> * _configurationNodesMap;
    std::map <int, const char *> * _indentationNameMap;
    bool _configurationFileFound;

    void InitializeConfiguration ( void );
    const char * FindUpperLevelLastName ( int indentation );
    void UpdateLastSeenName ( const char * line );
    ConfigurationParameter* TryReadParameter ( const char * source, const char * line, char separator );
    ConfigurationParameter * GetParameter ( const char * key, int index );
    ConfigurationParameter * GetParameter ( const char * key );
    void AddParameter ( ConfigurationParameter* node );
    void AddParameter ( ConfigurationParameter* node, bool clearIfOverridable );
    void ProcessParameter ( ConfigurationParameter * node );
    void ProcessParameter ( ConfigurationParameter * node, bool clearPrevious );
    void AddingConfiguration ( ConfigurationParameter* node );
    int NodeIsConfigurationFile ( ConfigurationParameter* node );
    const char * Transform ( ConfigurationParameter * node, char openingChar, char closingChar );

  public:
    static Configuration * Main;

    Configuration ( char* source, char separator );
    Configuration ( const char * source, const char ** parameters, int parametersCount, char separator );

    virtual ~Configuration ( void );

    const char * Get ( const char * key );
    const char * GetValue ( const char * key );
    int ContainsKey ( const char * key );
    std::map <org::types::CString, std::list <ConfigurationParameter *> *> * ExtractParametersMasked ( const char * mask );
    void Print ( void );
    void TransformAll ( void );
    void ReadFile ( const char * source, char separator );
    void Read ( const char * source, const char ** parameters, int parametersCount, char separator );
    bool ConfigurationFileFound ( void );
};

#endif
