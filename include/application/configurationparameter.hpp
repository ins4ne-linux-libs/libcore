
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Constants                             ║
// ║ Description      : Constants definitions.                ║
// ║ History:                                                 ║
// ║  - 18/12/2017 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef CONFIGURATION_PARAMETER_HPP
#define CONFIGURATION_PARAMETER_HPP

namespace org {
  namespace application {
    class ConfigurationParameter;
  }
}

class org::application::ConfigurationParameter : public virtual org::Object {
  protected:
    char* _source;
    char* _name;
    char* _value;
    char* _transformed;
    unsigned char _status;
    unsigned char _indentation;
    bool _overridable;
    ConfigurationParameter * _nested;

    void Configure ( const char * source, const char * name, const char * value );

  public:
    ConfigurationParameter ( const char * source, const char * name, const char * value );
    ConfigurationParameter ( const char * source, const char * line, char separator );
    ConfigurationParameter ( const char * newName, ConfigurationParameter * nested );

    virtual ~ConfigurationParameter ( void );

    bool Overridable ( void );
    void Overridable ( bool overridable );
    unsigned char GetStatus ( void );
    const char * GetSource ( void );
    const char * GetValue ( void );
    const char * GetName ( void );
    const char * GetTransformed ( void );
    int GetIndentation ( void );
    void SetTransformed ( const char * newTransformed );
    void SetName ( const char * newName );
    void SetValue ( const char * newValue );
    void SetSource ( const char * newSource );
    void Read ( const char * line, char separator );
    char * ToString ( void );
};

#endif
