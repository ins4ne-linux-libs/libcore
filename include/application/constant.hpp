
#ifndef APPLICATION_CONSTANT_HPP
#define APPLICATION_CONSTANT_HPP

namespace org {
  namespace application {
    class Constant;
  }
}

class org::application::Constant : public virtual org::Object {
  public:
    Constant ( void );

    ~Constant ( void );

    virtual int AsInteger ( void ) const;
    virtual const char * AsString ( void ) const;
    virtual char AsCharacter ( void ) const;
    virtual unsigned char AsByte ( void ) const;

    operator int ( void ) const {
      return this -> AsInteger ( );
    }

    operator const char * ( void ) const {
      return this -> AsString ( );
    }

    operator char ( void ) const {
      return this -> AsCharacter ( );
    }

    operator unsigned char ( void ) const {
      return this -> AsByte ( );
    }
    
    const char * ObjectType ( void ) const override {
      return "org::application::Constant";
    }
};

#endif
