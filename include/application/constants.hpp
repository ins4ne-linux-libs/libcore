
#ifndef APPLICATION_CONSTANTS_HPP
#define APPLICATION_CONSTANTS_HPP

namespace org {
  namespace application {
    class Constants;
    class Constant;
  }
}

class org::application::Constants : public virtual org::Object {
  private:
    struct ConstantsCheshireCat;
    struct ConstantsCheshireCat * _dPtr;

  public:
    static Constants * Current;

    Constants ( void );

    ~Constants ( void );

    void Reference ( org::application::Constant * ref );
    void ClearAll ( void );

    static void ReferenceInCurrent ( org::application::Constant * ref );
};

#endif
