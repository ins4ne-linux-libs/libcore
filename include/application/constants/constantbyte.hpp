
#ifndef APPLICATION_CONSTANT_BYTE_HPP
#define APPLICATION_CONSTANT_BYTE_HPP

namespace org {
  namespace application {
    namespace constants {
      class Byte;
    }
  }
}

#include <application/constant.hpp>

class org::application::constants::Byte : public virtual org::application::Constant {
  private:
    const unsigned char _constant;
    
  public:
    Byte ( unsigned char constant );

    ~Byte ( void );

    unsigned char AsByte ( void ) const override;
    
    const char * ObjectType ( void ) const override {
      return "org::application::constants::Byte";
    }
};

#endif
