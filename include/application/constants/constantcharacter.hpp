
#ifndef APPLICATION_CONSTANT_CHARACTER_HPP
#define APPLICATION_CONSTANT_CHARACTER_HPP

namespace org {
  namespace application {
    namespace constants {
      class Character;
    }
  }
}

#include <application/constant.hpp>

class org::application::constants::Character : public virtual org::application::Constant {
  private:
    const char _constant;
    
  public:
    Character ( char constant );

    ~Character ( void );

    char AsCharacter ( void ) const override;
    
    const char * ObjectType ( void ) const override {
      return "org::application::constants::Character";
    }
};

#endif
