
#ifndef APPLICATION_CONSTANT_INTEGER_HPP
#define APPLICATION_CONSTANT_INTEGER_HPP

namespace org {
  namespace application {
    namespace constants {
      class Integer;
    }
  }
}

#include <application/constant.hpp>

class org::application::constants::Integer : public virtual org::application::Constant {
  private:
    const int _constant;
    
  public:
    Integer ( int constant );

    ~Integer ( void );

    int AsInteger ( void ) const override;
    
    const char * ObjectType ( void ) const override {
      return "org::application::constants::Integer";
    }
};

#endif
