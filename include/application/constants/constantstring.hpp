
#ifndef APPLICATION_CONSTANT_STRING_HPP
#define APPLICATION_CONSTANT_STRING_HPP

namespace org {
  namespace application {
    namespace constants {
      class String;
    }
  }
}

#include <application/constant.hpp>

class org::application::constants::String : public virtual org::application::Constant {
  private:
    const char * _constant;
    
  public:
    String ( const char * constant );

    ~String ( void );

    const char * AsString ( void ) const override;
    
    const char * ObjectType ( void ) const override {
      return "org::application::constants::String";
    }
};

#endif
