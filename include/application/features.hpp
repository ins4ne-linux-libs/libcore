
#ifndef APPLICATION_FEATURES_HPP
#define APPLICATION_FEATURES_HPP

namespace org {
  namespace application {
    class Features;
  }
}

#include <entity/loggingconfiguredentity.hpp>

class org::application::Features : public virtual org::entity::LoggingConfiguredEntity {
  private:
    struct FeaturesCheshireCat;
    struct FeaturesCheshireCat * _dPtr;

    void FinishAutoConfiguration ( void ) override;
    void InitializeFeatures ( void );

  public:
    static Features * Current;

    Features ( void );

    ~Features ( void );

    bool IsOn ( const char * featureName );
};

#endif
