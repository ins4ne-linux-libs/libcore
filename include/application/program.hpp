
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Program                               ║
// ║ Description      : Defines program context, at lest one  ║
// ║                      object of this class should be      ║
// ║                      accessible by all.                  ║
// ║ History:                                                 ║
// ║  - 18/12/2017 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef PROGRAM_HPP
#define PROGRAM_HPP

#include <entity/loggingentity.hpp>
#include <application/configuration.hpp>
#include <inappmessaging/inappmessagecenter.hpp>
#include <filesystem/linkmanager.hpp>

namespace org {
  namespace application {
    class Program;
  }
}

class org::application::Program : public virtual org::entity::LoggingEntity {
  protected:
    struct ProgramCheshireCat;
    struct ProgramCheshireCat * _dPtr;

    char **               _argStrings;
    char **               _paramValues;
    char **               _paramStrings;
    int                   _argCount;
    int                   _paramCount;
    Configuration *       _configuration;
    org::filesystem::LinkManager *         _linkManager;
    org::inappmessaging::InAppMessageCenter *  _inAppMessageCenter;
    
    void InitializeProgram ( void );
    void ReadArgumentList ( void );
    void SetParameterName ( char* paramName, int paramIndex );
    void SetParameterValue ( char* paramValue, int paramIndex );
    void SetParameter ( char* paramName, char* paramValue, int paramIndex );
    void SetParameter ( char* paramName, int paramIndex );
    void CreateConfiguration ( void );

  public:
    static Program * Current;

    Program ( int argCount, char** argStrings );

    virtual ~Program ( void );

    const char * GetConfiguration ( const char * key );
    int GetInt ( char* key );
    char** GetNames ( void );
    int ParameterCount ( void );
    int Is ( char* paramName );
    const char * GetCommandParameter ( const char * paramName );
    const char * Get ( const char * paramName );
    char** GetParameteresSeparatedBy ( char separator );
    void Banner ( void );
    void ConfigurationList ( void );
    void EndOfProgram ( void );
    org::filesystem::LinkManager* GetLinkManager ( void );
    Configuration* Config ( void );
    org::inappmessaging::InAppMessageCenter* InApplicationMessageCenter ( void );
};

#endif
