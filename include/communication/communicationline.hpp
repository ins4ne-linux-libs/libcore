
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : CommunicationLine                     ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 26/02/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef COMMUNICATIONLINE_HPP
#define COMMUNICATIONLINE_HPP

namespace org {
  namespace communication {
    template <typename T> class CommunicationLine;
  }
}

#include <threads/taskable.hpp>

template <typename T>
class org::communication::CommunicationLine : public virtual org::threads::Taskable {
  #ifdef COMMUNICATIONLINE_TESTS_EXISTS
    friend class org::unittest::CommunicationLineTests;
  #endif
  protected:
    int   GetSize               ( void )      = 0;
    int   GetCount              ( void )      = 0;
    void  PutElementIn          ( T element ) = 0;
    T     GetElementOut         ( void )      = 0;
    void  FastForwardToElement  ( T element ) = 0;

  public:
    CommunicationLine ( void ) {
      this -> MakeTaskable ( );
    }

    int Size ( void ) {
      this -> ThreadLock ( );
      int size = this -> GetSize ( );
      this -> ThreadUnlock ( );
      return size;
    }

    int Count ( void ) {
      this -> ThreadLock ( );
      int count = this -> GetCount ( );
      this -> ThreadUnlock ( );
      return count;
    }

    void PutIn ( T element ) {
      this -> ThreadLock ( );
      this -> PutElementIn ( element );
      this -> ThreadUnlock ( );
    }

    T GetOut ( void ) {
      this -> ThreadLock ( );
      T out = this -> GetElementOut ( element );
      this -> ThreadUnlock ( );
      return out;
    }

    void FastForwardTo ( T element ) {
      this -> ThreadLock ( );
      this -> FastForwardToElement ( element );
      this -> ThreadUnlock ( );
    }

    const char * ObjectType ( void ) const override {
      return "org::communication::CommunicationLine";
    }
};

#endif

