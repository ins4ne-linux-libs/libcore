
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : CommunicationQueue                         ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 26/02/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef COMMUNICATIONQUEUE_HPP
#define COMMUNICATIONQUEUE_HPP

namespace org {
  namespace communication {
    template <typename T> class CommunicationQueue;
  }
}


#include <communication/communicationline.hpp>

template <typename T>
class org::communication::CommunicationQueue : public virtual org::communication::CommunicationLine <T> {
  #ifdef COMMUNICATIONQUEUE_TESTS_EXISTS
    friend class org::unittest::CommunicationQueueTests;
  #endif
  private:
    std::queue <T> * _line;

    void InitializeCommunicationQueue ( void ) {
      this -> _line = new std::queue <T> ( );
    }
  
  protected:
    int GetSize ( void ) {
      return this -> _line -> size ( );
    }

    int GetCount ( void ) {
      return this -> _line -> size ( );
    }

    void PutElementIn ( T element ) {
      this -> _line -> push ( element );
    }

    T GetElementOut ( void ) {
      T out = this -> _line -> front ( );
      this -> _line -> pop ( );
      return out;
    }

    void  FastForwardToElement ( T element ) {
      while ( this -> _line -> front ( ) != element )
        this -> _line -> pop ( );
    }

  public:
    CommunicationQueue ( void ) {
      this -> InitializeCommunicationQueue ( );
    }
    
    virtual ~CommunicationQueue ( void ) {
      delete this -> _line;
    }

    const char * ObjectType ( void ) const override {
      return "org::communication::CommunicationQueue";
    }
};

#endif

