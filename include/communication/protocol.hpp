
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Protocol                              ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 09/05/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef COMMUNICATION_PROTOCOL_HPP
#define COMMUNICATION_PROTOCOL_HPP

namespace org {
  namespace communication {
    template <typename T> class Protocol;
  }
}




#include <communication/protocol/sequence.hpp>
#include <process/memorymanager.hpp>
#include <entity/loggingconfiguredentity.hpp>

template <typename T>
class org::communication::Protocol : 
  public virtual org::entity::LoggingConfiguredEntity ,
  public virtual org::process::MemoryManager           {
  #ifdef COMMUNICATION_PROTOCOL_TESTS_EXISTS
    friend class org::unittest::ProtocolTests;
  #endif
  private:
    std::map <org::types::CString, org::communication::protocol::Sequence <T> *> * _sequences      ;
    org::types::CString *                                                          _sequenceOrder  ;
    T *                                                                           _buffer         ;
    T                                                                             _defaultValue   ;
    int                                                                           _bufferSize     ;

    void InitializeProtocol ( void ) {
      this -> _sequenceOrder  = nullptr;
      this -> _sequences      = nullptr;
      this -> _buffer         = nullptr;
      this -> _bufferSize     = 0;
    }
  
  protected:
    void FinishAutoConfiguration ( void ) override {
      org::entity::LoggingConfiguredEntity::FinishAutoConfiguration ( );
      
      this -> _bufferSize = this -> ContainsSettingAs <int> ( "buffer.size", this -> _bufferSize );

      if ( this -> _bufferSize > 0 )
        this -> _buffer = this -> Allocate <T> ( this -> _bufferSize );
    }

  public:
    Protocol ( void ) {
      this -> InitializeProtocol ( );
    }
    
    virtual ~Protocol ( void ) {

    }

    const char * ObjectType ( void ) const override {
      return "org::communication::Protocol";
    }
};

#endif

