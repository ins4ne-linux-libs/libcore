
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Sequence                         ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 09/05/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef COMMUNICATION_PROTOCOL_SEQUENCE_HPP
#define COMMUNICATION_PROTOCOL_SEQUENCE_HPP

namespace org {
  namespace communication::protocol {
    template <typename T> class Sequence;
  }
}



template <typename T>
class org::communication::protocol::Sequence : public virtual org::Object {
  #ifdef COMMUNICATION_PROTOCOL_SEQUENCE_TESTS_EXISTS
    friend class org::unittest::SequenceTests;
  #endif
  private:
    void InitializeSequence ( void ) {
    }

  public:
    Sequence ( void ) {
      this -> InitializeSequence ( );
    }
    
    virtual ~Sequence ( void ) {

    }

    const char * ObjectType ( void ) const override {
      return "org::communication::protocol::Sequence";
    }
};

#endif

