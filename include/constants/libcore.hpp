
#ifndef CONSTANTS_LIBCORE_HPP
#define CONSTANTS_LIBCORE_HPP

#include <application/constant.hpp>

namespace org {
  namespace constants {
    extern org::application::Constant *           DefaultSettingsFile;
    #define CONSTANT_SETTINGS_CONFIGURATION_PATH  (const char *)*org::constants::DefaultSettingsFile

    extern org::application::Constant *           DefaultLinkManagerName;
    #define CONSTANT_LINK_MANAGER_NAME            (const char *)*org::constants::DefaultLinkManagerName

    extern org::application::Constant *           DefaultConfigurationTag;
    #define CONSTANT_CONFIGURATION_TAG            (const char *)*org::constants::DefaultConfigurationTag

    extern org::application::Constant *           EmptyString;
    #define CONSTANT_EMPTY_STRING                 (const char *)*org::constants::EmptyString

    extern org::application::Constant *           StringFormatCharacters;
    #define CONSTANT_STRING_FROMAT_CHARACTERS     (const char *)*org::constants::StringFormatCharacters

    extern org::application::Constant *           FormatLengthCharacters;
    #define CONSTANT_FROMAT_LENGTH_CHARACTERS     (const char *)*org::constants::FormatLengthCharacters

    extern org::application::Constant *           StdOutLinkName;
    #define CONSTANT_STD_OUT_LINK_NAME            (const char *)*org::constants::StdOutLinkName

    extern org::application::Constant *           PipeReaderThreadName;
    #define CONSTANT_PIPE_READER_THREAD_NAME      (const char *)*org::constants::PipeReaderThreadName

    extern org::application::Constant *           SerialReaderThreadName;
    #define CONSTANT_SERIAL_READER_THREAD_NAME    (const char *)*org::constants::SerialReaderThreadName



    extern org::application::Constant *           LiteralTrue;
    #define LITERAL_TRUE                          (const char *)*org::constants::LiteralTrue

    extern org::application::Constant *           LiteralNull;
    #define LITERAL_NULL                          (const char *)*org::constants::LiteralNull

    extern org::application::Constant *           LiteralSlashAsString;
    #define LITERAL_SLASH_AS_STR                  (const char *)*org::constants::LiteralSlashAsString

    extern org::application::Constant *           LiteralPeriodAsString;
    #define LITERAL_PERIOD_AS_STR                 (const char *)*org::constants::LiteralPeriodAsString



    extern org::application::Constant *           LiteralHyphen;
    #define LITERAL_HYPHEN                        (char)*org::constants::LiteralHyphen

    extern org::application::Constant *           LiteralColon;
    #define LITERAL_COLON                         (char)*org::constants::LiteralColon

    extern org::application::Constant *           LiteralSemiColon;
    #define LITERAL_SEMI_COLON                    (char)*org::constants::LiteralSemiColon

    extern org::application::Constant *           LiteralSpace;
    #define LITERAL_SPACE                         (char)*org::constants::LiteralSpace

    extern org::application::Constant *           LiteralLessThan;
    #define LITERAL_LESS_THAN                     (char)*org::constants::LiteralLessThan

    extern org::application::Constant *           LiteralGreaterThan;
    #define LITERAL_GREATER_THAN                  (char)*org::constants::LiteralGreaterThan

    extern org::application::Constant *           LiteralPeriod;
    #define LITERAL_PERIOD                        (char)*org::constants::LiteralPeriod

    extern org::application::Constant *           LiteralOpenBrace;
    #define LITERAL_OPEN_BRACE                    (char)*org::constants::LiteralOpenBrace

    extern org::application::Constant *           LiteralCloseBrace;
    #define LITERAL_CLOSE_BRACE                    (char)*org::constants::LiteralCloseBrace

    extern org::application::Constant *           LiteralHash;
    #define LITERAL_HASH                          (char)*org::constants::LiteralHash

    extern org::application::Constant *           LiteralBackSlash;
    #define LITERAL_BACK_SLASH                    (char)*org::constants::LiteralBackSlash

    extern org::application::Constant *           LiteralPercent;
    #define LITERAL_PERCENT                       (char)*org::constants::LiteralPercent

    extern org::application::Constant *           LiteralH;
    #define LITERAL_H                             (char)*org::constants::LiteralH

    extern org::application::Constant *           LiteralL;
    #define LITERAL_L                             (char)*org::constants::LiteralL

    extern org::application::Constant *           LiteralS;
    #define LITERAL_S                             (char)*org::constants::LiteralS

    extern org::application::Constant *           Literal0;
    #define LITERAL_0                             (char)*org::constants::Literal0

    extern org::application::Constant *           Literal9;
    #define LITERAL_9                             (char)*org::constants::Literal9



    extern org::application::Constant *           SettingPath;
    #define SETTING_PATH                          (const char *)*org::constants::SettingPath

    extern org::application::Constant *           SettingFiles;
    #define SETTING_FILES                         (const char *)*org::constants::SettingFiles

    extern org::application::Constant *           SettingBlocking;
    #define SETTING_BLOCKING                      (const char *)*org::constants::SettingBlocking

    extern org::application::Constant *           SettingRoute;
    #define SETTING_ROUTE                         (const char *)*org::constants::SettingRoute

    extern org::application::Constant *           SettingBannerFile;
    #define SETTING_BANNER_FILE                   (const char *)*org::constants::SettingBannerFile

    extern org::application::Constant *           SettingLogger;
    #define SETTING_LOGGER                        (const char *)*org::constants::SettingLogger

    extern org::application::Constant *           SettingReadMinimum;
    #define SETTING_READ_MINIMUM                  (const char *)*org::constants::SettingReadMinimum

    extern org::application::Constant *           SettingReadTimeout;
    #define SETTING_READ_TIMEOUT                  (const char *)*org::constants::SettingReadTimeout

    extern org::application::Constant *           SettingReadMode;
    #define SETTING_READ_MODE                     (const char *)*org::constants::SettingReadMode

    extern org::application::Constant *           SettingWriteMode;
    #define SETTING_WRITE_MODE                    (const char *)*org::constants::SettingWriteMode

    extern org::application::Constant *           SettingUnlink;
    #define SETTING_UNLINK                        (const char *)*org::constants::SettingUnlink

    extern org::application::Constant *           SettingCreate;
    #define SETTING_CREATE                        (const char *)*org::constants::SettingCreate

    extern org::application::Constant *           SettingSleepingPeriod;
    #define SETTING_SLEEPING_PERIOD               (const char *)*org::constants::SettingSleepingPeriod

    extern org::application::Constant *           SettingCreatePermission;
    #define SETTING_CREATE_PERMISSION             (const char *)*org::constants::SettingCreatePermission

    extern org::application::Constant *           SettingNumericalIO;
    #define SETTING_NUMERICAL_IO                  (const char *)*org::constants::SettingNumericalIO

    extern org::application::Constant *           SettingByteCount;
    #define SETTING_BYTE_COUNT                    (const char *)*org::constants::SettingByteCount

    extern org::application::Constant *           SettingFloatingPointCount;
    #define SETTING_FLOATING_POINT_COUNT          (const char *)*org::constants::SettingFloatingPointCount

    extern org::application::Constant *           SettingMinimumChange;
    #define SETTING_MINIMUM_CHANGE                (const char *)*org::constants::SettingMinimumChange

    extern org::application::Constant *           SettingParameters;
    #define SETTING_PARAMETERS                    (const char *)*org::constants::SettingParameters

    extern org::application::Constant *           SettingTemperatures;
    #define SETTING_TEMPERATURES                  (const char *)*org::constants::SettingTemperatures

    extern org::application::Constant *           SettingName;
    #define SETTING_NAME                          (const char *)*org::constants::SettingName

    extern org::application::Constant *           SettingSpeed;
    #define SETTING_SPEED                         (const char *)*org::constants::SettingSpeed

    extern org::application::Constant *           SettingToFlush;
    #define SETTING_TO_FLUSH                      (const char *)*org::constants::SettingToFlush

    extern org::application::Constant *           SettingSimulatorEnable;
    #define SETTING_SIMULATOR_ENABLE              (const char *)*org::constants::SettingSimulatorEnable

    extern org::application::Constant *           SettingSimulatorOpenDoor;
    #define SETTING_SIMULATOR_OPEN_DOOR           (const char *)*org::constants::SettingSimulatorOpenDoor

    extern org::application::Constant *           SettingSimulatorType;
    #define SETTING_SIMULATOR_TYPE                (const char *)*org::constants::SettingSimulatorType

    extern org::application::Constant *           SettingSimulatorName;
    #define SETTING_SIMULATOR_NAME                (const char *)*org::constants::SettingSimulatorName



    extern org::application::Constant *           LogEndOfProgram;
    #define LOG_END_OF_PROGRAM                    (const char *)*org::constants::LogEndOfProgram

    extern org::application::Constant *           LogNewLine;
    #define LOG_NEW_LINE                          (const char *)*org::constants::LogNewLine

    extern org::application::Constant *           LogBootModeDetected;
    #define LOG_BOOT_MODE_DETECTED                (const char *)*org::constants::LogBootModeDetected

    extern org::application::Constant *           LogSendingSpecialGift;
    #define LOG_SENDING_SPECIAL_GIFT              (const char *)*org::constants::LogSendingSpecialGift

    extern org::application::Constant *           LogSent;
    #define LOG_SENT                              (const char *)*org::constants::LogSent

    extern org::application::Constant *           LogReceivedNiuTimeout;
    #define LOG_RECEIVED_NIU_TIMEOUT              (const char *)*org::constants::LogReceivedNiuTimeout

    extern org::application::Constant *           LogTryingToRead;
    #define LOG_TRYING_TO_READ                    (const char *)*org::constants::LogTryingToRead

    extern org::application::Constant *           LogStartWait;
    #define LOG_START_WAIT                        (const char *)*org::constants::LogStartWait

    extern org::application::Constant *           LogEndWait;
    #define LOG_END_WAIT                          (const char *)*org::constants::LogEndWait

    extern org::application::Constant *           LogReadSomething;
    #define LOG_READ_SOMETHING                    (const char *)*org::constants::LogReadSomething

    extern org::application::Constant *           LogOpeningNamedPipe;
    #define LOG_OPENING_NAMED_PIPE                (const char *)*org::constants::LogOpeningNamedPipe

    extern org::application::Constant *           LogNoAccessPipe;
    #define LOG_NO_ACCESS_PIPE                    (const char *)*org::constants::LogNoAccessPipe

    extern org::application::Constant *           LogTryOpenSerialPort;
    #define LOG_TRY_OPEN_SERIAL_PORT              (const char *)*org::constants::LogTryOpenSerialPort

    extern org::application::Constant *           LogSerialPortNowOpen;
    #define LOG_SERIAL_PORT_NOW_OPEN              (const char *)*org::constants::LogSerialPortNowOpen

    extern org::application::Constant *           LogFailed;
    #define LOG_FAILED                            (const char *)*org::constants::LogFailed

    extern org::application::Constant *           LogSerialPortClosed;
    #define LOG_SERIAL_PORT_CLOSED                (const char *)*org::constants::LogSerialPortClosed

    extern org::application::Constant *           LogClosingFailed;
    #define LOG_CLOSING_FAILED                    (const char *)*org::constants::LogClosingFailed



    // extern org::application::Constant *           Log;
    // #define LOG_                                  (const char *)*org::constants::
  }
}

#endif
