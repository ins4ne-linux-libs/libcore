
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Buffer                                ║
// ║ Description      : Specialized queue class for messaging ║
// ║                      problematics.                       ║
// ║ History:                                                 ║
// ║  - 18/12/2017 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef BUFFER_HPP
#define BUFFER_HPP

namespace org {
  namespace data {
    template <class T> class Buffer;
  }
}

#include <process/memorymanager.hpp>

template <typename T>
class org::data::Buffer : public virtual org::Object {
  private:
    int _size;
    int _safeShiftSize;
    int _currentIndex;
    int _readingCursor;
    T* _bufferMemory;
    T _defaultElementValue;
    int (*_comparer) ( T, T );
    
  protected:
    static int DefaultComparer ( T element1, T element2 ) {
      return element1 == element2;
    }

    void SetSize ( int newSize ) {
      this -> _size = newSize;
    }

    void SetSafeShiftSize ( int safeShiftSize ) {
      this -> _safeShiftSize = safeShiftSize;
    }
    
    void SetBufferMemory ( T* newMemory ) {
      this -> _bufferMemory = newMemory;
    }
    
    void SetCurrentIndex ( int newIndex ) {
      this -> _currentIndex = newIndex;
    }
    
    void SetReadingCursor ( int newReadingCursor ) {
      this -> _readingCursor = newReadingCursor;
    }
    
    void SetDefaultElementValue ( int newDefaultElementValue ) {
      this -> _defaultElementValue = newDefaultElementValue;
    }
    
    void SetComparer ( int (*newComparer) ( T, T ) ) {
      this -> _comparer = newComparer;
    }

    void IncrementIndex ( ) {
      this -> AddCurrentIndex ( 1 );
    }

    void DecrementIndex ( ) {
      this -> AddCurrentIndex ( -1 );
    }

    void AddCurrentIndex ( int increment ) {
      this -> _currentIndex += increment;
      if ( this -> _currentIndex < 0 )
        this -> _currentIndex = 0;
    }

    void IncrementReadingCursor ( ) {
      this -> AddReadingCursor ( 1 );
    }

    void DecrementReadingCursor ( ) {
      this -> AddReadingCursor ( -1 );
    }

    void AddReadingCursor ( int increment ) {
      this -> _readingCursor += increment;
      // if ( this -> _readingCursor < 0 || this -> _readingCursor >= this -> _size )
      //   this -> _readingCursor = 0;
    }

    void Initialize ( int size, int safeShiftSize, T defaultElementValue, int (*comparer) ( T, T ) ) {
      this -> _bufferMemory = nullptr;
      this -> SetSize ( size );
      this -> SetSafeShiftSize ( safeShiftSize );
      this -> SetCurrentIndex ( 0 );
      this -> SetReadingCursor ( 0 );
      this -> SetDefaultElementValue ( defaultElementValue );
      this -> SetComparer ( comparer );
      this -> SetBufferMemory ( ( T* ) org::process::MemoryManager::Malloc ( this -> _size, sizeof ( T ) ) );
    }

    T* GetElement ( int index ) {
      T* element = nullptr;
      if ( index >= 0 && index < this -> _size )
        element = &( this -> _bufferMemory [index] );
      return element;
    }

  public:
    Buffer ( int size, int safeShiftSize, T defaultElementValue ) {
      this -> Initialize ( size, safeShiftSize, defaultElementValue, &( DefaultComparer ) );
    }

    virtual ~Buffer ( void ) {
      if ( this -> _bufferMemory != nullptr ) {
        org::process::MemoryManager::FreeM ( ( void* ) this -> _bufferMemory );
        this -> _bufferMemory = nullptr;
      }
    }

    void Clear ( void ) {
      this -> SetReadingCursor ( 0 );
      if ( this -> _currentIndex > 0 ) {
        for ( int i = 0; i < this -> _currentIndex; i ++ ) {
          this -> _bufferMemory [i] = this -> _defaultElementValue;
        }
        this -> SetCurrentIndex ( 0 );
      }
    }

    int GetCount ( void ) {
      return this -> _currentIndex;
    }

    void Write ( T element ) {
      this -> _bufferMemory [this -> _currentIndex] = element;
      this -> IncrementIndex ( );
      if ( this -> _currentIndex == this -> _size )
        this -> Shift ( this -> _safeShiftSize );
    }

    void Write ( T* elements, int size ) {
      for ( int i = 0; i < size; i ++ )
        this -> Write ( elements [i] );
    }

    void ShiftToValue ( T value, int fromHere, int (*comparer) ( T, T ) ) {
      for ( int i = fromHere; i < this -> _size; i ++ )
        if ( comparer != nullptr )
          if ( (*comparer) ( value, this -> _bufferMemory [i] ) ) {
            this -> Shift ( fromHere, i - fromHere );
            break;
          }
    }

    void ShiftToValue ( T value ) {
      this -> ShiftToValue ( value, 0, this -> _comparer );
    }

    void Shift ( int shiftSize ) {
      this -> Shift ( 0, shiftSize );
    }

    void Shift ( int fromHere, int shiftSize ) {
      if ( fromHere >= 0 && shiftSize > 0 && fromHere + shiftSize < this -> _size ) {
        int j = fromHere;
        int i = fromHere + shiftSize;
        for ( ; i < this -> _size; i ++, j ++ )
          ( this -> _bufferMemory [j] ) = ( this -> _bufferMemory [i] );
        for ( ; j < this -> _size; j ++ )
          ( this -> _bufferMemory [j] ) = this -> _defaultElementValue;
        this -> AddCurrentIndex ( -1 * shiftSize );
      }
    }

    Buffer <T>* ExtractAndShift ( int size ) {
      return this -> ExtractAndShift ( 0, size );
    }

    Buffer <T>* ExtractAndShift ( int fromHere, int toHere ) {
      Buffer <T>* extract = this -> Extract ( fromHere, toHere );
      this -> Shift ( fromHere, toHere - fromHere );
      return extract;
    }

    Buffer <T>* Extract ( int size ) {
      return this -> Extract ( 0, size );
    }

    Buffer <T>* Extract ( int fromHere, int toHere ) {
      return this -> CreateCopy ( fromHere, toHere );
    }

    T* Read ( ) {
      T* element = this -> Read ( this -> _readingCursor );
      this -> IncrementReadingCursor ( );
      return element;
    }

    T* Read ( int index ) {
      T* element = nullptr;
      if ( index >= 0 && index < this -> _size && index < this -> _currentIndex )
        element = &( this -> _bufferMemory [index] );
      return element;
    }

    void ApplyOnElement ( void (*apply) (T), int elementIndex ) {
      T* element = this -> GetElement ( elementIndex );
      if ( element != nullptr )
        (*apply) ( *element );
    }

    void ApplyOnAll ( void (*apply) (T) ) {
      for ( int i = 0; i < this -> _size; i ++ )
        this -> ApplyOnElement ( apply, i );
    }

    void ResetReadingCursor ( ) {
      this -> SetReadingCursor ( 0 );
    }

    T* ToArray ( void ) {
      T* newArray = ( T* ) org::process::MemoryManager::Malloc ( this -> _currentIndex, sizeof ( T ) );

      this -> ResetReadingCursor ( );
      int i = 0;
      T* element = nullptr;
      while ( ( element = this -> Read ( ) ) != nullptr ) {
        newArray [i ++] = *element;
      }

      return newArray;
    }

    Buffer <T>* CreateCopy ( void ) {
      return this -> CreateCopy ( 0, this -> _currentIndex );
    }

    Buffer <T>* CreateCopy ( int fromHere, int toHere ) {
      Buffer* copy = new Buffer ( toHere - fromHere + 1, this -> _safeShiftSize, this -> _defaultElementValue );
      if ( fromHere < 0 )
        fromHere = 0;
        
      if ( toHere < 0 )
        toHere = 0;
      toHere = toHere - fromHere;
      if ( toHere > this -> _size )
        toHere = this -> _size;
      copy -> Write ( &( this -> _bufferMemory [fromHere] ), toHere );
      return copy;
    }
};

#endif
 