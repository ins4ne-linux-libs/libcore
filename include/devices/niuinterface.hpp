
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : NIUInterface                          ║
// ║ Description      : Niu to serial port logic.             ║
// ║ History:                                                 ║
// ║  - 18/12/2017 - DourvCle: Creation.                      ║
// ║  - 05/01/2018 - DourvCle: Transformed to configured.     ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef NIU_INTERFACE_HPP
#define NIU_INTERFACE_HPP

#include <data/buffer.hpp>
#include <hardware/serialport.hpp>

namespace org {
  namespace devices {
    class NIUInterface;
  }
}

class org::devices::NIUInterface :
  public virtual org::process::MemoryManager            ,
  public virtual org::entity::LoggingConfiguredEntity  ,
  public virtual org::threads::Taskable {
  private:
    org::data::Buffer <unsigned char> *                                   _buffer;
    org::hardware::SerialPort *                _serialPort;
    org::eventing::EventCall <org::data::Buffer <unsigned char>> * _eapMessageEvent;
    org::eventing::EventDelegateCall <NIUInterface, unsigned char> * _plugByteReceivedEvent;
    bool                                              _waitingBootMode;

    void InitializeNIUInterface ( void );

  protected:
    void FindMessages ( void );
    void NIUToComputer ( unsigned char receivedChar );
    void NewByteReceived ( org::eventing::EventArgs <void*, char*, unsigned char *>* args );
    void FinishAutoConfiguration ( void ) override;

  public:
    NIUInterface  ( const char * niuInterface );

    virtual ~NIUInterface ( void );

    void FlushBuffer ( void );
    void WaitBootMode ( void );
    bool OpenCommunication ( void );
    void CloseCommunication ( void );
    void StartListening ( void );
    void StopListening ( void );
    int SendToNIU ( unsigned char * message, int size );
    int ReadNIU ( unsigned char * bufferToFill, int countToRead );
    int SerialSpeed ( int newSpeed );
    org::hardware::SerialPort * Port ( void );
    void PlugEapMessageEvent ( org::eventing::EventCall <org::data::Buffer <unsigned char>>* eapMessageEvent );
};

#endif
