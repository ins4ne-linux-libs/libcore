
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : ConfiguredEntity                      ║
// ║ Description      : Defines a generic way of obtaining    ║
// ║                      configuration on the entity. This   ║
// ║                      entity needs to be in a program     ║
// ║                      context.                            ║
// ║ History:                                                 ║
// ║  - 18/12/2017 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef CONFIGURED_ENTITY_HPP
#define CONFIGURED_ENTITY_HPP

#include <tools/misc/stringextension.hpp>
#include <tools/misc/conversion.hpp>
#include <application/configuration.hpp>

namespace org {
  namespace entity {
    class ConfiguredEntity;
  }
}

class org::entity::ConfiguredEntity : public virtual org::Object {
  private:
    void AddConfiguration ( std::map <org::types::CString, std::list <org::application::ConfigurationParameter *> *> * newKeys );
    void FetchConfiguration ( const char * mask );
    const char * GetSetting ( const char * key );
    bool ContainsAndExtractSetting ( const char * key, const char ** setting );
    void InitializeConfiguredEntity ( void );

    template <typename T>
    std::list <T> * ContainsSettingListAndExtractAs ( const char * key ) {
      std::list <T> * settings = new std::list <T> ( );
      T setting;
      if ( this -> ContainsSetting ( key ) ) {
        std::list <org::application::ConfigurationParameter *>::iterator parameters = this -> _configuration -> at ( key ) -> begin ( );
        for ( ; parameters != this -> _configuration -> at ( key ) -> end ( ); parameters ++ )
          if ( org::tools::misc::Convert::FromTo <const char *, T> ( ( *parameters ) -> GetTransformed ( ), &setting ) )
            settings -> push_back ( setting );
      }
      return settings;
    }

    template <typename T>
    std::list <T *> * ContainsSettingListAndExtractFrom ( const char * key ) {
      std::list <T *> * settings = nullptr;
      if ( this -> ContainsSetting ( key ) ) {
        settings = new std::list <T *> ( );
        std::list <org::application::ConfigurationParameter *>::iterator parameters = this -> _configuration -> at ( key ) -> begin ( );
        for ( ; parameters != this -> _configuration -> at ( key ) -> end ( ); parameters ++ )
          settings -> push_back ( new T ( ( *parameters ) -> GetTransformed ( ) ) );
      }
      return settings;
    }

    template <typename T>
    std::map <org::types::CString, T> * ContainsSettingMapAndExtractAs ( const char * key ) {
      std::map <org::types::CString, T> * settings = nullptr;
      T setting;
      int keySize = org::tools::misc::CString::GetCCharArrayLength ( key ) + 1;
      settings = new std::map <org::types::CString, T> ( );
      std::map <org::types::CString, std::list <org::application::ConfigurationParameter *> *>::iterator parameters = this -> _configuration -> begin ( );
      for ( ; parameters != this -> _configuration -> end ( ); parameters ++ )
        if ( org::tools::misc::CString::IsPartiallyEqual ( key, parameters -> first ) )
          if ( org::tools::misc::Convert::FromTo <const char *, T> ( parameters -> second -> front ( ) -> GetTransformed ( ), &setting ) )
            settings -> insert ( std::pair <org::types::CString, T> ( &( ( ( const char * ) parameters -> first ) [keySize] ), setting ) );
      return settings;
    }

    template <typename T, typename U>
    std::map <T, U> * ContainsSettingMapAndExtractAs ( const char * key ) {
      std::map <T, U> * settings = nullptr;
      U setting;
      T convertedKey;
      int keySize = org::tools::misc::CString::GetCCharArrayLength ( key ) + 1;
      settings = new std::map <T, U> ( );
      std::map <org::types::CString, std::list <org::application::ConfigurationParameter *> *>::iterator parameters = this -> _configuration -> begin ( );
      for ( ; parameters != this -> _configuration -> end ( ); parameters ++ )
        if ( org::tools::misc::CString::IsPartiallyEqual ( key, parameters -> first ) )
          if ( org::tools::misc::Convert::FromTo <const char *, T> ( &( ( ( const char * ) parameters -> first ) [keySize] ), &convertedKey ) )
            if ( org::tools::misc::Convert::FromTo <const char *, U> ( parameters -> second -> front ( ) -> GetTransformed ( ), &setting ) )
              settings -> insert ( std::pair <T, U> ( convertedKey, setting ) );
      return settings;
    }

    template <typename T>
    bool ContainsSettingAndExtractAs ( const char * key, T * setting ) {
      bool containsSetting = false;
      const char * settingAsChar = nullptr;
      if ( this -> ContainsAndExtractSetting ( key, &settingAsChar ) )
        containsSetting = org::tools::misc::Convert::FromTo <const char *, T> ( settingAsChar, setting );
      return containsSetting;
    }

  protected:
    std::map <org::types::CString, std::list <org::application::ConfigurationParameter *> *> * _configuration;

    virtual void FinishAutoConfiguration ( void );
    void SetConfigurationMask ( const char * mask );
    bool ContainsSetting ( const char * key );

    template <typename T>
    T * NewFromSetting ( const char * key ) {
      const char * settingAsChar = this -> GetSetting ( key );
      return settingAsChar != nullptr ? new T ( settingAsChar ) : nullptr;
    }

    template <typename T>
    bool NewFromSetting ( const char * key, T ** ptr ) {
      *ptr = this -> NewFromSetting <T> ( key );
      return *ptr != nullptr;
    }

    template <typename T>
    std::list <T *> * NewFromSettingList ( const char * key ) {
      return this -> ContainsSettingListAndExtractFrom <T> ( key );
    }

    template <typename T>
    T ContainsSettingAs ( const char * key, T defaultValue ) {
      T answer;
      if ( ! this -> ContainsSettingAndExtractAs <T> ( key, &answer ) )
        answer = defaultValue;
      return answer;
    }

    template <typename T>
    T ContainsSettingAsAndFree ( const char * key, T defaultValue ) {
      T answer;
      if ( ! this -> ContainsSettingAndExtractAs <T> ( key, &answer ) )
        answer = defaultValue;
      return answer;
    }

    template <typename T>
    void PutSettingAs ( const char * key, T * defaultValue ) {
      if ( defaultValue != nullptr )
        *defaultValue = this -> ContainsSettingAs <T> ( key, *defaultValue );
    }

    template <typename T>
    std::list <T> * ContainsSettingListAs ( const char * key ) {
      return this -> ContainsSettingListAndExtractAs <T> ( key );
    }

    template <typename T>
    std::map <org::types::CString, T> * ContainsSettingMapAs ( const char * key ) {
      return this -> ContainsSettingMapAndExtractAs <T> ( key );
    }

    template <typename U, typename T>
    std::map <U, T> * ContainsSettingMapAs ( const char * key ) {
      return this -> ContainsSettingMapAndExtractAs <U, T> ( key );
    }
  
  public:
    ConfiguredEntity ( void );

    virtual ~ConfiguredEntity ( void );
};

#endif
 