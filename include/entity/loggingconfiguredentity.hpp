
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : LoggingConfiguredEntity               ║
// ║ Description      : Defines a generic way of obtaining    ║
// ║                      configuration on the entity. This   ║
// ║                      entity needs to be in a program     ║
// ║                      context.                            ║
// ║ History:                                                 ║
// ║  - 18/12/2017 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef LOGGING_CONFIGURED_ENTITY_HPP
#define LOGGING_CONFIGURED_ENTITY_HPP

#include <entity/loggingentity.hpp>
#include <entity/configuredentity.hpp>
#include <logging/loggerconfigured.hpp>

namespace org {
  namespace entity {
    class LoggingConfiguredEntity;
  }
}

class org::entity::LoggingConfiguredEntity :
  public virtual LoggingEntity,
  public virtual ConfiguredEntity {
  protected:
    void FinishAutoConfiguration ( void ) override;
    void LogSettings ( void );
};

#endif
