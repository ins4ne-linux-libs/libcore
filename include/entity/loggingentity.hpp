
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : LoggingEntity                         ║
// ║ Description      : Defines a generic way of tranforming  ║
// ║                      the current object into a logger    ║
// ║                      with a null Logger that must be set ║
// ║                      to be able to log. This logger can  ║
// ║                      be a configured logger.             ║
// ║ History:                                                 ║
// ║  - 18/12/2017 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef LOGGING_ENTITY_HPP
#define LOGGING_ENTITY_HPP

#include <logging/logger.hpp>
#include <process/memorymanager.hpp>

namespace org {
  namespace entity {
    class LoggingEntity;
  }
}

class org::entity::LoggingEntity : public virtual org::process::MemoryManager {
  private:
    org::logging::Logger * _logger;

    void InitializeLoggingEntity ( void );

  protected:
    void Trace ( const char * message );
    void Debug ( const char * message );
    void Info ( const char * message );
    void Notice ( const char * message );
    void Warning ( const char * message );
    void Error ( const char * message );
    void Critical ( const char * message );
    int HasALogger ( void );
  
  public:
    LoggingEntity ( void );

    virtual ~LoggingEntity ( void );

    void SetLogger ( org::logging::Logger * newLogger );
};

#endif
 