
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Event                         ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 26/02/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef EVENT_HPP
#define EVENT_HPP

namespace org {
  namespace eventing {
    template <typename F> class Event;
  }
}




template <typename F>
class org::eventing::Event : public org::Object {
  #ifdef EVENT_TESTS_EXISTS
    friend class org::unittest::EventTests;
  #endif
  private:
    std::list <org::eventing::EventCall <F> *> * _calls;
    
    void InitializeEvent ( void ) {
      this -> _calls = new std::list <org::eventing::EventCall <F> *> ( );
    }

  public:
    Event ( void ) {
      this -> InitializeEvent ( );
    }

    virtual ~Event ( void ) {
      delete this -> _calls;
    }

    void Add ( org::eventing::EventCall <F> newCall ) {
      this -> _calls -> push_back ( newCall );
    }

    void Clear ( void ) {
      this -> _calls -> clear ( );
    }

    void Trigger ( F argument ) {
      typename std::list <org::eventing::EventCall <F> *>::iterator calling = this -> _calls -> begin ( );
      for ( ; calling != this -> _calls -> end ( ); calling ++ )
        calling -> Call ( argument );
    }

    const char * ObjectType ( void ) const override {
      return "org::eventing::Event";
    }
};

#endif

