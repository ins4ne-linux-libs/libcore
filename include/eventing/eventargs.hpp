
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : EventArgs                             ║
// ║ Description      : Generic event args.                   ║
// ║ History:                                                 ║
// ║  - 18/12/2017 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef EVENTING_EVENTARGS_HPP
#define EVENTING_EVENTARGS_HPP

namespace org {
  namespace eventing {
    template <class T, typename U, class V> class EventArgs;
  }
}

template <class T, typename U, class V>
class org::eventing::EventArgs : public virtual org::Object {
  protected:
    T _launcher;
    U _type;
    V _args;

    void SetLauncher ( T launcher ) {
      this -> _launcher = launcher;
    }

    void SetType ( U type ) {
      this -> _type = type;
    }

    void SetArgs ( V args ) {
      this -> _args = args;
    }

    void Configure ( T launcher, U type, V args ) {
      this -> SetLauncher ( launcher );
      this -> SetType ( type );
      this -> SetArgs ( args );
    }

  public:
    EventArgs ( T launcher, U type, V args ) {
      this -> Configure ( launcher, type, args );
    }

    EventArgs ( void ) {
    }

    virtual ~EventArgs ( void ) {
      
    }

    T GetLauncher ( void ) {
      return this -> _launcher;
    }

    U GetType ( void ) {
      return this -> _type;
    }

    V GetArgs ( void ) {
      return this -> _args;
    }
    
    virtual EventArgs <T, U, V> * FreeEventMemory ( void ) {
      return this;
    }
};

#endif
 