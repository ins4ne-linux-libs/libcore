
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : EventCall                         ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 26/02/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef EVENTCALL_HPP
#define EVENTCALL_HPP

namespace org {
  namespace eventing {
    template <typename F> class EventCall;
  }
}

#include <tools/callable.hpp>
#include <eventing/eventargs.hpp>

template <typename F>
class org::eventing::EventCall : public org::tools::Callable <org::eventing::EventArgs <void*, char*, F*>> {
  #ifdef EVENTCALL_TESTS_EXISTS
    friend class org::unittest::EventCallTests;
  #endif
  private:
    void InitializeEventCall ( void ) {
    }

  protected:
    org::eventing::EventArgs <void*, char*, F*>* GetNewArgs ( void* launcher, char* type, F* args ) {
      return new org::eventing::EventArgs <void*, char*, F*> ( launcher, type, args );
    }

  public:
    EventCall ( void ) {
      this -> InitializeEventCall ( );
    }
    
    virtual ~EventCall ( void ) {

    }

    void Call ( F* args ) {
      this -> Call ( nullptr, nullptr, args );
    }

    void Call ( void* launcher, F* args ) {
      this -> Call ( launcher, nullptr, args );
    }

    void Call ( void* launcher, char* type, F* args ) {
      org::eventing::EventArgs <void*, char*, F*>* eventArgs = this -> GetNewArgs ( launcher, type, args );
      this -> Call ( eventArgs );
      // delete eventArgs;
    }

    virtual org::eventing::EventCall <F> * EventCopy ( void ) = 0;

    using org::tools::Callable<org::eventing::EventArgs <void*, char*, F*>>::Call;

    const char * ObjectType ( void ) const override {
      return "org::eventing::EventCall";
    }
};

#endif

