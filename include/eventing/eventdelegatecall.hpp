
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : EventDelegateCall                         ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 26/02/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef EVENTDELEGATECALL_HPP
#define EVENTDELEGATECALL_HPP

namespace org {
  namespace eventing {
    template <typename T, typename F> class EventDelegateCall;
  }
}

#include <tools/memberdelegatecall.hpp>
#include <eventing/eventcall.hpp>

template <typename T, typename F>
class org::eventing::EventDelegateCall :
  public virtual org::tools::MemberDelegateCall <void (T::*) ( org::eventing::EventArgs <void*, char*, F*>* ), T>,
  public virtual org::eventing::EventCall <F> {
  #ifdef EVENTDELEGATECALL_TESTS_EXISTS
    friend class org::unittest::EventDelegateCallTests;
  #endif
  private:
    void InitializeEventDelegateCall ( void ) {
    }

  public:
    EventDelegateCall ( T* instance, void (T::*call) ( org::eventing::EventArgs <void*, char*, F*>* ) ) {
      this -> InitializeEventDelegateCall ( );
      this -> _calledInstance = instance;
      this -> _call = call;
    }
    
    virtual ~EventDelegateCall ( void ) {

    }

    void Call ( org::eventing::EventArgs <void*, char*, F*>* args ) override {
      ( this -> _calledInstance ->* this -> _call ) ( args );
    }

    org::eventing::EventCall <F> * EventCopy ( void ) override {
      return new org::eventing::EventDelegateCall <T, F> ( this -> _calledInstance, this -> _call );
    }

    const char * ObjectType ( void ) const override {
      return "org::eventing::EventDelegateCall";
    }
};

#endif

