
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : File                                  ║
// ║ Description      : Basic file access.                    ║
// ║ History:                                                 ║
// ║  - 19/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef FILE_CLASS
#define FILE_CLASS

#include <entity/loggingconfiguredentity.hpp>
#include <process/memorymanager.hpp>

namespace org {
  namespace tools {
    class File;
  }
}

class org::tools::File : public virtual org::entity::LoggingConfiguredEntity, public virtual MemoryManager {
  public:
    File ( void ) { }

    virtual ~File ( void ) { }

    virtual void Open ( void ) { }
    virtual void Close ( void ) { }
    virtual void Create ( void ) { }
    virtual void Unlink ( void ) { }
    virtual char* Read ( void ) { }
    virtual void Write ( char* toWrite ) { }
};

#endif
