
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Link                                  ║
// ║ Description      : Defines a file system link, for       ║
// ║                      output stream only for now.         ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef LINK_HPP
#define LINK_HPP

#include <threads/taskable.hpp>

namespace org {
  namespace filesystem {
    class Link;
  }
}

class org::filesystem::Link : public virtual org::threads::Taskable {
  protected:
    struct LinkCheshireCat;
    struct LinkCheshireCat * _dPtr;
    char* _name;
    char* _value;
    unsigned char _type;

    void SetName ( const char * name );
    void SetValue ( const char * value );
    void SetType ( unsigned char type );
    char* GetValue ( void );
    void DeleteStream ( void );
    unsigned char GetType ( void );
    void CreateStream ( void );
    void CloseFile ( void );
    void OpenFile ( void );
    void Configure ( const char * name, const char * value, unsigned char type );
    bool OpenStream ( void );

  public:
    Link ( const char * name, const char * value, unsigned char type );
    Link ( const char * name );

    virtual ~Link ( void );

    bool Good ( void );
    bool IsOpen ( void );
    void Open ( void );
    void Close ( void );
    void Write ( const char* message );
    bool IsFile ( void );
    void WriteLine ( const char* message );
};

#endif
 