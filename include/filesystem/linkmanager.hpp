
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : LinkManager                           ║
// ║ Description      : Defines access to all the Link that   ║
// ║                      should be used in the application   ║
// ║                      for ordering purposes.              ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef LINK_MANAGER_HPP
#define LINK_MANAGER_HPP


#include <filesystem/link.hpp>
#include <entity/configuredentity.hpp>

namespace org {
  namespace filesystem {
    class LinkManager;
  }
}

class org::filesystem::LinkManager : public virtual org::entity::ConfiguredEntity {
  private:
    std::map <org::types::CString, Link*>* _linkMap;

  protected:
    void DeleteLinkMap ( void );
    void SetLinkMap ( std::map <org::types::CString, Link*>* linkMap );
    std::map <org::types::CString, Link*>* GetLinkMap ( void );
    void FinishAutoConfiguration ( void ) override;
    void InitializeLinkManager ( );

  public:
    static LinkManager * Current;

    LinkManager ( void );
    LinkManager ( const char * configurationName );

    virtual ~LinkManager ( void );

    void AddFiles ( char*** files, int size );
    void AddFiles ( std::map <org::types::CString, const char *> * filesMap );
    void AddFile ( const char * name, const char * value );
    void AddLink ( const char * name, const char * value, unsigned char type );
    void AddLink ( const char * name, Link* link );
    void CloseAll ( void );
    int ContainsLink ( char* name );
    Link* Get ( char* name );
    void WriteTo ( char* name, char* message );
};

#endif
 