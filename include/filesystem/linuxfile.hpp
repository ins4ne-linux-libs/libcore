
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Linux file                            ║
// ║ Description      : Linux file access.                    ║
// ║ History:                                                 ║
// ║  - 19/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef LINUX_FILE_CLASS
#define LINUX_FILE_CLASS

#include <filesystem/file.hpp>

namespace org {
  namespace tools {
    class LinuxFile;
  }
}

#ifndef LINUX_FILE_DESCRIPTOR_FLAGS
  #define LINUX_FILE_DESCRIPTOR_FLAGS

  union LinuxFileDescriptorFlagsUnion {
    unsigned char all [2];
    struct {
      bool write            : 1;
      bool read             : 1;
      bool blocking         : 1;
      bool append           : 1;
      bool create           : 1;
      bool disynchronize    : 1;
      bool checkExistence   : 1;
      bool noControlTTY     : 1;
      bool readSynchronize  : 1;
      bool synchronize      : 1;
      bool truncate         : 1;
      unsigned char _empty           : 5;
    } flags;
  };
  typedef union LinuxFileDescriptorFlagsUnion LinuxFileDescriptorFlags;

#endif

class org::tools::LinuxFile : public virtual File {
  private:
    void InitializeLinuxFile ( void ) {
      this -> _name = nullptr;
      this -> _flags = this -> Allocate <LinuxFileDescriptorFlags> ( 1 );
      this -> _createPermission = 0666;
    }

  protected:
    char* _name;
    LinuxFileDescriptorFlags* _flags;
    int _createPermission;
    
    void FinishAutoConfiguration ( void ) override {
      File::FinishAutoConfiguration ( );
      this -> _name                                 = this -> ContainsSetting       ( "name", this -> _name );
      this -> _createPermission                     = this -> ContainsSettingAs <int>  ( "create-permission", this -> _createPermission );
      
      ( *( this -> _flags ) ).flags.write           = this -> ContainsSettingAs <bool> ( "write", false );
      ( *( this -> _flags ) ).flags.read            = this -> ContainsSettingAs <bool> ( "read", false );
      ( *( this -> _flags ) ).flags.blocking        = this -> ContainsSettingAs <bool> ( "blocking", false );
      ( *( this -> _flags ) ).flags.create          = this -> ContainsSettingAs <bool> ( "create", false );
      ( *( this -> _flags ) ).flags.disynchronize   = this -> ContainsSettingAs <bool> ( "disynchronize", false );
      ( *( this -> _flags ) ).flags.checkExistence  = this -> ContainsSettingAs <bool> ( "check-existence", false );
      ( *( this -> _flags ) ).flags.noControlTTY    = this -> ContainsSettingAs <bool> ( "no-control-TTY", false );
      ( *( this -> _flags ) ).flags.readSynchronize = this -> ContainsSettingAs <bool> ( "read-synchronize", false );
      ( *( this -> _flags ) ).flags.synchronize     = this -> ContainsSettingAs <bool> ( "synchronize", false );
      ( *( this -> _flags ) ).flags.truncate        = this -> ContainsSettingAs <bool> ( "truncate", false );
    }

  public:
    LinuxFile ( void ) {
      this -> InitializeLinuxFile ( );
    }

    LinuxFile ( char* configurationName ) {
      this -> InitializeLinuxFile ( );
      this -> SetConfigurationMask ( configurationName );
    }

    virtual ~LinuxFile ( void ) {
      this -> Free ( this -> _flags );
    }

    virtual void Open ( void ) { }
    virtual void Close ( void ) { }
    virtual void Create ( void ) { }
    virtual void Unlink ( void ) { }
    virtual char* Read ( void ) { }
    virtual void Write ( char* toWrite ) { }
};

#endif
