
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : NamedPipe                             ║
// ║ Description      : Ease access and use of named pipes.   ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef NAMED_PIPE_CLASS
#define NAMED_PIPE_CLASS

#include <threads/tunnelpulleventtask.hpp>
#include <tools/misc/stringextension.hpp>
#include <tools/misc/conversion.hpp>
#include <entity/loggingconfiguredentity.hpp>
#include <process/memorymanager.hpp>

namespace org {
  namespace filesystem {
    class NamedPipe;
  }
}

class org::filesystem::NamedPipe :
  public virtual org::entity::LoggingConfiguredEntity  ,
  public virtual org::process::MemoryManager            ,
  public virtual org::threads::Puller <unsigned char *>           {
  private:
    char*           _name;
    int             _namedPipeFileDescriptor;
    int             _sleepingTimer;
    int             _createPermission;
    bool            _readMode;
    bool            _writeMode;
    bool            _blocking;
    bool            _unlinkOnDelete;
    bool            _createIfNotExist;
    org::threads::Task*     _readTask;
    org::eventing::EventCall <unsigned char> *  _byteReceivedEvent;
    org::eventing::EventDelegateCall <NamedPipe, unsigned char> * _readEvent;

    void MainEvent ( org::eventing::EventArgs <void*, char*, unsigned char *>* args );
    void InitializeNamedPipe ( void );

  protected:
    void FinishAutoConfiguration ( void ) override;
  
  public:
    NamedPipe ( const char * configurationName );

    virtual ~NamedPipe ( void );

    unsigned char * Pull ( void ) override;
    void Unlink ( void );
    void PlugReadEvent ( org::eventing::EventCall <unsigned char>* event );
    void Open ( void );
    void CreateFifoIfNotExist ( void );
    void Close ( void );
    unsigned char * Read  ( void );
    int Write ( unsigned char * element );
    int Write ( unsigned char * array, int size );
    int Write ( char* message );
    void ReadAsTask ( void );
    void StopReadingTask ( void );
};

#endif
