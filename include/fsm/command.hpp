
#ifndef FSM_COMMAND_HPP
#define FSM_COMMAND_HPP

namespace org {
  namespace fsm {
    class Command;
  }
}

class org::fsm::Command : public virtual org::Object {
  private:
    struct CommandCheshireCat;
    struct CommandCheshireCat * _dPtr;

    void InitializeCommand ( void );
  
  public:
    Command ( void );

    ~Command ( void );
    
    const char * ObjectType ( void ) const override;
};

#endif
