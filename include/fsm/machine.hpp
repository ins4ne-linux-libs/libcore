
#ifndef FSM_MACHINE_HPP
#define FSM_MACHINE_HPP

namespace org {
  namespace fsm {
    class Machine;
  }
}

class org::fsm::Machine : public virtual org::Object {
  private:
    struct MachineCheshireCat;
    struct MachineCheshireCat * _dPtr;

    void InitializeMachine ( void );
  
  public:
    Machine ( void );

    ~Machine ( void );
    
    const char * ObjectType ( void ) const override;
};

#endif
