
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : MachineState                          ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 26/02/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef MACHINESTATE_HPP
#define MACHINESTATE_HPP

namespace org {
  namespace hardware {
    class MachineState;
  }
}

#include <tools/misc/stringextension.hpp>
#include <process/memorymanager.hpp>
#include <tools/misc/conversion.hpp>

class org::hardware::MachineState : public virtual org::process::MemoryManager {
  #ifdef MACHINESTATE_TESTS_EXISTS
    friend class org::unittest::MachineStateTests;
  #endif
  private:
    int _cursorPosition;

    void InitializeMachineState ( void );

  protected:
    const char * _type;
    short _id;
    int _byteCount;
    unsigned char * _bytes;

    void AllocateByteArray ( void );
    void CopyIn ( int count, unsigned char * source );
    void CopyInAtCursor ( unsigned char * source, int count );
    int CopyStringIn ( const char * str );
    int CopyStringIn ( const char * str, int max );
    const char * PutOutString ( void );
    void CopyStringInMask ( const char * str, int maskSize );
    void ResetCursor ( void );
    
    template <typename T>
    void PutIn ( T value ) {
      this -> PutIn <T> ( this -> _cursorPosition, value );
      this -> _cursorPosition += sizeof ( T );
    }

    template <typename T>
    void PutIn ( int index, T value ) {
      org::tools::misc::Convert::PutIn <T> ( &( this -> _bytes [index] ), value, -1 );
    }

    template <typename T>
    T PutOut ( void ) {
      return this -> PutOut <T> ( 0 );
    }
    
    template <typename T>
    T PutOut ( T defaultValue ) {
      T value = this -> PutOut <T> ( this -> _cursorPosition, defaultValue );
      this -> _cursorPosition += sizeof ( T );
      return value;
    }

    template <typename T>
    T PutOut ( int index, T defaultValue ) {
      return org::tools::misc::Convert::PutOut <T> ( &( this -> _bytes [index] ), -1, defaultValue );
    }

  public:
    MachineState ( void );
    MachineState ( short id, const char * type );
    MachineState ( unsigned char * source, int size );
    
    virtual ~MachineState ( void );

    short Id ( void );
    const char * Type ( void );
    int ByteCount ( void );
    unsigned char * ByteArray ( void );
    virtual bool IsEqual ( MachineState * toThatOne );
    const char * ObjectType ( void ) const override;
};

#endif
