
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : MachineStates                         ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 26/02/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef MACHINESTATES_HPP
#define MACHINESTATES_HPP

namespace org {
  namespace hardware {
    class MachineStates;
  }
}


#include <process/memorymanager.hpp>
#include <hardware/machinestate.hpp>

class org::hardware::MachineStates : 
  public virtual org::process::MemoryManager  {
  #ifdef MACHINESTATES_TESTS_EXISTS
    friend class org::unittest::MachineStatesTests;
  #endif
  private:
    std::map <org::types::CString, std::map <int, MachineState *> *> * _machineStates;

    void InitializeMachineStates ( void );

  public:
    MachineStates ( void );
    
    virtual ~MachineStates ( void );

    bool HasRegister ( org::types::CString key );
    bool HasState ( org::types::CString registerName, int id );
    void AddRegister ( org::types::CString key );
    unsigned char * GetRegisterAsByteArray ( org::types::CString registerName, int * finalLength );
    unsigned char ** GetRegisterAsBytePaginatedArray ( org::types::CString registerName, int maximum, int ** sizes, int * pageCount );
    MachineState * GetState ( org::types::CString registerName, int id );
    void SetState ( org::types::CString registerName, int id, MachineState * newState );
    bool SetStateIfDifferent ( org::types::CString registerName, int id, MachineState * newState );
    const char * ObjectType ( void ) const override;
    
    template <typename T>
    void AddStateFromByteArray ( org::types::CString registerName, int count, unsigned char * source ) {
      MachineState * state = nullptr;
      int tryCount = 0;
      int offset = 0;
      int byteCount = 0;
      do {
        state = new T ( count, &( source [offset] ) );
        if ( state -> ByteArray ( ) != nullptr )
          this -> SetState ( registerName, state -> Id ( ), state );
        byteCount = state -> ByteCount ( );
        tryCount ++;
        offset = byteCount * tryCount;
      } while ( state -> ByteArray ( ) != nullptr && offset < count && byteCount > 0 );
    }
};

#endif

