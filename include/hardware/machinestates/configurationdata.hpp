
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : ConfigurationData                         ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 26/02/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef CONFIGURATIONDATA_HPP
#define CONFIGURATIONDATA_HPP

namespace org {
  namespace hardware {
    namespace machinestates {
      class ConfigurationData;
    }
  }
}

#include <hardware/machinestate.hpp>

class org::hardware::machinestates::ConfigurationData : public virtual org::hardware::MachineState {
  #ifdef CONFIGURATIONDATA_TESTS_EXISTS
    friend class org::unittest::ConfigurationDataTests;
  #endif
  private:
    void InitializeConfigurationData ( void );

  public:
    ConfigurationData ( void );
    ConfigurationData ( unsigned char id, unsigned char itemType, unsigned char swVersionMajor, unsigned char swVersionMinor, unsigned char swVersionBuild, unsigned int updateTime );
    ConfigurationData ( int size, unsigned char * source );
    
    virtual ~ConfigurationData ( void );

    const char * ObjectType ( void ) const override;
};

#endif
