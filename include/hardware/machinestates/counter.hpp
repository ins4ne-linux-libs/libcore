
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Counter                         ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 26/02/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef COUNTER_HPP
#define COUNTER_HPP

namespace org {
  namespace hardware {
    namespace machinestates {
      class Counter;
    }
  }
}

#include <hardware/machinestate.hpp>

class org::hardware::machinestates::Counter : public virtual org::hardware::MachineState {
  #ifdef COUNTER_TESTS_EXISTS
    friend class org::unittest::CounterTests;
  #endif
  private:
    void InitializeCounter ( void );

  public:
    Counter ( void );
    Counter ( unsigned char id, int value );
    Counter ( int size, unsigned char * source );
    
    virtual ~Counter ( void );

    const char * ObjectType ( void ) const override;
};

#endif
