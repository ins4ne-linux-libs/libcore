
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : DigitalIO                         ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 26/02/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef DIGITALIO_HPP
#define DIGITALIO_HPP

namespace org {
  namespace hardware {
    namespace machinestates {
      class DigitalIO;
    }
  }
}

#include <hardware/machinestate.hpp>

class org::hardware::machinestates::DigitalIO : public virtual org::hardware::MachineState {
  #ifdef DIGITALIO_TESTS_EXISTS
    friend class org::unittest::DigitalIOTests;
  #endif
  private:
    void InitializeDigitalIO ( void );

  public:
    DigitalIO ( void );
    DigitalIO ( unsigned char id, bool value );
    DigitalIO ( unsigned char value );
    DigitalIO ( int size, unsigned char * source );
    
    virtual ~DigitalIO ( void );

    const char * ObjectType ( void ) const override;
};

#endif
