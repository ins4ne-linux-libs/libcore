
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : EprSkyduoCommand                      ║
// ║ Description      : h4_epr_skyduo_slave_cmd_rec_t.        ║
// ║ History:                                                 ║
// ║  - 03/01/2019 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef EPRSKYDUOCOMMAND_HPP
#define EPRSKYDUOCOMMAND_HPP

namespace org {
  namespace hardware {
    namespace machinestates {
      class EprSkyduoCommand;
    }
  }
}

#include <hardware/machinestate.hpp>

class org::hardware::machinestates::EprSkyduoCommand : public virtual org::hardware::MachineState {
  #ifdef EPRSKYDUOPAIRINGCOMMAND_TESTS_EXISTS
    friend class org::unittest::EprSkyduoCommandTests;
  #endif
  private:
    unsigned char _command;
    unsigned short _family;
    unsigned short _category;
    unsigned short _preset;
    const char * _name;

    void InitializeEprSkyduoCommand ( void );

  public:
    EprSkyduoCommand ( unsigned char command, unsigned short family, unsigned short category, unsigned short preset, const char * name );
    EprSkyduoCommand ( int size, unsigned char * source );

    unsigned char Command ( void );
    unsigned short Family ( void );
    unsigned short Category ( void );
    unsigned short Preset ( void );
    const char * Name ( void );

    const char * ObjectType ( void ) const override;
};

#endif
