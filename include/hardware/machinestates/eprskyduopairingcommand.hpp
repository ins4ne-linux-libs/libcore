
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : EprSkyduoPairingCommand               ║
// ║ Description      : h4_epr_skyduo_pairing_cmd_rec_t.      ║
// ║ History:                                                 ║
// ║  - 03/01/2019 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef EPRSKYDUOPAIRINGCOMMAND_HPP
#define EPRSKYDUOPAIRINGCOMMAND_HPP

namespace org {
  namespace hardware {
    namespace machinestates {
      class EprSkyduoPairingCommand;
    }
  }
}

#include <hardware/machinestate.hpp>

class org::hardware::machinestates::EprSkyduoPairingCommand : public virtual org::hardware::MachineState {
  #ifdef EPRSKYDUOPAIRINGCOMMAND_TESTS_EXISTS
    friend class org::unittest::EprSkyduoPairingCommandTests;
  #endif
  private:
    void InitializeEprSkyduoPairingCommand ( void );

  public:
    EprSkyduoPairingCommand ( unsigned char command, const char * pnc, const char * sn );

    const char * ObjectType ( void ) const override;
};

#endif
