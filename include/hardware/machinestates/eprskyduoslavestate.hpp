
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : EprSkyduoSlaveState                   ║
// ║ Description      : h4_epr_skyduo_slave_cmd_rec_t.        ║
// ║ History:                                                 ║
// ║  - 03/01/2019 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef EPRSKYDUOSLAVESTATE_HPP
#define EPRSKYDUOSLAVESTATE_HPP

namespace org {
  namespace hardware {
    namespace machinestates {
      class EprSkyduoSlaveState;
    }
  }
}

#include <hardware/machinestate.hpp>

class org::hardware::machinestates::EprSkyduoSlaveState : public virtual org::hardware::MachineState {
  #ifdef EPRSKYDUOPAIRINGSLAVESTATE_TESTS_EXISTS
    friend class org::unittest::EprSkyduoSlaveStateTests;
  #endif
  private:
    unsigned char _state;
    unsigned short _family;
    unsigned short _category;
    unsigned short _preset;
    const char * _name;

    void InitializeEprSkyduoSlaveState ( void );

  public:
    EprSkyduoSlaveState ( unsigned char state, unsigned short family, unsigned short category, unsigned short preset, const char * name );
    EprSkyduoSlaveState ( int size, unsigned char * source );

    unsigned char State ( void );
    unsigned short Family ( void );
    unsigned short Category ( void );
    unsigned short Preset ( void );
    const char * Name ( void );

    const char * ObjectType ( void ) const override;
};

#endif
