
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : NumericalIO                           ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 26/02/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef NUMERICALIO_HPP
#define NUMERICALIO_HPP

namespace org {
  namespace hardware {
    namespace machinestates {
      class NumericalIO;
    }
  }
}

#include <hardware/machinestate.hpp>
#include <entity/configuredentity.hpp>

class org::hardware::machinestates::NumericalIO :
  public virtual org::hardware::MachineState ,
  public virtual org::entity::ConfiguredEntity  {
  #ifdef NUMERICALIO_TESTS_EXISTS
    friend class org::unittest::NumericalIOTests;
  #endif
  private:
    unsigned char _floatingPointCount;

    void InitializeNumericalIO ( void );
  
  protected:
    void FinishAutoConfiguration ( void ) override;

  public:
    NumericalIO ( void );
    NumericalIO ( unsigned char id, double value, unsigned char unit );
    NumericalIO ( unsigned char id, int coefficient, unsigned char exponent );
    NumericalIO ( int size, unsigned char * source );
    
    virtual ~NumericalIO ( void );

    const char * ObjectType ( void ) const override;
};

#endif

