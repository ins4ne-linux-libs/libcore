
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Parameter                         ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 26/02/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef PARAMETER_HPP
#define PARAMETER_HPP

namespace org {
  namespace hardware {
    namespace machinestates {
      class Parameter;
    }
  }
}

#include <hardware/machinestate.hpp>
#include <entity/configuredentity.hpp>

class org::hardware::machinestates::Parameter :
  public virtual org::hardware::MachineState ,
  public virtual org::entity::ConfiguredEntity  {
  #ifdef PARAMETER_TESTS_EXISTS
    friend class org::unittest::ParameterTests;
  #endif
  private:
    unsigned char _floatingPointCount;

    void InitializeParameter ( void );
  
  protected:
    void FinishAutoConfiguration ( void ) override;

  public:
    Parameter ( void );
    Parameter ( unsigned char id, double value );
    Parameter ( unsigned char id, int coefficient, unsigned char exponent );
    Parameter ( int size, unsigned char * source );
    
    virtual ~Parameter ( void );

    const char * ObjectType ( void ) const override;
};

#endif
