
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Temperature                           ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 26/02/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef TEMPERATURE_HPP
#define TEMPERATURE_HPP

namespace org {
  namespace hardware {
    namespace machinestates {
      class Temperature;
    }
  }
}

#include <hardware/machinestate.hpp>
#include <entity/configuredentity.hpp>

class org::hardware::machinestates::Temperature :
  public virtual org::hardware::MachineState ,
  public virtual org::entity::ConfiguredEntity  {
  #ifdef TEMPERATURE_TESTS_EXISTS
    friend class org::unittest::TemperatureTests;
  #endif
  private:
    int _floatingPointCount;
    double _minimumChange;

    void InitializeTemperature ( void );
  
  protected:
    void FinishAutoConfiguration ( void ) override;

  public:
    Temperature ( void );
    Temperature ( unsigned char id, unsigned char unit, double value );
    Temperature ( unsigned char id, unsigned char unit, short coefficient, unsigned char exponent );
    Temperature ( int size, unsigned char * source );
    
    virtual ~Temperature ( void );

    bool IsEqual ( MachineState * toThatOne ) override;
    const char * ObjectType ( void ) const override;
};

#endif
