
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : SerialPort                            ║
// ║ Description      : Serial port object API.               ║
// ║ History:                                                 ║
// ║  - 18/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef SERIAL_PORT_HPP
#define SERIAL_PORT_HPP

#include <entity/loggingconfiguredentity.hpp>
#include <process/memorymanager.hpp>
#include <threads/tunnelpulleventtask.hpp>
#include <eventing/eventcall.hpp>
#include <eventing/eventdelegatecall.hpp>
#include <virtualization/virtualstreamfactory.hpp>

namespace org {
  namespace hardware {
    class SerialPort;
  }
}

class org::hardware::SerialPort :
  public virtual org::entity::LoggingConfiguredEntity  ,
  public virtual org::process::MemoryManager            ,
  public virtual org::threads::Puller <unsigned char *>           {
  #ifdef SERIALPORT_TESTS_EXISTS
    friend class org::unittest::SerialPortTests;
  #endif
  private:
    const char *    _name;
    int             _speed;
    int             _portDescriptor;
    int             _readTimeout;
    int             _readMinimum;
    bool            _readMode;
    bool            _writeMode;
    bool            _toFlush;
    bool            _firstReading;
    bool            _simulationEnable;
    bool            _openingSimulation;
    bool            _stopping;
    org::virtualization::VirtualStream <unsigned char> * _simulationStream;
    org::threads::Task *     _readTask;
    org::eventing::EventCall <unsigned char> *  _byteReceivedEvent;
    org::eventing::EventDelegateCall <SerialPort, unsigned char> * _readTaskEvent;

    void SerialPortEvent ( org::eventing::EventArgs <void *, char *, unsigned char *> * args );
    void TriggerEvent ( unsigned char * data );
    void InitializeSerialPort ( void );

  protected:
    void FinishAutoConfiguration ( void ) override;

  public:
    SerialPort ( void );
    SerialPort ( const char * name );

    virtual ~SerialPort ( void );

    unsigned char * Pull ( void ) override;
    bool IsOpen ( void );
    bool Open ( void );
    void SetSpeed ( int newSpeed );
    int Speed ( void );
    void ConfigureForNIU ( void );
    void Close ( void );
    int Write ( unsigned char * bytes, int size );
    bool HasBytesInBuffer ( void );
    int Read ( unsigned char * bufferToFill, int countToRead );
    unsigned char * Read ( void );
    void  ReadAsTask ( void );
    void  StopReadingTask ( void );
    void PlugByteReceivedEvent ( org::eventing::EventCall <unsigned char> * byteReceivedEvent );
    const char * ObjectType ( void ) const override;
};

#endif
