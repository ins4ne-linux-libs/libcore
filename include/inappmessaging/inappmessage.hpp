
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : InAppMessage                          ║
// ║ Description      : In application message, contains      ║
// ║                      destination, command and parameters ║
// ║                      access if any.                      ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef IN_APP_MESSAGE_HPP
#define IN_APP_MESSAGE_HPP

#include <process/memorymanager.hpp>

namespace org {
  namespace inappmessaging {
    class InAppMessage;
  }
}

class org::inappmessaging::InAppMessage : public virtual org::process::MemoryManager {
  private:
    char* _recipient;
    char* _command;
    char*** _parameters;
    int _parameterCount;
    char* _message;

    void InitializeInAppMessage ( void );
    void ReadMessage ( void );

  public:
    InAppMessage ( char* message );

    virtual ~InAppMessage ( void );

    char* Message ( void );
    int ParameterCount ( void );
    char* Command ( void );
    char** ParameterAt ( int index );
    char*** Parameters ( void );
    char* Recipient ( void );
};

#endif
