
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : InAppMessageable                      ║
// ║ Description      : Defines an in application message     ║
// ║                      receiver. Should be able to send    ║
// ║                      messages through application also   ║
// ║                      in the future.                      ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef IN_APP_MESSAGEABLE_HPP
#define IN_APP_MESSAGEABLE_HPP

#include <process/memorymanager.hpp>
#include <inappmessaging/inappmessage.hpp>

namespace org {
  namespace inappmessaging {
    class InAppMessageCenter;
    class InAppMessageable;
  }
}

class org::inappmessaging::InAppMessageable : public virtual org::process::MemoryManager {
  private:
    char* _inAppMessageName;
    InAppMessageCenter* _inAppMessageCenter;

    void InitializeInAppMessageable ( void );

  protected:
    void SendInAppMessage ( InAppMessage* inAppMessage );
    void SetInAppMessageableName ( const char * name );

    virtual void AddToInAppMessageCenter ( void ) = 0;

  public:
    InAppMessageable ( void );
    
    virtual ~InAppMessageable ( void );

    virtual InAppMessage* PostInAppMessage ( InAppMessage* inAppMessage );
    char* InAppMessageableName ( void );
    void SetInAppMessageCenter ( InAppMessageCenter* inAppMessageCenter );
};

#endif
