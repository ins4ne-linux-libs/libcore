
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : InAppMessageCenter                    ║
// ║ Description      : The in application message center     ║
// ║                      orders all in app messages and give ║
// ║                      them to their recipient correctly.  ║
// ║                      Should execute in a separate thread ║
// ║                      in the future.                      ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef IN_APP_MESSAGE_CENTER_HPP
#define IN_APP_MESSAGE_CENTER_HPP

#include <inappmessaging/inappmessageable.hpp>



namespace org {
  namespace inappmessaging {
    class InAppMessageCenter;
  }
}

class org::inappmessaging::InAppMessageCenter : public virtual org::Object {
  private:
    std::map <org::types::CString, InAppMessageable*>* _messageableRepository;

    void InitializeInAppMessageCenter ( void );

  public:
    InAppMessageCenter ( void );
    
    virtual ~InAppMessageCenter ( void );

    bool ContainsInAppMessageable ( char* name );
    void AddMessageable ( InAppMessageable* messageable );
    InAppMessage* Post ( InAppMessage* message );
};

#endif
