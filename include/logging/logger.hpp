
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Logger                                ║
// ║ Description      : Main logger class.                    ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <threads/taskable.hpp>
#include <tools/misc/stringextension.hpp>
#include <process/memorymanager.hpp>

namespace org {
  namespace logging {
    class Logger;
  }
}

#define LOG_LEVEL_NUMBER 7

enum LoggingLevels {
  TRACE     = 0x00,
  DEBUG     = 0x01,
  INFO      = 0x02,
  NOTICE    = 0x03,
  WARNING   = 0x04,
  ERROR     = 0x05,
  CRITICAL  = 0x06
};
typedef enum LoggingLevels LoggingLevel;

typedef int ( *LogOutputMethod ) ( org::logging::Logger*, LoggingLevel level, char* );

class org::logging::Logger : public virtual org::threads::Taskable {
  private:
    static unsigned char             LoggerSystemInitialized;
    static char**           LevelsStrings;
    static char*            Format;
    static char*            TimeFormat;
    static LoggingLevel     DefaultLevel;
    static char*            DefaultName;
    static char*            DefaultLink;

  protected:
    char*           _name;
    char*           _loggingName;
    char*           _format;
    char*           _timeFormat;
    LoggingLevel    _currentLevel;
    LogOutputMethod _outputMethod;

    static void InitializeLoggerSystem ( void );
    static void SetLevelsString ( char** stringArray );
    static void SetLevelString ( LoggingLevel logLevel, char* logString );
    static void SetDefaultFormat ( char* newFormat );
    static void SetDefaultTimeFormat ( char* newTimeFormat );
    static void SetDefaultLevel ( LoggingLevel defaultLevel );
    static void SetDefaultName ( char* defaultName );

    void GetArgs ( char** toFill, LoggingLevel level, char* message );
    void FreeArgs ( char** args );
    virtual void Log ( LoggingLevel level, const char * message );
    void SetName ( char* newName );
    void SetOutputMethod ( LogOutputMethod newOutputMethod );
    void SetFormat ( char* newFormat );
    void SetTimeFormat ( char* newTimeFormat );
    virtual void TryAutoConfiguration ( void );
    void Configure ( void );

  public:
    Logger ( void );

    virtual ~Logger ( void );

    void SetLevel ( LoggingLevel newLogLevel );
    void LogAtLevel ( LoggingLevel level, char* message );
    void Trace ( const char * message );
    void Debug ( const char * message );
    void Info ( const char * message );
    void Notice ( const char * message );
    void Warning ( const char * message );
    void Error ( const char * message );
    void Critical ( const char * message );
    char* GetName ( void );
    LoggingLevel GetLevel ( void );
};

#endif
 