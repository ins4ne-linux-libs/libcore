
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : LoggerConfigured                         ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 26/02/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef LOGGERCONFIGURED_HPP
#define LOGGERCONFIGURED_HPP

namespace org {
  namespace logging {
    class LoggerConfigured;
  }
}


#include <entity/configuredentity.hpp>
#include <logging/logger.hpp>

class org::logging::LoggerConfigured :
  public virtual org::logging::Logger ,
  public virtual org::entity::ConfiguredEntity {
  #ifdef LOGGERCONFIGURED_TESTS_EXISTS
    friend class org::unittest::LoggerConfiguredTests;
  #endif
  private:
    const char * _linkName;
    bool _addNewLineBefore;

    void InitializeLoggerConfigured ( void );

  protected:
    static int Syslog ( Logger* logger, LoggingLevel level, char* message );
    static int LinkPrint ( Logger * logger, LoggingLevel level, char * message );

    void FinishAutoConfiguration ( void ) override;

  public:
    LoggerConfigured ( const char * configurationName );
    
    virtual ~LoggerConfigured ( void );

    const char * ObjectType ( void ) const override;
};

#endif
