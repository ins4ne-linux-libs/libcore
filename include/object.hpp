
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : EventTask                             ║
// ║ Description      : Eventing task, if it has to pull,     ║
// ║                      pulls, transforms then push. Needs  ║
// ║                      logically four delegates.           ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef OBJECT_HPP
#define OBJECT_HPP

namespace org {
  class Object;
}

class org::Object {
  private:
    char * _memoryBeacon;
    
  public:
    Object ( void );

    virtual ~Object ( void );

    virtual const char * ObjectType ( void ) const;
};

#endif
