
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : MemoryAllocation                      ║
// ║ Description      : Helper for memory allocation.         ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef MEMORY_ALLOCATION_HPP
#define MEMORY_ALLOCATION_HPP

namespace org {
  namespace process {
    template <typename T> class MemoryAllocation;
  }
}

#include <abstraction/memory.hpp>

template <typename T>
class org::process::MemoryAllocation : public virtual org::Object {
  private:
    T* _pointer;
    int _byteCount;

    void InitializeMemory ( ) {
      if ( this -> _pointer != nullptr ) {
        unsigned char * memory = ( unsigned char * ) this -> _pointer;
        for ( int i = 0; i < this -> _byteCount; i ++ )
          memory [i] = 0;
      }
    }

    void InitializeMemoryAllocation ( void ) {
      this -> _pointer = nullptr;
      this -> _byteCount = 0;
    }

  protected:
    void Allocate ( ) {
      if ( this -> _byteCount > 0 )
        this -> _pointer = ( T* ) org::abstraction::Memory::Allocate ( this -> _byteCount );
      if ( this -> _pointer != nullptr )
        this -> InitializeMemory ( );
    }
  
  public:
    MemoryAllocation ( int byteCount ) {
      this -> InitializeMemoryAllocation ( );
      this -> _byteCount = byteCount;
      this -> Allocate ( );
    }

    T* Pointer ( void ) {
      return this -> _pointer;
    }
    
    static T* New ( int byteCount ) {
      MemoryAllocation <T>* alloc = new MemoryAllocation <T> ( byteCount );
      T* ans = alloc -> Pointer ( );
      delete alloc;
      return ans;
    }
};

#endif
