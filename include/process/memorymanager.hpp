
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : MemoryAllocation                      ║
// ║ Description      : Defines a memory manager as           ║
// ║                      object-level.                       ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef MEMORY_MANAGER_HPP
#define MEMORY_MANAGER_HPP


#include <process/memoryallocation.hpp>

namespace org {
  namespace process {
    class MemoryManager;
  }
}

class org::process::MemoryManager : public virtual org::Object {
  protected:
    void Free ( void * pointer );

    template <typename T>
    T * Allocate ( int elementCount ) {
      // return MemoryAllocation <T>::New ( elementCount * sizeof ( T ) );
      return MemoryAllocation <T>::New ( elementCount * sizeof ( T ) );
    }
    
    template <typename T>
    T * Allocate ( ) {
      return this -> Allocate <T> ( 1 );
    }

    template <typename T>
    void Delete ( T * pointer ) {
      if ( pointer != nullptr )
        delete pointer;
    }
  
  public:
    template <typename T>
    static T * AllocateCopy ( T * toCopy ) {
      unsigned char * toCopyInBytes = ( unsigned char * ) toCopy;
      int size = sizeof ( T );
      unsigned char * newCopy = ( unsigned char * ) MemoryManager::Malloc ( 1, size );
      for ( int i = 0; i < size; i ++ )
        newCopy [i] = toCopyInBytes [i];
      return ( T* ) newCopy;
    }

    template <typename T>
    static T * AllocateCopy ( T toCopy ) {
      return AllocateCopy <T> ( &toCopy );
    }

    static void * Malloc ( int count, int size );
    static void FreeM ( void * pointer );
    static void FreeMA ( void ** pointer, int size );
};

#endif
