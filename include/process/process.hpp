
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Process                               ║
// ║ Description      : Process related functions.            ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef PROCESS_HPP
#define PROCESS_HPP

namespace org {
  namespace process {
    class System;
  }
}

class org::process::System : public virtual org::Object {
  public:
    static void Daemonize ( const char * daemonFile );
    static void Daemonize ( const char * daemonFile, const char * newDir );
    static void Daemonize ( const char * daemonFile, const char * newDir, const char * newOutputs, const char * modes );
    static void DaemonizeSaveOutput ( const char * daemonFile, const char * newOutput );
    static void Daemonize ( const char * daemonFile, const char * newDir, const char * stdIn, const char * stdInMode, const char * stdOut, const char * stdOutMode, const char * stdErr, const char * stdErrMode );
    static char* PID ( void );
};

#endif
