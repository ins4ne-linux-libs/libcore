
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Generic                               ║
// ║ Description      : Defines a generic protocol            ║
// ║                      definition.                         ║
// ║ History:                                                 ║
// ║  - 18/12/2017 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef GENERIC_PROTOCOL_HPP
#define GENERIC_PROTOCOL_HPP

namespace org {
  namespace protocol {
    class Generic;
  }
}

#include <process/memorymanager.hpp>
#include <tools/misc/stringextension.hpp>

class org::protocol::Generic : public virtual org::Object {
  protected:
    unsigned char   _partsCount;
    unsigned char *  _mainParts;
    unsigned char * _fillingNull;
    unsigned char ** _parts;

    void Configure ( int partsCount, unsigned char * main, unsigned char * filling );
    void FreeParts ( void );
    void FreePart ( int index );
    void InitializePart ( int index, int size );
    void InversePart ( int index1, int index2 );
    void SetPart ( int index, unsigned char * newPart );
    void SetPart ( int index, unsigned char * newPart, int size );
    void UpdatePartAndSize ( int index, int size );
    void SetUpParts ( void );
    void FillPart ( int index, unsigned char * newPart, int size );
    void FillParts ( unsigned char * rawMessage );
    int GetPartsSize ( int from, int to );
    unsigned char * GetParts ( int from, int to );
    char* GetPartsAsHex ( int from, int to );
    char* GetPartAsHex ( int index );
    virtual void SetSpecificPart ( int index, unsigned char * newPart, int size ) = 0;
    
    template <class T>
    T* CreateCopy ( void ) {
      unsigned char * message = this -> GetMessage ( );
      T* copy = new T ( message );
      org::process::MemoryManager::FreeM ( ( void* ) message );
      return copy;
    }

  public:
    Generic ( void );
    Generic  ( int partsCount, unsigned char * main, unsigned char * filling, unsigned char * message );

    virtual ~Generic ( void );

    unsigned char * GetPart ( int index );
    void Read ( unsigned char * message  );
    int GetSize ( int index );
    int GetMessageSize ( void );
    unsigned char * GetMessage ( void );
};

#endif
 