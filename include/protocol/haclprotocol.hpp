
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : HaclProtocol                          ║
// ║ Description      : Hacl protocol definition using        ║
// ║                      Generic algorithm.                  ║
// ║ History:                                                 ║
// ║  - 18/12/2017 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef HACL_PROTOCOL_HPP
#define HACL_PROTOCOL_HPP

#include <protocol/generic.hpp>

#include <entity/configuredentity.hpp>

namespace org {
  namespace protocol {
    class HaclProtocol;
  }
}

class org::protocol::HaclProtocol :
  public virtual Generic  ,
  public virtual org::process::MemoryManager ,
  public virtual org::entity::ConfiguredEntity {
  private:
    int _pageNumber;
    std::map <int, std::pair <unsigned char *, int> *> * _dataPages;

    void InitializeHaclProtocol ( void );
    void ReadMessage ( unsigned char * message );
    void BuildLightMessage ( unsigned char * lightMessage, int size );

  protected:
    static unsigned char * HaclMainProtocol;

    static void InitializeMainProtocol ( void );
    void SetSpecificPart ( int index, unsigned char * newPart, int size ) override;
    int GetRawDataSize ( void );
    void UpdateSize ( void );
    void FinishAutoConfiguration ( void ) override;

  public:
    HaclProtocol ( unsigned char * message );
    HaclProtocol ( unsigned char * lightMessage, int size );
    HaclProtocol ( const char * configurationName );
    HaclProtocol ( const char * configurationName, short activeSessionNumber );
    HaclProtocol ( short activeSessionNumber, unsigned char * lightMessage, int size );

    virtual ~HaclProtocol ( void );

    HaclProtocol* Copy ( void );
    bool IsPaginated ( void );
    void AddDataPage ( unsigned char * page, int size );
    bool SetDataToPage ( int pageIndex );
    bool HasDataPage ( int pageIndex );
    unsigned char * GetDataPage ( int pageIndex );
    int GetDataPageSize ( int pageIndex );
    int GetDataPageCount ( void );
    void UpdateChecksum ( void );

    // Tools
    char* LightMessageAsHex ( void );
    char* AsHexString ( void );
    char * DataAsString ( void );

    // Session number
    void CloseSession ( void );
    bool SessionIsOpen ( void );
    void OpenSession ( void );
    void SetSessionNumber ( short newSessionNumber );
    short GetSessionAsShort ( void );

    // Data
    void RemoveData ( void );
    void SetData ( unsigned char * newData, int size );
    int DataSize ( void );
    unsigned char * GetData ( void );

    // Source and destination
    unsigned char * GetSource ( void );
    unsigned char * GetDestination ( void );
    void InverseSourceDestination ( void );
    char * SourceAsHex ( void );
    char * DestinationAsHex ( void );

    // Modes
    bool IsResponse ( void );
    bool IsRequest ( void );
    bool IsInformation ( void );
    bool IsExecute ( void );
    void SetToAck ( void );
    void SetToResponse ( void );
    void SetToInformation ( void );
    void SetMode ( unsigned char newMode );
    unsigned char * GetMode ( void );

    // Names
    bool IsNoName ( void );
    bool IsPing ( void );
    bool IsInternalReset ( void );
    bool IsInternalBootMode ( void );
    bool IsInternalBlockIntuition ( void );
    bool IsInternalEnableIntuition ( void );
    bool IsInternalSpecialRequest ( void );
    bool IsInternalStartActivity ( void );
    bool IsInternalStopActivity ( void );
    bool IsApplianceStructureName ( void );
    bool IsStressTest ( void );
    unsigned char * GetName ( void );
    short GetNameAsShort ( void );
    char * NameAsHex ( void );
    const char * ObjectType ( void ) const override;
};

#endif
