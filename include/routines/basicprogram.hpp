
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : BasicProgram                         ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 09/05/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef ROUTINES_BASICPROGRAM_HPP
#define ROUTINES_BASICPROGRAM_HPP

namespace org {
  namespace routines {
    class BasicProgram;
  }
}

#include <routines/mainprogram.hpp>
#include <inappmessaging/inappmessageable.hpp>

#include <servers/server.hpp>

class org::routines::BasicProgram :
  public virtual org::routines::MainProgram,
  public virtual org::inappmessaging::InAppMessageable {
  #ifdef ROUTINES_BASICPROGRAM_TESTS_EXISTS
    friend class org::unittest::BasicProgramTests;
  #endif
  private:
    int           _period;
    int           _iterations;
    bool          _continuing;
    bool          _endLess;
    bool          _asDaemon;
    bool          _testRoutine;
    bool          _syslogEnabled;
    const char *  _syslogName;
    const char *  _daemonLockFile;
    const char *  _outputFile;
    std::map <org::types::CString, org::servers::Server *> * _servers;
    std::map <int, const char *> * _serversToStart;
    std::map <int, bool> * _signalsToHandle;

    void InitializeBasicProgram ( void );
    void LogPid ( void );
    void StartServers ( void );
    void StopServers ( void );
  
  protected:
    virtual void Configure ( int argCount, char ** argStrings );
    void AddServer ( org::servers::Server * newServer );
    void TryStartServer ( const char * serverName );
    void TryStopServer ( const char * serverName );
    void AddToInAppMessageCenter ( void ) override;
    void TimerEndMethod ( void );
    void ExecuteTestRoutine ( void );
    virtual void TestRoutine ( void );
    void FinishAutoConfiguration ( void ) override;

    template <typename T>
    void AddServer ( const char * configurationSectionName ) {
      this -> AddServer ( new T ( configurationSectionName ) );
    }

  public:
    BasicProgram ( void );
    
    virtual ~BasicProgram ( void );

    void Main ( void ) override;
    virtual void PreTimerEndMethod ( void );
    virtual void PostTimerEndMethod ( void );
    void DeleteVariable ( void ) override;
    virtual void UnloadModules ( void );
    org::inappmessaging::InAppMessage * PostInAppMessage ( org::inappmessaging::InAppMessage * inAppMessage ) override;
    const char * ObjectType ( void ) const override;
};

#endif
