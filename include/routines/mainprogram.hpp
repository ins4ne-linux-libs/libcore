
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : MainProgram                           ║
// ║ Description      : Main program, should be used for      ║
// ║                      inheritance only.                   ║
// ║ History:                                                 ║
// ║  - 18/12/2017 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef MAIN_PROGRAM_HPP
#define MAIN_PROGRAM_HPP

#include <entity/loggingconfiguredentity.hpp>

namespace org {
  namespace routines {
    class MainProgram;
  }
}

class org::routines::MainProgram : public virtual org::entity::LoggingConfiguredEntity {
  protected:
    int _argCount;
    char** _argStrings;

    virtual void Configure ( int argCount, char ** argStrings );

  public:
    MainProgram ( void );
    
    void Execute ( void );
    virtual void LoadModules ( void );
    virtual void CreateVariable ( void );
    virtual void Main ( void );
    virtual void DeleteVariable ( void );
    virtual void UnloadModules ( void );
};

#endif
