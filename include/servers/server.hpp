
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Server                                ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 26/02/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef SERVER_HPP
#define SERVER_HPP

namespace org {
  namespace servers {
    class Server;
  }
}

#include <entity/loggingconfiguredentity.hpp>
#include <process/memorymanager.hpp>

class org::servers::Server :
  public virtual org::entity::LoggingConfiguredEntity  ,
  public virtual org::process::MemoryManager            ,
  public virtual org::threads::Taskable                 {
  private:
    const char * _name;

    void InitializeServer ( void );

  protected:
    void FinishAutoConfiguration ( void ) override;

  public:
    Server ( void );
    
    virtual ~Server ( void );

    const char * ServerName ( void );

    virtual void Start ( void ) = 0;
    virtual void Stop ( void ) = 0;
};

#endif
