
#ifndef BUFFERSTREAM_TESTS_HPP
#define BUFFERSTREAM_TESTS_HPP

#define BUFFERSTREAM_TESTS_CLASS_NAME        BufferStreamTests
#define BUFFERSTREAM_TEST_NAME( name )       BufferStream_ ## name ## _Test
#define BUFFERSTREAM_TEST( name )            void BUFFERSTREAM_TEST_NAME ( name ) ( void )
#define BUFFERSTREAM_TEST_C( name )          void BUFFERSTREAM_TESTS_CLASS_NAME::BUFFERSTREAM_TEST_NAME ( name ) ( void )
#define BUFFERSTREAM_TESTS_ADD( name, desc ) this -> AddUnitTest ( # name, &BUFFERSTREAM_TESTS_CLASS_NAME:: BUFFERSTREAM_TEST_NAME ( name ) ,desc )

namespace org {
  namespace unittest {
    class BUFFERSTREAM_TESTS_CLASS_NAME;
  }
}

#define BUFFERSTREAM_TESTS_EXISTS

class org::unittest::BUFFERSTREAM_TESTS_CLASS_NAME : public virtual org::unittest::UnitTestClass <org::unittest::BUFFERSTREAM_TESTS_CLASS_NAME> {
  public:
    static BUFFERSTREAM_TESTS_CLASS_NAME * BUFFERSTREAM_TESTS_CLASS_NAME_instance;
    
    BUFFERSTREAM_TESTS_CLASS_NAME ( void );

    BUFFERSTREAM_TEST ( Looping );
    BUFFERSTREAM_TEST ( Base_10 );
    BUFFERSTREAM_TEST ( Configuration_Hex );
    BUFFERSTREAM_TEST ( Configuration_String );
    BUFFERSTREAM_TEST ( Count_0 );
    BUFFERSTREAM_TEST ( Creation );
    BUFFERSTREAM_TEST ( Delete );
    BUFFERSTREAM_TEST ( ObjectType );
};

#endif
