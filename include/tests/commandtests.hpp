
#ifndef COMMAND_TESTS_HPP
#define COMMAND_TESTS_HPP

#define COMMAND_TESTS_CLASS_NAME        CommandTests
#define COMMAND_TEST_NAME( name )       Command_ ## name ## _Test
#define COMMAND_TEST( name )            void COMMAND_TEST_NAME ( name ) ( void )
#define COMMAND_TEST_C( name )          void COMMAND_TESTS_CLASS_NAME::COMMAND_TEST_NAME ( name ) ( void )
#define COMMAND_TESTS_ADD( name, desc ) this -> AddUnitTest ( # name, &COMMAND_TESTS_CLASS_NAME:: COMMAND_TEST_NAME ( name ) ,desc )

namespace org {
  namespace unittest {
    class COMMAND_TESTS_CLASS_NAME;
  }
}

#define COMMAND_TESTS_EXISTS

class org::unittest::COMMAND_TESTS_CLASS_NAME : public virtual org::unittest::UnitTestClass <org::unittest::COMMAND_TESTS_CLASS_NAME> {
  public:
    static COMMAND_TESTS_CLASS_NAME * COMMAND_TESTS_CLASS_NAME_instance;
    
    COMMAND_TESTS_CLASS_NAME ( void );

    COMMAND_TEST ( Creation );
    COMMAND_TEST ( Delete );
    COMMAND_TEST ( ObjectType );
};

#endif
