
#ifndef CSTRING_TESTS_HPP
#define CSTRING_TESTS_HPP

#define CSTRING_TESTS_CLASS_NAME        CStringTests
#define CSTRING_TEST_NAME( name )       CString_ ## name ## _Test
#define CSTRING_TEST( name )            void CSTRING_TEST_NAME ( name ) ( void )
#define CSTRING_TEST_C( name )          void CSTRING_TESTS_CLASS_NAME::CSTRING_TEST_NAME ( name ) ( void )
#define CSTRING_TESTS_ADD( name, desc ) this -> AddUnitTest ( # name, &CSTRING_TESTS_CLASS_NAME:: CSTRING_TEST_NAME ( name ) ,desc )

namespace org {
  namespace unittest {
    class CSTRING_TESTS_CLASS_NAME;
  }
}

#define CSTRING_TESTS_EXISTS

class org::unittest::CSTRING_TESTS_CLASS_NAME : public virtual org::unittest::UnitTestClass <org::unittest::CSTRING_TESTS_CLASS_NAME> {
  public:
    static CSTRING_TESTS_CLASS_NAME * CSTRING_TESTS_CLASS_NAME_instance;
    
    CSTRING_TESTS_CLASS_NAME ( void );

    CSTRING_TEST ( Contains );
    CSTRING_TEST ( CStringToLower );
    CSTRING_TEST ( CStringToUpper );
    CSTRING_TEST ( CharToUpper );
    CSTRING_TEST ( CharToLower );
    CSTRING_TEST ( PutBetween );
    CSTRING_TEST ( Concatenate );
    CSTRING_TEST ( Equal );
};

#endif
