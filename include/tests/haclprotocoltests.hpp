
#ifndef HACLPROTOCOL_TESTS_HPP
#define HACLPROTOCOL_TESTS_HPP

#define HACLPROTOCOL_TESTS_CLASS_NAME        HaclProtocolTests
#define HACLPROTOCOL_TEST_NAME( name )       HaclProtocol_ ## name ## _Test
#define HACLPROTOCOL_TEST( name )            void HACLPROTOCOL_TEST_NAME ( name ) ( void )
#define HACLPROTOCOL_TEST_C( name )          void HACLPROTOCOL_TESTS_CLASS_NAME::HACLPROTOCOL_TEST_NAME ( name ) ( void )
#define HACLPROTOCOL_TESTS_ADD( name, desc ) this -> AddUnitTest ( # name, &HACLPROTOCOL_TESTS_CLASS_NAME:: HACLPROTOCOL_TEST_NAME ( name ) ,desc )

namespace org {
  namespace unittest {
    class HACLPROTOCOL_TESTS_CLASS_NAME;
  }
}

#define HACLPROTOCOL_TESTS_EXISTS

class org::unittest::HACLPROTOCOL_TESTS_CLASS_NAME : public virtual org::unittest::UnitTestClass <org::unittest::HACLPROTOCOL_TESTS_CLASS_NAME> {
  public:
    static HACLPROTOCOL_TESTS_CLASS_NAME * HACLPROTOCOL_TESTS_CLASS_NAME_instance;
    
    HACLPROTOCOL_TESTS_CLASS_NAME ( void );

    HACLPROTOCOL_TEST ( Creation );
    HACLPROTOCOL_TEST ( Delete );
    HACLPROTOCOL_TEST ( ObjectType );
    HACLPROTOCOL_TEST ( NameAsShort );
};

#endif
