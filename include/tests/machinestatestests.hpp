
#ifndef MACHINESTATES_TESTS_HPP
#define MACHINESTATES_TESTS_HPP

#define MACHINESTATES_TESTS_CLASS_NAME        MachineStatesTests
#define MACHINESTATES_TEST_NAME( name )       MachineStates_ ## name ## _Test
#define MACHINESTATES_TEST( name )            void MACHINESTATES_TEST_NAME ( name ) ( void )
#define MACHINESTATES_TEST_C( name )          void MACHINESTATES_TESTS_CLASS_NAME::MACHINESTATES_TEST_NAME ( name ) ( void )
#define MACHINESTATES_TESTS_ADD( name, desc ) this -> AddUnitTest ( # name, &MACHINESTATES_TESTS_CLASS_NAME:: MACHINESTATES_TEST_NAME ( name ) ,desc )

namespace org {
  namespace unittest {
    class MACHINESTATES_TESTS_CLASS_NAME;
  }
}

#define MACHINESTATES_TESTS_EXISTS

class org::unittest::MACHINESTATES_TESTS_CLASS_NAME : public virtual org::unittest::UnitTestClass <org::unittest::MACHINESTATES_TESTS_CLASS_NAME> {
  public:
    static MACHINESTATES_TESTS_CLASS_NAME * MACHINESTATES_TESTS_CLASS_NAME_instance;
    
    MACHINESTATES_TESTS_CLASS_NAME ( void );

    MACHINESTATES_TEST ( Creation );
    MACHINESTATES_TEST ( Delete );
    MACHINESTATES_TEST ( ObjectType );
    MACHINESTATES_TEST ( AddingRegister );
    MACHINESTATES_TEST ( AddingState );
    MACHINESTATES_TEST ( AddingStates );
    MACHINESTATES_TEST ( Concatenation );
    MACHINESTATES_TEST ( Pagination );
    MACHINESTATES_TEST ( AddingStateIfDifferent );
};

#endif
