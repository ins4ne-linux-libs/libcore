
#ifndef MACHINESTATE_TESTS_HPP
#define MACHINESTATE_TESTS_HPP

#define MACHINESTATE_TESTS_CLASS_NAME        MachineStateTests
#define MACHINESTATE_TEST_NAME( name )       MachineState_ ## name ## _Test
#define MACHINESTATE_TEST( name )            void MACHINESTATE_TEST_NAME ( name ) ( void )
#define MACHINESTATE_TEST_C( name )          void MACHINESTATE_TESTS_CLASS_NAME::MACHINESTATE_TEST_NAME ( name ) ( void )
#define MACHINESTATE_TESTS_ADD( name, desc ) this -> AddUnitTest ( # name, &MACHINESTATE_TESTS_CLASS_NAME:: MACHINESTATE_TEST_NAME ( name ) ,desc )

namespace org {
  namespace unittest {
    class MACHINESTATE_TESTS_CLASS_NAME;
  }
}

#define MACHINESTATE_TESTS_EXISTS

class org::unittest::MACHINESTATE_TESTS_CLASS_NAME : public virtual org::unittest::UnitTestClass <org::unittest::MACHINESTATE_TESTS_CLASS_NAME> {
  public:
    static MACHINESTATE_TESTS_CLASS_NAME * MACHINESTATE_TESTS_CLASS_NAME_instance;
    
    MACHINESTATE_TESTS_CLASS_NAME ( void );

    MACHINESTATE_TEST ( Creation );
    MACHINESTATE_TEST ( Delete );
    MACHINESTATE_TEST ( ObjectType );
};

#endif
