
#ifndef MACHINE_TESTS_HPP
#define MACHINE_TESTS_HPP

#define MACHINE_TESTS_CLASS_NAME        MachineTests
#define MACHINE_TEST_NAME( name )       Machine_ ## name ## _Test
#define MACHINE_TEST( name )            void MACHINE_TEST_NAME ( name ) ( void )
#define MACHINE_TEST_C( name )          void MACHINE_TESTS_CLASS_NAME::MACHINE_TEST_NAME ( name ) ( void )
#define MACHINE_TESTS_ADD( name, desc ) this -> AddUnitTest ( # name, &MACHINE_TESTS_CLASS_NAME:: MACHINE_TEST_NAME ( name ) ,desc )

namespace org {
  namespace unittest {
    class MACHINE_TESTS_CLASS_NAME;
  }
}

#define MACHINE_TESTS_EXISTS

class org::unittest::MACHINE_TESTS_CLASS_NAME : public virtual org::unittest::UnitTestClass <org::unittest::MACHINE_TESTS_CLASS_NAME> {
  public:
    static MACHINE_TESTS_CLASS_NAME * MACHINE_TESTS_CLASS_NAME_instance;
    
    MACHINE_TESTS_CLASS_NAME ( void );

    MACHINE_TEST ( Creation );
    MACHINE_TEST ( Delete );
    MACHINE_TEST ( ObjectType );
};

#endif
