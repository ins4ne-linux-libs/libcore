
#ifndef SEQUENCESTREAM_TESTS_HPP
#define SEQUENCESTREAM_TESTS_HPP

#define SEQUENCESTREAM_TESTS_CLASS_NAME        SequenceStreamTests
#define SEQUENCESTREAM_TEST_NAME( name )       SequenceStream_ ## name ## _Test
#define SEQUENCESTREAM_TEST( name )            void SEQUENCESTREAM_TEST_NAME ( name ) ( void )
#define SEQUENCESTREAM_TEST_C( name )          void SEQUENCESTREAM_TESTS_CLASS_NAME::SEQUENCESTREAM_TEST_NAME ( name ) ( void )
#define SEQUENCESTREAM_TESTS_ADD( name, desc ) this -> AddUnitTest ( # name, &SEQUENCESTREAM_TESTS_CLASS_NAME:: SEQUENCESTREAM_TEST_NAME ( name ) ,desc )

namespace org {
  namespace unittest {
    class SEQUENCESTREAM_TESTS_CLASS_NAME;
  }
}

#define SEQUENCESTREAM_TESTS_EXISTS

class org::unittest::SEQUENCESTREAM_TESTS_CLASS_NAME : public virtual org::unittest::UnitTestClass <org::unittest::SEQUENCESTREAM_TESTS_CLASS_NAME> {
  public:
    static SEQUENCESTREAM_TESTS_CLASS_NAME * SEQUENCESTREAM_TESTS_CLASS_NAME_instance;
    
    SEQUENCESTREAM_TESTS_CLASS_NAME ( void );

    SEQUENCESTREAM_TEST ( SequencingGoingThrough );
    SEQUENCESTREAM_TEST ( Configuration_Sequences );
    SEQUENCESTREAM_TEST ( Configuration_Type );
    SEQUENCESTREAM_TEST ( Creation );
    SEQUENCESTREAM_TEST ( Delete );
    SEQUENCESTREAM_TEST ( ObjectType );
};

#endif
