
#ifndef SERIALPORT_TESTS_HPP
#define SERIALPORT_TESTS_HPP

#define SERIALPORT_TESTS_CLASS_NAME        SerialPortTests
#define SERIALPORT_TEST_NAME( name )       SerialPort_ ## name ## _Test
#define SERIALPORT_TEST( name )            void SERIALPORT_TEST_NAME ( name ) ( void )
#define SERIALPORT_TEST_C( name )          void SERIALPORT_TESTS_CLASS_NAME::SERIALPORT_TEST_NAME ( name ) ( void )
#define SERIALPORT_TESTS_ADD( name, desc ) this -> AddUnitTest ( # name, &SERIALPORT_TESTS_CLASS_NAME:: SERIALPORT_TEST_NAME ( name ) ,desc )

namespace org {
  namespace unittest {
    class SERIALPORT_TESTS_CLASS_NAME;
  }
}

#define SERIALPORT_TESTS_EXISTS

class org::unittest::SERIALPORT_TESTS_CLASS_NAME : public virtual org::unittest::UnitTestClass <org::unittest::SERIALPORT_TESTS_CLASS_NAME> {
  public:
    static SERIALPORT_TESTS_CLASS_NAME * SERIALPORT_TESTS_CLASS_NAME_instance;
    
    SERIALPORT_TESTS_CLASS_NAME ( void );

    SERIALPORT_TEST ( ReadingSimulationEmptySequence );
    SERIALPORT_TEST ( ReadingSimulationSequenceTwoWithOverflow );
    SERIALPORT_TEST ( ReadingSimulationSequenceTwo );
    SERIALPORT_TEST ( ReadingSimulationBP );
    SERIALPORT_TEST ( ReadingSimulationOBO );
    SERIALPORT_TEST ( ConfigurationTestForSimulatorEnabled );
    SERIALPORT_TEST ( Creation );
    SERIALPORT_TEST ( Delete );
    SERIALPORT_TEST ( ObjectType );
};

#endif
