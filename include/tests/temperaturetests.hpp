
#ifndef TEMPERATURE_TESTS_HPP
#define TEMPERATURE_TESTS_HPP

#define TEMPERATURE_TESTS_CLASS_NAME        TemperatureTests
#define TEMPERATURE_TEST_NAME( name )       Temperature_ ## name ## _Test
#define TEMPERATURE_TEST( name )            void TEMPERATURE_TEST_NAME ( name ) ( void )
#define TEMPERATURE_TEST_C( name )          void TEMPERATURE_TESTS_CLASS_NAME::TEMPERATURE_TEST_NAME ( name ) ( void )
#define TEMPERATURE_TESTS_ADD( name, desc ) this -> AddUnitTest ( # name, &TEMPERATURE_TESTS_CLASS_NAME:: TEMPERATURE_TEST_NAME ( name ) ,desc )

namespace org {
  namespace unittest {
    class TEMPERATURE_TESTS_CLASS_NAME;
  }
}

#define TEMPERATURE_TESTS_EXISTS

class org::unittest::TEMPERATURE_TESTS_CLASS_NAME : public virtual org::unittest::UnitTestClass <org::unittest::TEMPERATURE_TESTS_CLASS_NAME> {
  public:
    static TEMPERATURE_TESTS_CLASS_NAME * TEMPERATURE_TESTS_CLASS_NAME_instance;
    
    TEMPERATURE_TESTS_CLASS_NAME ( void );

    TEMPERATURE_TEST ( MachineStateEqual );
    TEMPERATURE_TEST ( Creation );
    TEMPERATURE_TEST ( Delete );
    TEMPERATURE_TEST ( ObjectType );
    TEMPERATURE_TEST ( SettingBytes );
    TEMPERATURE_TEST ( DoubleConstructor );
};

#endif
