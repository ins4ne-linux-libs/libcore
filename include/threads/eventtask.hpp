
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : EventTask                             ║
// ║ Description      : Eventing task, if it has to pull,     ║
// ║                      pulls, transforms then push. Needs  ║
// ║                      logically four delegates.           ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef EVENT_TASK_HPP
#define EVENT_TASK_HPP

#include <threads/task.hpp>
#include <eventing/eventargs.hpp>

namespace org {
  namespace threads {
    template <typename T, typename U> class EventTask;
  }
}

template <typename T, typename U>
class org::threads::EventTask : public virtual org::threads::Task {
  protected:
    virtual bool HasToPull ( void ) = 0;
    virtual T PullNewData ( void ) = 0;
    virtual void Push ( U pushed ) = 0;
    virtual U Transform ( T toTransform ) = 0;

    void Auxiliary ( void ) override {
      while ( !( this -> IsAskedToTerminate ( ) ) )
        if ( this -> HasToPull ( ) )
          this -> Push ( this -> Transform ( this -> PullNewData ( ) ) );
    }

  public:
    const char * ObjectType ( void ) const override {
      return "org::threads::EventTask";
    }
};

#endif
 