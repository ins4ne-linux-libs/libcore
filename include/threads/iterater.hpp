
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Iterater                              ║
// ║ Description      : Pull / Push iterating task.           ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef ITERATER_HPP
#define ITERATER_HPP

#include <threads/task.hpp>
#include <threads/threadsafequeue.hpp>

namespace org {
  namespace threads {
    template <typename T, typename U> class Iterater;
  }
}

template <typename T, typename U>
class org::threads::Iterater : public virtual org::threads::Task {
  private:
    ThreadSafeQueue <U>* _inputQueue;
    ThreadSafeQueue <T>* _outputQueue;

    void Auxiliary ( void ) override {
      T result;
      while ( !( this -> IsAskedToTerminate ( ) ) ) {
        if ( this -> _automaticPush || !( this -> _inputQueue -> IsEmpty ( ) ) ) {
          if ( this -> _automaticPush )
            result = this -> PushPull ( 0 );
          else
            result = this -> PushPull ( this -> _inputQueue -> PopFront ( ) );
          if ( !( this -> IsAskedToTerminate ( ) ) ) {
            this -> _outputQueue -> Push ( result );
          }
        } else if ( !( this -> _waitForData ) )
          this -> AskToTerminate ( );
      }
    }
    
    void InitializeIterater ( void ) {
      this -> _inputQueue           = new ThreadSafeQueue <U> ( );
      this -> _outputQueue          = new ThreadSafeQueue <T> ( );
      this -> _waitForData          = false;
      this -> _automaticPush        = false;
      this -> _ignoreWhenReadIsNull = true;
    }
    
  protected:
    bool _waitForData;
    bool _automaticPush;
    bool _ignoreWhenReadIsNull;

    virtual T PushPull ( U parameter ) = 0;

  public:
    Iterater ( void ) {
      this -> InitializeIterater ( );
    }

    virtual ~Iterater ( void ) {
      if ( this -> _inputQueue != nullptr )
        delete this -> _inputQueue;
      if ( this -> _outputQueue != nullptr )
        delete this -> _outputQueue;
    }

    void PlugPushingEvent ( org::eventing::EventCall <T>* pushEvent ) {
      this -> _outputQueue -> PlugPushingEvent ( pushEvent );
    }

    void PushInput ( U element ) {
      if ( this -> _inputQueue != nullptr )
        this -> _inputQueue -> Push ( element );
    }

    bool OutputIsEmpty ( void ) {
      return this -> _outputQueue -> IsEmpty ( );
    }

    void AutomaticPush ( void ) {
      this -> _automaticPush = true;
    }

    void WaitForData ( void ) {
      this -> _waitForData = true;
    }

    void StopAutomaticPush ( void ) {
      this -> _automaticPush = false;
    }

    void DontWaitForData ( void ) {
      this -> _waitForData = false;
    }

    T PopOutput ( void ) {
      return this -> _outputQueue -> PopFront ( );
    }

    const char * ObjectType ( void ) const override {
      return "org::threads::Iterater";
    }
};

#endif
 