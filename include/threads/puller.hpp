
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Puller                                ║
// ║ Description      : A puller is used in puller tasks.     ║
// ║ History:                                                 ║
// ║  - 12/02/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef PULLER_HPP
#define PULLER_HPP

namespace org {
  namespace threads {
    template <typename T> class Puller;
  }
}



template <typename T>
class org::threads::Puller : public virtual org::Object {
  public:
    virtual T Pull ( void ) = 0;

    const char * ObjectType ( void ) const override {
      return "org::threads::Puller";
    }
};

#endif
 