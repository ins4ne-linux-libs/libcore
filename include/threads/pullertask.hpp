
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : PullerTask                            ║
// ║ Description      : Puller task, automaticly push         ║
// ║                      'nothing' into the input queue by   ║
// ║                      default.                            ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef PULLER_TASK_HPP
#define PULLER_TASK_HPP

#include <threads/iterater.hpp>
#include <threads/puller.hpp>

namespace org {
  namespace threads {
    template <typename T> class PullerTask;
  }
}

template <typename T>
class org::threads::PullerTask : public virtual org::threads::Iterater <T, unsigned char> {
  private:
    Puller <T>* _puller;

  protected:
    T PushPull ( unsigned char parameter ) override {
      return this -> _puller -> Pull ( );
    }

  public:
    PullerTask ( bool waitForData, Puller <T>* puller ) {
      this -> _waitForData = waitForData;
      this -> _automaticPush = true;
      this -> _puller = puller;
    }

    const char * ObjectType ( void ) const override {
      return "org::threads::PullerTask";
    }
};

#endif
