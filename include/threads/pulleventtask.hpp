
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : PullEventTask                         ║
// ║ Description      : Puller task + eventing task.          ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef PULL_EVENT_TASK_HPP
#define PULL_EVENT_TASK_HPP

namespace org {
  namespace threads {
    template <typename T, typename U> class PullEventTask;
  }
}

#include <threads/eventtask.hpp>
#include <threads/pullertask.hpp>
#include <eventing/eventdelegatecall.hpp>
#include <tools/misc/wait.hpp>
#include <lib/stdlib.hpp>

template <typename T, typename U>
class org::threads::PullEventTask : public virtual org::threads::EventTask <T, U> {
  protected:
    PullerTask <T>* _puller;
    org::eventing::EventDelegateCall <PullEventTask, T> * _pushingEvent;

    U PullNewData ( void ) override {
      return this -> _puller -> PopOutput ( );
    }

    bool HasToPull ( void ) override {
      bool hasToPull = this -> _puller != nullptr;
      if ( hasToPull )
        hasToPull = !( this -> _puller -> OutputIsEmpty ( ) );
      if ( !hasToPull )
        this -> AskToTerminate ( );
      return hasToPull;
    }

    void PullTaskPushEvent ( org::eventing::EventArgs <void*, char*, T*>* args ) {
      if ( !this -> IsExecuting ( ) )
        this -> Execute ( );
      if ( args != nullptr ) {
        if ( args -> GetArgs ( ) != nullptr )
          lib::Stdlib::Free ( ( void * ) args -> GetArgs ( ) );
        delete args;
      }

    }

    void SetPuller ( PullerTask <T>* newPuller ) {
      this -> _puller = newPuller;
      this -> _puller -> PlugPushingEvent ( this -> _pushingEvent );
    }

    void InitializePullEventTask ( void ) {
      this -> _puller = nullptr;
      this -> _pushingEvent = new org::eventing::EventDelegateCall <PullEventTask, T> ( this, &PullEventTask::PullTaskPushEvent );
    }

  public:
    PullEventTask ( void ) {
      this -> InitializePullEventTask ( );
    }

    virtual ~PullEventTask ( void ) {
      this -> Terminate ( );
      if ( this -> _puller != nullptr )
        delete this -> _puller;
      delete this -> _pushingEvent;
    }

    void Start ( ) override {
      if ( !this -> IsExecuting ( ) )
        if ( this -> _puller != nullptr )
          if ( ! this -> _puller -> IsExecuting ( ) ) {
            this -> _puller -> WaitForData ( );
            this -> _puller -> AutomaticPush ( );
            this -> _puller -> Start ( );
          }
    }

    void Terminate ( ) override {
      if ( this -> _puller != nullptr )
        if ( this -> _puller -> IsExecuting ( ) ) {
          this -> _puller -> DontWaitForData ( );
          this -> _puller -> StopAutomaticPush ( );
          this -> _puller -> AskToTerminate ( );
        }
      this -> AskToTerminate ( );
      if ( this -> _puller != nullptr )
        while ( this -> _puller -> IsExecuting ( ) )
          org::tools::misc::Wait::Milliseconds ( 1 );
      while ( this -> IsExecuting ( ) )
        org::tools::misc::Wait::Milliseconds ( 1 );
    }

    const char * ObjectType ( void ) const override {
      return "org::threads::PullEventTask";
    }
};

#endif
 