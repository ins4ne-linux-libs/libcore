
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Task & SimpleTask                     ║
// ║ Description      : Puller task + eventing task.          ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef TASK_HPP
#define TASK_HPP

namespace org {
  namespace threads {
    class Task;
  }
}

#include <threads/taskable.hpp>

class org::threads::Task : public virtual org::threads::Taskable {
  private:
    struct TaskCheshireCat;
    struct TaskCheshireCat * _dPtr;
    bool    _askedTerminate;
    bool    _inStartingBlock;
    bool    _executing;
    bool    _preparing;
    const char * _name;
    
    void StartThread ( void );
    static void StaticAuxiliary ( void * task );
    static void StaticAuxiliary ( Task* task );
    void StartAuxiliary ( void );
    bool Join ( void );
    void InitializeTask ( void );
    
  protected:
    virtual void Auxiliary ( void );

  public:
    Task ( void );

    virtual ~Task ( void );

    void SetThreadName ( const char * newName );
    const char * ThreadName ( void );
    void Execute ( void );
    void WaitForIt ( void );
    void AskToTerminate ( void );
    bool IsAskedToTerminate ( );
    bool IsExecuting ( );
    virtual void Start ( );
    virtual void Terminate ( );
    const char * ObjectType ( void ) const override;
};

#endif
