
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Taskable                              ║
// ║ Description      : Defines a taskable entity, using      ║
// ║                      mutex mainly.                       ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef TASKABLE_HPP
#define TASKABLE_HPP

namespace org {
  namespace threads {
    class Taskable;
  }
}

class org::threads::Taskable : public virtual org::Object {
  private:
    struct TaskableCheshireCat;
    struct TaskableCheshireCat * _dPtr;
    
    void InitializeTaskable ( void );
    
  protected:
    void MakeTaskable ( void );
    void ThreadLock ( void );
    void ThreadUnlock ( void );

  public:
    Taskable ( void );

    virtual ~Taskable ( void );
};

#endif
 