
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : ThreadSafeMap                         ║
// ║ Description      : Thread safe std::map wrapper.              ║
// ║ History:                                                 ║
// ║  - 26/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef THREAD_SAFE_MAP_HPP
#define THREAD_SAFE_MAP_HPP

#include <threads/taskable.hpp>

namespace org {
  namespace threads {
    template <typename T, typename U> class ThreadSafeMap;
  }
}

template <typename T, typename U>
class org::threads::ThreadSafeMap : public virtual org::threads::Taskable {
  private:
    std::map <T, U>* _map;

    void InitializeThreadSafeQueue ( void ) {
      this -> MakeTaskable ( );
      this -> _map = new std::map <T, U> ( );
    }

  protected:
    void InterfaceErase ( T key ) {
      this -> _map -> erase ( key );
    }

    bool InterfaceContains ( T key ) {
      return this -> _map -> count ( key ) > 0;
    }

    U InterfaceGet ( T key ) {
      return ( *( this -> _map ) ) [key];
    }
  
  public:
    ThreadSafeMap ( void ) {
      this -> InitializeThreadSafeQueue ( );
    }

    virtual ~ThreadSafeMap ( void ) {
      delete this -> _map;
    }

    bool Contains ( T key ) {
      this -> ThreadLock ( );
      bool ans = this -> InterfaceContains ( key );
      this -> ThreadUnlock ( );
      return ans;
    }

    void Clear ( void ) {
      this -> _map -> clear ( );
    }

    void Insert ( T key, U value ) {
      this -> ThreadLock ( );
      ( *( this -> _map ) ) [key] = value;
      this -> ThreadUnlock ( );
    }
    
    U Get ( T key ) {
      this -> ThreadLock ( );
      U ans = this -> InterfaceGet ( key );
      this -> ThreadUnlock ( );
      return ans;
    }

    void Erase ( T key ) {
      this -> ThreadLock ( );
      this -> InterfaceErase ( key );
      this -> ThreadUnlock ( );
    }

    bool IsEmpty ( void ) {
      this -> ThreadLock ( );
      bool ans = this -> _map -> empty ( );
      this -> ThreadUnlock ( );
      return ans;
    }

    typename std::map<T, U>::iterator Begin ( void ) {
      this -> ThreadLock ( );
      typename std::map <T, U>::iterator ans = this -> _map -> begin ( );
      this -> ThreadUnlock ( );
      return ans;
    }

    typename std::map<T, U>::iterator End ( void ) {
      this -> ThreadLock ( );
      typename std::map <T, U>::iterator ans = this -> _map -> end ( );
      this -> ThreadUnlock ( );
      return ans;
    }

    U at ( T key ) {
      this -> ThreadLock ( );
      U ans = this -> _map -> at ( key );
      this -> ThreadUnlock ( );
      return ans;
    }

    int count ( T key ) {
      this -> ThreadLock ( );
      int ans = this -> _map -> count ( key );
      this -> ThreadUnlock ( );
      return ans;
    }
};

#endif