
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : ThreadSafeQueue                       ║
// ║ Description      : Thread safe queue wrapper.            ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef THREAD_SAFE_QUEUE_HPP
#define THREAD_SAFE_QUEUE_HPP

namespace org {
  namespace threads {
    template <typename T> class ThreadSafeQueue;
  }
}

#include <threads/taskable.hpp>
#include <process/memorymanager.hpp>
#include <eventing/eventcall.hpp>



template <typename T>
class org::threads::ThreadSafeQueue : public virtual org::threads::Taskable {
  private:
    std::queue <T>* _queue;
    org::eventing::EventCall <T>* _pushingSignal;
    
    void AssignQueue ( std::queue <T>* newQueue ) {
      this -> _queue = newQueue;
    }

    std::queue <T>* GetQueue ( void ) {
      return this -> _queue;
    }

    void InitializeQueue ( void ) {
      this -> AssignQueue ( nullptr );
    }

    void DeleteQueue ( void ) {
      if ( this -> GetQueue ( ) != nullptr )
        delete this -> GetQueue ( );
      this -> InitializeQueue ( );
    }

    void SetQueue ( std::queue <T>* newQueue ) {
      this -> DeleteQueue ( );
      this -> AssignQueue ( newQueue );
    }

    T InterfaceFront ( void ) {
      return this -> _queue -> front ( );
    }

    void InterfacePop ( void ) {
      this -> _queue -> pop ( );
    }

    void InterfacePush ( T element ) {
      this -> _queue -> push ( element );
    }

    bool InterfaceEmpty ( void ) {
      return this -> _queue -> empty ( );
    }

    int InterfaceSize ( void ) {
      return this -> _queue -> size ( );
    }

    void InitializeThreadSafeQueue ( void ) {
      this -> InitializeQueue ( );
      this -> SetQueue ( new std::queue <T> ( ) );
      this -> _pushingSignal = nullptr;
    }
  
  public:
    ThreadSafeQueue ( void ) {
      this -> InitializeThreadSafeQueue ( );
      this -> MakeTaskable ( );
    }

    virtual ~ThreadSafeQueue ( void ) {
      this -> DeleteQueue ( );
    }

    int Size ( void ) {
      this -> ThreadLock ( );
      int size = this -> InterfaceSize ( );
      this -> ThreadUnlock ( );
      return size;
    }

    T Front ( void ) {
      this -> ThreadLock ( );
      T front = this -> InterfaceFront ( );
      this -> ThreadUnlock ( );
      return front;
    }

    T PopFront ( void ) {
      this -> ThreadLock ( );
      T front = this -> InterfaceFront ( );
      this -> InterfacePop ( );
      this -> ThreadUnlock ( );
      return front;
    }

    void PlugPushingEvent ( org::eventing::EventCall <T>* pushEvent ) {
      if ( this -> _pushingSignal != nullptr )
        delete this -> _pushingSignal;
      this -> _pushingSignal = pushEvent;
    }

    void Push ( T element ) {
      this -> ThreadLock ( );
      this -> InterfacePush ( element );
      this -> ThreadUnlock ( );
      if ( this -> _pushingSignal != nullptr )
        this -> _pushingSignal -> Call ( org::process::MemoryManager::AllocateCopy <T> ( element ) );
    }

    bool IsEmpty ( void ) {
      this -> ThreadLock ( );
      bool isEmpty = this -> InterfaceEmpty ( );
      this -> ThreadUnlock ( );
      return isEmpty;
    }
};

#endif