
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Timer                                 ║
// ║ Description      : Timer events.                         ║
// ║ History:                                                 ║
// ║  - 25/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef TIMER_HPP
#define TIMER_HPP


#include <threads/tunnelpulleventtask.hpp>
#include <threads/threadsafemap.hpp>

namespace org {
  namespace threads {
    class TimerEvent;
    class TimerEventMap;
    class TimerTask;
  }
}

class org::threads::TimerEvent : public virtual org::Object {
  private:
    char* _name;
    org::eventing::EventCall <org::threads::TimerEvent >* _event;
    unsigned int _remaining;
    int _periodicValue;

    void InitializeTimerEvent ( void );

  public:
    TimerEvent ( const char * name, org::eventing::EventCall <org::threads::TimerEvent>* event, int remaining, bool periodic );

    virtual ~TimerEvent ( void );

    bool IsPeriodic ( void );
    void DecrementRemaining ( void );
    bool IsFinished ( void );
    void SetRemaining ( int remaining );
    void Reset ( void );
    org::eventing::EventCall <org::threads::TimerEvent>* GetEvent ( void );
    char* GetName ( void );
};

class org::threads::TimerEventMap : public virtual ThreadSafeMap <org::types::CString, TimerEvent*> {
  public:
    TimerEventMap ( void );

    virtual ~TimerEventMap ( void );

    std::list <org::threads::TimerEvent*>* DecrementAll ( void );
    void Remove ( org::types::CString name );
    void RemoveAll ( void );
};

class org::threads::TimerTask :
  public virtual TunnelPullEventTask <std::list <org::threads::TimerEvent*>> ,
  public virtual Puller <std::list <org::threads::TimerEvent*>*>             {
  private:
    int _precision;
    TimerEventMap* _events;

    std::list <org::threads::TimerEvent*>* Timering ( void );
    void TimeEvent ( org::eventing::EventArgs <void*, char*, std::list <org::threads::TimerEvent*>*>* args );
    void Notify ( org::types::CString name, TimerEvent* timerEvent );
    void InitializeTimerTask ( void );

  public:
    TimerTask ( int precision );

    ~TimerTask ( void );
    
    std::list <org::threads::TimerEvent*>* Pull ( void ) override;
    void Notify ( org::types::CString name, org::eventing::EventCall <org::threads::TimerEvent>* event, int timeout, bool periodic );
    void NotifyIn ( org::types::CString name, org::eventing::EventCall <org::threads::TimerEvent>* event, int timeout );
    void NotifyEvery ( org::types::CString name, org::eventing::EventCall <org::threads::TimerEvent>* event, int timeout );
    bool Update ( org::types::CString name, int timeout );
    void Remove ( org::types::CString name );
    void DenotifyAll ( void );
    void Terminate ( ) override;
    const char * ObjectType ( void ) const override;
};

#endif
