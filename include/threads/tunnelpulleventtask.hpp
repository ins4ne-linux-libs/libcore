
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : TunnelPullEventTask                   ║
// ║ Description      : Pull event task using the same type   ║
// ║                      as pull and push.                   ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef TUNNEL_PULL_EVENT_TASK_HPP
#define TUNNEL_PULL_EVENT_TASK_HPP

namespace org {
  namespace threads {
    template <typename T> class TunnelPullEventTask;
  }
}

#include <threads/pulleventtask.hpp>
#include <tools/misc/stringextension.hpp>

template <typename T>
class org::threads::TunnelPullEventTask : public virtual org::threads::PullEventTask <T*, T*> {
  private:
    void InitializeTunnelPullEventTask ( void ) {
      this -> _event = nullptr;
    }

  protected:
    T* Transform ( T* toTransform ) override {
      return toTransform;
    }

    void Push ( T* pushed ) override {
      this -> _event -> Call ( pushed );
    }

    org::eventing::EventCall <T>* _event;

  public:
    TunnelPullEventTask ( void ) {
      this -> InitializeTunnelPullEventTask ( );
    }
  
    TunnelPullEventTask ( bool waitForData, Puller <T*>* puller, org::eventing::EventCall <T>* event, const char * threadName ) {
      this -> InitializeTunnelPullEventTask ( );
      this -> SetThreadName ( threadName );
      this -> SetPuller ( new PullerTask <T*> ( waitForData, puller ) );
      char * newName = org::tools::misc::CString::Concatenate ( this -> ThreadName ( ), "-puller" );
      this -> _puller -> SetThreadName ( newName );
      lib::Stdlib::Free ( ( void * ) newName );
      this -> _event = event;
    }

    const char * ObjectType ( void ) const override {
      return "org::threads::TunnelPullEventTask";
    }
};

#endif
 