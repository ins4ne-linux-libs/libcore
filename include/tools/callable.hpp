
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Callable                         ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 26/02/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef CALLABLE_HPP
#define CALLABLE_HPP

namespace org {
  namespace tools {
    template <typename F> class Callable;
  }
}



template <typename F>
class org::tools::Callable : public virtual org::Object {
  #ifdef CALLABLE_TESTS_EXISTS
    friend class org::unittest::CallableTests;
  #endif
  public:
    Callable ( void ) { }

    virtual ~Callable ( void ) { }

    virtual void Call ( F * arg ) = 0;

    const char * ObjectType ( void ) const override {
      return "org::tools::Callable";
    }
};

#endif

