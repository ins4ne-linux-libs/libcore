
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : DelegateCall                          ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 26/02/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef DELEGATECALL_HPP
#define DELEGATECALL_HPP

namespace org {
  namespace tools {
    template <typename T> class DelegateCall;
  }
}



template <typename T>
class org::tools::DelegateCall : public virtual org::Object {
  #ifdef DELEGATECALL_TESTS_EXISTS
    friend class org::unittest::DelegateCallTests;
  #endif
  protected:
    T _call;
    
  public:
    const char * ObjectType ( void ) const override {
      return "org::tools::DelegateCall";
    }
};

#endif

