
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : MemberDelegateCall                    ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 26/02/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef MEMBERDELEGATECALL_HPP
#define MEMBERDELEGATECALL_HPP

namespace org {
  namespace tools {
    template <typename T, typename F> class MemberDelegateCall;
  }
}

#include <tools/delegatecall.hpp>

template <typename T, typename F>
class org::tools::MemberDelegateCall : public virtual org::tools::DelegateCall <T> {
  #ifdef MEMBERDELEGATECALL_TESTS_EXISTS
    friend class org::unittest::MemberDelegateCallTests;
  #endif
  private:
    void InitializeMemberDelegateCall ( void ) {
      this -> _calledInstance = nullptr;
    }
  
  protected:
    F * _calledInstance;

  public:
    F * Instance ( void ) {
      return this -> _calledInstance;
    }

    const char * ObjectType ( void ) const override {
      return "org::tools::MemberDelegateCall";
    }
};

#endif

