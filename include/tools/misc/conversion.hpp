
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Conversion                            ║
// ║ Description      : Is used to convert types between      ║
// ║                      them.                               ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef MISC_CONVERSION_HPP
#define MISC_CONVERSION_HPP

#include <tools/misc/stringextension.hpp>
#include <process/memorymanager.hpp>
#include <lib/stdlib.hpp>

namespace org {
  namespace tools {
    namespace misc {
      class Convert;
    }
  }
}

class org::tools::misc::Convert {
  public:
    static bool FromTypeToType ( const char * toConvert, const char ** result );
    static bool FromTypeToType ( const char * toConvert, char ** result );
    static bool FromTypeToType ( const char * toConvert, int * result );
    static bool FromTypeToType ( const char * toConvert, double * result );
    static bool FromTypeToType ( const char * toConvert, char * result );
    static bool FromTypeToType ( const char * toConvert, unsigned char * result );
    static bool FromTypeToType ( const char * toConvert, short * result );
    static bool FromTypeToType ( const char * toConvert, bool * result );
    static bool FromTypeToType ( char toConvert, int * result );
    static short ToShort ( unsigned char * array );
    static bool ToBoolean ( char * toConvert, bool * result );
    static bool ToInt ( char * integerString, int * result );
    static bool ToByte ( char * integerString, unsigned char * result );
    static bool ToShort ( char * integerString, short * result );
    static char * ToCString ( short shortInteger );
    static const char * ToCString ( int value );
    static const char * ToCString ( int * value );
    static const char * ToCString ( void * value );
    static const char * ToCString ( char value );
    static const char * ToCString ( const char * value );
    static const char * ToCString ( char * value );
    static const char * ToCString ( double floating );
    static const char * ToCString ( const char * format, double floating );
    
    template <typename T>
    static void PutIn ( unsigned char * array, T value, int order ) {
      int size = sizeof ( value );
      unsigned char * memory = ( unsigned char * ) ( &value );
      if ( order == -1 )
        for ( int i = 0, j = size - 1; i < size; i ++, j -- )
          array [i] = memory [j];
      else
        for ( int i = 0; i < size; i ++ )
          array [i] = memory [i];
    }

    template <typename T>
    static T PutOut ( unsigned char * array, int order, T defaultValue ) {
      int size = sizeof ( T );
      T value = defaultValue;
      if ( array != nullptr ) {
        unsigned char * memory = ( unsigned char * ) ( &value );
        if ( order == -1 )
          for ( int i = 0, j = size - 1; i < size; i ++, j -- )
            memory [i] = array [j];
        else
          for ( int i = 0; i < size; i ++ )
            memory [i] = array [i];
      }
      return value;
    }

    template <typename T, typename U>
    static bool FromTo ( T toConvert, U * result ) {
      return org::tools::misc::Convert::FromTypeToType ( toConvert, result );
    }
    
    template <typename T>
    static T* ToArray ( const char * spaceSeparatedString, int base, int* count ) {
      T* numbersValues = nullptr;
      if ( spaceSeparatedString != nullptr ) {
        char** numbers = org::tools::misc::CString::SpaceSplit ( spaceSeparatedString, count );
        numbersValues = ( T* ) org::process::MemoryManager::Malloc ( *count, sizeof ( T ) );
        
        for ( int j = 0; j < *count; j ++ ) {
          numbersValues [j] = ( T ) lib::Stdlib::StrToul ( numbers [j], nullptr, base );
          org::process::MemoryManager::FreeM ( numbers [j] );
        }
        org::process::MemoryManager::FreeM ( numbers );
      }
      return numbersValues;
    }
    
    template <typename T>
    static const char * ToCString ( T value, int bufferSize, const char * format ) {
      char buffer [bufferSize];
      lib::Stdio::SnPrintF ( buffer, bufferSize, format, value );
      return org::tools::misc::CString::GetCopyOfCString ( buffer );
    }
};

#endif
