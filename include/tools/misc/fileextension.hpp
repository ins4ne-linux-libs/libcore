
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Conversion                            ║
// ║ Description      : Eases file access.                    ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef MISC_FILE_EXTENSION_HPP
#define MISC_FILE_EXTENSION_HPP

namespace org {
  namespace tools {
    namespace misc {
      class FileExtension;
    }
  }
}

class org::tools::misc::FileExtension : public virtual org::Object {
  public:
    static int CountLines ( const char * filePath );
    static const char ** ReadAllLines ( const char * filePath, int* count );
    static char * ReadFile ( const char * filePath );
    static unsigned char * ReadBinaryFile ( const char * filePath, int * size );
    static const char ** ReadAllLinesAndClean ( const char * filePath, int* count );
};

#endif
