
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Conversion                            ║
// ║ Description      : C-Strings operations.                 ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef STRING_EXTENSION_HPP
#define STRING_EXTENSION_HPP

namespace org {
  namespace tools {
    namespace misc {
      class CString;
    }
  }
}

#include <lib/stdio.hpp>

class org::tools::misc::CString {
  public:
    static char * GetCTime ( char* timeToFill, const char * format );
    static char* GetCharArray ( int size );
    static char** GetStringArray ( int size );
    static void FreeCharArray ( char* array );
    static void FreeStringArray ( char** array, int size );
    static char* GetStringFrom ( const char* array, int size );
    static void CopyStringToMax ( const char * source, char * destination, int maxSize );
    static void CopyStringTo ( const char * source, char * destination );
    static void CopyStringTo ( const char * source, char * destination, int size );
    static char* GetCopyOfCString ( const char* cString );
    static char** GetCopyOfStringArray ( char** cStrings, int stringCount );
    static int GetCCharArrayLength ( const char* cString );
    static char* Concatenate ( const char* cString1, const char* cString2 );
    static char* Concatenate ( const char* cString1, const char* cString2, char separator );
    static char* Concatenate ( const char** cStrings, int number );
    static char * Concatenate ( const char** cStrings, int number, const char * separator );
    static void PutIn ( const char * from, char* to, int fromHere, int toHere, int forAsMuch );
    static int IsPartiallyEqual ( const char * string1, const char * string2 );
    static bool IsEqual ( const char * string1, const char * string2 );
    static bool IsEmpty ( const char * string1 );
    static char* Filter ( char* toFilter, char toHide );
    static char** Split ( const char * toSplit, char splitting, int * count, int maxParts );
    static char** Split ( const char * toSplit, char splitting, int* count );
    static char** Split ( const char * toSplit, char splitting, int* count, int maxParts, bool trim, bool removeEmptyEntries );
    static char** SpaceSplit ( const char * toSplit, int* count );
    static char** SplitAndTrim ( const char * toSplit, char splitting, int* count, int maxParts );
    static char** SplitAndRemoveEmptyEntries ( const char * toSplit, char splitting, int* count, int maxParts );
    static char** RemoveEmptyEntries ( char** stringArray, int size, int* newSize );
    static int CountInRange ( const char * cString, char lowerBound, char upperBound );
    static int CountOccurences ( const char * cString, char value );
    static char* OldTrim ( char* toTrim );
    static char* Trim ( const char * toTrim );
    static char* TrimStart ( const char * toTrim );
    static char* TrimEnd ( const char * toTrim );
    static char* TrimRangeZone ( const char * toTrim, char lowerBound, char upperBound );
    static char* TrimRangeZone ( const char * toTrim, char lowerBound, char upperBound, int start, int end );
    static int CountInRange ( const char * cString, char lowerBound, char upperBound, int start, int end );
    static int FindFirstIndex ( const char * cString, char value );
    static int FindLastIndex ( char* cString, char value );
    static int FindLastIndex ( char* cString, char value, int beforeIndex );
    static int FindInRange ( const char * cString, char lowerBound, char upperBound, int start, int beforeIndex, int maximumTimes );
    static int Find ( const char * cString, char value, int start, int beforeIndex, int maximumTimes );
    static int Find ( const char * cString, const char * value, int start, int beforeIndex, int maximumTimes );
    static bool IsMasked ( const char * cString, const char * mask );
    static char* Replace ( const char * cString, int startIndex, int endIndex, const char * replacement );
    static char* Replace ( const char * cString, const char * toReplace, const char * replacement );
    static char* CutAtValue ( char* toCut, char value );
    static int ToInt ( const char * integerString );
    static int FindAllArgsInFormat (const char* format, char*** argsFormatArray, int maxNumber);
    static bool IsInRange ( char character, int lowerBoung, int upperBound );
    static bool IsCharacter ( char character );
    static bool IsUpperCharacter ( char character );
    static bool IsLowerCharacter ( char character );
    static bool IsNumberCharacter ( char character );
    static bool IsPonctuationCharacter ( char character );
    static bool IsPrintableCharacter ( char character );
    static bool CharacterIsInArray (char character, const char* array, int size);
    static char* FindFormat (const char* fromHere);
    static char** CreateArgStrings (char** formats, void** args, int argCount);
    static char* CreateArgString (char* format, void* arg);
    static char GetArgTypeFromFormat (char* format);
    static char* FormatString (const char * format, int argc, void** args);
    static char * PutBetween ( const char * cString, char surroundings );
    static char * PutBetween ( const char * cString, char prefix, char suffix );
    static char * PutBetween ( const char * cString, const char * prefix, const char * suffix );
    static char ToLower ( char character );
    static char ToUpper ( char character );
    static char * ToLower ( const char * toLower );
    static char * ToUpper ( const char * toUpper );
    static bool Contains ( const char * toLookIn, char toLookFor );
    static char * Remove ( const char * cString, int from, int to );
    static int FindClosing ( const char * str, int start, int end, char closing, const char * openings, const char * closings, const char * inhibiting, const char * closingInhibiting, const char * escapeCharacters );
    static int FindOccurence ( const char * str, int start, int end, char character, int occurence, const char * inhibiting, const char * escapeCharacters );

    static const char * Substring ( const char * str, int start, int end );

    template <typename T>
    static char* ArrayToString ( T * array, int size, const char * format, int maxSizePerElement, char interString ) {
      char** strings = GetStringArray ( size );
      int interLength = interString != 0 ? 1 : 0;
      int interCharIndex = 0;
      for ( int i = 0; i < size; i ++ ) {
        strings [i] = GetCharArray ( maxSizePerElement + 2 + interLength );
        interCharIndex = lib::Stdio::SPrintF ( strings [i], format, array [i] );
        if ( interLength > 0 && i < size - 1 )
          strings [i] [interCharIndex] = interString;
      }
      char* fullString = Concatenate ( ( const char ** ) strings, size );
      FreeStringArray ( strings, size );
      return fullString;
    }
};

#endif
