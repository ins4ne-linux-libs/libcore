
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : Conversion                            ║
// ║ Description      : Sleeping thread functions.            ║
// ║ History:                                                 ║
// ║  - 02/01/2018 - DourvCle: Creation.                      ║
// ║  - 00/00/0000 - XxxxxXxx: Modification.                  ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef MISC_WAIT_HPP
#define MISC_WAIT_HPP



namespace org {
  namespace tools {
    namespace misc {
      class Wait;
    }
  }
}

class org::tools::misc::Wait : public virtual org::Object {
  public:
    static void Seconds ( int seconds );
    static void Milliseconds ( int milliseconds );
    static void Microseconds ( int microseconds );
};

#endif
