
#ifndef ORG_TYPES_CSTRING_HPP
#define ORG_TYPES_CSTRING_HPP

namespace org {
  namespace types {
    class CString;
  }
}

class org::types::CString : org::Object {
  private:
    char * _string;
    int _length;
    bool _hasSavedString;
    bool _automaticAllocation;

    const char * ConversionConstChar ( void ) const;
    CString& OperatorAssignCString ( const CString& cString );
    CString& OperatorAssignConstChar ( const char * cString );
    CString& OperatorAddConstChar ( const char * cString );
    CString& OperatorAddChar ( char character );
    CString& OperatorAddCString ( CString& cString );
    bool OperatorLess ( const CString &b ) const;
    bool OperatorEqual ( const CString &b ) const;
    bool OperatorNotEqual ( const CString &b ) const;
    bool OperatorEqual ( const char * b ) const;
    bool OperatorNotEqual ( const char * b ) const;
    void InitializeCString ( void );
  
  protected:
    void SaveString ( const char * cString );
    void SaveString ( const char * cString, bool useString );

  public:
    CString ( void );
    CString ( const char * cString );
    CString ( const char * cString, bool useString );
    CString ( const CString &obj );

    virtual ~CString ( void );

    int length ( void );
    char back ( void );
    char at ( int index );
    void append ( const char * toAppend );
    const char * c_str ( void ) const;
    void InsertAt ( const char * toInsert, int index );
    void InsertBefore ( const char * toInsert );
    void Add ( const char * atTheEnd );
    void Cut ( int from, int to );
    void Cut ( int from );
    void Trim ( void );
    int FindClosing ( int start, int end, char closing, const char * openings, const char * closings, const char * inhibiting, const char * closingInhibiting, const char * escapeCharacters );
    int FindClosing ( int start, char closing, const char * openings, const char * closings, const char * inhibiting, const char * closingInhibiting, const char * escapeCharacters );
    int FindClosing ( char closing, const char * openings, const char * closings, const char * inhibiting, const char * closingInhibiting, const char * escapeCharacters );
    int FindOccurence ( int start, int end, char character, int occurence, const char * inhibiting, const char * escapeCharacters );
    int FindOccurence ( char character, int occurence, const char * inhibiting, const char * escapeCharacters );
    int Find ( const char * value, int start, int beforeIndex, int maximumTimes );
    void Replace ( const char * toReplace, const char * replacement );
    CString * SubString ( int start, int end );
    CString * SubString ( int start );
    CString ** Split ( char splitting, int * count, int maxParts, bool trim, bool removeEmptyEntries );
    CString * Copy ( void );
    const char * ObjectType ( void ) const override;
    
    operator const char * ( void ) const {
      return this -> ConversionConstChar ( );
    }

    CString& operator = ( const char * cString ) {
      return this -> OperatorAssignConstChar ( cString );
    }

    CString& operator = ( const CString &cString ) {
      return this -> OperatorAssignCString ( cString );
    }

    CString& operator + ( const char * cString ) {
      return this -> OperatorAddConstChar ( cString );
    }

    CString& operator += ( const char * cString ) {
      return this -> OperatorAddConstChar ( cString );
    }

    CString& operator + ( char character ) {
      return this -> OperatorAddChar ( character );
    }

    CString& operator + ( CString& cString ) {
      return this -> OperatorAddCString ( cString );
    }

    bool operator < ( const CString &b ) const {
      return this -> OperatorLess ( b );
    }

    bool operator == ( const CString &b ) const {
      return this -> OperatorEqual ( b );
    }

    bool operator != ( const CString &b ) const {
      return this -> OperatorNotEqual ( b );
    }

    bool operator == ( const char * b ) const {
      return this -> OperatorEqual ( b );
    }

    bool operator != ( const char * b ) const {
      return this -> OperatorNotEqual ( b );
    }
};

#endif
