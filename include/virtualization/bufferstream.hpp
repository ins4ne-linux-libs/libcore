
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : BufferStream                         ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 30/05/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef VIRTUALIZATION_VIRTUAL_FACTORY_HPP
#define VIRTUALIZATION_VIRTUAL_FACTORY_HPP

namespace org {
  namespace unittest {
    class BufferStreamTests;
  }
  namespace virtualization {
    template <typename T> class BufferStream;
  }
}

#include <virtualization/virtualstream.hpp>
#include <entity/configuredentity.hpp>

template <typename T>
class org::virtualization::BufferStream : public virtual org::virtualization::VirtualStream <T> {
  friend class org::unittest::BufferStreamTests;
  private:
    T * _buffer;
    int _size;
    bool _looping;
    int _cursor;
    const char * _type;
    bool _isPause;

    void InitializeBufferStream ( void ) {
      this -> _buffer = nullptr;
      this -> _size = 0;
      this -> _looping = false;
      this -> _cursor = 0;
      this -> _type = nullptr;
      this -> _isPause = false;
    }
  
  protected:
    void FinishAutoConfiguration ( void ) override {
      org::virtualization::VirtualStream <T>::FinishAutoConfiguration ( );
      
      this -> _looping  = this -> template ContainsSettingAs <bool> ( "looping", this -> _looping );
      this -> _type     = this -> template ContainsSettingAs <const char *>  ( "type", this -> _type );
      char * value      = this -> template ContainsSettingAs <char *>  ( "value", nullptr );

      if ( org::tools::misc::CString::IsEqual ( this -> _type, "hex" ) )
        this -> _buffer = org::tools::misc::Convert::ToArray <T> ( value, this -> template ContainsSettingAs <int>  ( "base", 10 ), &( this -> _size ) );
      else if ( org::tools::misc::CString::IsEqual ( this -> _type, "pause" ) )
        this -> _isPause = true;
      else if ( org::tools::misc::CString::IsEqual ( this -> _type, "string" ) ) {
        this -> _buffer = ( T * ) value;
        this -> _size = org::tools::misc::CString::GetCCharArrayLength ( value );
      }
    }

  public:
    BufferStream ( void ) {
      this -> InitializeBufferStream ( );
    }
    
    BufferStream ( const char * configurationName ) {
      this -> InitializeBufferStream ( );
      this -> SetConfigurationMask ( configurationName );
    }
    
    virtual ~BufferStream ( void ) {

    }

    int GetPack ( T * toFill, int count ) override {
      int read = 0;
      for ( int i = 0; i < count; i ++ )
        if ( this -> GetOne ( &( toFill [read] ) ) )
          read ++;
        else
          break;
      return read;
    }

    bool GetOne ( T * toSet ) override {
      bool read = false;
      if ( this -> _buffer != nullptr ) {
        if ( this -> _cursor >= this -> _size && this -> _looping )
          this -> _cursor = 0;
        if ( this -> _cursor < this -> _size ) {
          *toSet = this -> _buffer [this -> _cursor];
          this -> _cursor ++;
          read = true;
        }
      }
      return read;
    }

    int PutPack ( T * toPut, int count ) override {
      return count;
    }

    bool PutOne ( T toPut ) override {
      return true;
    }

    int Count ( void ) override {
      return this -> _size - this -> _cursor;
    }

    const char * ObjectType ( void ) const override {
      return "org::virtualization::BufferStream";
    }
};

#endif
