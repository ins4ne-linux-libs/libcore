
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : SequenceStream                         ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 30/05/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef VIRTUALIZATION_SEQUENCESTREAM_HPP
#define VIRTUALIZATION_SEQUENCESTREAM_HPP

namespace org {
  namespace unittest {
    class SequenceStreamTests;
  }
  namespace virtualization {
    template <typename T> class SequenceStream;
  }
}

#include <virtualization/virtualstream.hpp>
#include <virtualization/bufferstream.hpp>

template <typename T>
class org::virtualization::SequenceStream : public virtual org::virtualization::VirtualStream <T> {
  friend class org::unittest::SequenceStreamTests;
  private:
    const char * _type;
    std::list <BufferStream <T> *> * _sequences;

    void InitializeSequenceStream ( void ) {
      this -> _sequences = new std::list <BufferStream <T> *> ( );
      this -> _type = nullptr;
    }

  protected:
    void FinishAutoConfiguration ( void ) override {
      org::virtualization::VirtualStream <T>::FinishAutoConfiguration ( );

      this -> template PutSettingAs <const char *> ( "sequence-type", &( this -> _type ) );

      typename std::list <BufferStream <T> *>::iterator seqsIt = this -> _sequences -> begin ( );
      for ( ; seqsIt != this -> _sequences -> end ( ); ++ seqsIt )
        delete *seqsIt;
      delete this -> _sequences;
      this -> _sequences = this -> template NewFromSettingList <BufferStream <T>> ( "sequences" );
    }

  public:
    SequenceStream ( void ) {
      this -> InitializeSequenceStream ( );
    }
    
    SequenceStream ( const char * configurationName ) {
      this -> InitializeSequenceStream ( );
      this -> SetConfigurationMask ( configurationName );
    }
    
    virtual ~SequenceStream ( void ) {
      typename std::list <BufferStream <T> *>::iterator seqsIt = this -> _sequences -> begin ( );
      for ( ; seqsIt != this -> _sequences -> end ( ); ++ seqsIt )
        delete *seqsIt;
      delete this -> _sequences;
    }

    int GetPack ( T * toFill, int count ) override {
      int read = 0;
      if ( this -> _sequences -> size ( ) > 0 ) {
        read = this -> _sequences -> front ( ) -> GetPack ( toFill, count );
        if ( read == 0 || this -> _sequences -> front ( ) -> Count ( ) == 0 ) {
          delete this -> _sequences -> front ( );
          this -> _sequences -> pop_front ( );
        }
      }
      return read;
    }

    bool GetOne ( T * toSet ) override {
      bool ans = this -> _sequences -> size ( ) > 0 ? this -> _sequences -> front ( ) -> GetOne ( toSet ) : false;
      if ( ! ans && this -> _sequences -> size ( ) > 0 ) {
        delete this -> _sequences -> front ( );
        this -> _sequences -> pop_front ( );
      }
      return ans;
    }

    int PutPack ( T * toPut, int count ) override {
      return count;
    }

    bool PutOne ( T toPut ) override {
      return true;
    }

    int Count ( void ) override {
      return this -> _sequences -> size ( ) > 0 ? this -> _sequences -> front ( ) -> Count ( ) : 0;
    }

    const char * ObjectType ( void ) const override {
      return "org::virtualization::SequenceStream";
    }
};

#endif
