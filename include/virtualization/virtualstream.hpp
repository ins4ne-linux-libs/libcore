
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : VirtualStream                         ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 30/05/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef VIRTUALIZATION_VIRTUALSTREAM_HPP
#define VIRTUALIZATION_VIRTUALSTREAM_HPP

namespace org {
  namespace virtualization {
    template <typename T> class VirtualStream;
  }
}

#include <entity/configuredentity.hpp>

template <typename T>
class org::virtualization::VirtualStream : public virtual org::entity::ConfiguredEntity {
  #ifdef VIRTUALIZATION_VIRTUALSTREAM_TESTS_EXISTS
    friend class org::unittest::VirtualStreamTests;
  #endif
  public:
    virtual int GetPack ( T * toFill, int count ) = 0;
    virtual bool GetOne ( T * toSet ) = 0;
    virtual int PutPack ( T * toPut, int count ) = 0;
    virtual bool PutOne ( T toPut ) = 0;
    virtual int Count ( void ) = 0;

    const char * ObjectType ( void ) const override {
      return "org::virtualization::VirtualStream";
    }
};

#endif
