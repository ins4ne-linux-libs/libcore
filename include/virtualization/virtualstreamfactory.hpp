
// ╔══════════════════════════════════════════════════════════╗
// ║ Class definition : VirtualStreamFactory                         ║
// ║ Description      : .                                     ║
// ║ History:                                                 ║
// ║  - 30/05/2018 - DourvCle: Creation.                      ║
// ╚══════════════════════════════════════════════════════════╝

#ifndef VIRTUALIZATION_VIRTUALSTREAMFACTORY_HPP
#define VIRTUALIZATION_VIRTUALSTREAMFACTORY_HPP

namespace org {
  namespace virtualization {
    class VirtualStreamFactory;
  }
}


#include <virtualization/bufferstream.hpp>
#include <virtualization/sequencestream.hpp>
#include <tools/misc/stringextension.hpp>

class org::virtualization::VirtualStreamFactory : public virtual org::Object {
  #ifdef VIRTUALIZATION_VIRTUALSTREAMFACTORY_TESTS_EXISTS
    friend class org::unittest::VirtualStreamFactoryTests;
  #endif
  public:
    template <typename T>
    static org::virtualization::VirtualStream <T> * New ( const char * type, const char * configurationName ) {
      org::virtualization::VirtualStream <T> * ans = nullptr;
      if ( org::tools::misc::CString::IsEqual ( type, "sequence" ) )
        ans = new org::virtualization::SequenceStream <T> ( configurationName );
      else if ( org::tools::misc::CString::IsEqual ( type, "buffer" ) )
        ans = new org::virtualization::BufferStream <T> ( configurationName );
      return ans;
    }
};

#endif
