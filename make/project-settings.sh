#!/bin/bash
# -*- coding: utf-8 -*-

export LIB_DEPENDENCIES="abstract"

export CXXFLAGS="$CXXFLAGS -fPIC -I./$LIBRARY_PROJECT_PATH/include"
export LDFLAGS="$LDFLAGS -pthread -shared"
export LIBRARY_PATH="$LIBRARY_PROJECT_PATH/source"
export FILES=$(find . -wholename "./$LIBRARY_PROJECT_PATH/*.cpp" -and -not -path '*/tests/*' | awk '{ print substr($0,3) }' | paste -sd " ")
export OBJECT_FILES="$OBJECT_FILES $FILES"
export OUTPUT_NAME="$UNIT_NAME.so"
