


#include <application/configuration.hpp>

#include <constants/libcore.hpp>
#include <tools/misc/fileextension.hpp>
#include <tools/misc/stringextension.hpp>
#include <lib/stdlib.hpp>

namespace org {
  namespace application {
    Configuration * Configuration::Main;

    void Configuration::InitializeConfiguration ( ) {
      this -> _configurationFileFound = false;
      this -> _configurationNodesMap  = new std::map <org::types::CString, std::list <ConfigurationParameter *> *> ( );
      this -> _indentationNameMap     = new std::map <int, const char *> ( );
    }

    const char * Configuration::FindUpperLevelLastName ( int indentation ) {
      const char * lastName = nullptr;
      bool noAnswerFound = true;
      int trying = indentation - 1;
      if ( indentation >= 0 ) {
        do {
          if ( trying <= 0 )
            trying = 0;
          if ( this -> _indentationNameMap -> count ( trying ) > 0 ) {
            lastName = this -> _indentationNameMap -> at ( trying );
            noAnswerFound = false;
          } else if ( trying == 0 )
            noAnswerFound = false;
          else
            trying --;
        } while ( noAnswerFound );
      }
      return lastName;
    }

    void Configuration::UpdateLastSeenName ( const char * line ) {
      int index = 0;
      int indentation = 0;
      bool hasInsertion = false;
      while ( line [index ++] == LITERAL_SPACE )
        indentation ++;
      int count = 0;
      char ** parts =  org::tools::misc::CString::Split ( line, LITERAL_COLON, &count );

      if ( count < 2 ) {
        for ( int i = 0; i < count; i ++ )
          lib::Stdlib::Free ( ( void * ) parts [i] );
        lib::Stdlib::Free ( ( void * ) parts );
        count = 0;
        parts =  org::tools::misc::CString::Split ( line, LITERAL_LESS_THAN, &count );
        if ( count >= 2 )
          hasInsertion = true;
      }

      char * trimmed = parts [0];
      if ( trimmed != nullptr ) {
        if ( trimmed [0] == LITERAL_HYPHEN )
          trimmed = org::tools::misc::CString::Concatenate ( this -> FindUpperLevelLastName ( indentation ), &( trimmed [1] ) );
      }
      char * newLastName = org::tools::misc::CString::Concatenate ( trimmed, LITERAL_PERIOD_AS_STR );

      if ( trimmed != parts [0] )
        lib::Stdlib::Free ( ( void * ) trimmed );

      ConfigurationParameter * toAdd = nullptr;
      if ( count > 1 && hasInsertion ) {
        const char * newOwner =  org::tools::misc::CString::Concatenate ( parts [1], LITERAL_PERIOD_AS_STR );
        std::map <org::types::CString, std::list <ConfigurationParameter *> *> * parameters = this -> ExtractParametersMasked ( newOwner );
        lib::Stdlib::Free ( ( void * ) newOwner );
        std::map <org::types::CString, std::list <ConfigurationParameter *> *>::iterator parameterList = parameters -> begin ( );
        bool clearPrevious = true;
        char * paramName = nullptr;
        for ( ; parameterList != parameters -> end ( ); parameterList ++ ) {
          clearPrevious = true;
          for ( std::list <ConfigurationParameter *>::iterator listIt = parameterList -> second -> begin ( ); listIt != parameterList -> second -> end ( ); listIt ++ ) {
            paramName = org::tools::misc::CString::Concatenate ( newLastName, parameterList -> first );
            toAdd = new ConfigurationParameter ( paramName, * listIt );
            lib::Stdlib::Free ( ( void * ) paramName );
            toAdd -> Overridable ( true );
            this -> ProcessParameter ( toAdd, clearPrevious );
            clearPrevious = false;
          }
        }
        delete parameters;
      }
      
      for ( int i = 0; i < count; i ++ )
        lib::Stdlib::Free ( ( void * ) parts [i] );
      lib::Stdlib::Free ( ( void * ) parts );
      
      if ( this -> _indentationNameMap -> count ( indentation ) > 0 ) {
        lib::Stdlib::Free ( ( void * ) this -> _indentationNameMap -> at ( indentation ) );
        this -> _indentationNameMap -> erase ( indentation );
      }
      
      this -> _indentationNameMap -> insert ( std::pair <int, const char *> ( indentation, newLastName ) );
    }

    ConfigurationParameter * Configuration::TryReadParameter ( const char * source, const char * line, char separator ) {
      ConfigurationParameter * node = new ConfigurationParameter ( ( char * ) source, ( char * ) line, separator );
      this -> UpdateLastSeenName ( line );
      if ( node -> GetStatus ( ) != 1 ) {
        delete node;
        node = nullptr;
      }
      return node;
    }

    ConfigurationParameter * Configuration::GetParameter ( const char * key, int index ) {
      ConfigurationParameter * askedFor = nullptr;
      org::types::CString keyStr ( key );
      if ( this -> _configurationNodesMap -> count ( keyStr ) > 0 )
        askedFor = this -> _configurationNodesMap -> at ( keyStr ) -> front ( );
      return askedFor;
    }

    ConfigurationParameter * Configuration::GetParameter ( const char * key ) {
      ConfigurationParameter * askedFor = nullptr;
      if ( this -> ContainsKey ( key ) )
        askedFor = this -> GetParameter ( key, 0 );
      return askedFor;
    }
    
    void Configuration::AddParameter ( ConfigurationParameter * node ) {
      this -> AddParameter ( node, true );
    }

    void Configuration::AddParameter ( ConfigurationParameter * node, bool clearIfOverridable ) {
      const char * name = node -> GetName ( );
      bool completion = name [0] == LITERAL_HYPHEN;
      const char * levelName = completion ? this -> FindUpperLevelLastName ( node -> GetIndentation ( ) ) : nullptr;
      org::types::CString key;
      if ( completion && levelName != nullptr ) {
        if (  org::tools::misc::CString::GetCCharArrayLength ( &( name [1] ) ) == 0 ) {
          const char * listName = nullptr;
          if ( levelName [org::tools::misc::CString::GetCCharArrayLength ( levelName ) - 1] == LITERAL_PERIOD )
            listName =  org::tools::misc::CString::GetStringFrom ( levelName,  org::tools::misc::CString::GetCCharArrayLength ( levelName ) - 1 );
          else
            listName =  org::tools::misc::CString::GetStringFrom ( levelName,  org::tools::misc::CString::GetCCharArrayLength ( levelName ) );
          key = listName;
          lib::Stdlib::Free ( ( void * ) listName );
        } else {
          key = levelName;
          key.Add ( &( name [1] ) );
        }
        node -> SetName ( key );
        name = node -> GetName ( );
      } else
        key = name;

      if ( this -> _configurationNodesMap -> count ( key ) <= 0 )
        this -> _configurationNodesMap -> insert ( std::pair <org::types::CString, std::list <ConfigurationParameter *> *> ( key, new std::list <ConfigurationParameter *> ( ) ) );

      if ( this -> _configurationNodesMap -> at ( key ) -> size ( ) > 0 )
        if ( this -> _configurationNodesMap -> at ( key ) -> front ( ) -> Overridable ( ) && clearIfOverridable ) {
          delete this -> _configurationNodesMap -> at ( key ) -> front ( );
          this -> _configurationNodesMap -> at ( key ) -> clear ( );
        }
    
      this -> _configurationNodesMap -> at ( key ) -> push_back ( node );
    }
    
    void Configuration::ProcessParameter ( ConfigurationParameter * node ) {
      this -> ProcessParameter ( node, true );
    }

    void Configuration::ProcessParameter ( ConfigurationParameter * node, bool clearPrevious ) {
      if ( node != nullptr ) {
        if ( this -> NodeIsConfigurationFile ( node ) )
          this -> AddingConfiguration ( node );
        else
          this -> AddParameter ( node, clearPrevious );
      }
    }

    void Configuration::AddingConfiguration ( ConfigurationParameter * node ) {
      this -> Transform ( node, LITERAL_OPEN_BRACE, LITERAL_CLOSE_BRACE );
      this -> _configurationFileFound = true;
      this -> ReadFile ( node -> GetTransformed ( ), LITERAL_COLON );
      delete node;
    }

    int Configuration::NodeIsConfigurationFile ( ConfigurationParameter * node ) {
      return  org::tools::misc::CString::IsPartiallyEqual ( node -> GetName ( ), CONSTANT_CONFIGURATION_TAG );
    }
    
    const char * Configuration::Transform ( ConfigurationParameter * node, char openingChar, char closingChar ) {
      int   looking       = 1;
      int   rank          = 1;
      int   startingAt    = 0;
      int   closingIndex  = 0;
      int   openingIndex  = 0;
      char * key           = nullptr;
      char * replaced      = nullptr;
      char * toTransform =  org::tools::misc::CString::GetCopyOfCString ( node -> GetValue ( ) );
      while ( looking )
        if ( ( closingIndex =  org::tools::misc::CString::Find ( toTransform, closingChar, startingAt, -1, rank ) ) != -1 )
          if ( ( openingIndex =  org::tools::misc::CString::Find ( toTransform, openingChar, startingAt, closingIndex, -1 ) ) != -1 ) {
            key =  org::tools::misc::CString::GetStringFrom ( &( toTransform [openingIndex + 1] ), closingIndex - openingIndex - 1 );
            if ( this -> ContainsKey ( key ) ) {
              replaced =  org::tools::misc::CString::Replace ( toTransform, openingIndex, closingIndex + 1, this -> GetValue ( key ) );
               org::tools::misc::CString::FreeCharArray ( toTransform );
              toTransform = replaced;
            } else
              startingAt = closingIndex + 1;
             org::tools::misc::CString::FreeCharArray ( key );
          } else
            startingAt = closingIndex + 1;
        else
          looking = 0;
      if ( replaced == nullptr )
         org::tools::misc::CString::FreeCharArray ( toTransform );
      node -> SetTransformed ( replaced );
       org::tools::misc::CString::FreeCharArray ( replaced );
      return node -> GetTransformed ( );
    }

    Configuration::Configuration ( char * source, char separator ) {
      this -> InitializeConfiguration ( );
      this -> ReadFile ( source, separator );
    }

    Configuration::Configuration ( const char * source, const char ** parameters, int parametersCount, char separator ) {
      this -> InitializeConfiguration ( );
      this -> Read ( source, parameters, parametersCount, separator );
    }

    Configuration::~Configuration ( void ) {
      std::map <org::types::CString, std::list <ConfigurationParameter *> *>::iterator repoIt = this -> _configurationNodesMap -> begin ( );
      std::list <ConfigurationParameter *>::iterator parameterIt;
      for ( ; repoIt != this -> _configurationNodesMap -> end ( ); repoIt ++ ) {
        for ( parameterIt = repoIt -> second -> begin ( ); parameterIt != repoIt -> second -> end ( ); parameterIt ++ )
          delete *parameterIt;
        delete repoIt -> second;
      }
      delete this -> _configurationNodesMap;
      
      std::map <int, const char *>::iterator indentMapIt = this -> _indentationNameMap -> begin ( );
      for ( ; indentMapIt != this -> _indentationNameMap -> end ( ); indentMapIt ++ )
        lib::Stdlib::Free ( ( void * ) indentMapIt -> second );
      delete this -> _indentationNameMap;
    }

    const char * Configuration::Get ( const char * key ) {
      return this -> ContainsKey ( key ) ? this -> Transform ( this -> GetParameter ( key ), LITERAL_OPEN_BRACE, LITERAL_CLOSE_BRACE ) : nullptr;
    }

    const char * Configuration::GetValue ( const char * key ) {
      return this -> ContainsKey ( key ) ? this -> GetParameter ( key ) -> GetValue ( ) : nullptr;
    }

    int Configuration::ContainsKey ( const char * key ) {
      org::types::CString keyString ( key );
      return this -> _configurationNodesMap -> count ( keyString ) > 0;
    }

    std::map <org::types::CString, std::list <ConfigurationParameter *> *> * Configuration::ExtractParametersMasked ( const char * mask ) {
      std::map <org::types::CString, std::list <ConfigurationParameter *> *> * parameters = new std::map <org::types::CString, std::list <ConfigurationParameter *> *> ( );

      if ( mask != nullptr ) {
        std::map <org::types::CString, std::list <ConfigurationParameter *> *>::iterator mapIt = this -> _configurationNodesMap -> begin ( );
        for ( ; mapIt != this -> _configurationNodesMap -> end ( ); mapIt ++ )
          if ( org::tools::misc::CString::IsMasked ( mapIt -> first, mask ) )
            parameters -> insert ( std::pair <org::types::CString, std::list <ConfigurationParameter *> *> ( &( ( const char * ) mapIt -> first ) [org::tools::misc::CString::GetCCharArrayLength ( mask )], mapIt -> second ) );
      }

      return parameters;
    }

    void Configuration::Print ( void ) {
      std::map <org::types::CString, std::list <ConfigurationParameter *> *>::iterator it = this -> _configurationNodesMap -> begin ( );
      char * line = nullptr;
      for ( ; it != this -> _configurationNodesMap -> end ( ); it ++ ) {
        line = it -> second -> front ( ) -> ToString ( );
        this -> Debug ( line );
        lib::Stdlib::Free ( line );
      }
    }

    void Configuration::TransformAll ( void ) {
      std::map <org::types::CString, std::list <ConfigurationParameter *> *>::iterator mapIt = this -> _configurationNodesMap -> begin ( );
      std::list <ConfigurationParameter *>::iterator listIt;
      for ( ; mapIt != this -> _configurationNodesMap -> end ( ); mapIt ++ ) {
        listIt = mapIt -> second -> begin ( );
        for ( ; listIt != mapIt -> second -> end ( ); listIt ++ ) {
          this -> Transform ( *listIt, LITERAL_OPEN_BRACE, LITERAL_CLOSE_BRACE );
        }
      }
    }

    void Configuration::ReadFile ( const char * source, char separator ) {
      int lineCount = 0;
      const char ** lines = org::tools::misc::FileExtension::ReadAllLinesAndClean ( source, &lineCount );
      this -> Read ( source, lines, lineCount, separator );

      for ( int i = 0; i < lineCount; i ++ )
        lib::Stdlib::Free ( ( void * ) lines [i] );
      lib::Stdlib::Free ( ( void * ) lines );
    }

    void Configuration::Read ( const char * source, const char ** parameters, int parametersCount, char separator ) {
      ConfigurationParameter * node = nullptr;
      if ( parameters != nullptr )
        for ( int i = 0; i < parametersCount; i ++ )
          if ( parameters [i] [0] != LITERAL_HASH && ! org::tools::misc::CString::IsEqual ( parameters [i], CONSTANT_EMPTY_STRING ) ) {
            node = this -> TryReadParameter ( source, parameters [i], separator );
            if ( node != nullptr )
              this -> ProcessParameter ( node );
          }
    }

    bool Configuration::ConfigurationFileFound ( void ) {
      return this -> _configurationFileFound;
    }
  }
}
