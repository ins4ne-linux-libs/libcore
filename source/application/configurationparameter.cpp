


#include <application/configurationparameter.hpp>

#include <constants/libcore.hpp>
#include <process/memorymanager.hpp>
#include <tools/misc/stringextension.hpp>
#include <lib/stdlib.hpp>

namespace org {
  namespace application {
    void ConfigurationParameter::Configure ( const char * source, const char * name, const char * value ) {
      this -> _source = nullptr;
      this -> _name = nullptr;
      this -> _value = nullptr;
      this -> _transformed = nullptr;
      this -> _indentation = 0;
      this -> _status = 0;
      this -> SetSource ( source );
      this -> SetName ( name );
      this -> SetValue ( value );
      this -> _nested = nullptr;
      this -> _overridable = false;
    }
    
    ConfigurationParameter::ConfigurationParameter ( const char * source, const char * name, const char * value ) {
      this -> Configure ( source, name, value );
    }

    ConfigurationParameter::ConfigurationParameter ( const char * source, const char * line, char separator ) {
      this -> Configure ( source, nullptr, nullptr );
      this -> Read ( line, separator );
    }

    ConfigurationParameter::ConfigurationParameter ( const char * newName, ConfigurationParameter * nested ) {
      this -> Configure ( nullptr, nullptr, nullptr );
      this -> SetName ( ( char * ) newName );
      this -> _nested = nested;
    }

    ConfigurationParameter::~ConfigurationParameter ( void ) {
      lib::Stdlib::Free ( ( void* ) this -> _source );
      lib::Stdlib::Free ( ( void* ) this -> _name );
      lib::Stdlib::Free ( ( void* ) this -> _value ); 
      lib::Stdlib::Free ( ( void* ) this -> _transformed );
    }

    bool ConfigurationParameter::Overridable ( void ) {
      return this -> _overridable;
    }

    void ConfigurationParameter::Overridable ( bool overridable ) {
      this -> _overridable = overridable;
    }

    unsigned char ConfigurationParameter::GetStatus ( void ) {
      return this -> _nested == nullptr ? this -> _status : this -> _nested -> GetStatus ( );
    }

    const char * ConfigurationParameter::GetSource ( void ) {
      return this -> _nested == nullptr ? this -> _source : this -> _nested -> GetSource ( );
    }

    const char * ConfigurationParameter::GetValue ( void ) {
      return this -> _nested == nullptr ? this -> _value : this -> _nested -> GetValue ( );
    }

    const char * ConfigurationParameter::GetName ( void ) {
      return this -> _name;
    }

    const char * ConfigurationParameter::GetTransformed ( void ) {
      return this -> _nested == nullptr ? ( this -> _transformed == nullptr ? this -> _value : this -> _transformed ) : this -> _nested -> GetTransformed ( );
    }

    int ConfigurationParameter::GetIndentation ( void ) {
      return this -> _nested == nullptr ? this -> _indentation : this -> _nested -> GetIndentation ( );
    }
    
    void ConfigurationParameter::SetTransformed ( const char * newTransformed ) {
      lib::Stdlib::Free ( ( void* ) this -> _transformed );
      this -> _transformed = newTransformed != nullptr ? org::tools::misc::CString::GetCopyOfCString ( newTransformed ) : nullptr;
    }

    void ConfigurationParameter::SetName ( const char * newName ) {
      lib::Stdlib::Free ( ( void* ) this -> _name );
      this -> _name = newName != nullptr ? org::tools::misc::CString::Trim ( newName ) : nullptr;
    }

    void ConfigurationParameter::SetValue ( const char * newValue ) {
      lib::Stdlib::Free ( ( void* ) this -> _value );
      this -> _value = newValue != nullptr ? org::tools::misc::CString::GetCopyOfCString ( newValue ) : nullptr;
    }

    void ConfigurationParameter::SetSource ( const char * newSource ) {
      lib::Stdlib::Free ( ( void* ) this -> _source );
      this -> _source = newSource != nullptr ? org::tools::misc::CString::GetCopyOfCString ( newSource ) : nullptr;
    }

    void ConfigurationParameter::Read ( const char * line, char separator ) {
      if ( line != nullptr ) {
        if ( org::tools::misc::CString::Contains ( line, separator ) ) {
          int count = 0;
          while ( line [this -> _indentation] == LITERAL_SPACE )
            this -> _indentation ++;
          const char ** parts = ( const char ** ) org::tools::misc::CString::SplitAndRemoveEmptyEntries ( line, separator, &count, 2 );
          if ( count >= 1 ) {
            this -> SetName ( parts [0] );
            if ( count >= 2 )
              this -> SetValue ( parts [1] );
            else {
              const char * tmp = ( const char * ) org::process::MemoryManager::Malloc ( 1, 1 );
              this -> SetValue ( tmp );
              lib::Stdlib::Free ( ( void * ) tmp );
            }
            this -> _status = 1;
          }
          lib::Stdlib::Free <const char *> ( parts, count );
        }
      }
    }

    char * ConfigurationParameter::ToString ( void ) {
      return org::tools::misc::CString::Concatenate ( this -> GetName ( ), this -> GetTransformed ( ) == nullptr ? LITERAL_NULL : this -> GetTransformed ( ), LITERAL_COLON );
    }
  }
}
