
#include <application/constant.hpp>
#include <application/constants.hpp>

namespace org {
  namespace application {
    Constant::Constant ( void ) {
      Constants::ReferenceInCurrent ( this );
    }

    Constant::~Constant ( void ) { }

    int Constant::AsInteger ( void ) const {
      return 0;
    }

    const char * Constant::AsString ( void ) const {
      return nullptr;
    }

    unsigned char Constant::AsByte ( void ) const {
      return 0;
    }

    char Constant::AsCharacter ( void ) const {
      return '\0';
    }
  }
}
