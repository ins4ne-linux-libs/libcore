
#include <application/constants.hpp>

#include <list>
#include <application/constant.hpp>

namespace org {
  namespace application {
    Constants * Constants::Current;

    struct Constants::ConstantsCheshireCat {
      std::list <org::application::Constant *> * _all;
    };

    Constants::Constants ( void ) {
      Constants::Current = this;
      this -> _dPtr = new ConstantsCheshireCat ( );
      this -> _dPtr -> _all = new std::list <org::application::Constant *> ( );
    }

    Constants::~Constants ( void ) {
      this -> ClearAll ( );
      delete this -> _dPtr -> _all;
      delete this -> _dPtr;
    }

    void Constants::Reference ( org::application::Constant * ref ) {
      this -> _dPtr -> _all -> push_back ( ref );
    }

    void Constants::ClearAll ( void ) {
      std::list <org::application::Constant *>::iterator cstIt = this -> _dPtr -> _all -> begin ( );
      for ( ; cstIt != this -> _dPtr -> _all -> end ( ); ++ cstIt )
        delete *cstIt;
    }

    void Constants::ReferenceInCurrent ( org::application::Constant * ref ) {
      if ( Constants::Current == nullptr )
        Constants::Current = new Constants ( );
      Constants::Current -> Reference ( ref );
    }
  }
}
