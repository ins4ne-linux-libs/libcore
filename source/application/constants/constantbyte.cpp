
#include <application/constants/constantbyte.hpp>

namespace org {
  namespace application {
    namespace constants {
      Byte::Byte ( unsigned char constant ) : _constant ( constant ) { }

      Byte::~Byte ( void ) { }

      unsigned char Byte::AsByte ( void ) const {
        return this -> _constant;
      }
    }
  }
}
