
#include <application/constants/constantcharacter.hpp>

namespace org {
  namespace application {
    namespace constants {
      Character::Character ( char constant ) : _constant ( constant )  { }

      Character::~Character ( void ) { }

      char Character::AsCharacter ( void ) const {
        return this -> _constant;
      }
    }
  }
}
