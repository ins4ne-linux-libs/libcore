
#include <application/constants/constantinteger.hpp>

namespace org {
  namespace application {
    namespace constants {
      Integer::Integer ( int constant ) : _constant ( constant ) { }

      Integer::~Integer ( void ) { }

      int Integer::AsInteger ( void ) const {
        return this -> _constant;
      }
    }
  }
}
