
#include <application/constants/constantstring.hpp>

namespace org {
  namespace application {
    namespace constants {
      String::String ( const char * constant ) : _constant ( constant )  { }

      String::~String ( void ) { }

      const char * String::AsString ( void ) const {
        return this -> _constant;
      }
    }
  }
}
