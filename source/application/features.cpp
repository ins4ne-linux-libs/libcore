
#include <application/features.hpp>

#include <map>

namespace org {
  namespace application {
    Features * Features::Current = nullptr;

    struct Features::FeaturesCheshireCat {
      std::map <org::types::CString, bool> * _features;
    };

    void Features::FinishAutoConfiguration ( void ) {
      org::entity::LoggingConfiguredEntity::FinishAutoConfiguration ( );

      this -> _dPtr -> _features = this -> ContainsSettingMapAs <bool> ( "features" );
    }
    
    void Features::InitializeFeatures ( void ) {
      Features::Current = this;
      this -> _dPtr = new FeaturesCheshireCat ( );
    }

    Features::Features ( void ) {
      this -> InitializeFeatures ( );
      this -> SetConfigurationMask ( "feature-flipping" );
    }

    Features::~Features ( void ) {
      delete this -> _dPtr -> _features;
      delete this -> _dPtr;
    }

    bool Features::IsOn ( const char * featureName ) {
      bool ans = false;
      if ( this -> _dPtr -> _features != nullptr )
        if ( this -> _dPtr -> _features -> find ( featureName ) != this -> _dPtr -> _features -> end ( ) )
          ans = this -> _dPtr -> _features -> at ( featureName );
      return ans;
    }
  }
}
