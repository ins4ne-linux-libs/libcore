


#include <application/program.hpp>

#include <constants/libcore.hpp>
#include <tools/misc/stringextension.hpp>
#include <tools/misc/fileextension.hpp>
#include <application/constants.hpp>
#include <application/features.hpp>

namespace org {
  namespace application {
    Program * Program::Current = nullptr;

    struct Program::ProgramCheshireCat {
      org::application::Features * _features;
    };

    void Program::InitializeProgram ( void ) {
      this -> _argStrings         = nullptr;
      this -> _paramValues        = nullptr;
      this -> _paramStrings       = nullptr;
      this -> _configuration      = nullptr;
      this -> _linkManager        = nullptr;
      this -> _paramCount         = 0;
      this -> _argCount           = 0;
      this -> _inAppMessageCenter = new org::inappmessaging::InAppMessageCenter ( );
      this -> _dPtr = new ProgramCheshireCat ( );
    }

    void Program::ReadArgumentList ( void ) {
      for ( int i = 0; i < this -> _argCount; i ++ )
        if ( this -> _argStrings [i] [0] == LITERAL_HYPHEN )
          this -> _paramCount ++;
      this -> _paramCount ++;
      this -> _paramStrings = org::tools::misc::CString::GetStringArray ( this -> _paramCount );
      this -> _paramValues = org::tools::misc::CString::GetStringArray ( this -> _paramCount );
      for ( int i = 0, j = 0; i < this -> _argCount; i ++ )
        if ( this -> _argStrings [i] [0] == LITERAL_HYPHEN ) {
          this -> SetParameter ( org::tools::misc::CString::GetCopyOfCString ( &( this -> _argStrings [i] [1] ) ), j ++ );
          if ( i < this -> _argCount - 1 )
            if ( this -> _argStrings [i + 1] [0] != LITERAL_HYPHEN )
              this -> SetParameterValue ( org::tools::misc::CString::GetCopyOfCString ( this -> _argStrings [++ i] ), j - 1 );
            else
              this -> SetParameterValue ( org::tools::misc::CString::GetCopyOfCString ( LITERAL_TRUE ), j - 1 );
          else
            this -> SetParameterValue ( org::tools::misc::CString::GetCopyOfCString ( LITERAL_TRUE ), j - 1 );
        } else
          this -> SetParameter ( org::tools::misc::CString::GetCopyOfCString ( this -> _argStrings [i] ), j ++ );
    }

    void Program::SetParameterName ( char* paramName, int paramIndex ) {
      this -> _paramStrings [paramIndex] = paramName;
    }

    void Program::SetParameterValue ( char* paramValue, int paramIndex ) {
      this -> _paramValues [paramIndex] = paramValue;
    }

    void Program::SetParameter ( char* paramName, char* paramValue, int paramIndex ) {
      this -> SetParameterName ( paramName, paramIndex );
      this -> SetParameterValue ( paramValue, paramIndex );
    }

    void Program::SetParameter ( char* paramName, int paramIndex ) {
      this -> SetParameter ( paramName, nullptr, paramIndex );
    }

    void Program::CreateConfiguration ( void ) {
      char** params = this -> GetParameteresSeparatedBy ( LITERAL_COLON );
      this -> _configuration = new Configuration ( this -> _argStrings [0], ( const char ** ) params, this -> ParameterCount ( ), LITERAL_COLON );
      if ( ! ( this -> _configuration -> ConfigurationFileFound ( ) ) )
        this -> _configuration -> ReadFile ( CONSTANT_SETTINGS_CONFIGURATION_PATH, LITERAL_COLON );
      org::tools::misc::CString::FreeStringArray ( params, this -> ParameterCount ( ) );
      this -> _configuration -> TransformAll ( );
      Configuration::Main = this -> _configuration;
    }

    Program::Program ( int argCount, char** argStrings ) {
      this -> InitializeProgram ( );
      this -> _argCount = argCount;
      this -> _argStrings = org::tools::misc::CString::GetCopyOfStringArray ( argStrings, this -> _argCount );
      this -> ReadArgumentList ( );
      this -> CreateConfiguration ( );
      this -> _linkManager = new org::filesystem::LinkManager ( CONSTANT_LINK_MANAGER_NAME );
      this -> _dPtr -> _features = new org::application::Features ( );

      org::filesystem::LinkManager::Current = this -> _linkManager;
    }

    Program::~Program ( void ) {
      MemoryManager::FreeMA ( ( void** ) this -> _argStrings, this -> _argCount );
      MemoryManager::FreeMA ( ( void** ) this -> _paramValues, this -> _paramCount );
      MemoryManager::FreeMA ( ( void** ) this -> _paramStrings, this -> _paramCount );
      if ( this -> _linkManager != nullptr )
        delete this -> _linkManager;
      if ( this -> _configuration != nullptr )
        delete this -> _configuration;
      if ( this -> _inAppMessageCenter != nullptr )
        delete this -> _inAppMessageCenter;
      if ( org::application::Constants::Current != nullptr )
        delete org::application::Constants::Current;
      if ( this -> _dPtr -> _features != nullptr )
        delete this -> _dPtr -> _features;
      delete this -> _dPtr;
    }

    org::filesystem::LinkManager* Program::GetLinkManager ( void ) {
      return this -> _linkManager;
    }

    Configuration* Program::Config ( void ) {
      return this -> _configuration;
    }

    const char * Program::GetConfiguration ( const char * key ) {
      return this -> Config ( ) -> Get ( key );
    }

    int Program::GetInt ( char* key ) {
      return org::tools::misc::CString::ToInt ( this -> GetConfiguration ( key ) );
    }

    char** Program::GetNames ( void ) {
      return this -> _paramStrings;
    }

    int Program::ParameterCount ( void ) {
      return this -> _paramCount;
    }

    int Program::Is ( char* paramName ) {
      return this -> Get ( paramName ) == ( ( char* ) -1 ) ? 0 : 1;
    }

    const char * Program::GetCommandParameter ( const char * paramName ) {
      const char * value = nullptr;
      for ( int i = 0; i < this -> _paramCount; i ++ )
        if ( org::tools::misc::CString::IsPartiallyEqual ( paramName, this -> _paramStrings [i] ) ) {
          value = this -> _paramValues [i];
          break;
        }
      return value;
    }

    const char * Program::Get ( const char * paramName ) {
      const char * value = this -> GetConfiguration ( paramName );
      if ( value == nullptr )
        value = this -> GetCommandParameter ( paramName );
      return value;
    }

    char** Program::GetParameteresSeparatedBy ( char separator ) {
      char** array = org::tools::misc::CString::GetStringArray ( this -> _paramCount );
      for ( int i = 0; i < this -> _paramCount; i ++ )
        array [i] = org::tools::misc::CString::Concatenate ( this -> _paramStrings [i], this -> _paramValues [i], separator );
      return array;
    }

    void Program::Banner ( void ) {
      if ( this -> HasALogger ( ) ) {
        int size = 0;
        const char** lines = org::tools::misc::FileExtension::ReadAllLines ( this -> Get ( SETTING_BANNER_FILE ), &size );
        for ( int i = 0; i < size; i ++ )
          this -> Info ( lines [i] );
        MemoryManager::FreeMA ( ( void** ) lines, size );
      }
    }

    void Program::ConfigurationList ( void ) {
      this -> _configuration -> Print ( );
    }

    void Program::EndOfProgram ( void ) {
      this -> Info ( LOG_END_OF_PROGRAM );
      this -> Info ( LOG_NEW_LINE );
    }

    org::inappmessaging::InAppMessageCenter* Program::InApplicationMessageCenter ( void ) {
      return this -> _inAppMessageCenter;
    }
  }
}
