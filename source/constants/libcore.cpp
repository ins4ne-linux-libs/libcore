
#include <constants/libcore.hpp>

#include <constants/libcore.hpp>
#include <application/constants/constantstring.hpp>
#include <application/constants/constantcharacter.hpp>

namespace org {
  namespace constants {
    org::application::Constant * DefaultSettingsFile        = new org::application::constants::String ( "settings.conf" );
    org::application::Constant * DefaultLinkManagerName     = new org::application::constants::String ( "link.manager" );
    org::application::Constant * DefaultConfigurationTag    = new org::application::constants::String ( "configuration" );
    org::application::Constant * EmptyString                = new org::application::constants::String ( "" );
    org::application::Constant * StringFormatCharacters     = new org::application::constants::String ( "duoxXfFeEgGaAcspn" );
    org::application::Constant * FormatLengthCharacters     = new org::application::constants::String ( "hljztL" );
    org::application::Constant * StdOutLinkName             = new org::application::constants::String ( "std.cout" );
    org::application::Constant * PipeReaderThreadName       = new org::application::constants::String ( "pipe" );
    org::application::Constant * SerialReaderThreadName     = new org::application::constants::String ( "serial" );
    
    org::application::Constant * LiteralTrue                = new org::application::constants::String ( "true" );
    org::application::Constant * LiteralNull                = new org::application::constants::String ( "null" );
    org::application::Constant * LiteralPeriodAsString      = new org::application::constants::String ( "." );
    org::application::Constant * LiteralSlashAsString       = new org::application::constants::String ( "/" );

    org::application::Constant * LiteralHyphen              = new org::application::constants::Character ( '-' );
    org::application::Constant * LiteralColon               = new org::application::constants::Character ( ':' );
    org::application::Constant * LiteralSemiColon           = new org::application::constants::Character ( ';' );
    org::application::Constant * LiteralSpace               = new org::application::constants::Character ( ' ' );
    org::application::Constant * LiteralLessThan            = new org::application::constants::Character ( '<' );
    org::application::Constant * LiteralGreaterThan         = new org::application::constants::Character ( '>' );
    org::application::Constant * LiteralPeriod              = new org::application::constants::Character ( '.' );
    org::application::Constant * LiteralOpenBrace           = new org::application::constants::Character ( '{' );
    org::application::Constant * LiteralCloseBrace          = new org::application::constants::Character ( '}' );
    org::application::Constant * LiteralHash                = new org::application::constants::Character ( '#' );
    org::application::Constant * LiteralBackSlash           = new org::application::constants::Character ( '\\' );
    org::application::Constant * LiteralPercent             = new org::application::constants::Character ( '%' );
    
    org::application::Constant * LiteralH                   = new org::application::constants::Character ( 'h' );
    org::application::Constant * LiteralL                   = new org::application::constants::Character ( 'l' );
    org::application::Constant * LiteralS                   = new org::application::constants::Character ( 's' );
    org::application::Constant * Literal0                   = new org::application::constants::Character ( '0' );
    org::application::Constant * Literal9                   = new org::application::constants::Character ( '9' );
    
    org::application::Constant * SettingPath                = new org::application::constants::String ( "path" );
    org::application::Constant * SettingFiles               = new org::application::constants::String ( "files" );
    org::application::Constant * SettingBlocking            = new org::application::constants::String ( "blocking" );
    org::application::Constant * SettingRoute               = new org::application::constants::String ( "route" );
    org::application::Constant * SettingBannerFile          = new org::application::constants::String ( "files.misc.banner" );
    org::application::Constant * SettingLogger              = new org::application::constants::String ( "logger" );
    org::application::Constant * SettingReadMode            = new org::application::constants::String ( "read-mode" );
    org::application::Constant * SettingUnlink              = new org::application::constants::String ( "unlink" );
    org::application::Constant * SettingCreate              = new org::application::constants::String ( "create" );
    org::application::Constant * SettingSleepingPeriod      = new org::application::constants::String ( "sleeping-period" );
    org::application::Constant * SettingCreatePermission    = new org::application::constants::String ( "create-permission" );
    org::application::Constant * SettingByteCount           = new org::application::constants::String ( "byte-count" );
    org::application::Constant * SettingFloatingPointCount  = new org::application::constants::String ( "floating-point-count" );
    org::application::Constant * SettingMinimumChange       = new org::application::constants::String ( "minimum-change" );
    org::application::Constant * SettingNumericalIO         = new org::application::constants::String ( "machine-state.numerical-io" );
    org::application::Constant * SettingParameters          = new org::application::constants::String ( "machine-state.parameters" );
    org::application::Constant * SettingTemperatures        = new org::application::constants::String ( "machine-state.temperatures" );
    org::application::Constant * SettingName                = new org::application::constants::String ( "name" );
    org::application::Constant * SettingWriteMode           = new org::application::constants::String ( "write-mode" );
    org::application::Constant * SettingReadMinimum         = new org::application::constants::String ( "read-minimum" );
    org::application::Constant * SettingReadTimeout         = new org::application::constants::String ( "read-timeout" );
    org::application::Constant * SettingSpeed               = new org::application::constants::String ( "speed" );
    org::application::Constant * SettingToFlush             = new org::application::constants::String ( "to-flush" );
    org::application::Constant * SettingSimulatorEnable     = new org::application::constants::String ( "simulator.enable" );
    org::application::Constant * SettingSimulatorOpenDoor   = new org::application::constants::String ( "simulator.open-door" );
    org::application::Constant * SettingSimulatorType       = new org::application::constants::String ( "simulator.type" );
    org::application::Constant * SettingSimulatorName       = new org::application::constants::String ( "simulator.name" );
    
    org::application::Constant * LogEndOfProgram            = new org::application::constants::String ( "End of program." );
    org::application::Constant * LogNewLine                 = new org::application::constants::String ( "\n" );
    org::application::Constant * LogBootModeDetected        = new org::application::constants::String ( "BOOT MODE detected" );
    org::application::Constant * LogSendingSpecialGift      = new org::application::constants::String ( "Sending special gift." );
    org::application::Constant * LogSent                    = new org::application::constants::String ( "Sent." );
    org::application::Constant * LogReceivedNiuTimeout      = new org::application::constants::String ( "Received serial port timeout." );
    org::application::Constant * LogTryingToRead            = new org::application::constants::String ( "Trying reading." );
    org::application::Constant * LogStartWait               = new org::application::constants::String ( "Start wait." );
    org::application::Constant * LogEndWait                 = new org::application::constants::String ( "End wait." );
    org::application::Constant * LogReadSomething           = new org::application::constants::String ( "Read something." );
    org::application::Constant * LogOpeningNamedPipe        = new org::application::constants::String ( "Opening named pipe." );
    org::application::Constant * LogNoAccessPipe            = new org::application::constants::String ( "Could not open named pipe." );
    org::application::Constant * LogTryOpenSerialPort       = new org::application::constants::String ( "Trying to open serial port." );
    org::application::Constant * LogSerialPortNowOpen       = new org::application::constants::String ( "Serial port now open." );
    org::application::Constant * LogFailed                  = new org::application::constants::String ( "Failed." );
    org::application::Constant * LogSerialPortClosed        = new org::application::constants::String ( "Serial port closed." );
    org::application::Constant * LogClosingFailed           = new org::application::constants::String ( "Closing failed." );
  }
}
