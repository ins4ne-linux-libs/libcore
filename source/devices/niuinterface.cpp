


#include <devices/niuinterface.hpp>

#include <constants/libcore.hpp>

namespace org {
  namespace devices {
    void NIUInterface::InitializeNIUInterface ( void ) {
      this -> _buffer           = new org::data::Buffer <unsigned char> ( 1024, 512, 0 );
      this -> _serialPort       = nullptr;
      this -> _eapMessageEvent  = nullptr;
      this -> _waitingBootMode  = false;
      this -> MakeTaskable ( );
    }
    
    void NIUInterface::FindMessages ( void ) {
      this -> _buffer -> ShiftToValue ( 0xC6 );
      if ( this -> _buffer -> GetCount ( ) > 2 )
        if ( *( this -> _buffer -> Read ( 0 ) ) == ( ( unsigned char ) 0xC6 ) )
          if ( *( this -> _buffer -> Read ( 1 ) ) != 0 && *( this -> _buffer -> Read ( 1 ) ) + 3 <= this -> _buffer -> GetCount ( ) )
            if ( this -> _eapMessageEvent != nullptr )
              this -> _eapMessageEvent -> Call ( ( void* ) this, this -> _buffer -> ExtractAndShift ( *( this -> _buffer -> Read ( 1 ) ) + 3 ) );
    }

    void NIUInterface::NIUToComputer ( unsigned char receivedChar ) {
      this -> _buffer -> Write ( receivedChar );
    }

    void NIUInterface::NewByteReceived ( org::eventing::EventArgs <void*, char*, unsigned char *>* args ) {
      if ( args != nullptr ) {
        if ( args -> GetArgs ( ) != nullptr ) {
          if ( this -> _waitingBootMode ) {
            if ( args -> GetArgs ( ) [0] == 0 ) {
              this -> Warning ( LOG_BOOT_MODE_DETECTED );
              this -> _waitingBootMode = false;
              this -> Warning ( LOG_SENDING_SPECIAL_GIFT );

              unsigned char toSend2 [] = { 0xC6, 0x03, 0xB7, 0x05, 0xB1, 0x00 };
              this -> SendToNIU ( ( unsigned char * ) toSend2, 6 );
              this -> Warning ( LOG_SENT );
            }
          } else {
            this -> NIUToComputer ( args -> GetArgs ( ) [0] );
            this -> FindMessages ( );
          }
          this -> Free ( args -> GetArgs ( ) );
        } else {
          this -> _buffer -> Clear ( );
          this -> Debug ( LOG_RECEIVED_NIU_TIMEOUT );
        }
        delete args;
      }
    }

    void NIUInterface::FinishAutoConfiguration ( void ) {
      org::entity::LoggingConfiguredEntity::FinishAutoConfiguration ( );
      this -> _plugByteReceivedEvent = new org::eventing::EventDelegateCall <NIUInterface, unsigned char> ( this, &NIUInterface::NewByteReceived );
      this -> _serialPort = this -> NewFromSetting <org::hardware::SerialPort> ( SETTING_ROUTE );

      if ( this -> _serialPort != nullptr )
        this -> _serialPort -> PlugByteReceivedEvent ( this -> _plugByteReceivedEvent );
    }
    
    NIUInterface::NIUInterface  ( const char * niuInterface ) {
      this -> InitializeNIUInterface ( );
      this -> SetConfigurationMask ( niuInterface );
    }

    NIUInterface::~NIUInterface ( void ) {
      delete this -> _buffer;
      if ( this -> _serialPort != nullptr )
        delete this -> _serialPort;
      delete this -> _plugByteReceivedEvent;
    }

    void NIUInterface::FlushBuffer ( void ) {
      this -> _buffer -> Clear ( );
    }

    void NIUInterface::WaitBootMode ( void ) {
      this -> _waitingBootMode = true;
    }

    bool NIUInterface::OpenCommunication ( void ) {
      return this -> _serialPort -> Open ( );
    }

    void NIUInterface::CloseCommunication ( void ) {
      this -> _serialPort -> Close ( );
    }

    void NIUInterface::StartListening ( void ) {
      if ( this -> OpenCommunication ( ) )
        this -> _serialPort -> ReadAsTask ( );
    }

    void NIUInterface::StopListening ( void ) {
      this -> _serialPort -> StopReadingTask ( );
      this -> CloseCommunication ( );
    }

    int NIUInterface::SendToNIU ( unsigned char * message, int size ) {
      this -> ThreadLock ( );
      int written = this -> _serialPort -> Write ( message, size );
      // MemoryManager::FreeM ( ( void* ) message );
      this -> ThreadUnlock ( );
      return written;
    }

    int NIUInterface::ReadNIU ( unsigned char * bufferToFill, int countToRead ) {
      return this -> _serialPort -> Read ( bufferToFill, countToRead );
    }

    int NIUInterface::SerialSpeed ( int newSpeed ) {
      this -> _serialPort -> SetSpeed ( newSpeed );
      return newSpeed;
    }

    org::hardware::SerialPort * NIUInterface::Port ( void ) {
      return this -> _serialPort;
    }

    void NIUInterface::PlugEapMessageEvent ( org::eventing::EventCall <org::data::Buffer <unsigned char>>* eapMessageEvent ) {
      this -> Delete <org::eventing::EventCall <org::data::Buffer <unsigned char>>> ( this -> _eapMessageEvent );
      this -> _eapMessageEvent = eapMessageEvent;
    }
  }
}
