


#include <entity/configuredentity.hpp>

#include <constants/libcore.hpp>

namespace org {
  namespace entity {
    void ConfiguredEntity::AddConfiguration ( std::map <org::types::CString, std::list <org::application::ConfigurationParameter *> *> * newKeys ) {
      this -> _configuration -> insert ( newKeys -> begin ( ), newKeys -> end ( ) );
    }

    void ConfiguredEntity::FetchConfiguration ( const char * mask ) {
      char * openMask = org::tools::misc::CString::Concatenate ( mask, ( char* ) LITERAL_PERIOD_AS_STR );
      this -> _configuration -> clear ( );
      if ( org::application::Configuration::Main != nullptr ) {
        std::map <org::types::CString, std::list <org::application::ConfigurationParameter *> *> * newKeys = org::application::Configuration::Main -> ExtractParametersMasked ( openMask );
        this -> AddConfiguration ( newKeys );
        delete newKeys;
      }
      lib::Stdlib::Free ( openMask );
      this -> FinishAutoConfiguration ( );
    }

    const char * ConfiguredEntity::GetSetting ( const char * key ) {
      return this -> ContainsSetting ( key ) ? this -> _configuration -> at ( key ) -> front ( ) -> GetTransformed ( ) : nullptr;
    }
    
    bool ConfiguredEntity::ContainsAndExtractSetting ( const char * key, const char ** setting ) {
      *setting = this -> GetSetting ( key );
      return ( *setting ) != nullptr;
    }

    void ConfiguredEntity::InitializeConfiguredEntity ( void ) {
      this -> _configuration = new std::map <org::types::CString, std::list <org::application::ConfigurationParameter *> *> ( );
    }
    
    void ConfiguredEntity::FinishAutoConfiguration ( void ) { }

    void ConfiguredEntity::SetConfigurationMask ( const char * mask ) {
      if ( mask != nullptr )
        this -> FetchConfiguration ( mask );
    }
    
    bool ConfiguredEntity::ContainsSetting ( const char * key ) {
      return this -> _configuration -> count ( key ) > 0;
    }

    ConfiguredEntity::ConfiguredEntity ( void ) {
      this -> InitializeConfiguredEntity ( );
    }

    ConfiguredEntity::~ConfiguredEntity ( void ) {
      if ( this -> _configuration != nullptr )
        delete this -> _configuration;
    }
  }
}
