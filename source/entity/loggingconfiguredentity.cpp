


#include <entity/loggingconfiguredentity.hpp>

#include <constants/libcore.hpp>

namespace org {
  namespace entity {
    void org::entity::LoggingConfiguredEntity::FinishAutoConfiguration ( void ) {
      ConfiguredEntity::FinishAutoConfiguration ( );
      this -> SetLogger ( this -> NewFromSetting <org::logging::LoggerConfigured> ( SETTING_LOGGER ) );
    }

    void org::entity::LoggingConfiguredEntity::LogSettings ( void ) {
      // std::map <org::types::CString, std::list <ConfigurationParameter *> *>::iterator it = this -> _configuration -> begin ( );
      // for ( ; it != this -> _configuration -> end ( ); it ++ ) {
      //   printf ( "%s - %s\n", it -> first.c_str ( ), it -> second -> front ( ) -> GetTransformed ( ) );
      // }
    }
  }
}
