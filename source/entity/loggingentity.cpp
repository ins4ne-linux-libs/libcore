


#include <entity/loggingentity.hpp>

namespace org {
  namespace entity {
    void LoggingEntity::InitializeLoggingEntity ( void ) {
      this -> _logger = nullptr;
    }
    
    void LoggingEntity::Trace    ( const char * message ) {
      if ( this -> HasALogger ( ) )
        this -> _logger -> Trace    ( message );
    }

    void LoggingEntity::Debug    ( const char * message ) {
      if ( this -> HasALogger ( ) )
      this -> _logger -> Debug    ( message );
    }

    void LoggingEntity::Info     ( const char * message ) {
      if ( this -> HasALogger ( ) )
        this -> _logger -> Info     ( message );
    }

    void LoggingEntity::Notice   ( const char * message ) {
      if ( this -> HasALogger ( ) )
        this -> _logger -> Notice   ( message );
    }

    void LoggingEntity::Warning  ( const char * message ) {
      if ( this -> HasALogger ( ) )
        this -> _logger -> Warning  ( message );
    }

    void LoggingEntity::Error    ( const char * message ) {
      if ( this -> HasALogger ( ) )
        this -> _logger -> Error    ( message );
    }

    void LoggingEntity::Critical ( const char * message ) {
      if ( this -> HasALogger ( ) )
        this -> _logger -> Critical ( message );
    }

    int LoggingEntity::HasALogger ( void ) {
      return this -> _logger != nullptr;
    }
    
    LoggingEntity::LoggingEntity ( void ) {
      this -> InitializeLoggingEntity ( );
    }

    LoggingEntity::~LoggingEntity ( void ) {
      this -> Delete <org::logging::Logger> ( this -> _logger );
    }

    void LoggingEntity::SetLogger ( org::logging::Logger* newLogger ) {
      this -> Delete <org::logging::Logger> ( this -> _logger );
      this -> _logger = newLogger;
    }
  }
}
