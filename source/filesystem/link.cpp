


#include <filesystem/link.hpp>

#include <constants/libcore.hpp>
#include <tools/misc/stringextension.hpp>
#include <process/memorymanager.hpp>
#include <wrappers/ofstream.hpp>
#include <lib/stdlib.hpp>

namespace org {
  namespace filesystem {
    struct Link::LinkCheshireCat {
      org::wrappers::OFStream *  _stream;
    };

    void Link::SetName ( const char * name ) {
      lib::Stdlib::Free ( ( void* ) this -> _name );
      this -> _name = org::tools::misc::CString::GetCopyOfCString ( name );
    }

    void Link::SetValue ( const char * value ) {
      lib::Stdlib::Free ( ( void* ) this -> _value );
      this -> _value = org::tools::misc::CString::GetCopyOfCString ( value );
    }

    void Link::SetType ( unsigned char type ) {
      this -> _type = type;
    }

    char* Link::GetValue ( void ) {
      return this -> _value;
    }

    unsigned char Link::GetType ( void ) {
      return this -> _type;
    }

    void Link::Configure ( const char * name, const char * value, unsigned char type ) {
      this -> _dPtr = new struct LinkCheshireCat ( );
      this -> _name = nullptr;
      this -> _value = nullptr;
      this -> _type = 0;
      this -> _dPtr -> _stream = nullptr;
      this -> SetName ( name );
      this -> SetValue ( value );
      this -> SetType ( type );
    }

    bool Link::OpenStream ( void ) {
      if ( this -> _dPtr -> _stream == nullptr )
        if ( this -> GetType ( ) == 1 )
          this -> _dPtr -> _stream = new org::wrappers::OFStream ( );

      if ( this -> _dPtr -> _stream != nullptr ) {
        this -> _dPtr -> _stream -> Open ( this -> GetValue ( ), false, true, true );
      }

      return this -> Good ( ) && this -> IsOpen ( );
    }
    
    Link::Link ( const char * name, const char * value, unsigned char type ) {
      this -> Configure ( name, value, type );
    }

    Link::Link ( const char * name ) {
      this -> Configure ( name, nullptr, 0 );
      if ( org::tools::misc::CString::IsEqual ( name, CONSTANT_STD_OUT_LINK_NAME ) ) {
        this -> _dPtr -> _stream = new org::wrappers::OFStream ( );
        this -> _dPtr -> _stream -> SetToConsoleOutput ( );
      }
    }

    Link::~Link ( void ) {
      if ( this -> IsOpen ( ) )
        this -> Close ( );
      lib::Stdlib::Free ( ( void* ) this -> _name );
      lib::Stdlib::Free ( ( void* ) this -> _value );
      if ( this -> _dPtr -> _stream != nullptr )
        delete this -> _dPtr -> _stream;
      delete this -> _dPtr;
    }

    bool Link::Good ( void ) {
      return this -> _dPtr -> _stream != nullptr ? this -> _dPtr -> _stream -> Good ( ) : false;
    }

    bool Link::IsOpen ( void ) {
      return this -> _dPtr -> _stream != nullptr ? this -> _dPtr -> _stream -> IsOpen ( ) : false;
    }

    void Link::Open ( void ) {
      this -> ThreadLock ( );
      this -> OpenStream ( );
      this -> ThreadUnlock ( );
    }

    void Link::Close ( void ) {
      this -> ThreadLock ( );
      if ( this -> GetType ( ) == 1 )
        if ( this -> _dPtr -> _stream != nullptr )
          this -> _dPtr -> _stream -> Close ( );
      this -> ThreadUnlock ( );
    }

    void Link::Write ( const char * message ) {
      bool write = true;
      this -> ThreadLock ( );

      if ( this -> IsFile ( ) )
        if ( ! ( this -> Good ( ) && this -> IsOpen ( ) ) )
          write = this -> OpenStream ( );

      if ( write )
        this -> _dPtr -> _stream -> Write ( message );

      this -> ThreadUnlock ( );
    }

    bool Link::IsFile ( void ) {
      return this -> GetType ( ) == 1;
    }

    void Link::WriteLine ( const char * message ) {
      this -> Write ( message );
      this -> Write ( LOG_NEW_LINE );
    }
  }
}
