


#include <filesystem/linkmanager.hpp>

#include <constants/libcore.hpp>

namespace org {
  namespace filesystem {
    LinkManager * LinkManager::Current = nullptr;

    void LinkManager::DeleteLinkMap ( void ) {
      if ( this -> _linkMap != nullptr ) {
        std::map <org::types::CString, Link*>::iterator it = this -> _linkMap -> begin ( );
        for ( ; it != this -> _linkMap -> end ( ); it ++ )
          delete it -> second;
        delete this -> _linkMap;
      }
    }

    void LinkManager::SetLinkMap ( std::map <org::types::CString, Link*>* linkMap ) {
      this -> DeleteLinkMap ( );
      this -> _linkMap = linkMap;
    }

    std::map <org::types::CString, Link*>* LinkManager::GetLinkMap ( void ) {
      return this -> _linkMap;
    }

    void LinkManager::FinishAutoConfiguration ( void ) {
      ConfiguredEntity::FinishAutoConfiguration ( );
      std::map <org::types::CString, const char *> * files = this -> ContainsSettingMapAs <const char *> ( SETTING_FILES );
      this -> AddFiles ( files );
      std::map <org::types::CString, const char *>::iterator filesIt = files -> begin ( );
      for ( ; filesIt != files -> end ( ); ++ filesIt )
        lib::Stdlib::Free ( ( void * ) filesIt -> second );
      delete files;
    }

    void LinkManager::InitializeLinkManager ( ) {
      this -> _linkMap = nullptr;
      this -> SetLinkMap ( new std::map <org::types::CString, Link*> ( ) );
      this -> AddLink ( CONSTANT_STD_OUT_LINK_NAME, new Link ( CONSTANT_STD_OUT_LINK_NAME ) );
    }

    LinkManager::LinkManager ( ) {
      this -> InitializeLinkManager ( );
    }

    LinkManager::LinkManager ( const char * configurationName ) {
      this -> InitializeLinkManager ( );
      this -> SetConfigurationMask ( configurationName );
    }

    LinkManager::~LinkManager ( void ) {
      this -> CloseAll ( );
      this -> DeleteLinkMap ( );
    }

    void LinkManager::AddFiles ( char*** files, int size ) {
      for ( int i = 0; i < size; i ++ ) {
        this -> AddFile ( files [i] [0], files [i] [1] );
      }
    }

    void LinkManager::AddFiles ( std::map <org::types::CString, const char *> * filesMap ) {
      std::map <org::types::CString, const char *>::iterator files = filesMap -> begin ( );
      for ( ; files != filesMap -> end ( ); files ++ )
        this -> AddFile ( files -> first, files -> second );
    }

    void LinkManager::AddFile ( const char * name, const char * value ) {
      this -> AddLink ( name, value, 1 );
    }

    void LinkManager::AddLink ( const char * name, const char * value, unsigned char type ) {
      this -> AddLink ( name, new Link ( name, value, type ) );
    }

    void LinkManager::AddLink ( const char * name, Link* link ) {
      ( *( this -> _linkMap ) ) [name] = link;
    }

    void LinkManager::CloseAll ( void ) {
      if ( this -> _linkMap != nullptr ) {
        std::map <org::types::CString, Link*>::iterator it = this -> _linkMap -> begin ( );
        for ( ; it != this -> _linkMap -> end ( ); it ++ )
          it -> second -> Close ( );
      }
    }

    int LinkManager::ContainsLink ( char* name ) {
      int contains = 0;
      if ( this -> GetLinkMap ( ) != nullptr )
        contains = this -> _linkMap -> count ( name ) > 0;
      return contains;
    }

    Link* LinkManager::Get ( char* name ) {
      Link* link = nullptr;
      if ( this -> ContainsLink ( name ) )
        link = ( *( this -> _linkMap ) ) [name];
      return link;
    }

    void LinkManager::WriteTo ( char* name, char* message ) {
      Link* link = this -> Get ( name );
      if ( link != nullptr )
        link -> Write ( message );
    }
  }
}
