


#include <filesystem/namedpipe.hpp>

#include <constants/libcore.hpp>
#include <lib/freax/unistd.hpp>
#include <lib/freax/fcntl.hpp>
#include <lib/freax/sysstat.hpp>

namespace org {
  namespace filesystem {
    void NamedPipe::MainEvent ( org::eventing::EventArgs <void*, char*, unsigned char *>* args ) {
      if ( args != nullptr ) {
        if ( this -> _byteReceivedEvent != nullptr && args != nullptr ) {
          if ( args -> GetArgs ( ) != nullptr ) {
            this -> _byteReceivedEvent -> Call ( this, ( unsigned char * ) org::tools::misc::CString::GetCopyOfCString ( ( char* ) args -> GetArgs ( ) ) );
          }
        }
        lib::Stdlib::Free ( ( void* ) args -> GetArgs ( ) );
        delete args;
      }
    }

    void NamedPipe::InitializeNamedPipe ( void ) {
      this -> _name                     = nullptr;
      this -> _readTask                 = nullptr;
      this -> _readEvent                = new org::eventing::EventDelegateCall <NamedPipe, unsigned char> ( this, &NamedPipe::MainEvent );

      this -> _byteReceivedEvent        = nullptr;

      this -> _readMode                 = false;
      this -> _writeMode                = false;
      this -> _blocking                 = false;
      this -> _unlinkOnDelete           = false;
      this -> _createIfNotExist         = false;
      this -> _sleepingTimer            = 100;
      this -> _createPermission         = 0666;
      this -> _namedPipeFileDescriptor  = 0;
    }
    
    void NamedPipe::FinishAutoConfiguration ( void ) {
      org::entity::LoggingConfiguredEntity::FinishAutoConfiguration ( );
      
      this -> PutSettingAs <char *>  ( SETTING_PATH               , &( this -> _name              ) );
      this -> PutSettingAs <bool>    ( SETTING_READ_MODE          , &( this -> _readMode          ) );
      this -> PutSettingAs <bool>    ( SETTING_BLOCKING           , &( this -> _blocking          ) );
      this -> PutSettingAs <bool>    ( SETTING_UNLINK             , &( this -> _unlinkOnDelete    ) );
      this -> PutSettingAs <bool>    ( SETTING_CREATE             , &( this -> _createIfNotExist  ) );
      this -> PutSettingAs <int>     ( SETTING_SLEEPING_PERIOD    , &( this -> _sleepingTimer     ) );
      this -> PutSettingAs <int>     ( SETTING_CREATE_PERMISSION  , &( this -> _createPermission  ) );

      this -> _writeMode        = !this -> _readMode;
      this -> _readMode         = true;
    }
    
    NamedPipe::NamedPipe ( const char * configurationName ) {
      this -> InitializeNamedPipe ( );
      this -> SetConfigurationMask ( configurationName );
      if ( this -> _createIfNotExist )
        this -> CreateFifoIfNotExist ( );
    }

    NamedPipe::~NamedPipe ( void ) {
      if ( this -> _readTask != nullptr ) {
        this -> _readTask -> Terminate ( );
        delete this -> _readTask;
      }
      if ( this -> _unlinkOnDelete )
        this -> Unlink ( );
      delete this -> _readEvent;
      lib::Stdlib::Free ( this -> _name );
    }

    unsigned char * NamedPipe::Pull ( void ) {
      unsigned char * buffer = nullptr;
      while ( buffer == nullptr && !( this -> _readTask -> IsAskedToTerminate ( ) ) ) {
        buffer = this -> Read ( );
        // this -> Debug ( LOG_TRYING_TO_READ );
        if ( buffer == nullptr ) {
          // this -> Debug ( LOG_START_WAIT );
          org::tools::misc::Wait::Milliseconds ( this -> _sleepingTimer );
          // this -> Debug ( LOG_END_WAIT );
        }
      }
      // this -> Debug ( LOG_READ_SOMETHING );
      return buffer;
    }

    void NamedPipe::Unlink ( void ) {
      if ( this -> _name != nullptr )
        lib::freax::Unistd::UnLink ( this -> _name );
    }

    void NamedPipe::PlugReadEvent ( org::eventing::EventCall <unsigned char>* event ) {
      if ( this -> _byteReceivedEvent != nullptr )
        delete this -> _byteReceivedEvent;
      this -> _byteReceivedEvent = event;
    }

    void NamedPipe::Open ( void ) {
      this -> Debug ( LOG_OPENING_NAMED_PIPE );
      this -> Debug ( this -> _name );
      if ( this -> _namedPipeFileDescriptor <= 0 && this -> _name != nullptr )
        this -> _namedPipeFileDescriptor = lib::freax::Fcntl::Open ( this -> _name, this -> _readMode, this -> _writeMode,
          false, false, false, false, false, false, ! this -> _blocking, false, false, false, 0666 );
    }

    void NamedPipe::CreateFifoIfNotExist ( void ) {
      lib::freax::SysStat::UMask ( 0000 );
      if ( this -> _name != nullptr )
        if ( lib::freax::Unistd::Access ( this -> _name, true, false, false, false ) != 0 )
          lib::freax::SysStat::MkFifo ( ( const char* ) this -> _name, 0666 );
    }

    void NamedPipe::Close ( void ) {
      if ( this -> _namedPipeFileDescriptor > 0 ) {
        lib::freax::Unistd::Close ( this -> _namedPipeFileDescriptor );
        this -> _namedPipeFileDescriptor = 0;
      }
    }
    
    unsigned char * NamedPipe::Read  ( void ) {
      unsigned char * readByte = nullptr;
      if ( !( this -> _namedPipeFileDescriptor > 0 ) )
        this -> Open ( );
      if ( this -> _namedPipeFileDescriptor > 0 ) {
        readByte = this -> Allocate <unsigned char> ( 2 );
        if ( lib::freax::Unistd::Read ( this -> _namedPipeFileDescriptor, readByte, 1 ) < 1 ) {
          lib::Stdlib::Free ( ( void * ) readByte );
          readByte = nullptr;
        }
      }
      return readByte;
    }
    
    int NamedPipe::Write ( unsigned char * element ) {
      int numberRead = 0;
      if ( !( this -> _namedPipeFileDescriptor > 0 ) )
        this -> Open ( );
      if ( this -> _namedPipeFileDescriptor > 0 )
        numberRead = lib::freax::Unistd::Write ( this -> _namedPipeFileDescriptor, element, 1 );
      return numberRead;
    }

    int NamedPipe::Write ( unsigned char * array, int size ) {
      int numberWritten = 0;
      for ( int i = 0; i < size; i ++ )
        numberWritten += this -> Write ( &( array [i] ) );
      return numberWritten;
    }

    int NamedPipe::Write ( char* message ) {
      return this -> Write ( ( unsigned char * ) message, org::tools::misc::CString::GetCCharArrayLength ( message ) );
    }
    
    void NamedPipe::ReadAsTask ( void ) {
      if ( this -> _readTask == nullptr )
        this -> _readTask = new org::threads::TunnelPullEventTask <unsigned char> ( false, this, this -> _readEvent, CONSTANT_PIPE_READER_THREAD_NAME );
      this -> Open ( );
      if ( this -> _namedPipeFileDescriptor > 0 ) {
        if ( !( this -> _readTask -> IsExecuting ( ) ) )
          this -> _readTask -> Start ( );
      } else
        this -> Error ( LOG_NO_ACCESS_PIPE );
    }
    
    void NamedPipe::StopReadingTask ( void ) {
      if ( this -> _readTask != nullptr )
        this -> _readTask -> Terminate ( );
    }
  }
}
