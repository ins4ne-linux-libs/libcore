
#include <fsm/command.hpp>

namespace org {
  namespace fsm {
    struct Command::CommandCheshireCat {
      org::types::CString * _name;
    };

    void Command::InitializeCommand ( void ) {
      this -> _dPtr = new struct CommandCheshireCat ( );
      this -> _dPtr -> _name = nullptr;

    }

    Command::Command ( void ) {
      this -> InitializeCommand ( );
    }

    Command::~Command ( void ) {
      delete this -> _dPtr;
    }

    const char * Command::ObjectType ( void ) const {
      return "org::fsm::Command";
    }
  }
}
