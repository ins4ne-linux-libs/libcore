
#include <fsm/machine.hpp>

#include <types/cstring.hpp>
#include <list>
#include <map>

#include <fsm/command.hpp>

namespace org {
  namespace fsm {
    struct Machine::MachineCheshireCat {
      org::types::CString * _name;
      std::map <int, org::fsm::Command> * _commands;
    };

    void Machine::InitializeMachine ( void ) {
      this -> _dPtr = new struct MachineCheshireCat ( );
      this -> _dPtr -> _name = nullptr;
      this -> _dPtr -> _commands = nullptr;
    }

    Machine::Machine ( void ) {
      this -> InitializeMachine ( );
    }

    Machine::~Machine ( void ) {
      delete this -> _dPtr;
    }

    const char * Machine::ObjectType ( void ) const {
      return "org::fsm::Machine";
    }
  }
}
