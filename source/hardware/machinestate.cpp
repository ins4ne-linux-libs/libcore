
#include <hardware/machinestate.hpp>

#include <tools/misc/stringextension.hpp>

namespace org {
  namespace hardware {
    void MachineState::InitializeMachineState ( void ) {
      this -> _id = 0;
      this -> _type = nullptr;
      this -> _bytes = nullptr;
      this -> _byteCount = 0;
      this -> _cursorPosition = 0;
    }
    
    void MachineState::AllocateByteArray ( void ) {
      this -> _bytes = this -> Allocate <unsigned char> ( this -> ByteCount ( ) );
    }

    void MachineState::CopyIn ( int count, unsigned char * source ) {
      if ( count >= this -> _byteCount ) {
        if ( this -> _bytes != nullptr )
          this -> Free ( ( void * ) this -> _bytes );
        this -> _byteCount = count;
        this -> AllocateByteArray ( );
        for ( int i = 0; i < this -> _byteCount; i ++ )
          this -> _bytes [i] = source [i];
      }
    }

    void MachineState::CopyInAtCursor ( unsigned char * source, int count ) {
      if ( source != nullptr && this -> _bytes != nullptr )
        for ( int i = 0; i < count; i ++, this -> _cursorPosition ++ )
          this -> _bytes [this -> _cursorPosition] = source [i];
    }

    int MachineState::CopyStringIn ( const char * str ) {
      return this -> CopyStringIn ( str, -1 );
    }

    int MachineState::CopyStringIn ( const char * str, int max ) {
      int copied = org::tools::misc::CString::GetCCharArrayLength ( str );
      if ( copied > max && max > 0 )
        copied = max;
      this -> CopyInAtCursor ( ( unsigned char * ) str, copied );
      return copied;
    }

    const char * MachineState::PutOutString ( void ) {
      const char * cString = ( const char * ) & ( this -> _bytes [this -> _cursorPosition] );
      bool again = true;
      while ( this -> _cursorPosition < this -> _byteCount && again )
        if ( ( again = this -> _bytes [this -> _cursorPosition] != 0 ) )
          this -> _cursorPosition ++;
      if ( this -> _cursorPosition < this -> _byteCount )
        this -> _cursorPosition ++;
      return cString;
    }

    void MachineState::CopyStringInMask ( const char * str, int maskSize ) {
      int toFill = maskSize - this -> CopyStringIn ( str, maskSize - 1 );
      for ( int i = 0; i < toFill; i ++ )
        this -> PutIn <unsigned char> ( 0 );
    }

    void MachineState::ResetCursor ( void ) {
      this -> _cursorPosition = 0;
    }
    
    MachineState::MachineState ( void ) {
      this -> InitializeMachineState ( );
    }

    MachineState::MachineState ( short id, const char * type ) {
      this -> InitializeMachineState ( );
      this -> _id = id;
      this -> _type = org::tools::misc::CString::GetCopyOfCString ( type );
    }

    MachineState::MachineState ( unsigned char * source, int size ) {
      this -> InitializeMachineState ( );
      this -> _byteCount = size;
      this -> AllocateByteArray ( );
      for ( int i = 0; i < size; i ++ )
        this -> _bytes [i] = source [i];
    }
    
    MachineState::~MachineState ( void ) {
      this -> Free ( ( void * ) this -> _type );
      if ( this -> _bytes != nullptr )
        this -> Free ( ( void * ) this -> _bytes );
    }

    short MachineState::Id ( void ) {
      return this -> _id;
    }

    const char * MachineState::Type ( void ) {
      return this -> _type;
    }

    int MachineState::ByteCount ( void ) {
      return this -> _byteCount;
    }

    unsigned char * MachineState::ByteArray ( void ) {
      return this -> _bytes;
    }

    bool MachineState::IsEqual ( MachineState * toThatOne ) {
      bool isTheSame = false;

      if ( toThatOne != nullptr )
        if ( ( isTheSame = this -> _byteCount == toThatOne -> _byteCount ) )
          for ( int i = 0; i < this -> _byteCount && isTheSame; i ++ )
            isTheSame = this -> _bytes [i] == toThatOne -> _bytes [i];

      return isTheSame;
    }

    const char * MachineState::ObjectType ( void ) const {
      return "org::hardware::MachineState";
    }
  }
}
