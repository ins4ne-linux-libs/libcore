


#include <hardware/machinestates.hpp>

namespace org {
  namespace hardware {
    void MachineStates::InitializeMachineStates ( void ) {
      this -> _machineStates = new std::map <org::types::CString, std::map <int, MachineState *> *> ( );
    }
    
    MachineStates::MachineStates ( void ) {
      this -> InitializeMachineStates ( );
    }
    
    MachineStates::~MachineStates ( void ) {
      std::map <org::types::CString, std::map <int, MachineState *> *>::iterator reposIt = this -> _machineStates -> begin ( );
      std::map <int, MachineState *>::iterator statesIt;
      for ( ; reposIt != this -> _machineStates -> end ( ); ++ reposIt ) {
        statesIt = reposIt -> second -> begin ( );
        for ( ; statesIt != reposIt -> second -> end ( ); ++ statesIt )
          delete statesIt -> second;
        delete reposIt -> second;
      }
      delete this -> _machineStates;
    }

    bool MachineStates::HasRegister ( org::types::CString key ) {
      return this -> _machineStates -> count ( key ) > 0 ? true : false;
    }

    bool MachineStates::HasState ( org::types::CString registerName, int id ) {
      bool has = this -> HasRegister ( registerName );
      if ( has )
        has = this -> _machineStates -> at ( registerName ) -> count ( id ) > 0 ? true : false;
      return has;
    }

    void MachineStates::AddRegister ( org::types::CString key ) {
      if ( ! this -> HasRegister ( key ) )
        this -> _machineStates -> insert ( std::pair <org::types::CString, std::map <int, MachineState *> *> ( key, new std::map <int, MachineState *> ( ) ) );
    }

    unsigned char * MachineStates::GetRegisterAsByteArray ( org::types::CString registerName, int * finalLength ) {
      unsigned char * array = nullptr;
      if ( this -> HasRegister ( registerName ) ) {
        if ( this -> _machineStates -> at ( registerName ) -> size ( ) > 0 ) {
          int length = 0;
          int count = 0;
          int cursor = 4;
          int i = 0;
          unsigned char * temp = nullptr;
          std::map <int, MachineState *>::iterator it = this -> _machineStates -> at ( registerName ) -> begin ( );

          for ( ; it != this -> _machineStates -> at ( registerName ) -> end ( ); it ++, count ++ )
            length += it -> second -> ByteCount ( );

          length += 4;
          *finalLength = length;
          array = this -> Allocate <unsigned char> ( length );
          it = this -> _machineStates -> at ( registerName ) -> begin ( );
          for ( ; it != this -> _machineStates -> at ( registerName ) -> end ( ); it ++ ) {
            temp = it -> second -> ByteArray ( );
            for ( i = 0; i < it -> second -> ByteCount ( ); i ++ )
              array [cursor ++] = temp [i];
          }
          array [1] = ( unsigned char ) 1;
          array [3] = ( unsigned char ) count;
        } else
          *finalLength = 0;
      }
      
      return array;
    }

    unsigned char ** MachineStates::GetRegisterAsBytePaginatedArray ( org::types::CString registerName, int maximum, int ** sizes, int * pageCount ) {
      unsigned char ** pages = nullptr;
      unsigned char * temp = nullptr;
      unsigned char * currentPage = nullptr;
      int elemPageCount = 0;
      if ( this -> HasRegister ( registerName ) ) {
        if ( this -> _machineStates -> at ( registerName ) -> size ( ) > 0 ) {
          int maximumBytesPerPage = maximum;
          int length = 0;
          int count = 0;
          std::map <int, MachineState *>::iterator it = this -> _machineStates -> at ( registerName ) -> begin ( );
          for ( ; it != this -> _machineStates -> at ( registerName ) -> end ( ); it ++, count ++ )
            length += it -> second -> ByteCount ( );
          it = this -> _machineStates -> at ( registerName ) -> begin ( );
          int elemSize = it -> second -> ByteCount ( );
          int elemCountByPage = maximumBytesPerPage / elemSize;
          int pageNumber = ( count % elemCountByPage > 0 ? 1 : 0 ) + count / elemCountByPage;
          int * pagesSizes = this -> Allocate <int> ( pageNumber );
          for ( int i = 0; i < pageNumber; i ++ )
            pagesSizes [i] = elemCountByPage;
          if ( count % elemCountByPage > 0 )
            pagesSizes [pageNumber - 1] = count % elemCountByPage;
          for ( int i = 0; i < pageNumber; i ++ )
            pagesSizes [i] *= elemSize;

          *sizes = pagesSizes;
          *pageCount = pageNumber;

          int currentCount = 0;
          int currentPageCursor = 0;
          int currentPageIndex = 0;

          pages = this -> Allocate <unsigned char *> ( pageNumber );

          for ( ; it != this -> _machineStates -> at ( registerName ) -> end ( ); it ++ ) {
            if ( currentCount % elemCountByPage == 0 ) {
              if ( currentCount != 0 ) {
                currentPageIndex ++;
              }
              currentPageCursor = 0;
              pagesSizes [currentPageIndex] += 4;
              currentPage = this -> Allocate <unsigned char> ( pagesSizes [currentPageIndex] );
              pages [currentPageIndex] = currentPage;
              elemPageCount = ( pagesSizes [currentPageIndex] - 4 ) / elemSize;
              currentPage [0] = ( unsigned char ) 0x04;
              currentPage [1] = ( unsigned char ) 0x01;
              currentPage [3] = ( ( elemPageCount & 0x0000FF00 ) >> 8 );
              currentPage [3] = ( elemPageCount & 0x000000FF );
              currentPageCursor = 4;
            }

            temp = it -> second -> ByteArray ( );
            for ( int i = 0; i < it -> second -> ByteCount ( ); i ++ )
              currentPage [currentPageCursor ++] = temp [i];
            
            currentCount ++;
          }
        }
      }
      
      return pages;
    }

    MachineState * MachineStates::GetState ( org::types::CString registerName, int id ) {
      return this -> HasState ( registerName, id ) ? this -> _machineStates -> at ( registerName ) -> at ( id ) : nullptr;
    }

    void MachineStates::SetState ( org::types::CString registerName, int id, MachineState * newState ) {
      if ( ! this -> HasRegister ( registerName ) )
        this -> AddRegister ( registerName );
      if ( this -> HasState ( registerName, id ) )
        this -> _machineStates -> at ( registerName ) -> erase ( id );
      this -> _machineStates -> at ( registerName ) -> insert ( std::pair <int, MachineState *> ( id, newState ) );
    }

    bool MachineStates::SetStateIfDifferent ( org::types::CString registerName, int id, MachineState * newState ) {
      bool toInsert = true;
      if ( ! this -> HasRegister ( registerName ) )
        this -> AddRegister ( registerName );
      if ( this -> HasState ( registerName, id ) )
        if ( ( toInsert = ! this -> GetState ( registerName, id ) -> IsEqual ( newState ) ) ) {
          delete this -> _machineStates -> at ( registerName ) -> at ( id );
          this -> _machineStates -> at ( registerName ) -> erase ( id );
        }
      if ( toInsert )
        this -> _machineStates -> at ( registerName ) -> insert ( std::pair <int, MachineState *> ( id, newState ) );
      return toInsert;
    }

    const char * MachineStates::ObjectType ( void ) const {
      return "org::hardware::MachineStates";
    }
  }
}
