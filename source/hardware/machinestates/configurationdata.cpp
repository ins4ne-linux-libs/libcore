


#include <hardware/machinestates/configurationdata.hpp>

namespace org {
  namespace hardware {
    namespace machinestates {
      void ConfigurationData::InitializeConfigurationData ( void ) {
        this -> _byteCount = 9;
      }
      
      ConfigurationData::ConfigurationData ( void ) {
        this -> InitializeConfigurationData ( );
      }
      
      ConfigurationData::ConfigurationData ( unsigned char id, unsigned char itemType, unsigned char swVersionMajor, unsigned char swVersionMinor, unsigned char swVersionBuild, unsigned int updateTime ) {
        this -> InitializeConfigurationData ( );
        this -> AllocateByteArray ( );
        
        this -> _id = id;
        this -> PutIn <unsigned char> ( itemType );
        this -> PutIn <unsigned char> ( this -> _id );
        this -> PutIn <unsigned char> ( swVersionMajor );
        this -> PutIn <unsigned char> ( swVersionMinor );
        this -> PutIn <unsigned char> ( swVersionBuild );
        this -> PutIn <unsigned int> ( updateTime );
      }

      ConfigurationData::ConfigurationData ( int size, unsigned char * source ) {
        this -> InitializeConfigurationData ( );
        this -> CopyIn ( size, source );
      }
      
      ConfigurationData::~ConfigurationData ( void ) {

      }

      const char * ConfigurationData::ObjectType ( void ) const {
        return "org::hardware::machinestates::ConfigurationData";
      }
    }
  }
}
