


#include <hardware/machinestates/counter.hpp>

namespace org {
  namespace hardware {
    namespace machinestates {
      void Counter::InitializeCounter ( void ) {
        this -> _byteCount = 5;
      }
      
      Counter::Counter ( void ) {
        this -> InitializeCounter ( );
      }

      Counter::Counter ( unsigned char id, int value ) {
        this -> InitializeCounter ( );
        this -> AllocateByteArray ( );
        this -> _id = id;
        this -> PutIn <unsigned char> ( this -> _id );
        this -> PutIn <int> ( value );
      }

      Counter::Counter ( int size, unsigned char * source ) {
        this -> InitializeCounter ( );
        this -> CopyIn ( size, source );
        if ( this -> _bytes != nullptr )
          this -> _id = this -> _bytes [0];
      }
      
      Counter::~Counter ( void ) {

      }

      const char * Counter::ObjectType ( void ) const {
        return "org::hardware::machinestates::Counter";
      }
    }
  }
}
