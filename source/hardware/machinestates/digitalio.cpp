


#include <hardware/machinestates/digitalio.hpp>

namespace org {
  namespace hardware {
    namespace machinestates {
      void DigitalIO::InitializeDigitalIO ( void ) {
        this -> _byteCount = 1;
      }
      
      DigitalIO::DigitalIO ( void ) {
        this -> InitializeDigitalIO ( );
      }

      DigitalIO::DigitalIO ( unsigned char id, bool value ) {
        this -> InitializeDigitalIO ( );
        this -> AllocateByteArray ( );

        this -> _id = id;
        this -> PutIn <unsigned char> ( this -> _id | ( value ? 0x80 : 0x00 ) );
      }

      DigitalIO::DigitalIO ( unsigned char value ) {
        this -> InitializeDigitalIO ( );
        this -> AllocateByteArray ( );
        this -> PutIn <unsigned char> ( value );
        this -> _id = this -> _bytes [0] & 0x7F;
      }

      DigitalIO::DigitalIO ( int size, unsigned char * source ) {
        this -> InitializeDigitalIO ( );
        this -> CopyIn ( size, source );
        if ( this -> _bytes != nullptr )
          this -> _id = this -> _bytes [0] & 0x7F;
      }
      
      DigitalIO::~DigitalIO ( void ) {

      }

      const char * DigitalIO::ObjectType ( void ) const {
        return "org::hardware::machinestates::DigitalIO";
      }
    }
  }
}
