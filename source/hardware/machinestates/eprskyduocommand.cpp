
#include <hardware/machinestates/eprskyduocommand.hpp>

#include <tools/misc/stringextension.hpp>

namespace org {
  namespace hardware {
    namespace machinestates {
      void EprSkyduoCommand::InitializeEprSkyduoCommand ( void ) {
        this -> _byteCount = 8;
      }
      
      EprSkyduoCommand::EprSkyduoCommand ( unsigned char command, unsigned short family, unsigned short category, unsigned short preset, const char * name ) {
        this -> InitializeEprSkyduoCommand ( );
        this -> _byteCount += org::tools::misc::CString::GetCCharArrayLength ( name );
        this -> AllocateByteArray ( );
        
        this -> PutIn <unsigned char> ( command );
        this -> PutIn <unsigned short> ( family );
        this -> PutIn <unsigned short> ( category );
        this -> PutIn <unsigned short> ( preset );
        this -> CopyStringIn ( name );

        this -> _command = command;
        this -> _family = family;
        this -> _category = category;
        this -> _preset = preset;
        this -> _name = nullptr;
      }

      EprSkyduoCommand::EprSkyduoCommand ( int size, unsigned char * source ) {
        this -> InitializeEprSkyduoCommand ( );
        this -> CopyIn ( size, source );
        if ( this -> _bytes != nullptr ) {
          this -> ResetCursor ( );
          this -> _command = this -> PutOut <unsigned char> ( );
          this -> _family = this -> PutOut <unsigned short> ( );
          this -> _category = this -> PutOut <unsigned short> ( );
          this -> _preset = this -> PutOut <unsigned short> ( );
          this -> _name = this -> PutOutString ( );
        }
      }

      unsigned char EprSkyduoCommand::Command ( void ) {
        return this -> _command;
      }

      unsigned short EprSkyduoCommand::Family ( void ) {
        return this -> _family;
      }

      unsigned short EprSkyduoCommand::Category ( void ) {
        return this -> _category;
      }

      unsigned short EprSkyduoCommand::Preset ( void ) {
        return this -> _preset;
      }

      const char * EprSkyduoCommand::Name ( void ) {
        return this -> _name;
      }


      const char * EprSkyduoCommand::ObjectType ( void ) const {
        return "org::hardware::machinestates::EprSkyduoCommand";
      }
    }
  }
}
