
#include <hardware/machinestates/eprskyduopairingcommand.hpp>

#include <tools/misc/stringextension.hpp>

namespace org {
  namespace hardware {
    namespace machinestates {
      void EprSkyduoPairingCommand::InitializeEprSkyduoPairingCommand ( void ) {
        this -> _byteCount = 67;
      }
      
      EprSkyduoPairingCommand::EprSkyduoPairingCommand ( unsigned char command, const char * pnc, const char * sn ) {
        this -> InitializeEprSkyduoPairingCommand ( );
        this -> AllocateByteArray ( );
        
        this -> PutIn <unsigned char> ( command );
        this -> CopyStringInMask ( pnc, 33 );
        this -> CopyStringInMask ( sn, 33 );
      }

      const char * EprSkyduoPairingCommand::ObjectType ( void ) const {
        return "org::hardware::machinestates::EprSkyduoPairingCommand";
      }
    }
  }
}
