
#include <hardware/machinestates/eprskyduoslavestate.hpp>

#include <tools/misc/stringextension.hpp>

namespace org {
  namespace hardware {
    namespace machinestates {
      void EprSkyduoSlaveState::InitializeEprSkyduoSlaveState ( void ) {
        this -> _byteCount = 8;
      }
      
      EprSkyduoSlaveState::EprSkyduoSlaveState ( unsigned char state, unsigned short family, unsigned short category, unsigned short preset, const char * name ) {
        this -> InitializeEprSkyduoSlaveState ( );
        this -> _byteCount += org::tools::misc::CString::GetCCharArrayLength ( name );
        this -> AllocateByteArray ( );
        
        this -> PutIn <unsigned char> ( state );
        this -> PutIn <unsigned short> ( family );
        this -> PutIn <unsigned short> ( category );
        this -> PutIn <unsigned short> ( preset );
        this -> CopyStringIn ( name );
      }

      EprSkyduoSlaveState::EprSkyduoSlaveState ( int size, unsigned char * source ) {
        this -> InitializeEprSkyduoSlaveState ( );
        this -> CopyIn ( size, source );
        if ( this -> _bytes != nullptr ) {
          this -> ResetCursor ( );
          this -> _state = this -> PutOut <unsigned char> ( );
          this -> _family = this -> PutOut <unsigned short> ( );
          this -> _category = this -> PutOut <unsigned short> ( );
          this -> _preset = this -> PutOut <unsigned short> ( );
          this -> _name = this -> PutOutString ( );
        }
      }

      unsigned char EprSkyduoSlaveState::State ( void ) {
        return this -> _state;
      }

      unsigned short EprSkyduoSlaveState::Family ( void ) {
        return this -> _family;
      }

      unsigned short EprSkyduoSlaveState::Category ( void ) {
        return this -> _category;
      }

      unsigned short EprSkyduoSlaveState::Preset ( void ) {
        return this -> _preset;
      }

      const char * EprSkyduoSlaveState::Name ( void ) {
        return this -> _name;
      }


      const char * EprSkyduoSlaveState::ObjectType ( void ) const {
        return "org::hardware::machinestates::EprSkyduoSlaveState";
      }
    }
  }
}
