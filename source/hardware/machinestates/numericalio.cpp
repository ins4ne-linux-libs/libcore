


#include <hardware/machinestates/numericalio.hpp>

#include <constants/libcore.hpp>
#include <lib/math.hpp>

namespace org {
  namespace hardware {
    namespace machinestates {
      unsigned char _floatingPointCount;

      void NumericalIO::InitializeNumericalIO ( void ) {
        this -> _byteCount = 6;
        this -> _floatingPointCount = -2;
        this -> SetConfigurationMask ( SETTING_NUMERICAL_IO );
      }
      
      void NumericalIO::FinishAutoConfiguration ( void ) {
        this -> PutSettingAs <int>  ( SETTING_BYTE_COUNT            , &( this -> _byteCount           ) );
        this -> PutSettingAs <unsigned char> ( SETTING_FLOATING_POINT_COUNT  , &( this -> _floatingPointCount  ) );
      }
      
      NumericalIO::NumericalIO ( void ) {
        this -> InitializeNumericalIO ( );
      }

      NumericalIO::NumericalIO ( unsigned char id, double value, unsigned char unit ) {
        this -> InitializeNumericalIO ( );
        this -> AllocateByteArray ( );
        
        this -> _id = id;
        this -> PutIn <unsigned char> ( this -> _id );
        this -> PutIn <int> ( ( int ) ( value * lib::Math::Pow ( 10.0, -1 * ( ( char ) this -> _floatingPointCount ) ) ) );
        this -> PutIn <unsigned char> ( this -> _floatingPointCount );
        // this -> PutIn <unsigned char> ( unit );
      }

      NumericalIO::NumericalIO ( unsigned char id, int coefficient, unsigned char exponent ) {
        this -> InitializeNumericalIO ( );
        this -> AllocateByteArray ( );
        
        this -> _id = id;
        this -> PutIn <unsigned char> ( this -> _id );
        this -> PutIn <int> ( coefficient );
        this -> PutIn <unsigned char> ( exponent );
      }

      NumericalIO::NumericalIO ( int size, unsigned char * source ) {
        this -> InitializeNumericalIO ( );
        this -> CopyIn ( size, source );
        if ( this -> _bytes != nullptr )
          this -> _id = this -> _bytes [0];
      }
      
      NumericalIO::~NumericalIO ( void ) {

      }

      const char * NumericalIO::ObjectType ( void ) const {
        return "org::hardware::machinestates::NumericalIO";
      }
    }
  }
}
