


#include <hardware/machinestates/parameter.hpp>

#include <constants/libcore.hpp>
#include <lib/math.hpp>

namespace org {
  namespace hardware {
    namespace machinestates {
      void Parameter::InitializeParameter ( void ) {
        this -> _byteCount = 6;
        this -> _floatingPointCount = -2;
        this -> SetConfigurationMask ( SETTING_PARAMETERS );
      }
      
      void Parameter::FinishAutoConfiguration ( void ) {
        this -> PutSettingAs <int>  ( SETTING_BYTE_COUNT            , &( this -> _byteCount           ) );
        this -> PutSettingAs <unsigned char> ( SETTING_FLOATING_POINT_COUNT  , &( this -> _floatingPointCount  ) );
      }
      
      Parameter::Parameter ( void ) {
        this -> InitializeParameter ( );
      }

      Parameter::Parameter ( unsigned char id, double value ) {
        this -> InitializeParameter ( );
        this -> AllocateByteArray ( );
        
        this -> _id = id;
        this -> PutIn <unsigned char> ( this -> _id );
        this -> PutIn <int> ( ( int ) ( value * lib::Math::Pow ( 10.0, -1 * ( ( char ) this -> _floatingPointCount ) ) ) );
        this -> PutIn <unsigned char> ( this -> _floatingPointCount );
      }

      Parameter::Parameter ( unsigned char id, int coefficient, unsigned char exponent ) {
        this -> InitializeParameter ( );
        this -> AllocateByteArray ( );
        
        this -> _id = id;
        this -> PutIn <unsigned char> ( this -> _id );
        this -> PutIn <int> ( coefficient );
        this -> PutIn <unsigned char> ( exponent );
      }

      Parameter::Parameter ( int size, unsigned char * source ) {
        this -> InitializeParameter ( );
        this -> CopyIn ( size, source );
        if ( this -> _bytes != nullptr )
          this -> _id = this -> _bytes [0];
      }
      
      Parameter::~Parameter ( void ) {

      }

      const char * Parameter::ObjectType ( void ) const {
        return "org::hardware::machinestates::Parameter";
      }
    }
  }
}
