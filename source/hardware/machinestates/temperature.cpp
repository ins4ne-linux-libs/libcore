


#include <hardware/machinestates/temperature.hpp>

#include <constants/libcore.hpp>
#include <lib/math.hpp>

namespace org {
  namespace hardware {
    namespace machinestates {
      void Temperature::InitializeTemperature ( void ) {
        this -> _byteCount = 5;
        this -> _floatingPointCount = -2;
        this -> _minimumChange = 1.0;
        this -> SetConfigurationMask ( SETTING_TEMPERATURES );
      }
      
      void Temperature::FinishAutoConfiguration ( void ) {
        this -> PutSettingAs <int>    ( SETTING_BYTE_COUNT            , &( this -> _byteCount           ) );
        this -> PutSettingAs <int>    ( SETTING_FLOATING_POINT_COUNT  , &( this -> _floatingPointCount  ) );
        this -> PutSettingAs <double> ( SETTING_MINIMUM_CHANGE        , &( this -> _minimumChange       ) );
      }
      
      Temperature::Temperature ( void ) {
        this -> InitializeTemperature ( );
      }

      Temperature::Temperature ( unsigned char id, unsigned char unit, double value ) {
        this -> InitializeTemperature ( );
        this -> AllocateByteArray ( );
        this -> _id = id;
        this -> PutIn <unsigned char> ( this -> _id );
        this -> PutIn <unsigned char> ( unit );
        this -> PutIn <short> ( ( short ) ( value * lib::Math::Pow ( 10.0, -1 * this -> _floatingPointCount ) ) );
        this -> PutIn <unsigned char> ( ( char ) this -> _floatingPointCount );
      }

      Temperature::Temperature ( unsigned char id, unsigned char unit, short coefficient, unsigned char exponent ) {
        this -> InitializeTemperature ( );
        this -> AllocateByteArray ( );
        this -> _id = id;
        this -> PutIn <unsigned char> ( this -> _id );
        this -> PutIn <unsigned char> ( unit );
        this -> PutIn <short> ( coefficient );
        this -> PutIn <unsigned char> ( exponent );
      }

      Temperature::Temperature ( int size, unsigned char * source ) {
        this -> InitializeTemperature ( );
        this -> CopyIn ( size, source );
        if ( this -> _bytes != nullptr )
          this -> _id = this -> _bytes [0];
      }
      
      Temperature::~Temperature ( void ) {

      }

      bool Temperature::IsEqual ( MachineState * toThatOne ) {
        double mine = org::tools::misc::Convert::PutOut <short> ( &( this -> _bytes [2] ), -1, 0 )* lib::Math::Pow ( 10.0, -1 * this -> _bytes [4] );
        double other = org::tools::misc::Convert::PutOut <short> ( &( toThatOne -> ByteArray ( ) [2] ), -1, 0 ) * lib::Math::Pow ( 10.0, -1 * toThatOne -> ByteArray ( ) [4] );
        double max = mine > other ? mine : other;
        double min = mine <= other ? mine : other;

        if ( ( max - min ) < this -> _minimumChange )
          return true;
        else
          return false;
      }

      const char * Temperature::ObjectType ( void ) const {
        return "org::hardware::machinestates::Temperature";
      }
    }
  }
}
