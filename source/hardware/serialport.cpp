


#include <hardware/serialport.hpp>

#include <constants/libcore.hpp>
#include <lib/freax/fcntl.hpp>
#include <lib/freax/unistd.hpp>
#include <abstraction/serialport.hpp>

bool lastmessage = false;

namespace org {
  namespace hardware {
    void SerialPort::SerialPortEvent ( org::eventing::EventArgs <void *, char *, unsigned char *> * args ) {
      if ( args != nullptr ) {
        this -> TriggerEvent ( args -> GetArgs ( ) );
        delete args;
      }
    }

    void SerialPort::TriggerEvent ( unsigned char * data ) {
      if ( this -> _byteReceivedEvent != nullptr )
        this -> _byteReceivedEvent -> Call ( ( void * ) this, data );
      else
        this -> Free ( data );
    }

    void SerialPort::InitializeSerialPort ( void ) {
      this -> _readMode           = false;
      this -> _writeMode          = false;
      this -> _stopping           = false;
      this -> _toFlush            = true;
      this -> _name               = nullptr;
      this -> _portDescriptor     = -1;
      this -> _readTimeout        = 5;
      this -> _readMinimum        = 0;
      this -> _firstReading       = true;
      this -> _speed              = 0;
      this -> _byteReceivedEvent  = nullptr;
      this -> _readTask           = nullptr;
      this -> _simulationEnable   = false;
      this -> _openingSimulation  = true;
      this -> _simulationStream   = nullptr;
      this -> _readTaskEvent      = new org::eventing::EventDelegateCall <SerialPort, unsigned char> ( this, &SerialPort::SerialPortEvent );
    }
    
    void SerialPort::FinishAutoConfiguration ( void ) {
      org::entity::LoggingConfiguredEntity::FinishAutoConfiguration ( );

      this -> PutSettingAs <const char *> ( SETTING_NAME                , &( this -> _name              ) );
      this -> PutSettingAs <int>          ( SETTING_SPEED               , &( this -> _speed             ) );
      this -> PutSettingAs <int>          ( SETTING_READ_TIMEOUT        , &( this -> _readTimeout       ) );
      this -> PutSettingAs <int>          ( SETTING_READ_MINIMUM        , &( this -> _readMinimum       ) );
      this -> PutSettingAs <bool>         ( SETTING_READ_MODE           , &( this -> _readMode          ) );
      this -> PutSettingAs <bool>         ( SETTING_WRITE_MODE          , &( this -> _writeMode         ) );
      this -> PutSettingAs <bool>         ( SETTING_TO_FLUSH            , &( this -> _toFlush           ) );
      this -> PutSettingAs <bool>         ( SETTING_SIMULATOR_ENABLE    , &( this -> _simulationEnable  ) );
      this -> PutSettingAs <bool>         ( SETTING_SIMULATOR_OPEN_DOOR , &( this -> _openingSimulation ) );
      
      const char * type = this -> ContainsSettingAs <const char *> ( SETTING_SIMULATOR_TYPE , nullptr );
      const char * name = this -> ContainsSettingAs <const char *> ( SETTING_SIMULATOR_NAME , nullptr );
      
      if ( this -> _simulationEnable )
        this -> _simulationStream = org::virtualization::VirtualStreamFactory::New <unsigned char> ( type, name );
        
      lib::Stdlib::Free ( ( void * ) type );
      lib::Stdlib::Free ( ( void * ) name );
    }
    
    SerialPort::SerialPort ( void ) {
      this -> InitializeSerialPort ( );
    }
    
    SerialPort::SerialPort ( const char * name ) {
      this -> InitializeSerialPort ( );
      this -> SetConfigurationMask ( name );
    }

    SerialPort::~SerialPort ( void ) {
      lib::Stdlib::Free ( ( void * ) this -> _name );
      if ( this -> _readTask != nullptr ) {
        this -> _readTask -> Terminate ( );
        delete this -> _readTask;
      }
      delete this -> _readTaskEvent;
    }
 
    unsigned char * SerialPort::Pull ( void ) {
      return this -> Read ( );
    }

    bool SerialPort::IsOpen ( void ) {
      return this -> _portDescriptor > 0;
    }

    bool SerialPort::Open ( void ) {
      this -> Debug ( LOG_TRY_OPEN_SERIAL_PORT );
      if ( this -> _readMode || this -> _writeMode ) {
        if ( this -> _simulationEnable )
          this -> _portDescriptor = this -> _openingSimulation ? 1 : 0;
        else {
          this -> _portDescriptor = lib::freax::Fcntl::Open ( this -> _name, this -> _readMode, this -> _writeMode, this -> _readMode & this -> _writeMode, false,
                                                                false, false, false, true, false, false, false, false );
        }
        
        if ( this -> IsOpen ( ) ) {
          this -> Notice ( LOG_SERIAL_PORT_NOW_OPEN );
          this -> ConfigureForNIU ( );
          this -> _firstReading = this -> _toFlush;
        } else
          this -> Debug ( LOG_FAILED );
      }
      return this -> IsOpen ( );
    }

    void SerialPort::ConfigureForNIU ( void ) {
      if ( ! this -> _simulationEnable )
        org::abstraction::SerialPort::Configure ( this -> _portDescriptor, this -> _readTimeout * 10, this -> _speed, 8, true, false, true, true );
    }

    void SerialPort::SetSpeed ( int newSpeed ) {
      this -> _speed = newSpeed;
    }

    int SerialPort::Speed ( void ) {
      return this -> _speed;
    }

    void SerialPort::Close ( void ) {
      if ( this -> IsOpen ( ) ) {
        if ( ! this -> _simulationEnable )
          if ( lib::freax::Unistd::Close ( this -> _portDescriptor ) < 0 )
            this -> Warning ( LOG_CLOSING_FAILED );
          else
            this -> Debug ( LOG_SERIAL_PORT_CLOSED );
        else
          this -> Debug ( LOG_SERIAL_PORT_CLOSED );
      }
      this -> _portDescriptor = -1;
    }

    int SerialPort::Write ( unsigned char * bytes, int size ) {
      int written = 0;
      if ( ! this -> IsOpen ( ) )
        this -> Open ( );
      if ( this -> IsOpen ( ) ) {
        if ( ! this -> _simulationEnable )
          written = lib::freax::Unistd::Write ( this -> _portDescriptor, bytes, size );
        else if ( this -> _simulationStream != nullptr )
          written = this -> _simulationStream -> PutPack ( bytes, size );
      }
      return written;
    }

    bool SerialPort::HasBytesInBuffer ( void ) {
      return this -> _simulationEnable ?
        this -> _simulationStream -> Count ( ) :
        org::abstraction::SerialPort::ByteCountInBuffer ( this -> _portDescriptor ) > 0;
    }

    int SerialPort::Read ( unsigned char * bufferToFill, int countToRead ) {
      int read = 0;
      if ( this -> _simulationEnable ) { 
        if ( this -> _simulationStream != nullptr )
          read = this -> _simulationStream -> GetPack ( bufferToFill, countToRead );
      } else if ( this -> _readTimeout > 0 || this -> HasBytesInBuffer ( ) )
        read = lib::freax::Unistd::Read ( this -> _portDescriptor, bufferToFill, countToRead );
      return read;
    }

    unsigned char * SerialPort::Read ( void ) {
      unsigned char buffer [1];
      unsigned char * readByte = nullptr;
      if ( ! this -> IsOpen ( ) )
        this -> Open ( );
      if ( this -> IsOpen ( ) ) {
        if ( this -> Read ( buffer, 1 ) >= 1 ) {
          if ( this -> _firstReading && buffer [0] != 0 )
            this -> _firstReading = false;
          if ( ! this -> _firstReading ) {
            readByte = this -> Allocate <unsigned char> ( 2 );
            readByte [0] = buffer [0];
            char message [50];
            sprintf ( message, "Read something : %.2hhX", buffer [0] );
            this -> Debug ( message );
            if ( this -> _stopping ) {
              lib::Stdlib::Free ( ( void * ) readByte );
              readByte = nullptr;
            }
          }
        }
      }
      return readByte;
    }
    
    void SerialPort::ReadAsTask ( void ) {
      this -> _stopping = false;
      if ( this -> _readTask == nullptr )
        this -> _readTask = new org::threads::TunnelPullEventTask <unsigned char> ( false, this, this -> _readTaskEvent, CONSTANT_SERIAL_READER_THREAD_NAME );
      if ( !( this -> _readTask -> IsExecuting ( ) ) )
        this -> _readTask -> Start ( );
    }
    
    void SerialPort::StopReadingTask ( void ) {
      this -> _stopping = true;
      if ( this -> _readTask != nullptr )
        this -> _readTask -> Terminate ( );
    }

    void SerialPort::PlugByteReceivedEvent ( org::eventing::EventCall <unsigned char> * byteReceivedEvent ) {
      if ( this -> _byteReceivedEvent != nullptr )
        this -> Delete <org::eventing::EventCall <unsigned char>> ( this -> _byteReceivedEvent );
      this -> _byteReceivedEvent = byteReceivedEvent;
    }

    const char * SerialPort::ObjectType ( void ) const {
      return "org::hardware::SerialPort";
    }
  }
}
