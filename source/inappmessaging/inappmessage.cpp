


#include <inappmessaging/inappmessage.hpp>

#include <constants/libcore.hpp>
#include <tools/misc/stringextension.hpp>
#include <lib/stdlib.hpp>

namespace org {
  namespace inappmessaging {
    void InAppMessage::InitializeInAppMessage ( void ) {
      this -> _command = nullptr;
      this -> _recipient = nullptr;
      this -> _parameterCount = 0;
      this -> _parameters = nullptr;
      this -> _message = nullptr;
    }

    void InAppMessage::ReadMessage ( void ) {
      int count = 0;
      int tmp = 0;
      char** spaceSplit = org::tools::misc::CString::Split ( this -> _message, LITERAL_HYPHEN, &count );
      if ( count > 0 ) {
        char** main = org::tools::misc::CString::Split ( spaceSplit [0], LITERAL_SPACE, &tmp );
        lib::Stdlib::Free ( ( void * ) spaceSplit [0] );
        if ( tmp == 2 ) {
          this -> _recipient = main [0];
          this -> _command = main [1];
          lib::Stdlib::Free ( ( void * ) main );
        }
        if ( count > 1 ) {
          this -> _parameters = this -> Allocate <char**> ( count - 1 );
          char** tmpSplit = nullptr;
          for ( int i = 0; i < count - 1; i ++ ) {
            this -> _parameters [i] = this -> Allocate <char*> ( 2 );
            tmpSplit = org::tools::misc::CString::Split ( spaceSplit [i + 1], LITERAL_SPACE, &tmp, 2, true, true );
            lib::Stdlib::Free ( ( void * ) spaceSplit [i + 1] );
            this -> _parameters [i] [0] = tmpSplit [0];
            if ( tmp > 1 )
              this -> _parameters [i] [1] = tmpSplit [1];
            lib::Stdlib::Free ( ( void * ) tmpSplit );
          }
        }
      }
      lib::Stdlib::Free ( ( void * ) spaceSplit );
      this -> _parameterCount = count - 1;
    }
    
    InAppMessage::InAppMessage ( char* message ) {
      this -> InitializeInAppMessage ( );
      this -> _message = org::tools::misc::CString::GetCopyOfCString ( message );
      this -> ReadMessage ( );
    }

    InAppMessage::~InAppMessage ( void ) {
      lib::Stdlib::Free ( ( void * ) this -> _message );
      lib::Stdlib::Free ( ( void * ) this -> _recipient );
      lib::Stdlib::Free ( ( void * ) this -> _command );
      if ( this -> _parameters != nullptr ) {
        for ( int i = 0; i < this -> _parameterCount; i ++ ) {
          lib::Stdlib::Free ( ( void * ) this -> _parameters [i] [0] );
          lib::Stdlib::Free ( ( void * ) this -> _parameters [i] [1]);
          lib::Stdlib::Free ( ( void * ) this -> _parameters [i] );
        }
        lib::Stdlib::Free ( ( void * ) this -> _parameters );
      }
    }

    char* InAppMessage::Message ( void ) {
      return this -> _message;
    }

    int InAppMessage::ParameterCount ( void ) {
      return this -> _parameterCount;
    }

    char* InAppMessage::Command ( void ) {
      return this -> _command;
    }

    char** InAppMessage::ParameterAt ( int index ) {
      char** parameter = nullptr;
      if ( index < this -> _parameterCount )
        parameter = this -> _parameters [index];
      return parameter;
    }

    char*** InAppMessage::Parameters ( void ) {
      return this -> _parameters;
    }

    char* InAppMessage::Recipient ( void ) {
      return this -> _recipient;
    }
  }
}
