


#include <inappmessaging/inappmessageable.hpp>

#include <tools/misc/stringextension.hpp>
#include <lib/stdlib.hpp>

namespace org {
  namespace inappmessaging {
    void InAppMessageable::InitializeInAppMessageable ( void ) {
      this -> _inAppMessageName = nullptr;
      this -> _inAppMessageCenter = nullptr;
    }
    
    void InAppMessageable::SendInAppMessage ( InAppMessage* inAppMessage ) {
        delete inAppMessage;
    }

    void InAppMessageable::SetInAppMessageableName ( const char * name ) {
      if ( this -> _inAppMessageName != nullptr )
        this -> Free ( this -> _inAppMessageName );
      this -> _inAppMessageName = org::tools::misc::CString::GetCopyOfCString ( name );
      this -> AddToInAppMessageCenter ( );
    }
    
    InAppMessageable::InAppMessageable ( void ) {
      this -> InitializeInAppMessageable ( );
    }
    
    InAppMessageable::~InAppMessageable ( void ) {
      lib::Stdlib::Free ( ( void * ) this -> _inAppMessageName );
    }

    InAppMessage* InAppMessageable::PostInAppMessage ( InAppMessage* inAppMessage ) {
      return nullptr;
    }

    char* InAppMessageable::InAppMessageableName ( void ) {
      return this -> _inAppMessageName;
    }

    void InAppMessageable::SetInAppMessageCenter ( InAppMessageCenter* inAppMessageCenter ) {
      this -> _inAppMessageCenter = inAppMessageCenter;
    }
  }
}
