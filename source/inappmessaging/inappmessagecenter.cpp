


#include <inappmessaging/inappmessagecenter.hpp>

namespace org {
  namespace inappmessaging {
    void InAppMessageCenter::InitializeInAppMessageCenter ( void ) {
      this -> _messageableRepository = new std::map <org::types::CString, InAppMessageable*> ( );
    }
    
    InAppMessageCenter::InAppMessageCenter ( void ) {
      this -> InitializeInAppMessageCenter ( );
    }
    
    InAppMessageCenter::~InAppMessageCenter ( void ) {
      delete this -> _messageableRepository;
    }

    bool InAppMessageCenter::ContainsInAppMessageable ( char* name ) {
      bool answer = name != nullptr;
      if ( answer )
        answer = this -> _messageableRepository -> count ( name ) > 0;
      return answer;
    }

    void InAppMessageCenter::AddMessageable ( InAppMessageable* messageable ) {
      if ( messageable != nullptr )
        if ( messageable -> InAppMessageableName ( ) != nullptr )
          if ( !( this -> ContainsInAppMessageable ( messageable -> InAppMessageableName ( ) ) ) )
            ( *( this -> _messageableRepository ) ) [messageable -> InAppMessageableName ( )] = messageable;
    }

    InAppMessage* InAppMessageCenter::Post ( InAppMessage* message ) {
      InAppMessage* answer = nullptr;
      if ( this -> ContainsInAppMessageable ( message -> Recipient ( ) ) )
        answer = ( *( this -> _messageableRepository ) ) [message -> Recipient ( )] -> PostInAppMessage ( message );
      delete message;
      return answer;
    }
  }
}
