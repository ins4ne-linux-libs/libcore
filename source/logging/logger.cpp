


#include <logging/logger.hpp>

#include <lib/stdlib.hpp>

int LoggerCount = 0;

namespace org {
  namespace logging {
    unsigned char Logger::LoggerSystemInitialized;
    char**        Logger::LevelsStrings;
    char*         Logger::Format;
    char*         Logger::TimeFormat;
    LoggingLevel  Logger::DefaultLevel;
    char*         Logger::DefaultName;
    char*         Logger::DefaultLink;

    void Logger::InitializeLoggerSystem ( void ) {
      LoggerCount ++;
      if ( LoggerSystemInitialized == 0 ) {
        LoggerSystemInitialized = 1;
        SetLevelsString         ( ( char** ) org::process::MemoryManager::Malloc ( LOG_LEVEL_NUMBER, sizeof ( char* ) ) );
        SetLevelString          ( TRACE,    ( char* ) "TRACE"     );
        SetLevelString          ( DEBUG,    ( char* ) "DEBUG"     );
        SetLevelString          ( INFO,     ( char* ) "INFO"      );
        SetLevelString          ( NOTICE,   ( char* ) "NOTICE"    );
        SetLevelString          ( WARNING,  ( char* ) "WARNING"   );
        SetLevelString          ( ERROR,    ( char* ) "ERROR"     );
        SetLevelString          ( CRITICAL, ( char* ) "CRITICAL"  );
        SetDefaultFormat        ( ( char* ) "\n[%s] [%s] %s: %s"  );
        SetDefaultTimeFormat    ( ( char* ) "%Y:%m:%d %H:%M:%S"   );
        SetDefaultLevel         ( INFO );
        SetDefaultName          ( ( char* ) "Logger" );
      }
    }

    void Logger::SetLevelsString ( char** stringArray ) {
      org::process::MemoryManager::FreeMA ( ( void** ) LevelsStrings, LOG_LEVEL_NUMBER );
      LevelsStrings = stringArray;
    }

    void Logger::SetLevelString ( LoggingLevel logLevel, char* logString ) {
      org::process::MemoryManager::FreeM ( ( void* ) LevelsStrings [logLevel] );
      LevelsStrings [logLevel] = logString;
    }

    void Logger::SetDefaultFormat ( char* newFormat ) {
      lib::Stdlib::Free ( ( void* ) Format );
      Format = newFormat;
    }

    void Logger::SetDefaultTimeFormat ( char* newTimeFormat ) {
      org::process::MemoryManager::FreeM ( ( void* ) TimeFormat );
      TimeFormat = newTimeFormat;
    }

    void Logger::SetDefaultLevel ( LoggingLevel defaultLevel ) {
      DefaultLevel = defaultLevel;
    }

    void Logger::SetDefaultName ( char* defaultName ) {
      DefaultName = defaultName;
    }

    void Logger::GetArgs ( char** toFill, LoggingLevel level, char* message ) {
      toFill [0] = LevelsStrings [level];
      toFill [1] = org::tools::misc::CString::GetCTime ( (char*) org::process::MemoryManager::Malloc ( 24, sizeof (char) ), this -> _timeFormat );
      toFill [2] = this -> _loggingName;
      toFill [3] = message;
    }

    void Logger::FreeArgs ( char** args ) {
      org::process::MemoryManager::FreeM ( ( void* ) args [1] );
      org::process::MemoryManager::FreeM ( ( void* ) args [3] );
    }

    void Logger::Log ( LoggingLevel level, const char * message ) {
      this -> ThreadLock ( );
      char* safeMessage = org::tools::misc::CString::GetCopyOfCString ( message );
      if ( level >= this -> _currentLevel && this -> _outputMethod != nullptr ) {
        char* args [4];
        this -> GetArgs ( args, level, safeMessage );
        char* log = org::tools::misc::CString::FormatString ( this -> _format, 4, ( void** ) args );
        
        if ( this -> _outputMethod != nullptr )
          this -> _outputMethod ( this, level, log );

        org::process::MemoryManager::FreeM ( ( void* ) log );
        this -> FreeArgs ( args );
      } else
        org::process::MemoryManager::FreeM ( ( void* ) safeMessage );
      this -> ThreadUnlock ( );
    }

    void Logger::SetName ( char* newName ) {
      this -> _name = newName;
    }

    void Logger::SetOutputMethod ( LogOutputMethod newOutputMethod ) {
      this -> _outputMethod = newOutputMethod;
    }

    void Logger::SetFormat ( char* newFormat ) {
      this -> _format = newFormat;
    }

    void Logger::SetTimeFormat ( char* newTimeFormat ) {
      this -> _timeFormat = newTimeFormat;
    }

    void Logger::TryAutoConfiguration ( void ) {
    }
    
    void Logger::Configure ( void ) {
      this -> _name = nullptr;
      this -> _loggingName = nullptr;
      this -> _format = nullptr;
      this -> _currentLevel = INFO;
      this -> _outputMethod = nullptr;
      this -> SetFormat ( org::tools::misc::CString::GetCopyOfCString ( Logger::Format ) );
      this -> SetTimeFormat ( org::tools::misc::CString::GetCopyOfCString ( Logger::TimeFormat ) );
      this -> _loggingName = org::tools::misc::CString::GetCopyOfCString ( this -> _name );
      this -> TryAutoConfiguration ( );
    }
    
    Logger::Logger ( void ) {
      InitializeLoggerSystem ( );
      this -> MakeTaskable ( );
      this -> Configure ( );
    }

    Logger::~Logger ( void ) {
      LoggerCount --;
      if ( LoggerCount == 0 ) {
        lib::Stdlib::Free ( ( void * ) LevelsStrings );
        LoggerSystemInitialized = 0;
      }
      lib::Stdlib::Free ( ( void* ) this -> _name );
      lib::Stdlib::Free ( ( void* ) this -> _format );
      lib::Stdlib::Free ( ( void* ) this -> _timeFormat );
      lib::Stdlib::Free ( ( void* ) this -> _loggingName );
    }

    void Logger::SetLevel ( LoggingLevel newLogLevel ) {
      this -> _currentLevel = newLogLevel;
    }

    void Logger::LogAtLevel ( LoggingLevel level, char* message ) {
      this -> Log ( level, message );
    }

    void Logger::Trace ( const char * message ) {
      this -> Log ( TRACE, message );
    }

    void Logger::Debug ( const char * message ) {
      this -> Log ( DEBUG, message );
    }

    void Logger::Info ( const char * message ) {
      this -> Log ( INFO, message );
    }

    void Logger::Notice ( const char * message ) {
      this -> Log ( NOTICE, message );
    }

    void Logger::Warning ( const char * message ) {
      this -> Log ( WARNING, message );
    }

    void Logger::Error ( const char * message ) {
      this -> Log ( ERROR, message );
    }

    void Logger::Critical ( const char * message ) {
      this -> Log ( CRITICAL, message );
    }

    char* Logger::GetName ( void ) {
      return this -> _name;
    }

    LoggingLevel Logger::GetLevel ( void ) {
      return this -> _currentLevel;
    }
  }
}
