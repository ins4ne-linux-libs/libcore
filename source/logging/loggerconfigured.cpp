


#include <logging/loggerconfigured.hpp>

#include <constants/libcore.hpp>
#include <lib/freax/syslog.hpp>
#include <filesystem/linkmanager.hpp>

namespace org {
  namespace logging {
    void LoggerConfigured::InitializeLoggerConfigured ( void ) {
      this -> _linkName = nullptr;
      this -> _addNewLineBefore = true;
    }
    
    int LoggerConfigured::Syslog ( Logger* logger, LoggingLevel level, char* message ) {
      logger = logger;
      level = level;
      lib::freax::Syslog::SysLogInfo ( message );
      return 0;
    }

    int LoggerConfigured::LinkPrint ( Logger * logger, LoggingLevel level, char * message ) {
      LoggerConfigured * cLogger = dynamic_cast <LoggerConfigured *> ( logger );
      void * toFree = nullptr;
      if ( org::filesystem::LinkManager::Current != nullptr )
        if ( level >= logger -> GetLevel ( ) ) {
          if ( cLogger -> _addNewLineBefore && ! org::tools::misc::CString::IsEqual ( cLogger -> _linkName, CONSTANT_STD_OUT_LINK_NAME ) )
            toFree = ( void * ) ( message = org::tools::misc::CString::Concatenate ( message, "\n" ) );
          org::filesystem::LinkManager::Current -> WriteTo ( ( char * ) cLogger -> _linkName, message );
          lib::Stdlib::Free ( toFree );
        }
      return 0;
    }

    void LoggerConfigured::FinishAutoConfiguration ( void ) {
      ConfiguredEntity::FinishAutoConfiguration ( );

      this -> SetOutputMethod ( &LoggerConfigured::LinkPrint );

      char * tmp = this -> _loggingName;
      this -> _loggingName    = this -> ContainsSettingAs <char *> ( "logging-name", this -> _loggingName );
      if ( this -> _loggingName != tmp )
        lib::Stdlib::Free ( ( void * ) tmp );

      tmp = this -> _format;
      this -> _format         = this -> ContainsSettingAs <char *> ( "format", this -> _format );
      if ( this -> _format != tmp )
        lib::Stdlib::Free ( ( void * ) tmp );

      tmp = this -> _timeFormat;
      this -> _timeFormat     = this -> ContainsSettingAs <char *> ( "time-format", this -> _timeFormat );
      if ( this -> _timeFormat != tmp )
        lib::Stdlib::Free ( ( void * ) tmp );

      tmp = ( char * ) this -> _linkName;
      this -> _linkName       = this -> ContainsSettingAs <const char *> ( "link-name", this -> _linkName );
      if ( this -> _linkName != tmp )
        lib::Stdlib::Free ( ( void * ) tmp );
        
      this -> _currentLevel   = ( LoggingLevel ) this -> ContainsSettingAs <int> ( "level", ( int ) this -> _currentLevel );

      if ( this -> _linkName == nullptr )
        this -> _linkName = org::tools::misc::CString::GetCopyOfCString ( ( char* ) CONSTANT_STD_OUT_LINK_NAME );

      if ( org::tools::misc::CString::IsPartiallyEqual ( this -> _linkName, ( char* ) "syslog" ) )
        this -> SetOutputMethod ( &LoggerConfigured::Syslog );
    
    }
    
    LoggerConfigured::LoggerConfigured ( const char * configurationName ) {
      this -> InitializeLoggerConfigured ( );
      this -> SetConfigurationMask ( configurationName );
    }
    
    LoggerConfigured::~LoggerConfigured ( void ) {
      lib::Stdlib::Free ( ( void * ) this -> _linkName );
    }

    const char * LoggerConfigured::ObjectType ( void ) const {
      return "org::logging::LoggerConfigured";
    }
  }
}
