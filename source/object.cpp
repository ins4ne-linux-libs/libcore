


#include <object.hpp>

#include <stdio.h>
#include <lib/stdlib.hpp>

namespace org {
  Object::Object ( void ) {
    SaveAddress ( ( void * ) this, -1 );
  }

  Object::~Object ( void ) {
    UnsaveAddress ( ( void * ) this );
  }

  const char * Object::ObjectType ( void ) const {
    return "org::Object";
  }
}
