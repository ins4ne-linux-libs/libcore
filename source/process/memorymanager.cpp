


#include <process/memorymanager.hpp>

#include <lib/stdlib.hpp>
#include <abstraction/memory.hpp>

namespace org {
  namespace process {
    void MemoryManager::Free ( void * pointer ) {
      lib::Stdlib::Free ( pointer );
    }

    void * MemoryManager::Malloc ( int count, int size ) {
      unsigned char * allocation = ( unsigned char * ) org::abstraction::Memory::Allocate ( count * size );
      for ( int i = 0; i < ( count * size ); i ++ )
        allocation [i] = 0;
      return ( void* ) allocation;
    }

    void MemoryManager::FreeM ( void * pointer ) {
      lib::Stdlib::Free ( pointer );
    }

    void MemoryManager::FreeMA ( void ** pointer, int size ) {
      if ( pointer != nullptr ) {
        unsigned char ** array = ( unsigned char ** ) pointer;
        for ( int i = 0; i < size; i ++ )
          MemoryManager::FreeM ( ( void* ) array [i] );
        MemoryManager::FreeM ( ( void* ) pointer );
      }
    }
  }
}
