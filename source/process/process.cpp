


#include <process/process.hpp>

#include <tools/misc/stringextension.hpp>
#include <lib/freax/unistd.hpp>
#include <lib/freax/sysstat.hpp>
#include <lib/freax/fcntl.hpp>
#include <lib/stdlib.hpp>
#include <lib/stdio.hpp>

#include <stdio.h>

namespace org {
  namespace process {
    void System::Daemonize ( const char * daemonFile ) {
      return Daemonize ( daemonFile, nullptr, nullptr, "r", nullptr, "r", nullptr, "r" );
    }

    void System::Daemonize ( const char * daemonFile, const char * newDir ) {
      return System::Daemonize ( daemonFile, newDir, nullptr, "r", nullptr, "r", nullptr, "r" );
    }

    void System::Daemonize ( const char * daemonFile, const char * newDir, const char * newOutputs, const char * modes ) {
      return System::Daemonize ( daemonFile, newDir, newOutputs, modes, newOutputs, modes, newOutputs, modes );
    }

    void System::DaemonizeSaveOutput ( const char * daemonFile, const char * newOutput ) {
      return System::Daemonize ( daemonFile, nullptr, nullptr, "r", newOutput, "a+", newOutput, "a+" );
    }

    void System::Daemonize ( const char * daemonFile, const char * newDir, const char * stdIn, const char * stdInMode, const char * stdOut, const char * stdOutMode, const char * stdErr, const char * stdErrMode ) {
      int fd = 0;
      char tmp [12];
      
      if ( lib::freax::Unistd::Fork ( ) > 0 )
        lib::Stdlib::Exit ( 0 );
        
      lib::freax::Unistd::SetPGid ( 0, 0 );
      lib::freax::Unistd::SetSid ( );
      
      if ( lib::freax::Unistd::Fork ( ) > 0 )
        lib::Stdlib::Exit ( 0 );
        
      for ( int i = lib::freax::Unistd::GetDTableSize ( ); i >= 3; -- i )
        lib::freax::Unistd::Close ( i );

      if ( ! lib::Stdio::RedirectStdin ( stdIn, stdInMode ) )
        lib::Stdio::PrintF ( "stdin redirection failed\n" );
      if ( ! lib::Stdio::RedirectStdout ( stdOut, stdOutMode ) )
        lib::Stdio::PrintF ( "stdin redirection failed\n" );
      if ( ! lib::Stdio::RedirectStderr ( stdErr, stdErrMode ) )
        lib::Stdio::PrintF ( "stdin redirection failed\n" );

      lib::freax::SysStat::UMask ( 0000 );

      if ( newDir != nullptr ) {
        lib::freax::SysStat::MkDir ( newDir, 0755 );
        if ( ! lib::freax::Unistd::ChDir ( newDir ) )
          lib::Stdio::PrintF ( "Daemonizing and directory changing didn't go perfectly\n" );
      }
      
      fd = lib::freax::Fcntl::Open ( daemonFile, false, false, true, false, true, false, false, false, false, false, false, false, 0666 );

      if ( 0 > lib::freax::Unistd::LockFTLock ( fd, 0 ) ) {
        lib::Stdio::PrintF ( "Pid file access problem" );
        lib::Stdlib::Exit ( 0 );
      }
      
      lib::Stdio::SnPrintF ( tmp, 12, "%d\n", lib::freax::Unistd::GetPid ( ) );
      if ( lib::freax::Unistd::Write ( fd, tmp, org::tools::misc::CString::GetCCharArrayLength ( tmp ) ) == 0 )
        lib::Stdio::PrintF ( "pid writing failed\n" );
    }

    char * System::PID ( void ) {
      char tmp [12];
      lib::Stdio::SnPrintF ( tmp, 12, "%d", lib::freax::Unistd::GetPid ( ) );
      return org::tools::misc::CString::GetCopyOfCString ( tmp );
    }
  }
}
