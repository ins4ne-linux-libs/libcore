


#include <protocol/generic.hpp>

#include <constants/libcore.hpp>
#include <lib/stdlib.hpp>

namespace org {
  namespace protocol {
    void Generic::Configure ( int partsCount, unsigned char * main, unsigned char * filling ) {
      this -> _partsCount = partsCount;
      this -> _mainParts = main;
      this -> _fillingNull = filling;
      this -> _parts = nullptr;
      this -> SetUpParts ( );
    }

    void Generic::FreeParts ( void ) {
      org::process::MemoryManager::FreeMA ( ( void** ) this -> _parts, this -> _partsCount );
    }

    void Generic::FreePart ( int index ) {
      if ( this -> _parts != nullptr )
        if ( this -> _parts [index] != nullptr )
          lib::Stdlib::Free ( ( void* ) this -> _parts [index] );
    }

    void Generic::InitializePart ( int index, int size ) {
      if ( this -> _parts != nullptr ) {
        this -> FreePart ( index );
        if ( size > 0 )
          this -> _parts [index] = ( unsigned char * ) org::process::MemoryManager::Malloc ( size, sizeof ( unsigned char ) );
        else
          this -> _parts [index] = nullptr;
      }
    }

    void Generic::InversePart ( int index1, int index2 ) {
      unsigned char * backup = this -> _parts [index1];
      this -> _parts [index1] = this -> _parts [index2];
      this -> _parts [index2] = backup;
    }

    void Generic::SetPart ( int index, unsigned char * newPart ) {
      this -> SetPart ( index, newPart, this -> GetSize ( index ) );
    }

    void Generic::SetPart ( int index, unsigned char * newPart, int size ) {
      if ( this -> _mainParts [index] == 0 )
        this -> SetSpecificPart ( index, newPart, size );
      else
        this -> FillPart ( index, newPart, size );
    }

    void Generic::UpdatePartAndSize ( int index, int size ) {
      if ( this -> _mainParts [index] == 0 ) {
        int ans = 0;
        for ( int i = 0; i < index; i ++ )
          if ( this -> _mainParts [i] == 0 )
            ans ++;
        if ( this -> _fillingNull [ans] != size )
          this -> InitializePart ( index, size );
        this -> _fillingNull [ans] = size;
      }
    }

    void Generic::SetUpParts ( void ) {
      this -> FreeParts ( );
      if ( this -> _partsCount > 0 && this -> _mainParts != nullptr ) {
        this -> _parts = ( unsigned char ** ) org::process::MemoryManager::Malloc ( this -> _partsCount, sizeof ( unsigned char * ) );
        for ( int i = 0, j = 0; i < this -> _partsCount; i ++ )
          if ( this -> _mainParts [i] != 0 )
            this -> InitializePart ( i, this -> _mainParts [i] );
          else
            this -> InitializePart ( i, this -> _fillingNull [j++] );
      }
    }

    void Generic::FillPart ( int index, unsigned char * newPart, int size ) {
      this -> UpdatePartAndSize ( index, size );
      unsigned char * part = this -> GetPart ( index );
      for ( int i = 0; i < size; i ++ )
        part [i] = newPart [i];
    }

    void Generic::FillParts ( unsigned char * rawMessage ) {
      this -> SetUpParts ( );
      if ( this -> _parts != nullptr ) {
        for ( int i = 0, j = 0, k = 0, l = 0; i < this -> _partsCount; i ++ )
          if ( this -> _mainParts [i] != 0 )
            for ( k = 0; k < this -> _mainParts [i]; k ++, l ++ )
              this -> _parts [i] [k] = rawMessage [l];
          else {
            for ( k = 0; k < this -> _fillingNull [j]; k ++, l ++)
              this -> _parts [i] [k] = rawMessage [l];
            j++;
          }
      }
    }

    int Generic::GetPartsSize ( int from, int to ) {
      int size = 0;
      for ( int i = from, j = 0; i < to; i ++ )
        size += this -> _mainParts [i] == 0 ? this -> _fillingNull [j++] : this -> _mainParts [i];
      return size;
    }
    
    unsigned char * Generic::GetParts ( int from, int to ) {
      unsigned char * message = ( unsigned char * ) org::process::MemoryManager::Malloc ( this -> GetPartsSize ( from, to ), sizeof ( unsigned char ) );
      int messageCursor = 0;
      unsigned char * part = nullptr;
      unsigned char partSize = 0;
      int i = from, j = 0;

      for ( ; i < to; i ++ )
        for ( part = this -> GetPart ( i ), partSize = this -> GetSize ( i ), j = 0; j < partSize; j ++ )
          message [messageCursor ++] = part [j];

      return message;
    }

    char* Generic::GetPartsAsHex ( int from, int to ) {
      unsigned char * message = this -> GetParts ( from, to );
      char* cString = org::tools::misc::CString::ArrayToString <unsigned char> ( message, this -> GetPartsSize ( from, to ), "%.2hhX", 2, LITERAL_SPACE );
      lib::Stdlib::Free ( ( void* ) message );
      return cString;
    }

    char* Generic::GetPartAsHex ( int index ) {
      return this -> GetPartsAsHex ( index, index + 1 );
    }
    
    Generic::Generic ( void ) {
      this -> Configure ( 0, nullptr, nullptr );
    }

    Generic::Generic  ( int partsCount, unsigned char * main, unsigned char * filling, unsigned char * message ) {
      this -> Configure ( partsCount, main, filling );
      this -> FillParts ( message );
    }

    Generic::~Generic ( void ) {
      lib::Stdlib::Free ( ( void* ) this -> _fillingNull );
      this -> FreeParts ( );
    }

    unsigned char * Generic::GetPart ( int index ) {
      return index < this -> _partsCount && index >= 0 ? this -> _parts [index] : nullptr;
    }

    void Generic::Read ( unsigned char * message  ) {
      this -> FillParts ( message );
    }

    int Generic::GetSize ( int index ) {
      int ans = 0;
      for ( int i = 0; i <= index; i ++ )
        if ( this -> _mainParts [i] == 0 )
          ans ++;
      if ( ans != 0  && this -> _mainParts [index] == 0 )
        ans = this -> _fillingNull [ans - 1];
      else
        ans = this -> _mainParts [index];
      return ans;
    }

    int Generic::GetMessageSize ( void ) {
      return this -> GetPartsSize ( 0, this -> _partsCount );
    }

    unsigned char * Generic::GetMessage ( void ) {
      return this -> GetParts ( 0, this -> _partsCount );
    }
  }
}
