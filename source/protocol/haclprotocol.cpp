


#include <protocol/haclprotocol.hpp>

#include <constants/libcore.hpp>

unsigned char * org::protocol::HaclProtocol::HaclMainProtocol = nullptr;
int counterMessages = 0;

namespace org {
  namespace protocol {
    void HaclProtocol::InitializeHaclProtocol ( void ) {
      counterMessages ++;
      InitializeMainProtocol ( );
      this -> _pageNumber = 0;
      this -> _dataPages = nullptr;
    }

    void HaclProtocol::ReadMessage ( unsigned char * message ) {
      unsigned char * filling = (unsigned char *) MemoryManager::Malloc ( 1, sizeof ( unsigned char ) );
      filling [0] = message [1] - 12;
      this -> Configure ( 10, HaclProtocol::HaclMainProtocol, filling );
      this -> FillParts ( message );
    }

    void HaclProtocol::BuildLightMessage ( unsigned char * lightMessage, int size ) {
      unsigned char * message = this -> Allocate <unsigned char> ( size + 6 );
      int offset = 5;
      message [0] = 0xC6;
      message [1] = size + 3;
      message [2] = 0x00;
      message [3] = 0x00;
      message [4] = 0xAD;
      for ( int i = 0; i < size; i ++ )
        message [i + offset] = lightMessage [i];
      this -> ReadMessage ( message );
      this -> Free ( message );
    }
    
    void HaclProtocol::InitializeMainProtocol ( void ) {
      if ( HaclProtocol::HaclMainProtocol == nullptr ) {
        HaclProtocol::HaclMainProtocol = (unsigned char *) MemoryManager::Malloc ( 10, sizeof ( unsigned char ) );
        HaclProtocol::HaclMainProtocol [0] = 1; // Header
        HaclProtocol::HaclMainProtocol [1] = 1; // Size
        HaclProtocol::HaclMainProtocol [2] = 2; // Session
        HaclProtocol::HaclMainProtocol [3] = 1; // Protocol
        HaclProtocol::HaclMainProtocol [4] = 3; // To
        HaclProtocol::HaclMainProtocol [5] = 3; // From
        HaclProtocol::HaclMainProtocol [6] = 2; // Name
        HaclProtocol::HaclMainProtocol [7] = 1; // Mode
        HaclProtocol::HaclMainProtocol [8] = 0; // Data
        HaclProtocol::HaclMainProtocol [9] = 1; // Checksum
      }
    }

    void HaclProtocol::SetSpecificPart ( int index, unsigned char * newPart, int size ) {
      this -> FillPart ( index, newPart, size );
      this -> UpdateSize ( );
    }

    int HaclProtocol::GetRawDataSize ( void ) {
      return 12 + this -> _fillingNull [0];
    }

    void HaclProtocol::UpdateSize ( void ) {
      unsigned char newSize = this -> GetRawDataSize ( );
      this -> SetPart ( 1, &newSize );
    }

    void HaclProtocol::FinishAutoConfiguration ( void ) {
      org::entity::ConfiguredEntity::FinishAutoConfiguration ( );

      int i = 0;
      int length = 5;
      char* properties [5] = {
        ( char* ) "destination",
        ( char* ) "source",
        ( char* ) "name",
        ( char* ) "mode",
        ( char* ) "data",
      }; 

      char* lightMessage = this -> Allocate <char> ( 1 );
      char* tmp = nullptr;
      char * tmp2 = nullptr;

      for ( i = 0; i < length; i ++ ) {
        tmp = lightMessage;
        tmp2 = this -> ContainsSettingAs <char *> ( properties [i], ( char* ) nullptr );
        lightMessage = org::tools::misc::CString::Concatenate ( lightMessage, tmp2, LITERAL_SPACE );
        lib::Stdlib::Free ( ( void * ) tmp );
        lib::Stdlib::Free ( ( void * ) tmp2 );
      }
      
      i = 0;
      unsigned char * byteValues = org::tools::misc::Convert::ToArray <unsigned char> ( lightMessage, 16, &i );
      this -> Free ( lightMessage );

      this -> BuildLightMessage ( byteValues, i );
      this -> Free ( byteValues );
    }
    
    HaclProtocol::HaclProtocol ( unsigned char * message ) {
      InitializeHaclProtocol ( );
      this -> ReadMessage ( message );
    }

    HaclProtocol::HaclProtocol ( unsigned char * lightMessage, int size ) {
      InitializeHaclProtocol ( );
      this -> BuildLightMessage ( lightMessage, size );
    }

    HaclProtocol::HaclProtocol ( const char * configurationName ) {
      this -> InitializeHaclProtocol ( );
      this -> SetConfigurationMask ( configurationName );
    }

    HaclProtocol::HaclProtocol ( const char * configurationName, short activeSessionNumber ) {
      this -> InitializeHaclProtocol ( );
      this -> SetConfigurationMask ( configurationName );
      this -> SetSessionNumber ( activeSessionNumber );
      this -> OpenSession ( );
      this -> UpdateChecksum ( );
    }

    HaclProtocol::HaclProtocol ( short activeSessionNumber, unsigned char * lightMessage, int size ) {
      InitializeHaclProtocol ( );
      this -> BuildLightMessage ( lightMessage, size );
      this -> SetSessionNumber ( activeSessionNumber );
      this -> OpenSession ( );
      this -> UpdateChecksum ( );
    }

    HaclProtocol::~HaclProtocol ( void ) {
      counterMessages --;
      if ( counterMessages == 0 ) {
        lib::Stdlib::Free ( ( void * ) HaclProtocol::HaclMainProtocol );
        HaclProtocol::HaclMainProtocol = nullptr;
      }
      if ( this -> _dataPages != nullptr ) {
        std::map <int, std::pair <unsigned char *, int> *>::iterator pagesIt = this -> _dataPages -> begin ( );
        for ( ; pagesIt != this -> _dataPages -> end ( ); ++ pagesIt ) {
          lib::Stdlib::Free ( ( void * ) pagesIt -> second -> first );
          delete pagesIt -> second;
        }
        delete this -> _dataPages;
      }
    }

    HaclProtocol* HaclProtocol::Copy ( void ) {
      return this -> CreateCopy <HaclProtocol> ( );
    }

    bool HaclProtocol::IsPaginated ( void ) {
      return this -> _pageNumber > 0 ? true : false;
    }

    void HaclProtocol::AddDataPage ( unsigned char * page, int size ) {
      if ( this -> _dataPages == nullptr )
        this -> _dataPages = new std::map <int, std::pair <unsigned char *, int> *> ( );
      this -> _dataPages -> insert ( std::pair <int, std::pair <unsigned char *, int> *> ( this -> _pageNumber, new std::pair <unsigned char *, int> ( page, size ) ) );
      this -> _pageNumber ++;
    }

    bool HaclProtocol::SetDataToPage ( int pageIndex ) {
      if ( this -> HasDataPage ( pageIndex ) )
        this -> SetData ( this -> GetDataPage ( pageIndex ), this -> GetDataPageSize ( pageIndex ) );
      return this -> HasDataPage ( pageIndex );
    }

    bool HaclProtocol::HasDataPage ( int pageIndex ) {
      bool hasPage = false;
      if ( this -> _dataPages != nullptr )
        hasPage = this -> _dataPages -> count ( pageIndex ) > 0 ? true : false;
      return hasPage;
    }

    unsigned char * HaclProtocol::GetDataPage ( int pageIndex ) {
      unsigned char * page = nullptr;
      if ( this -> HasDataPage ( pageIndex ) )
        page = this -> _dataPages -> at ( pageIndex ) -> first;
      return page;
    }

    int HaclProtocol::GetDataPageSize ( int pageIndex ) {
      int size = 0;
      if ( this -> HasDataPage ( pageIndex ) )
        size = this -> _dataPages -> at ( pageIndex ) -> second;
      return size;
    }

    int HaclProtocol::GetDataPageCount ( void ) {
      return this -> _pageNumber;
    }

    void HaclProtocol::UpdateChecksum ( void ) {
      unsigned char newChecksum = 0x00;
      unsigned char * part = nullptr;
      for ( int i = 1, size = 0, j = 0; i < 9; i ++ )
        for ( j = 0, size = this -> GetSize ( i ), part = this -> GetPart ( i ); j < size; j ++ )
          if ( part != nullptr )
            newChecksum ^= part [j];
      this -> SetPart ( 9, &newChecksum );
    }
    
    char* HaclProtocol::LightMessageAsHex ( void ) {
      return this -> GetPartsAsHex ( 4, this -> _partsCount - 1 );
    }

    char* HaclProtocol::AsHexString ( void ) {
      unsigned char * message = this -> GetMessage ( );
      char* cString = org::tools::misc::CString::ArrayToString <unsigned char> ( message, this -> GetMessageSize ( ), ( char* ) "0x%.2hhX", 4, LITERAL_SPACE );
      MemoryManager::FreeM ( ( void* ) message );
      return cString;
    }

    char * HaclProtocol::DataAsString ( void ) {
      return org::tools::misc::CString::GetStringFrom ( ( const char * ) this -> GetData ( ), this -> DataSize ( ) );
    }
    
    void HaclProtocol::CloseSession ( void ) {
      unsigned char * session = this -> GetPart ( 2 );
      session [0] &= 0x7F;
    }

    bool HaclProtocol::SessionIsOpen ( void ) {
      return ( ( this -> GetPart ( 2 ) [0] & 0x80 ) != 0 );
    }

    void HaclProtocol::OpenSession ( void ) {
      unsigned char * session = this -> GetPart ( 2 );
      session [0] |= 0x80;
    }

    void HaclProtocol::SetSessionNumber ( short newSessionNumber ) {
      unsigned char * sessionNumber = this -> GetPart ( 2 );
      sessionNumber [0] = ( unsigned char ) ( newSessionNumber >> 8 );
      sessionNumber [1] = ( unsigned char ) newSessionNumber;
    }

    short HaclProtocol::GetSessionAsShort ( void ) {
      unsigned char * sessionNumber = this -> GetPart ( 2 );
      short sessionNumberShort = ( short ) sessionNumber [0];
      sessionNumberShort <<= 8;
      sessionNumberShort += ( short ) sessionNumber [1];
      return sessionNumberShort;
    }
    
    void HaclProtocol::RemoveData ( void ) {
      this -> SetData ( nullptr, 0 );
    }

    void HaclProtocol::SetData ( unsigned char * newData, int size ) {
      this -> SetPart ( 8, newData, size );
    }

    int HaclProtocol::DataSize ( void ) {
      return this -> GetSize ( 8 );
    }

    unsigned char * HaclProtocol::GetData ( void ) {
      return this -> GetPart ( 8 );
    }
    
    unsigned char * HaclProtocol::GetSource ( void ) {
      return this -> GetPart ( 5 );
    }

    unsigned char * HaclProtocol::GetDestination ( void ) {
      return this -> GetPart ( 4 );
    }

    void HaclProtocol::InverseSourceDestination ( void ) {
      this -> InversePart ( 4, 5 );
    }

    char * HaclProtocol::SourceAsHex ( void ) {
      return this -> GetPartsAsHex ( 5, 6 );
    }

    char * HaclProtocol::DestinationAsHex ( void ) {
      return this -> GetPartsAsHex ( 4, 5 );
    }
    
    bool HaclProtocol::IsResponse ( void ) {
      return this -> GetPart ( 7 ) [0] == 0x02;
    }

    bool HaclProtocol::IsRequest ( void ) {
      return this -> GetPart ( 7 ) [0] == 0x00;
    }

    bool HaclProtocol::IsInformation ( void ) {
      return this -> GetPart ( 7 ) [0] == 0x06;
    }

    bool HaclProtocol::IsExecute ( void ) {
      return this -> GetPart ( 7 ) [0] == 0x04;
    }

    void HaclProtocol::SetToAck ( void ) {
      unsigned char ackMode = *( this -> GetPart ( 7 ) ) + 1;
      this -> SetMode ( ackMode );
    }

    void HaclProtocol::SetToResponse ( void ) {
      this -> SetMode ( 0x02 );
    }

    void HaclProtocol::SetToInformation ( void ) {
      this -> SetMode ( 0x06 );
    }

    void HaclProtocol::SetMode ( unsigned char newMode ) {
      this -> SetPart ( 7, &newMode );
    }

    unsigned char * HaclProtocol::GetMode ( void ) {
      return this -> GetPart ( 7 );
    }
    
    bool HaclProtocol::IsNoName ( void ) {
      unsigned char * name = this -> GetPart ( 6 );
      return name [0] == 0x00 && name [1] == 0x00;
    }

    bool HaclProtocol::IsPing ( void ) {
      unsigned char * name = this -> GetPart ( 6 );
      return name [0] == 0x00 && name [1] == 0x02;
    }

    bool HaclProtocol::IsInternalReset ( void ) {
      unsigned char * name = this -> GetPart ( 6 );
      return name [0] == 0x00 && name [1] == 0x03;
    }

    bool HaclProtocol::IsInternalBootMode ( void ) {
      unsigned char * name = this -> GetPart ( 6 );
      return name [0] == 0x00 && name [1] == 0x04;
    }

    bool HaclProtocol::IsInternalBlockIntuition ( void ) {
      unsigned char * name = this -> GetPart ( 6 );
      return name [0] == 0x00 && name [1] == 0x05;
    }

    bool HaclProtocol::IsInternalEnableIntuition ( void ) {
      unsigned char * name = this -> GetPart ( 6 );
      return name [0] == 0x00 && name [1] == 0x06;
    }

    bool HaclProtocol::IsInternalSpecialRequest ( void ) {
      unsigned char * name = this -> GetPart ( 6 );
      return name [0] == 0x00 && name [1] == 0x07;
    }

    bool HaclProtocol::IsInternalStartActivity ( void ) {
      unsigned char * name = this -> GetPart ( 6 );
      return name [0] == 0x00 && name [1] == 0x08;
    }

    bool HaclProtocol::IsInternalStopActivity ( void ) {
      unsigned char * name = this -> GetPart ( 6 );
      return name [0] == 0x00 && name [1] == 0x09;
    }

    bool HaclProtocol::IsStressTest ( void ) {
      unsigned char * name = this -> GetPart ( 6 );
      return name [0] == 0x00 && name [1] == 0x0A;
    }

    bool HaclProtocol::IsApplianceStructureName ( void ) {
      unsigned char * name = this -> GetPart ( 6 );
      return name [0] == 0x00 && name [1] == 0x01;
    }

    unsigned char * HaclProtocol::GetName ( void ) {
      return this -> GetPart ( 6 );
    }

    short HaclProtocol::GetNameAsShort ( void ) {
      unsigned char * name = this -> GetPart ( 6 );
      short nameShort = ( short ) name [0];
      nameShort <<= 8;
      nameShort += ( short ) name [1];
      return nameShort;
    }
    
    char * HaclProtocol::NameAsHex ( void ) {
      return this -> GetPartsAsHex ( 6, 7 );
    }

    const char * HaclProtocol::ObjectType ( void ) const {
      return "org::protocol::HaclProtocol";
    }
  }
}
