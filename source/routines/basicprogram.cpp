


#include <routines/basicprogram.hpp>

#include <tools/misc/wait.hpp>
#include <process/process.hpp>
#include <application/program.hpp>
#include <abstraction/osevent.hpp>

#include <lib/freax/syslog.hpp>

namespace org {
  namespace routines {
    void BasicProgram::InitializeBasicProgram ( void ) {
      this -> _period             = 1;
      this -> _iterations         = 1;
      this -> _continuing         = true;
      this -> _endLess            = false;
      this -> _asDaemon           = false;
      this -> _testRoutine        = false;
      this -> _syslogEnabled      = false;
      this -> _syslogName         = nullptr;
      this -> _daemonLockFile     = nullptr;
      this -> _servers            = new std::map <org::types::CString, org::servers::Server *> ( );
      this -> _serversToStart     = nullptr;
      this -> _outputFile         = nullptr;
      this -> _signalsToHandle    = nullptr;
    }

    void BasicProgram::LogPid ( void ) {
      char * tmp = org::process::System::PID ( );
      char * msg = org::tools::misc::CString::Concatenate ( "Current pid: ", tmp );
      this -> Info ( msg );
      this -> Free ( tmp );
      this -> Free ( msg );
    }

    void BasicProgram::StartServers ( void ) {
      if ( this -> _serversToStart != nullptr ) {
        this -> Debug ( "Starting servers" );
        std::map <int, const char *>::iterator server = this -> _serversToStart -> begin ( );
        for ( ; server != this -> _serversToStart -> end ( ); server ++ )
          this -> TryStartServer ( server -> second );
        this -> Debug ( "Finish starting servers" );
      } else
        this -> Debug ( "No servers to start" );
    }

    void BasicProgram::StopServers ( void ) {
      if ( this -> _serversToStart != nullptr ) {
        if ( this -> _serversToStart -> size ( ) > 0 ) {
          this -> Debug ( "Stoping servers" );
          std::map <int, const char *>::reverse_iterator server = this -> _serversToStart -> rbegin ( );
          for ( ; server != this -> _serversToStart -> rend ( ); server ++ )
            this -> TryStopServer ( server -> second );
          this -> Debug ( "Finish stoping servers" );
        }
      }
    }
    
    void BasicProgram::Configure ( int argCount, char ** argStrings ) {
      this -> _argCount = argCount;
      this -> _argStrings = argStrings;
      
      org::application::Program::Current = new org::application::Program ( this -> _argCount, this -> _argStrings );
      org::application::Program::Current -> SetLogger ( new org::logging::LoggerConfigured ( "logging.logger.process" ) );
      org::application::Program::Current -> Config ( ) -> SetLogger ( new org::logging::LoggerConfigured ( "logging.logger.configuration" ) );
    }

    void BasicProgram::AddServer ( org::servers::Server * newServer ) {
      org::types::CString key = newServer -> ServerName ( );
      if ( this -> _servers -> count ( key ) == 0 )
        this -> _servers -> insert ( std::pair <org::types::CString, org::servers::Server *> ( key, newServer ) );
    }

    void BasicProgram::TryStartServer ( const char * serverName ) {
      char * message = nullptr;
      const char * event = "Trying to start non-existant server: ";
      if ( this -> _servers -> count ( serverName ) > 0 ) {
        this -> _servers -> at ( serverName ) -> Start ( );
        event = "Starting server: ";
      }

      message = org::tools::misc::CString::Concatenate ( event, serverName );
      this -> Notice ( message );
      lib::Stdlib::Free ( message );
    }

    void BasicProgram::TryStopServer ( const char * serverName ) {
      char * message = nullptr;
      const char * event = "Trying to stop non-existant server: ";
      if ( this -> _servers -> count ( serverName ) > 0 ) {
        this -> _servers -> at ( serverName ) -> Stop ( );
        event = "Server is stoping: ";
      }

      message = org::tools::misc::CString::Concatenate ( event, serverName );
      this -> Notice ( message );
      lib::Stdlib::Free ( message );
    }

    void BasicProgram::AddToInAppMessageCenter ( void ) {
      if ( org::application::Program::Current != nullptr )
        org::application::Program::Current -> InApplicationMessageCenter ( ) -> AddMessageable ( this );
    }

    void BasicProgram::TimerEndMethod ( void ) {
      if ( ! this -> _endLess )
        this -> Info ( "Working for xx second(s)" );
      this -> _continuing = true;
      int inc = this -> _endLess ? 0 : 1;
      for ( int i = 0; i < this -> _iterations && this -> _continuing; i += inc )
        org::tools::misc::Wait::Seconds ( this -> _period );
    }

    void BasicProgram::ExecuteTestRoutine ( void ) {
      if ( this -> _testRoutine ) {
        this -> Warning ( "In test routine now" );
        this -> Warning ( "In test routine now" );
        this -> Warning ( "In test routine now" );

        this -> TestRoutine ( );
        
        this -> Warning ( "Out of test routine now" );
        this -> Warning ( "Out of test routine now" );
        this -> Warning ( "Out of test routine now" );
      }
    }
    
    void BasicProgram::TestRoutine ( void ) {
    }

    void BasicProgram::FinishAutoConfiguration ( void ) {
      MainProgram::FinishAutoConfiguration ( );
      
      this -> _period           = this -> ContainsSettingAs <int>           ( "period", this -> _period );
      this -> _iterations       = this -> ContainsSettingAs <int>           ( "iterations", this -> _iterations );
      this -> _endLess          = this -> ContainsSettingAs <bool>          ( "end-less", this -> _endLess );
      this -> _asDaemon         = this -> ContainsSettingAs <bool>          ( "daemon.enable", this -> _asDaemon );
      this -> _daemonLockFile   = this -> ContainsSettingAs <const char *>  ( "daemon.lock-file-name", this -> _daemonLockFile );
      this -> _testRoutine      = this -> ContainsSettingAs <bool>          ( "test-routine.enable", this -> _testRoutine );
      this -> _syslogEnabled    = this -> ContainsSettingAs <bool>          ( "syslog.enable", this -> _syslogEnabled );
      this -> _syslogName       = this -> ContainsSettingAs <const char *>  ( "syslog.name", this -> _syslogName );
      this -> _serversToStart   = this -> ContainsSettingMapAs <int, const char *> ( "servers" );
      this -> _signalsToHandle  = this -> ContainsSettingMapAs <int, bool> ( "handled-signals" );

      this -> PutSettingAs <const char *> ( "daemon.out-file-name", &( this -> _outputFile ) );
      
      char * inAppName = this -> ContainsSettingAs <char *> ( "in-app-messaging-name", nullptr );
      this -> SetInAppMessageableName ( inAppName );
      lib::Stdlib::Free ( ( void * ) inAppName );
      this -> LogSettings ( );
    }
    
    BasicProgram::BasicProgram ( void ) {
      this -> InitializeBasicProgram ( );
    }
    
    BasicProgram::~BasicProgram ( void ) {
      lib::Stdlib::Free ( ( void * ) this -> _daemonLockFile );
      lib::Stdlib::Free ( ( void * ) this -> _outputFile );
      lib::Stdlib::Free ( ( void * ) this -> _syslogName );
      if ( this -> _serversToStart != nullptr ) {
        std::map <int, const char *>::iterator serversIt = this -> _serversToStart -> begin ( );
        for ( ; serversIt != this -> _serversToStart -> end ( ); ++ serversIt ) {
          if ( this -> _servers -> count ( serversIt -> second ) > 0 )
            delete this -> _servers -> at ( serversIt -> second );
          lib::Stdlib::Free ( ( void * ) serversIt -> second );
        }
        delete this -> _serversToStart;
      }
      if ( this -> _signalsToHandle != nullptr )
        delete this -> _signalsToHandle;
      if ( this -> _servers != nullptr )
        delete this -> _servers;
    }

    void BasicProgram::Main ( void ) {
      if ( this -> _asDaemon )
        org::process::System::DaemonizeSaveOutput ( this -> _daemonLockFile, this -> _outputFile );

      if ( this -> _syslogEnabled )
        lib::freax::Syslog::OpenLog ( this -> _syslogName, true, true, true, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false );
        
      org::application::Program::Current -> Banner ( );
      org::application::Program::Current -> ConfigurationList ( );
      
      if ( this -> _signalsToHandle != nullptr ) {
        char tmp [255];
        for ( std::map <int, bool>::iterator signalIt = this -> _signalsToHandle -> begin ( ); signalIt != this -> _signalsToHandle -> end ( ); ++ signalIt )
          if ( signalIt -> second == true ) {
            sprintf ( tmp, "Handling signal %d", signalIt -> first );
            this -> Notice ( tmp );
            org::abstraction::OsEvent::PlugHandler ( signalIt -> first, org::abstraction::OsEvent::DefaultSignalHander );
          }
      }
      
      this -> LogSettings ( );
      this -> LogPid ( );

      if ( this -> _testRoutine )
        this -> TestRoutine ( );
        
      this -> PreTimerEndMethod ( );
      
      this -> StartServers ( );
      this -> TimerEndMethod ( );
      this -> StopServers ( );
      
      this -> PostTimerEndMethod ( );
      
      org::application::Program::Current -> EndOfProgram ( );
    }

    void BasicProgram::PreTimerEndMethod ( void ) {

    }

    void BasicProgram::PostTimerEndMethod ( void ) {

    }

    void BasicProgram::DeleteVariable ( void ) {
      if ( org::application::Program::Current != nullptr ) {
        delete org::application::Program::Current;
        org::application::Program::Current = nullptr;
      }
    }
    
    void BasicProgram::UnloadModules ( void ) {
      if ( this -> _syslogEnabled )
        lib::freax::Syslog::CloseLog ( );
    }

    org::inappmessaging::InAppMessage * BasicProgram::PostInAppMessage ( org::inappmessaging::InAppMessage * inAppMessage ) {
      if ( org::tools::misc::CString::IsEqual ( "endloop", inAppMessage -> Command ( ) ) )
        this -> _continuing = false;
      if ( org::tools::misc::CString::IsEqual ( "log", inAppMessage -> Command ( ) ) )
        this -> Info ( inAppMessage -> ParameterAt ( 0 ) [0] );
      return nullptr;
    }

    const char * BasicProgram::ObjectType ( void ) const {
      return "org::routines::BasicProgram";
    }
  }
}
