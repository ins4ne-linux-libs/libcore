


#include <routines/mainprogram.hpp>

namespace org {
  namespace routines {
    void MainProgram::Configure ( int argCount, char ** argStrings ) {
      this -> _argCount = argCount;
      this -> _argStrings = argStrings;
    }
    
    MainProgram::MainProgram ( void ) {
      this -> Configure ( 0, nullptr );
    }
    
    void MainProgram::Execute ( void ) {
      this -> LoadModules     ( );
      this -> CreateVariable  ( );
      this -> Main            ( );
      this -> DeleteVariable  ( );
      this -> UnloadModules   ( );
    }

    void MainProgram::LoadModules    ( void ) { }
    void MainProgram::CreateVariable ( void ) { }
    void MainProgram::Main           ( void ) { }
    void MainProgram::DeleteVariable ( void ) { }
    void MainProgram::UnloadModules  ( void ) { }
  }
}
