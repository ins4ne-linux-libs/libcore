


#include <servers/server.hpp>

namespace org {
  namespace servers {
    void Server::InitializeServer ( void ) {
      this -> _name = nullptr;
    }
    
    void Server::FinishAutoConfiguration ( void ) {
      org::entity::LoggingConfiguredEntity::FinishAutoConfiguration ( );
      
      this -> _name = this -> ContainsSettingAs <const char *> ( "server.name", this -> _name );
    }
    
    Server::Server ( void ) {
      this -> InitializeServer ( );
    }

    Server::~Server ( void ) {
      lib::Stdlib::Free ( ( void * ) this -> _name );
    }

    const char * Server::ServerName ( void ) {
      return this -> _name;
    }
  }
}
