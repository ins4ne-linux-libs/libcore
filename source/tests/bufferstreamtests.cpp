
#include <tests/bufferstreamtests.hpp>

#include <virtualization/bufferstream.hpp>

using namespace org::unittest;
using namespace org::virtualization;

namespace org {
  namespace unittest {
    BUFFERSTREAM_TESTS_CLASS_NAME * BUFFERSTREAM_TESTS_CLASS_NAME::BUFFERSTREAM_TESTS_CLASS_NAME_instance = new BUFFERSTREAM_TESTS_CLASS_NAME ( );

    BUFFERSTREAM_TESTS_CLASS_NAME::BUFFERSTREAM_TESTS_CLASS_NAME ( void ) {
      this -> SetIdAndGroup ( "bufferstream", "BufferStream related tests" );
      
      BUFFERSTREAM_TESTS_ADD ( Creation, "Creation test" );
      BUFFERSTREAM_TESTS_ADD ( Delete, "Delete test" );
      BUFFERSTREAM_TESTS_ADD ( ObjectType, "Testing object type string" );
      BUFFERSTREAM_TESTS_ADD ( Count_0, "Testing count at simple initialization" );
      BUFFERSTREAM_TESTS_ADD ( Configuration_String, "Testing configuration for a string" );
      BUFFERSTREAM_TESTS_ADD ( Configuration_Hex, "Testing configuration for a hex suite" );
      BUFFERSTREAM_TESTS_ADD ( Looping, "Testing looping feature" );
      BUFFERSTREAM_TESTS_ADD ( Base_10, "Testing basing at 10" );
    }

    BUFFERSTREAM_TEST_C ( Looping ) {
      BufferStream <unsigned char> * bufferstream = new BufferStream <unsigned char> ( "buffertest_3" );
      this -> AssertTrue ( bufferstream -> Count ( ) == 4, "Should be a count == 4" );
      this -> AssertEqual ( bufferstream -> _looping, true, "Should be looping" );
      this -> AssertEqual ( bufferstream -> _type, "hex", "Should be hex string" );

      unsigned char test = 0;
      this -> AssertTrue ( bufferstream -> GetOne ( &test ), "Should return having read something" );
      this -> AssertEqual ( 0x00, ( int ) test, "First should be 0x00" );
      
      this -> AssertTrue ( bufferstream -> GetOne ( &test ), "Should return having read something" );
      this -> AssertEqual ( 0xA0, ( int ) test, "First should be 0xA0" );
      
      this -> AssertTrue ( bufferstream -> GetOne ( &test ), "Should return having read something" );
      this -> AssertEqual ( 0x32, ( int ) test, "First should be 0x32" );
      
      this -> AssertTrue ( bufferstream -> GetOne ( &test ), "Should return having read something" );
      this -> AssertEqual ( 0x46, ( int ) test, "First should be 0x46" );
      
      this -> AssertTrue ( bufferstream -> GetOne ( &test ), "Should return having read something" );
      this -> AssertEqual ( 0x00, ( int ) test, "First should be 0x00" );
      
      this -> AssertTrue ( bufferstream -> GetOne ( &test ), "Should return having read something" );
      this -> AssertEqual ( 0xA0, ( int ) test, "First should be 0xA0" );
      
      this -> AssertEqual ( bufferstream -> _cursor, 2, "Should be at position 2" );
    }

    BUFFERSTREAM_TEST_C ( Base_10 ) {
      BufferStream <unsigned char> * bufferstream = new BufferStream <unsigned char> ( "buffertest_4" );
      this -> AssertTrue ( bufferstream -> Count ( ) == 4, "Should be a count == 4" );
      this -> AssertEqual ( bufferstream -> _looping, false, "Should not be looping" );
      this -> AssertEqual ( bufferstream -> _type, "hex", "Should be string" );

      unsigned char test = 0;
      this -> AssertTrue ( bufferstream -> GetOne ( &test ), "Should return having read something" );
      this -> AssertEqual ( ( int ) test, 10, "First should be 10" );
      
      this -> AssertTrue ( bufferstream -> GetOne ( &test ), "Should return having read something" );
      this -> AssertEqual ( ( int ) test, 20, "First should be 20" );
      
      this -> AssertTrue ( bufferstream -> GetOne ( &test ), "Should return having read something" );
      this -> AssertEqual ( ( int ) test, 30, "First should be 30" );
      
      this -> AssertTrue ( bufferstream -> GetOne ( &test ), "Should return having read something" );
      this -> AssertEqual ( ( int ) test, 47, "First should be 47" );
      
      this -> AssertFalse ( bufferstream -> GetOne ( &test ), "Should return not having been able to read something" );
    }

    BUFFERSTREAM_TEST_C ( Configuration_Hex ) {
      BufferStream <unsigned char> * bufferstream = new BufferStream <unsigned char> ( "buffertest_2" );
      this -> AssertTrue ( bufferstream -> Count ( ) == 4, "Should be a count == 4" );
      this -> AssertEqual ( bufferstream -> _looping, false, "Should not be looping" );
      this -> AssertEqual ( bufferstream -> _type, "hex", "Should be string" );

      unsigned char test = 0;
      this -> AssertTrue ( bufferstream -> GetOne ( &test ), "Should return having read something" );
      this -> AssertEqual ( ( int ) test, 0x00, "First should be 0x00" );
      this -> AssertEqual ( 3, bufferstream -> Count ( ), "Should be a count == 3" );
      
      this -> AssertTrue ( bufferstream -> GetOne ( &test ), "Should return having read something" );
      this -> AssertEqual ( ( int ) test, 0xA0, "First should be 0xA0" );
      this -> AssertEqual ( 2, bufferstream -> Count ( ), "Should be a count == 2" );
      
      this -> AssertTrue ( bufferstream -> GetOne ( &test ), "Should return having read something" );
      this -> AssertEqual ( ( int ) test, 0x32, "First should be 0x32" );
      this -> AssertEqual ( 1, bufferstream -> Count ( ), "Should be a count == 1" );
      
      this -> AssertTrue ( bufferstream -> GetOne ( &test ), "Should return having read something" );
      this -> AssertEqual ( ( int ) test, 0x46, "First should be 0x46" );
      this -> AssertEqual ( 0, bufferstream -> Count ( ), "Should be a count == 0" );
      
      this -> AssertFalse ( bufferstream -> GetOne ( &test ), "Should return not having been able to read something" );
      this -> AssertFalse ( bufferstream -> GetOne ( &test ), "Should return not having been able to read something" );
    }

    BUFFERSTREAM_TEST_C ( Configuration_String ) {
      BufferStream <unsigned char> * bufferstream = new BufferStream <unsigned char> ( "buffertest" );
      this -> AssertTrue ( bufferstream -> Count ( ) == 11, "Should be a count == 11" );
      this -> AssertEqual ( bufferstream -> _looping, true, "Should be looping" );
      this -> AssertEqual ( bufferstream -> _type, "string", "Should be string" );
    }

    BUFFERSTREAM_TEST_C ( Count_0 ) {
      BufferStream <unsigned char> * bufferstream = new BufferStream <unsigned char> ( );
      this -> AssertTrue ( bufferstream -> Count ( ) == 0, "Should be a count == 0" );
    }

    BUFFERSTREAM_TEST_C ( Creation ) {
      BufferStream <unsigned char> * bufferstream = new BufferStream <unsigned char> ( );
      bufferstream = bufferstream;
    }

    BUFFERSTREAM_TEST_C ( Delete ) {
      BufferStream <unsigned char> * bufferstream = new BufferStream <unsigned char> ( );
      delete bufferstream;
    }

    BUFFERSTREAM_TEST_C ( ObjectType ) {
      Object * obj = new BufferStream <unsigned char> ( );
      this -> AssertEqual ( "org::virtualization::BufferStream", obj -> ObjectType ( ), "Object type in error" );
      delete obj;
    }
  }
}
