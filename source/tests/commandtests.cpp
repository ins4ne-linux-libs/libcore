
#include <tests/commandtests.hpp>

#include <fsm/command.hpp>

using namespace org::unittest;
using namespace org::fsm;

namespace org {
  namespace unittest {
    COMMAND_TESTS_CLASS_NAME * COMMAND_TESTS_CLASS_NAME::COMMAND_TESTS_CLASS_NAME_instance = new COMMAND_TESTS_CLASS_NAME ( );

    COMMAND_TESTS_CLASS_NAME::COMMAND_TESTS_CLASS_NAME ( void ) {
      this -> SetIdAndGroup ( "command", "Command related tests" );
      
      COMMAND_TESTS_ADD ( Creation, "Creation test" );
      COMMAND_TESTS_ADD ( Delete, "Delete test" );
      COMMAND_TESTS_ADD ( ObjectType, "Testing object type string" );
    }

    COMMAND_TEST_C ( Creation ) {
      Command * command = new Command ( );
      command = command;
      delete command;
    }

    COMMAND_TEST_C ( Delete ) {
      Command * command = new Command ( );
      delete command;
    }

    COMMAND_TEST_C ( ObjectType ) {
      Object * obj = new Command ( );
      this -> AssertEqual ( "org::fsm::Command", obj -> ObjectType ( ), "Object type in error" );
      delete obj;
    }
  }
}
