
#include <tests/cstringtests.hpp>

#include <constants/libcore.hpp>
#include <tools/misc/stringextension.hpp>

using namespace org::unittest;
using namespace org::tools::misc;

namespace org {
  namespace unittest {
    CSTRING_TESTS_CLASS_NAME * CSTRING_TESTS_CLASS_NAME::CSTRING_TESTS_CLASS_NAME_instance = new CSTRING_TESTS_CLASS_NAME ( );

    CSTRING_TESTS_CLASS_NAME::CSTRING_TESTS_CLASS_NAME ( void ) {
      this -> SetIdAndGroup ( "cstring", "CString related tests" );
      
      CSTRING_TESTS_ADD ( Equal, "Controls that two strings equal returns true if the same, false otherwise" );
      CSTRING_TESTS_ADD ( PutBetween, "Putting a string between two other pieces" );
      CSTRING_TESTS_ADD ( Concatenate, "Concatenating strings" );
      CSTRING_TESTS_ADD ( CharToLower, "Putting char to lower" );
      CSTRING_TESTS_ADD ( CharToUpper, "Putting char to upper" );
      CSTRING_TESTS_ADD ( CStringToUpper, "Putting c string to upper" );
      CSTRING_TESTS_ADD ( CStringToLower, "Putting c string to lower" );
      CSTRING_TESTS_ADD ( Contains, "Putting c string to lower" );
    }

    CSTRING_TEST_C ( Contains ) {
      this -> AssertTrue ( CString::Contains ( "test", 'e' ), "Upper function not working" );
    }

    CSTRING_TEST_C ( CStringToLower ) {
      const char * toLower = CString::ToLower ( "Hello there - my MY" );
      this -> AssertEqual ( "hello there - my my", toLower, "Upper function not working" );
      lib::Stdlib::Free ( ( void * ) toLower );
    }

    CSTRING_TEST_C ( CStringToUpper ) {
      const char * toUpper = CString::ToUpper ( "Hello there - my MY" );
      this -> AssertEqual ( "HELLO THERE - MY MY", toUpper, "Upper function not working" );
      lib::Stdlib::Free ( ( void * ) toUpper );
    }

    CSTRING_TEST_C ( CharToUpper ) {
      this -> AssertEqual ( 'A', CString::ToUpper ( 'A' ), "Upper function not working" );
      this -> AssertEqual ( 'Z', CString::ToUpper ( 'Z' ), "Upper function not working" );
      this -> AssertEqual ( 'A', CString::ToUpper ( 'a' ), "Upper function not working" );
      this -> AssertEqual ( 'Z', CString::ToUpper ( 'z' ), "Upper function not working" );
      this -> AssertEqual ( 'B', CString::ToUpper ( 'B' ), "Upper function should not change an already uppered" );
      this -> AssertEqual ( LITERAL_HYPHEN, CString::ToUpper ( LITERAL_HYPHEN ), "Upper function should not change ponctuation" );
    }

    CSTRING_TEST_C ( CharToLower ) {
      this -> AssertEqual ( 'a', CString::ToLower ( 'A' ), "Lower function not working" );
      this -> AssertEqual ( 'z', CString::ToLower ( 'Z' ), "Lower function not working" );
      this -> AssertEqual ( 'a', CString::ToLower ( 'a' ), "Lower function not working" );
      this -> AssertEqual ( 'z', CString::ToLower ( 'z' ), "Lower function not working" );
      this -> AssertEqual ( 'b', CString::ToLower ( 'b' ), "Lower function should not change an already lowered" );
      this -> AssertEqual ( LITERAL_HYPHEN, CString::ToLower ( LITERAL_HYPHEN ), "Lower function should not change ponctuation" );
    }

    CSTRING_TEST_C ( PutBetween ) {
      const char * test = CString::PutBetween ( "test", "<", " />" );
      this -> AssertEqual ( "<test />", test, "put between strings" );
      lib::Stdlib::Free ( ( void * ) test );
    }

    CSTRING_TEST_C ( Concatenate ) {
      const char * str1 = "string 1";
      const char * str2 = "string 2";
      const char * strs [4] = { "str1", "str2", "str3", "str4" };
      const char * strs2 [4] = { "str1", "str2", nullptr, "str4" };
      const char * strs3 [4] = { nullptr, "str2", "str3", "str4" };
      
      const char * concatenated = nullptr;

      this -> AssertEqual ( "string 1string 2", ( concatenated = CString::Concatenate ( str1, str2 ) ), "simple concatenation" );
      lib::Stdlib::Free ( ( void * ) concatenated );
      this -> AssertEqual ( "string 1", ( concatenated = CString::Concatenate ( str1, nullptr ) ), "concatenation with nullptr" );
      lib::Stdlib::Free ( ( void * ) concatenated );
      this -> AssertEqual ( "string 2", ( concatenated = CString::Concatenate ( nullptr, str2 ) ), "concatenation with nullptr" );
      lib::Stdlib::Free ( ( void * ) concatenated );
      this -> AssertEqual ( "string 1:string 2", ( concatenated = CString::Concatenate ( str1, str2, LITERAL_COLON ) ), "concatenation with separator" );
      lib::Stdlib::Free ( ( void * ) concatenated );
      this -> AssertNotEqual ( "string 1string 2", ( concatenated = CString::Concatenate ( str1, str2, LITERAL_COLON ) ), "failed concatenation" );
      lib::Stdlib::Free ( ( void * ) concatenated );
      this -> AssertEqual ( "str1str2str3str4", ( concatenated = CString::Concatenate ( strs, 4 ) ), "array concatenation" );
      lib::Stdlib::Free ( ( void * ) concatenated );
      this -> AssertEqual ( "str1str2str4", ( concatenated = CString::Concatenate ( strs2, 4 ) ), "array concatenation with null in middle" );
      lib::Stdlib::Free ( ( void * ) concatenated );
      this -> AssertEqual ( "str2str3str4", ( concatenated = CString::Concatenate ( strs3, 4 ) ), "array concatenation with null at the begining" );
      lib::Stdlib::Free ( ( void * ) concatenated );
    }

    CSTRING_TEST_C ( Equal ) {
      this -> AssertFalse ( CString::IsEqual ( "240", "2400" ), "240 to 2400 should not be equal." );
      this -> AssertFalse ( CString::IsEqual ( "2400", "240" ), "2400 to 240 should not be equal." );
      this -> AssertTrue ( CString::IsEqual ( "very long sentence with * specials_characters.", "very long sentence with * specials_characters." ), "Should be equal." );
    }
  }
}
