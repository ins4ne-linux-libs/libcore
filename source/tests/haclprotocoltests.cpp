
#include <tests/haclprotocoltests.hpp>

#include <protocol/haclprotocol.hpp>

using namespace org::unittest;
using namespace org::protocol;

namespace org {
  namespace unittest {
    HACLPROTOCOL_TESTS_CLASS_NAME * HACLPROTOCOL_TESTS_CLASS_NAME::HACLPROTOCOL_TESTS_CLASS_NAME_instance = new HACLPROTOCOL_TESTS_CLASS_NAME ( );

    HACLPROTOCOL_TESTS_CLASS_NAME::HACLPROTOCOL_TESTS_CLASS_NAME ( void ) {
      this -> SetIdAndGroup ( "haclprotocol", "HaclProtocol related tests" );
      
      HACLPROTOCOL_TESTS_ADD ( Creation, "Creation test" );
      HACLPROTOCOL_TESTS_ADD ( Delete, "Delete test" );
      HACLPROTOCOL_TESTS_ADD ( ObjectType, "Testing object type string" );
      HACLPROTOCOL_TESTS_ADD ( NameAsShort, "Testing GetNameAsShort" );
    }

    HACLPROTOCOL_TEST_C ( Creation ) {
      unsigned char message [] = { 0xC6, 0x0C, 0x80, 0x01, 0xAD, 0x4E, 0x49, 0x55, 0x4F, 0x56, 0x31, 0x00, 0x2A, 0x04, 0x74 };
      HaclProtocol * haclprotocol = new HaclProtocol ( message );
      haclprotocol = haclprotocol;
    }

    HACLPROTOCOL_TEST_C ( Delete ) {
      unsigned char message [] = { 0xC6, 0x0C, 0x80, 0x01, 0xAD, 0x4E, 0x49, 0x55, 0x4F, 0x56, 0x31, 0x00, 0x2A, 0x04, 0x74 };
      HaclProtocol * haclprotocol = new HaclProtocol ( message );
      delete haclprotocol;
    }

    HACLPROTOCOL_TEST_C ( ObjectType ) {
      unsigned char message [] = { 0xC6, 0x0C, 0x80, 0x01, 0xAD, 0x4E, 0x49, 0x55, 0x4F, 0x56, 0x31, 0x00, 0x2A, 0x04, 0x74 };
      Object * obj = new HaclProtocol ( message );
      this -> AssertEqual ( "org::protocol::HaclProtocol", obj -> ObjectType ( ), "Object type in error" );
      delete obj;
    }

    HACLPROTOCOL_TEST_C ( NameAsShort ) {
      int i = 0;
      unsigned char * byteValues = org::tools::misc::Convert::ToArray <unsigned char> ( "4E 49 55 4F 56 31 00 02 00", 16, &i );
      org::protocol::HaclProtocol * message = new org::protocol::HaclProtocol ( byteValues, i );
      
      this -> AssertTrue ( message -> GetNameAsShort ( ) == 0x0002, "Should be name equal to 0x0002" );

      delete message;
    }
  }
}
