
#include <tests/machinestatestests.hpp>

#include <hardware/machinestates.hpp>
#include <hardware/machinestates/temperature.hpp>

using namespace org::unittest;
using namespace org::hardware;

namespace org {
  namespace unittest {
    MACHINESTATES_TESTS_CLASS_NAME * MACHINESTATES_TESTS_CLASS_NAME::MACHINESTATES_TESTS_CLASS_NAME_instance = new MACHINESTATES_TESTS_CLASS_NAME ( );

    MACHINESTATES_TESTS_CLASS_NAME::MACHINESTATES_TESTS_CLASS_NAME ( void ) {
      this -> SetIdAndGroup ( "machinestates", "MachineStates related tests" );
      
      MACHINESTATES_TESTS_ADD ( Creation, "Creation test" );
      MACHINESTATES_TESTS_ADD ( Delete, "Delete test" );
      MACHINESTATES_TESTS_ADD ( ObjectType, "Testing object type string" );
      MACHINESTATES_TESTS_ADD ( AddingRegister, "Testing adding register" );
      MACHINESTATES_TESTS_ADD ( AddingState, "Testing adding a new state (using temperature)" );
      MACHINESTATES_TESTS_ADD ( AddingStates, "Testing adding a new states (using temperatures)" );
      MACHINESTATES_TESTS_ADD ( Concatenation, "Testing the concatenation of the machine states" );
      MACHINESTATES_TESTS_ADD ( Pagination, "Testing the Pagination of the machine states" );
      MACHINESTATES_TESTS_ADD ( AddingStateIfDifferent, "Testing adding state if different only" );
    }

    MACHINESTATES_TEST_C ( Creation ) {
      MachineStates * machinestates = new MachineStates ( );
      machinestates = machinestates;
    }

    MACHINESTATES_TEST_C ( Delete ) {
      MachineStates * machinestates = new MachineStates ( );
      delete machinestates;
    }

    MACHINESTATES_TEST_C ( ObjectType ) {
      Object * obj = new MachineStates ( );
      this -> AssertEqual ( "org::hardware::MachineStates", obj -> ObjectType ( ), "Object type in error" );
      delete obj;
    }

    MACHINESTATES_TEST_C ( AddingRegister ) {
      MachineStates * machinestates = new MachineStates ( );

      org::types::CString name = "test";
      this -> AssertFalse ( machinestates -> HasRegister ( name ), "Sould not know the register yet." );
      machinestates -> AddRegister ( name );
      this -> AssertTrue ( machinestates -> HasRegister ( name ), "Sould know the register." );

      delete machinestates;
    }

    MACHINESTATES_TEST_C ( AddingState ) {
      MachineStates * machinestates = new MachineStates ( );

      org::types::CString name = "temperatures";
      this -> AssertFalse ( machinestates -> HasRegister ( name ), "Sould not know the register yet." );

      machinestates -> SetState ( name, 3, new machinestates::Temperature ( 3, 0, ( short ) 3098, 1 ) );

      this -> AssertTrue ( machinestates -> HasRegister ( name ), "Sould know the register." );
      this -> AssertTrue ( machinestates -> HasState ( name, 3 ), "Sould know the register." );

      unsigned char * array = machinestates -> GetState ( name, 3 ) -> ByteArray ( );
      this -> AssertTrue ( array [0] == 3, "Should be equal" );
      this -> AssertTrue ( array [1] == 0, "Should be equal" );
      this -> AssertTrue ( array [2] == 0x0C, "Should be equal" );
      this -> AssertTrue ( array [3] == 0x1A, "Should be equal" );
      this -> AssertTrue ( array [4] == 1, "Should be equal" );
      delete machinestates;
    }

    MACHINESTATES_TEST_C ( AddingStates ) {
      MachineStates * machinestates = new MachineStates ( );

      org::types::CString name = "temperatures";
      this -> AssertFalse ( machinestates -> HasRegister ( name ), "Sould not know the register yet." );

      machinestates -> SetState ( name, 1, new machinestates::Temperature ( 1, 0, ( short ) 3096, 1 ) );
      machinestates -> SetState ( name, 2, new machinestates::Temperature ( 2, 0, ( short ) 3097, 1 ) );
      machinestates -> SetState ( name, 3, new machinestates::Temperature ( 3, 0, ( short ) 3098, 1 ) );

      this -> AssertTrue ( machinestates -> HasRegister ( name ), "Sould know the register." );
      this -> AssertTrue ( machinestates -> HasState ( name, 3 ), "Sould know the register." );

      unsigned char * array = machinestates -> GetState ( name, 2 ) -> ByteArray ( );
      this -> AssertTrue ( array [0] == 2, "Should be equal" );
      this -> AssertTrue ( array [1] == 0, "Should be equal" );
      this -> AssertTrue ( array [2] == 0x0C, "Should be equal" );
      this -> AssertTrue ( array [3] == 0x19, "Should be equal" );
      this -> AssertTrue ( array [4] == 1, "Should be equal" );
      delete machinestates;
    }

    MACHINESTATES_TEST_C ( Concatenation ) {
      MachineStates * machinestates = new MachineStates ( );

      org::types::CString name = "temperatures";
      this -> AssertFalse ( machinestates -> HasRegister ( name ), "Sould not know the register yet." );

      machinestates -> SetState ( name, 1, new machinestates::Temperature ( 1, 0, ( short ) 3096, 1 ) );
      machinestates -> SetState ( name, 2, new machinestates::Temperature ( 2, 0, ( short ) 3097, 1 ) );
      machinestates -> SetState ( name, 3, new machinestates::Temperature ( 3, 0, ( short ) 3098, 1 ) );

      this -> AssertTrue ( machinestates -> HasRegister ( name ), "Sould know the register." );
      this -> AssertTrue ( machinestates -> HasState ( name, 3 ), "Sould know the register." );

      int length = 0;
      unsigned char * array = machinestates -> GetRegisterAsByteArray ( "temperatures", &length );
      unsigned char test [] = { 0, 1, 0, 3, 1, 0, 0x0C, 0x18, 1, 2, 0, 0x0C, 0x19, 1, 3, 0, 0x0C, 0x1A, 1 };
      
      this -> AssertTrue ( length == 19, "Length should be 9." );

      for ( int i = 0; i < 19; i ++ )
        this -> AssertTrue ( array [i] == test [i], "test and array should be always equal." );

      delete machinestates;
    }

    MACHINESTATES_TEST_C ( Pagination ) {
      MachineStates * machinestates = new MachineStates ( );

      org::types::CString name = "temperatures";
      this -> AssertFalse ( machinestates -> HasRegister ( name ), "Sould not know the register yet." );

      machinestates -> SetState ( name, 1, new machinestates::Temperature ( 1, 0, ( short ) 3096, 1 ) );
      machinestates -> SetState ( name, 2, new machinestates::Temperature ( 2, 0, ( short ) 3097, 1 ) );
      machinestates -> SetState ( name, 3, new machinestates::Temperature ( 3, 0, ( short ) 3098, 1 ) );

      this -> AssertTrue ( machinestates -> HasRegister ( name ), "Sould know the register." );
      this -> AssertTrue ( machinestates -> HasState ( name, 3 ), "Sould know the register." );
      
      // int * sizes = nullptr;
      // int count = 0;
      // unsigned char ** array = machinestates -> GetRegisterAsBytePaginatedArray ( "temperatures", 12, &sizes, &count );
      // unsigned char * array1 = array [0];
      // unsigned char * array2 = array [1];

      // for ( int i = 0; i < 2; i ++ ) {
      //   printf ( "%d, ", sizes [i] );
      // }

      // for ( int i = 0; i < 14; i ++ ) {
      //   printf ( "0x%.2hhX, ", array1 [i] );
      // }

      // printf ( "\n" );

      // for ( int i = 0; i < 9; i ++ ) {
      //   printf ( "0x%.2hhX, ", array2 [i] );
      // }

      this -> AssertTrue ( false, "Test to finish" );

      delete machinestates;
    }

    MACHINESTATES_TEST_C ( AddingStateIfDifferent ) {
      MachineStates * machinestates = new MachineStates ( );

      org::types::CString name = "temperatures";
      this -> AssertFalse ( machinestates -> HasRegister ( name ), "Sould not know the register yet." );

      MachineState * state1 = new machinestates::Temperature ( 3, 0, ( short ) 3098, 1 );
      machinestates -> SetState ( name, 3, state1 );
      this -> AssertTrue ( machinestates -> GetState ( name, 3 ) == state1, "State should be state1 object" );

      MachineState * state2 = new machinestates::Temperature ( 3, 0, ( short ) 3098, 1 );
      machinestates -> SetState ( name, 3, state2 );
      this -> AssertTrue ( machinestates -> GetState ( name, 3 ) == state2, "State should be state2 object" );

      this -> AssertTrue ( state1 != state2, "States should not be the same objects." );

      MachineState * state3 = new machinestates::Temperature ( 2, 0, ( short ) 3098, 1 );
      this -> AssertTrue ( machinestates -> SetStateIfDifferent ( name, 2, state3 ), "Should be inserted as not existing yet" );
      this -> AssertTrue ( machinestates -> GetState ( name, 2 ) == state3, "State should be state3 object" );

      MachineState * state4 = new machinestates::Temperature ( 2, 0, ( short ) 3098, 1 );
      this -> AssertFalse ( machinestates -> SetStateIfDifferent ( name, 2, state4 ), "Should not be inserted as equal to previous state" );
      this -> AssertTrue ( machinestates -> GetState ( name, 2 ) == state3, "State should be STILL be state3 object" );

      this -> AssertTrue ( state3 != state4, "States should not be the same objects." );

      delete machinestates;
    }
  }
}
