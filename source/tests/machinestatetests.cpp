
#include <tests/machinestatetests.hpp>

#include <hardware/machinestate.hpp>

using namespace org::unittest;
using namespace org::hardware;

namespace org {
  namespace unittest {
    MACHINESTATE_TESTS_CLASS_NAME * MACHINESTATE_TESTS_CLASS_NAME::MACHINESTATE_TESTS_CLASS_NAME_instance = new MACHINESTATE_TESTS_CLASS_NAME ( );

    MACHINESTATE_TESTS_CLASS_NAME::MACHINESTATE_TESTS_CLASS_NAME ( void ) {
      this -> SetIdAndGroup ( "machinestate", "MachineState related tests" );
      
      MACHINESTATE_TESTS_ADD ( Creation, "Creation test" );
      MACHINESTATE_TESTS_ADD ( Delete, "Delete test" );
      MACHINESTATE_TESTS_ADD ( ObjectType, "Testing object type string" );
    }

    MACHINESTATE_TEST_C ( Creation ) {
      MachineState * machinestate = new MachineState ( );
      machinestate = machinestate;
    }

    MACHINESTATE_TEST_C ( Delete ) {
      MachineState * machinestate = new MachineState ( );
      delete machinestate;
    }

    MACHINESTATE_TEST_C ( ObjectType ) {
      Object * obj = new MachineState ( );
      this -> AssertEqual ( "org::hardware::MachineState", obj -> ObjectType ( ), "Object type in error" );
      delete obj;
    }
  }
}
