
#include <tests/machinetests.hpp>

#include <fsm/machine.hpp>

using namespace org::unittest;
using namespace org::fsm;

namespace org {
  namespace unittest {
    MACHINE_TESTS_CLASS_NAME * MACHINE_TESTS_CLASS_NAME::MACHINE_TESTS_CLASS_NAME_instance = new MACHINE_TESTS_CLASS_NAME ( );

    MACHINE_TESTS_CLASS_NAME::MACHINE_TESTS_CLASS_NAME ( void ) {
      this -> SetIdAndGroup ( "machine", "Machine related tests" );
      
      MACHINE_TESTS_ADD ( Creation, "Creation test" );
      MACHINE_TESTS_ADD ( Delete, "Delete test" );
      MACHINE_TESTS_ADD ( ObjectType, "Testing object type string" );
    }

    MACHINE_TEST_C ( Creation ) {
      Machine * machine = new Machine ( );
      machine = machine;
      delete machine;
    }

    MACHINE_TEST_C ( Delete ) {
      Machine * machine = new Machine ( );
      delete machine;
    }

    MACHINE_TEST_C ( ObjectType ) {
      Object * obj = new Machine ( );
      this -> AssertEqual ( "org::fsm::Machine", obj -> ObjectType ( ), "Object type in error" );
      delete obj;
    }
  }
}
