
#include <tests/sequencestreamtests.hpp>

#include <virtualization/sequencestream.hpp>

using namespace org::unittest;
using namespace org::virtualization;

namespace org {
  namespace unittest {
    SEQUENCESTREAM_TESTS_CLASS_NAME * SEQUENCESTREAM_TESTS_CLASS_NAME::SEQUENCESTREAM_TESTS_CLASS_NAME_instance = new SEQUENCESTREAM_TESTS_CLASS_NAME ( );

    SEQUENCESTREAM_TESTS_CLASS_NAME::SEQUENCESTREAM_TESTS_CLASS_NAME ( void ) {
      this -> SetIdAndGroup ( "sequencestream", "SequenceStream related tests" );
      
      SEQUENCESTREAM_TESTS_ADD ( Creation, "Creation test" );
      SEQUENCESTREAM_TESTS_ADD ( Delete, "Delete test" );
      SEQUENCESTREAM_TESTS_ADD ( ObjectType, "Testing object type string" );
      SEQUENCESTREAM_TESTS_ADD ( Configuration_Type, "Testing type sequence in configuration" );
      SEQUENCESTREAM_TESTS_ADD ( Configuration_Sequences, "Testing sequences configuration as buffer streams" );
      SEQUENCESTREAM_TESTS_ADD ( SequencingGoingThrough, "Testing sequences data flow" );
    }

    SEQUENCESTREAM_TEST_C ( SequencingGoingThrough ) {
      SequenceStream <unsigned char> * sequencestream = new SequenceStream <unsigned char> ( "sequence-test-1" );
      unsigned char test = 0;
      
      this -> AssertEqual ( 3, sequencestream -> _sequences -> size ( ), "There should be 3 buffers, check also conf" );
      this -> AssertEqual ( 4, sequencestream -> Count ( ), "Count should be 4 -> count from first sequence, check also conf" );
      
      this -> AssertTrue ( sequencestream -> GetOne ( &test ), "Should return having read something" );
      this -> AssertEqual ( ( int ) test, 0x00, "First should be 10" );
      
      this -> AssertTrue ( sequencestream -> GetOne ( &test ), "Should return having read something" );
      this -> AssertEqual ( ( int ) test, 0x01, "First should be 20" );
      
      this -> AssertTrue ( sequencestream -> GetOne ( &test ), "Should return having read something" );
      this -> AssertEqual ( ( int ) test, 0x02, "First should be 30" );
      
      this -> AssertTrue ( sequencestream -> GetOne ( &test ), "Should return having read something" );
      this -> AssertEqual ( ( int ) test, 0x03, "First should be 47" );
      
      this -> AssertFalse ( sequencestream -> GetOne ( &test ), "Should return not having been able to read something" );
      
      this -> AssertEqual ( 2, sequencestream -> _sequences -> size ( ), "There should be 3 buffers, check also conf" );
      this -> AssertEqual ( 5, sequencestream -> Count ( ), "Count should be 4 -> count from first sequence, check also conf" );
      
      this -> AssertTrue ( sequencestream -> GetOne ( &test ), "Should return having read something" );
      this -> AssertEqual ( 0x10, ( int ) test, "First should be 10" );
    }

    SEQUENCESTREAM_TEST_C ( Configuration_Sequences ) {
      SequenceStream <unsigned char> * sequencestream = new SequenceStream <unsigned char> ( "sequence-test-1" );
      
      this -> AssertEqual ( 3, sequencestream -> _sequences -> size ( ), "There should be 3 buffers, check also conf" );
      this -> AssertEqual ( 4, sequencestream -> Count ( ), "Count should be 4 -> count from first sequence, check also conf" );
    }

    SEQUENCESTREAM_TEST_C ( Configuration_Type ) {
      SequenceStream <unsigned char> * sequencestream = new SequenceStream <unsigned char> ( "sequence-test-1" );
      
      this -> AssertEqual ( "buffer", sequencestream -> _type, "Type should be 'buffer', check also conf" );
    }

    SEQUENCESTREAM_TEST_C ( Creation ) {
      SequenceStream <unsigned char> * sequencestream = new SequenceStream <unsigned char> ( );
      sequencestream = sequencestream;
    }

    SEQUENCESTREAM_TEST_C ( Delete ) {
      SequenceStream <unsigned char> * sequencestream = new SequenceStream <unsigned char> ( );
      delete sequencestream;
    }

    SEQUENCESTREAM_TEST_C ( ObjectType ) {
      Object * obj = new SequenceStream <unsigned char> ( );
      this -> AssertEqual ( "org::virtualization::SequenceStream", obj -> ObjectType ( ), "Object type in error" );
      delete obj;
    }
  }
}
