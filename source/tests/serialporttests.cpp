
#include <tests/serialporttests.hpp>

#include <hardware/serialport.hpp>

using namespace org::unittest;
using namespace org::hardware;

namespace org {
  namespace unittest {
    SERIALPORT_TESTS_CLASS_NAME * SERIALPORT_TESTS_CLASS_NAME::SERIALPORT_TESTS_CLASS_NAME_instance = new SERIALPORT_TESTS_CLASS_NAME ( );

    SERIALPORT_TESTS_CLASS_NAME::SERIALPORT_TESTS_CLASS_NAME ( void ) {
      this -> SetIdAndGroup ( "serialport", "SerialPort related tests" );
      
      SERIALPORT_TESTS_ADD ( Creation, "Creation test" );
      SERIALPORT_TESTS_ADD ( Delete, "Delete test" );
      SERIALPORT_TESTS_ADD ( ObjectType, "Testing object type string" );
      SERIALPORT_TESTS_ADD ( ConfigurationTestForSimulatorEnabled, "Testing object simulation configuration" );
      SERIALPORT_TESTS_ADD ( ReadingSimulationOBO, "Testing reading flow simulation One By One" );
      SERIALPORT_TESTS_ADD ( ReadingSimulationBP, "Testing reading flow simulation By Pack" );
      SERIALPORT_TESTS_ADD ( ReadingSimulationSequenceTwo, "Testing reading flow simulation on two sequences" );
      SERIALPORT_TESTS_ADD ( ReadingSimulationSequenceTwoWithOverflow, "Testing reading flow simulation on two sequences adding a reading overflow on the second sequence" );
      SERIALPORT_TESTS_ADD ( ReadingSimulationEmptySequence, "Testing reading flow simulation on an empty sequence" );
    }

    SERIALPORT_TEST_C ( ReadingSimulationEmptySequence ) {
      SerialPort * serialport = new SerialPort ( "serial-port-test" );
      
      this -> AssertTrue ( serialport -> _simulationEnable, "Simulator should be enabled" );
      this -> AssertTrue ( serialport -> _openingSimulation, "Simulator should be able to open the door" );
      this -> AssertTrue ( serialport -> _simulationStream != nullptr, "Simulation stream should have been set" );
      this -> AssertEqual ( "org::virtualization::SequenceStream", serialport -> _simulationStream -> ObjectType ( ), "Simulation stream should be a sequence virtual stream" );

      unsigned char answer [32];
      this -> AssertEqual ( 4, serialport -> Read ( answer, 15 ), "Should have read 4" );
      this -> AssertEqual ( 5, serialport -> Read ( answer, 15 ), "Should have read 5" );
      this -> AssertEqual ( 0, serialport -> Read ( answer, 15 ), "Should have read 0" );
      this -> AssertEqual ( 6, serialport -> Read ( answer, 15 ), "Should have read 6" );
    }

    SERIALPORT_TEST_C ( ReadingSimulationSequenceTwoWithOverflow ) {
      SerialPort * serialport = new SerialPort ( "serial-port-test" );
      
      this -> AssertTrue ( serialport -> _simulationEnable, "Simulator should be enabled" );
      this -> AssertTrue ( serialport -> _openingSimulation, "Simulator should be able to open the door" );
      this -> AssertTrue ( serialport -> _simulationStream != nullptr, "Simulation stream should have been set" );
      this -> AssertEqual ( "org::virtualization::SequenceStream", serialport -> _simulationStream -> ObjectType ( ), "Simulation stream should be a sequence virtual stream" );

      unsigned char answer [15];

      this -> AssertEqual ( 4, serialport -> Read ( answer, 15 ), "The answer should not be null" );

      this -> AssertEqual ( ( unsigned char ) 0x00, answer [0], "0x00 was waited here" );
      this -> AssertEqual ( ( unsigned char ) 0x01, answer [1], "0x01 was waited here" );
      this -> AssertEqual ( ( unsigned char ) 0x02, answer [2], "0x02 was waited here" );
      this -> AssertEqual ( ( unsigned char ) 0x03, answer [3], "0x03 was waited here" );

      this -> AssertEqual ( 3, serialport -> Read ( answer, 3 ), "The answer should not be null" );

      this -> AssertEqual ( ( unsigned char ) 0x10, answer [0], "0x00 was waited here" );
      this -> AssertEqual ( ( unsigned char ) 0x20, answer [1], "0x01 was waited here" );
      this -> AssertEqual ( ( unsigned char ) 0x30, answer [2], "0x02 was waited here" );

      this -> AssertEqual ( 2, serialport -> Read ( answer, 15 ), "The answer should not be null" );

      this -> AssertEqual ( ( unsigned char ) 0x40, answer [0], "0x03 was waited here" );
      this -> AssertEqual ( ( unsigned char ) 0x50, answer [1], "0x03 was waited here" );
    }

    SERIALPORT_TEST_C ( ReadingSimulationSequenceTwo ) {
      SerialPort * serialport = new SerialPort ( "serial-port-test" );
      
      this -> AssertTrue ( serialport -> _simulationEnable, "Simulator should be enabled" );
      this -> AssertTrue ( serialport -> _openingSimulation, "Simulator should be able to open the door" );
      this -> AssertTrue ( serialport -> _simulationStream != nullptr, "Simulation stream should have been set" );
      this -> AssertEqual ( "org::virtualization::SequenceStream", serialport -> _simulationStream -> ObjectType ( ), "Simulation stream should be a sequence virtual stream" );

      unsigned char answer [15];

      this -> AssertEqual ( 4, serialport -> Read ( answer, 15 ), "The answer should not be null" );

      this -> AssertEqual ( ( unsigned char ) 0x00, answer [0], "0x00 was waited here" );
      this -> AssertEqual ( ( unsigned char ) 0x01, answer [1], "0x01 was waited here" );
      this -> AssertEqual ( ( unsigned char ) 0x02, answer [2], "0x02 was waited here" );
      this -> AssertEqual ( ( unsigned char ) 0x03, answer [3], "0x03 was waited here" );

      this -> AssertEqual ( 5, serialport -> Read ( answer, 15 ), "The answer should not be null" );

      this -> AssertEqual ( ( unsigned char ) 0x10, answer [0], "0x00 was waited here" );
      this -> AssertEqual ( ( unsigned char ) 0x20, answer [1], "0x01 was waited here" );
      this -> AssertEqual ( ( unsigned char ) 0x30, answer [2], "0x02 was waited here" );
      this -> AssertEqual ( ( unsigned char ) 0x40, answer [3], "0x03 was waited here" );
      this -> AssertEqual ( ( unsigned char ) 0x50, answer [4], "0x03 was waited here" );

      this -> AssertEqual ( 0, serialport -> Read ( answer, 15 ), "The answer should not be null" );

      this -> AssertEqual ( 6, serialport -> Read ( answer, 15 ), "The answer should not be null" );

      this -> AssertEqual ( ( unsigned char ) 0x11, answer [0], "0x00 was waited here" );
      this -> AssertEqual ( ( unsigned char ) 0x22, answer [1], "0x01 was waited here" );
      this -> AssertEqual ( ( unsigned char ) 0x33, answer [2], "0x02 was waited here" );
      this -> AssertEqual ( ( unsigned char ) 0x44, answer [3], "0x03 was waited here" );
      this -> AssertEqual ( ( unsigned char ) 0x55, answer [4], "0x03 was waited here" );
      this -> AssertEqual ( ( unsigned char ) 0x66, answer [5], "0x03 was waited here" );
    }

    SERIALPORT_TEST_C ( ReadingSimulationBP ) {
      SerialPort * serialport = new SerialPort ( "serial-port-test" );
      
      this -> AssertTrue ( serialport -> _simulationEnable, "Simulator should be enabled" );
      this -> AssertTrue ( serialport -> _openingSimulation, "Simulator should be able to open the door" );
      this -> AssertTrue ( serialport -> _simulationStream != nullptr, "Simulation stream should have been set" );
      this -> AssertEqual ( "org::virtualization::SequenceStream", serialport -> _simulationStream -> ObjectType ( ), "Simulation stream should be a sequence virtual stream" );

      unsigned char answer [15];

      this -> AssertEqual ( 4, serialport -> Read ( answer, 15 ), "The answer should not be null" );

      this -> AssertEqual ( ( unsigned char ) 0x00, answer [0], "0x00 was waited here" );
      this -> AssertEqual ( ( unsigned char ) 0x01, answer [1], "0x01 was waited here" );
      this -> AssertEqual ( ( unsigned char ) 0x02, answer [2], "0x02 was waited here" );
      this -> AssertEqual ( ( unsigned char ) 0x03, answer [3], "0x03 was waited here" );
    }

    SERIALPORT_TEST_C ( ReadingSimulationOBO ) {
      SerialPort * serialport = new SerialPort ( "serial-port-test" );
      
      this -> AssertTrue ( serialport -> _simulationEnable, "Simulator should be enabled" );
      this -> AssertTrue ( serialport -> _openingSimulation, "Simulator should be able to open the door" );
      this -> AssertTrue ( serialport -> _simulationStream != nullptr, "Simulation stream should have been set" );
      this -> AssertEqual ( "org::virtualization::SequenceStream", serialport -> _simulationStream -> ObjectType ( ), "Simulation stream should be a sequence virtual stream" );

      unsigned char * answer = serialport -> Read ( );
      this -> AssertNotEqual ( nullptr, answer, "The answer should not be null" );
      this -> AssertEqual ( ( unsigned char ) 0x00, answer [0], "First  byte should be 0x00" );

      answer = serialport -> Read ( );
      this -> AssertNotEqual ( nullptr, answer, "The second answer should not be null" );
      this -> AssertEqual ( ( unsigned char ) 0x01, answer [0], "Second byte should be 0x01" );

      answer = serialport -> Read ( );
      this -> AssertNotEqual ( nullptr, answer, "The second answer should not be null" );
      this -> AssertEqual ( ( unsigned char ) 0x02, answer [0], "Second byte should be 0x02" );

      answer = serialport -> Read ( );
      this -> AssertNotEqual ( nullptr, answer, "The second answer should not be null" );
      this -> AssertEqual ( ( unsigned char ) 0x03, answer [0], "Second byte should be 0x03" );

      answer = serialport -> Read ( );
      this -> AssertNotEqual ( nullptr, answer, "The second answer should not be null" );
      this -> AssertEqual ( ( unsigned char ) 0x10, answer [0], "Second byte should be 0x10" );
    }

    SERIALPORT_TEST_C ( ConfigurationTestForSimulatorEnabled ) {
      SerialPort * serialport = new SerialPort ( "serial-port-test" );
      
      this -> AssertTrue ( serialport -> _simulationEnable, "Simulator should be enabled" );
      this -> AssertTrue ( serialport -> _openingSimulation, "Simulator should be able to open the door" );
      this -> AssertTrue ( serialport -> _simulationStream != nullptr, "Simulation stream should have been set" );
      this -> AssertEqual ( serialport -> _simulationStream -> ObjectType ( ), "org::virtualization::SequenceStream", "Simulation stream should be a sequence virtual stream" );
    }

    SERIALPORT_TEST_C ( Creation ) {
      SerialPort * serialport = new SerialPort ( );
      serialport = serialport;
    }

    SERIALPORT_TEST_C ( Delete ) {
      SerialPort * serialport = new SerialPort ( );
      delete serialport;
    }

    SERIALPORT_TEST_C ( ObjectType ) {
      Object * obj = new SerialPort ( );
      this -> AssertEqual ( "org::hardware::SerialPort", obj -> ObjectType ( ), "Object type in error" );
      delete obj;
    }
  }
}
