
#include <tests/temperaturetests.hpp>

#include <hardware/machinestates/temperature.hpp>

using namespace org::unittest;
using namespace org::hardware::machinestates;

namespace org {
  namespace unittest {
    TEMPERATURE_TESTS_CLASS_NAME * TEMPERATURE_TESTS_CLASS_NAME::TEMPERATURE_TESTS_CLASS_NAME_instance = new TEMPERATURE_TESTS_CLASS_NAME ( );

    TEMPERATURE_TESTS_CLASS_NAME::TEMPERATURE_TESTS_CLASS_NAME ( void ) {
      this -> SetIdAndGroup ( "temperature", "Temperature related tests" );
      
      TEMPERATURE_TESTS_ADD ( Creation, "Creation test" );
      TEMPERATURE_TESTS_ADD ( Delete, "Delete test" );
      TEMPERATURE_TESTS_ADD ( ObjectType, "Testing object type string" );
      TEMPERATURE_TESTS_ADD ( SettingBytes, "Controling the bytes array" );
      TEMPERATURE_TESTS_ADD ( DoubleConstructor, "Controling double construction" );
      TEMPERATURE_TESTS_ADD ( MachineStateEqual, "Controling double construction" );
    }

    TEMPERATURE_TEST_C ( MachineStateEqual ) {
      Temperature * temperature = new Temperature ( 3, 0, ( short ) -210, -2 );
      Temperature * doubleTemp = new Temperature ( 3, 0, -2.1 );
      Temperature * id = new Temperature ( 2, 0, -2.1 );
      Temperature * value = new Temperature ( 3, 0, -2.2 );
      Temperature * sign = new Temperature ( 3, 0, 2.1 );
      Temperature * power = new Temperature ( 3, 0, 21 );
      
      this -> AssertTrue ( temperature -> IsEqual ( doubleTemp ), "The machine states should be equals" );
      this -> AssertFalse ( temperature -> IsEqual ( id ), "Id should not be equal" );
      this -> AssertFalse ( temperature -> IsEqual ( value ), "Temperature should not be equal" );
      this -> AssertFalse ( temperature -> IsEqual ( sign ), "Temperature sign should not be equal" );
      this -> AssertFalse ( temperature -> IsEqual ( power ), "Temperature power should not be equal" );

      delete temperature;
    }

    TEMPERATURE_TEST_C ( Creation ) {
      Temperature * temperature = new Temperature ( );
      temperature = temperature;
    }

    TEMPERATURE_TEST_C ( Delete ) {
      Temperature * temperature = new Temperature ( );
      delete temperature;
    }

    TEMPERATURE_TEST_C ( ObjectType ) {
      Object * obj = new Temperature ( );
      this -> AssertEqual ( "org::hardware::machinestates::Temperature", obj -> ObjectType ( ), "Object type in error" );
      delete obj;
    }

    TEMPERATURE_TEST_C ( SettingBytes ) {
      Temperature * temperature = new Temperature ( 3, 0, ( short ) 3098, 1 );
      
      unsigned char * array = temperature -> ByteArray ( );
      this -> AssertTrue ( array [0] == 3, "Should be equal" );
      this -> AssertTrue ( array [1] == 0, "Should be equal" );
      this -> AssertTrue ( array [2] == 0x0C, "Should be equal" );
      this -> AssertTrue ( array [3] == 0x1A, "Should be equal" );
      this -> AssertTrue ( array [4] == 1, "Should be equal" );

      delete temperature;
    }

    TEMPERATURE_TEST_C ( DoubleConstructor ) {
      Temperature * temperature = new Temperature ( 3, 0, ( short ) -210, -2 );
      Temperature * doubleTemp = new Temperature ( 3, 0, -2.1 );
      
      unsigned char * array = temperature -> ByteArray ( );
      unsigned char * doubled = doubleTemp -> ByteArray ( );
      
      for ( int i = 0; i < 5; i ++ )
        this -> AssertEqual ( array [i], doubled [i], "construction with or without double should give the same bytes" );

      delete temperature;
    }
  }
}
