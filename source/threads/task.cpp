
#include <threads/task.hpp>

#include <wrappers/thread.hpp>
#include <lib/stdlib.hpp>
#include <tools/misc/wait.hpp>
#include <tools/misc/stringextension.hpp>
#include <sys/types.h>
#include <sys/syscall.h>
#include <unistd.h>

namespace org {
  namespace threads {
    struct Task::TaskCheshireCat {
      org::wrappers::Thread * _thread;
    };
    
    void Task::StartThread ( void ) {
      if ( !this -> IsExecuting ( ) ) {
        this -> _executing = true;
        if ( this -> Join ( ) )
          delete this -> _dPtr -> _thread;
        this -> _dPtr -> _thread = new org::wrappers::Thread ( &Task::StaticAuxiliary, ( void * ) this );
      }
    }

    void Task::StaticAuxiliary ( void * task ) {
      Task * realTask = ( Task * ) ( task );
      if ( realTask != nullptr )
        StaticAuxiliary ( realTask );
    }

    void Task::StaticAuxiliary ( Task* task ) {
      task -> StartAuxiliary ( );
    }

    void Task::StartAuxiliary ( void ) {
      if ( this -> _name != nullptr )
        pthread_setname_np ( pthread_self ( ), this -> _name );
      this -> _askedTerminate   = false;
      this -> _inStartingBlock  = true;
      
      while ( this -> _inStartingBlock )
        org::tools::misc::Wait::Milliseconds ( 1 );

      this -> Auxiliary ( );

      this -> _executing        = false;
      this -> _askedTerminate   = false;
    }

    bool Task::Join ( void ) {
      if ( this -> _dPtr -> _thread != nullptr )
        if ( this -> _dPtr -> _thread -> Joinable ( ) )
          this -> _dPtr -> _thread -> Join ( );
      return this -> _dPtr -> _thread != nullptr;
    }
    
    void Task::InitializeTask ( void ) {
      this -> _dPtr             = new struct TaskCheshireCat ( );
      this -> _dPtr -> _thread  = nullptr;
      this -> _executing        = false;
      this -> _askedTerminate   = false;
      this -> _inStartingBlock  = false;
      this -> _name             = nullptr;
    }
    
    void Task::Auxiliary ( void ) { }
    
    Task::Task ( void ) {
      this -> InitializeTask ( );
    }

    Task::~Task ( void ) {
      if ( this -> _dPtr -> _thread != nullptr )
        if ( this -> Join ( ) )
          delete this -> _dPtr -> _thread;
      delete this -> _dPtr;
      lib::Stdlib::Free ( ( void * ) this -> _name );
    }

    void Task::SetThreadName ( const char * newName ) {
      lib::Stdlib::Free ( ( void * ) this -> _name );
      this -> _name = org::tools::misc::CString::GetCopyOfCString ( newName );
    }

    const char * Task::ThreadName ( void ) {
      return this -> _name;
    }

    void Task::Execute ( void ) {
      this -> _inStartingBlock = false;
      this -> StartThread ( );
      while ( ! this -> _inStartingBlock )
        org::tools::misc::Wait::Milliseconds ( 1 );
      this -> _inStartingBlock = false;
    }

    void Task::WaitForIt ( void ) {
      this -> Join ( );
    }

    void Task::AskToTerminate ( void ) {
      this -> _askedTerminate = true;
    }

    bool Task::IsAskedToTerminate ( ) {
      return this -> _askedTerminate;
    }

    bool Task::IsExecuting ( ) {
      return this -> _executing;
    }

    void Task::Start ( ) {
      if ( !this -> IsExecuting ( ) )
        this -> Execute ( );
    }

    void Task::Terminate ( ) {
      this -> AskToTerminate ( );
      this -> WaitForIt ( );
    }

    const char * Task::ObjectType ( void ) const {
      return "org::threads::Task";
    }
  }
}
