


#include <threads/taskable.hpp>

#include <wrappers/mutex.hpp>

namespace org {
  namespace threads {
    struct Taskable::TaskableCheshireCat {
      org::wrappers::Mutex * _mutex;
    };

    void Taskable::InitializeTaskable ( void ) {
      this -> _dPtr           = new struct TaskableCheshireCat ( );
      this -> _dPtr -> _mutex = nullptr;
    }
    
    void Taskable::MakeTaskable ( void ) {
      if ( this -> _dPtr -> _mutex != nullptr )
        delete this -> _dPtr -> _mutex;
      this -> _dPtr -> _mutex = new org::wrappers::Mutex ( );
    }

    void Taskable::ThreadLock ( void ) {
      if ( this -> _dPtr -> _mutex != nullptr )
        this -> _dPtr -> _mutex -> Lock ( );
    }

    void Taskable::ThreadUnlock ( void ) {
      if ( this -> _dPtr -> _mutex != nullptr )
        this -> _dPtr -> _mutex -> Unlock ( );
    }
    
    Taskable::Taskable ( void ) {
      this -> InitializeTaskable ( );
    }

    Taskable::~Taskable ( void ) {
      if ( this -> _dPtr -> _mutex != nullptr )
        delete this -> _dPtr -> _mutex;
      delete this -> _dPtr;
    }
  }
}
