


#include <threads/timer.hpp>

#include <tools/misc/wait.hpp>

namespace org {
  namespace threads {
    void TimerEvent::InitializeTimerEvent ( void ) {
      this -> _event = nullptr;
      this -> _remaining = 0;
      this -> _periodicValue = -1;
    }
    
    TimerEvent::TimerEvent ( const char * name, org::eventing::EventCall <org::threads::TimerEvent>* event, int remaining, bool periodic ) {
      this -> InitializeTimerEvent ( );
      this -> _name = org::tools::misc::CString::GetCopyOfCString ( name );
      this -> _event = event;
      this -> _remaining = remaining > 0 ? remaining : 0;
      if ( periodic )
        this -> _periodicValue = this -> _remaining;
    }
    
    TimerEvent::~TimerEvent ( void ) {
      lib::Stdlib::Free ( this -> _name );
      delete this -> _event;
    }

    bool TimerEvent::IsPeriodic ( void ) {
      return this -> _periodicValue != -1;
    }

    void TimerEvent::DecrementRemaining ( void ) {
      if ( this -> _remaining > 0 )
        this -> _remaining --;
    }

    bool TimerEvent::IsFinished ( void ) {
      return this -> _remaining == 0;
    }

    void TimerEvent::SetRemaining ( int remaining ) {
      this -> _remaining = remaining;
    }

    void TimerEvent::Reset ( void ) {
      this -> _remaining = this -> _periodicValue;
    }

    org::eventing::EventCall <org::threads::TimerEvent>* TimerEvent::GetEvent ( void ) {
      return this -> _event;
    }

    char* TimerEvent::GetName ( void ) {
      return this -> _name;
    }
    
    TimerEventMap::TimerEventMap ( void ) { }

    TimerEventMap::~TimerEventMap ( void ) {
      
    }

    std::list <org::threads::TimerEvent*>* TimerEventMap::DecrementAll ( void ) {
      std::list <org::threads::TimerEvent*>* eventsToTrigger = new std::list <org::threads::TimerEvent*> ( );
      std::map <org::types::CString, TimerEvent*>::iterator mapIt = this -> Begin ( );
      std::map <org::types::CString, TimerEvent*>::iterator end = this -> End ( );
      std::list <org::types::CString *> * toRemove = new std::list <org::types::CString *> ( );
      this -> ThreadLock ( );
      for ( ; mapIt != end; ++ mapIt ) {
        mapIt -> second -> DecrementRemaining ( );
        if ( mapIt -> second -> IsFinished ( ) ) {
          eventsToTrigger -> push_back ( mapIt -> second );
          if ( mapIt -> second -> IsPeriodic ( ) )
            mapIt -> second -> Reset ( );
          else {
            toRemove -> push_back ( new org::types::CString ( ( mapIt -> first ).c_str ( ) ) );
          }
        }
      }

      for ( std::list <org::types::CString *>::iterator toRemIt = toRemove -> begin ( ); toRemIt != toRemove -> end ( ); ++ toRemIt ) {
        this -> InterfaceErase ( * ( * toRemIt ) );
        delete *toRemIt;
      }

      delete toRemove;

      this -> ThreadUnlock ( );

      if ( eventsToTrigger -> size ( ) == 0 ) {
        delete eventsToTrigger;
        eventsToTrigger = nullptr;
      }
      return eventsToTrigger;
    }

    void TimerEventMap::Remove ( org::types::CString name ) {
      this -> ThreadLock ( );
      if ( this -> InterfaceContains ( name ) ) {
        delete this -> InterfaceGet ( name );
        this -> InterfaceErase ( name );
      }
      this -> ThreadUnlock ( );
    }

    void TimerEventMap::RemoveAll ( void ) {
      std::map <org::types::CString, TimerEvent*>::iterator mapIt = this -> Begin ( );
      std::map <org::types::CString, TimerEvent*>::iterator end = this -> End ( );
      this -> ThreadLock ( );
      for ( ; mapIt != end; mapIt ++ ) {
        delete mapIt -> second;
        // this -> InterfaceErase ( mapIt -> first );
      }
      this -> Clear ( );
      this -> ThreadUnlock ( );
    }
    
    std::list <org::threads::TimerEvent*>* TimerTask::Timering ( void ) {
      org::tools::misc::Wait::Milliseconds ( this -> _precision );
      std::list <org::threads::TimerEvent*>* ans = this -> _events -> DecrementAll ( );
      return ans;
    }

    void TimerTask::TimeEvent ( org::eventing::EventArgs <void*, char*, std::list <org::threads::TimerEvent*>*>* args ) {
      if ( args != nullptr ) {
        if ( args -> GetArgs ( ) != nullptr ) {
          for ( std::list <org::threads::TimerEvent*>::iterator it = args -> GetArgs ( ) -> begin ( ); it != args -> GetArgs ( ) -> end ( ); it ++ ) {
            if ( ( *it ) -> GetEvent ( ) != nullptr ) {
              ( *it ) -> GetEvent ( ) -> Call ( ( void* ) this, ( *it ) );
              if ( ! ( *it ) -> IsPeriodic ( ) )
                delete *it;
            } else {
              if ( ! ( *it ) -> IsPeriodic ( ) )
                delete *it;
            }
          }
          delete args -> GetArgs ( );
        }
        delete args;
      }
      if ( this -> _events -> IsEmpty ( ) )
        this -> _puller -> AskToTerminate ( );
    }

    void TimerTask::Notify ( org::types::CString name, TimerEvent* timerEvent ) {
      if ( ! this -> _events -> Contains ( name ) )
        this -> _events -> Insert ( name, timerEvent );
      else
        delete timerEvent;
      if ( ! this -> IsExecuting ( ) ) {
        this -> Start ( );
      }
    }

    void TimerTask::InitializeTimerTask ( void ) {
      this -> MakeTaskable ( );
      this -> _precision      = 1000;
      this -> _events         = new TimerEventMap ( );
      this -> _event          = new org::eventing::EventDelegateCall <TimerTask, std::list <org::threads::TimerEvent*>> ( this, &TimerTask::TimeEvent );
      this -> SetThreadName ( "timer" );
      this -> SetPuller ( new PullerTask <std::list <org::threads::TimerEvent*>*> ( false, this ) );
      char * newName          = org::tools::misc::CString::Concatenate ( this -> ThreadName ( ), "-puller" );
      this -> _puller -> SetThreadName ( newName );
      lib::Stdlib::Free ( ( void * ) newName );
    }
    
    TimerTask::TimerTask ( int precision ) {
      this -> InitializeTimerTask ( );
      this -> _precision = precision;
    }
    
    TimerTask::~TimerTask ( void ) {
      this -> _events -> RemoveAll ( );
      delete this -> _event;
      delete this -> _events;
    }

    std::list <org::threads::TimerEvent*>* TimerTask::Pull ( void ) {
      std::list <org::threads::TimerEvent*>* ans = nullptr;
      while ( ans == nullptr && ! ( this -> IsAskedToTerminate ( ) ) && ! ( this -> _puller -> IsAskedToTerminate ( ) ) )
        ans = this -> Timering ( );
      return ans;
    }

    void TimerTask::Notify ( org::types::CString name, org::eventing::EventCall <org::threads::TimerEvent>* event, int timeout, bool periodic ) {
      this -> Notify ( name, new TimerEvent ( name.c_str ( ), event, timeout, periodic ) );
    }

    void TimerTask::NotifyIn ( org::types::CString name, org::eventing::EventCall <org::threads::TimerEvent>* event, int timeout ) {
      this -> Notify ( name, event, timeout, false );
    }

    void TimerTask::NotifyEvery ( org::types::CString name, org::eventing::EventCall <org::threads::TimerEvent>* event, int timeout ) {
      this -> Notify ( name, event, timeout, true );
    }

    bool TimerTask::Update ( org::types::CString name, int timeout ) {
      bool updated = this -> _events -> Contains ( name );
      if ( updated )
        this -> _events -> Get ( name ) -> SetRemaining ( timeout );
      return updated;
    }

    void TimerTask::Remove ( org::types::CString name ) {
      this -> _events -> Remove ( name );
    }

    void TimerTask::DenotifyAll ( void ) {
      this -> _events -> RemoveAll ( );
    }

    void TimerTask::Terminate ( ) {
      this -> DenotifyAll ( );
      TunnelPullEventTask <std::list <org::threads::TimerEvent*>>::Terminate ( );
    }

    const char * TimerTask::ObjectType ( void ) const {
      return "org::threads::TimerTask";
    }
  }
}
