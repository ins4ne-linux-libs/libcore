


#include <tools/misc/conversion.hpp>

#include <constants/libcore.hpp>
#include <lib/stdio.hpp>

namespace org {
  namespace tools {
    namespace misc {
      bool Convert::FromTypeToType ( const char * toConvert, const char ** result ) {
        bool converted = toConvert != nullptr;
        if ( converted )
          *result = org::tools::misc::CString::GetCopyOfCString ( toConvert );
        return converted;
      }

      bool Convert::FromTypeToType ( const char * toConvert, char ** result ) {
        bool converted = toConvert != nullptr;
        if ( converted )
          *result = ( char * ) org::tools::misc::CString::GetCopyOfCString ( toConvert );
        return converted;
      }

      bool Convert::FromTypeToType ( const char * toConvert, int * result ) {
        bool converted = toConvert != nullptr;
        if ( converted )
          *result = lib::Stdlib::Atoi ( toConvert );
        return converted;
      }

      bool Convert::FromTypeToType ( const char * toConvert, char * result ) {
        bool converted = toConvert != nullptr;
        if ( converted )
          *result = toConvert [0];
        return converted;
      }

      bool Convert::FromTypeToType ( const char * toConvert, unsigned char * result ) {
        bool converted = toConvert != nullptr;
        if ( converted )
          *result = ( unsigned char ) lib::Stdlib::Atoi ( toConvert );
        return converted;
      }

      bool Convert::FromTypeToType ( const char * toConvert, short * result ) {
        bool converted = toConvert != nullptr;
        if ( converted )
          *result = ( short ) lib::Stdlib::Atoi ( toConvert );
        return converted;
      }

      bool Convert::FromTypeToType ( const char * toConvert, bool * result ) {
        bool converted = false;
        if ( toConvert != nullptr ) {
          if ( org::tools::misc::CString::IsEqual ( toConvert, "false" ) ) {
            *result = false;
            converted = true;
          } else if ( org::tools::misc::CString::IsEqual ( toConvert, LITERAL_TRUE ) ) {
            *result = true;
            converted = true;
          }
        }
        return converted;
      }

      bool Convert::FromTypeToType ( const char * toConvert, double * result ) {
        bool converted = toConvert != nullptr;
        if ( converted )
          *result = lib::Stdlib::Atof ( toConvert );
        return converted;
      }

      bool Convert::FromTypeToType ( char toConvert, int * result ) {
        bool converted = toConvert >= LITERAL_0 && toConvert <= LITERAL_9;
        if ( converted )
          *result = toConvert - LITERAL_0;
        return converted;
      }

      short Convert::ToShort ( unsigned char * array ) {
        short ans = array [0];
        ans <<= 8;
        ans |= array [1];
        return ans;
      }

      bool Convert::ToBoolean ( char * toConvert, bool * result ) {
        bool converted = false;
        if ( org::tools::misc::CString::IsEqual ( toConvert, "false" ) ) {
          *result = false;
          converted = true;
        } else if ( org::tools::misc::CString::IsEqual ( toConvert, LITERAL_TRUE ) ) {
          *result = true;
          converted = true;
        }
        return converted;
      }
      
      bool Convert::ToInt ( char * integerString, int * result ) {
        bool converted = false;
        if ( integerString != nullptr ) {
          *result = lib::Stdlib::Atoi ( integerString );
          converted = true;
        }
        return converted;
      }
      
      bool Convert::ToByte ( char * integerString, unsigned char * result ) {
        bool converted = false;
        if ( integerString != nullptr ) {
          *result = ( unsigned char ) lib::Stdlib::Atoi ( integerString );
          converted = true;
        }
        return converted;
      }
      
      bool Convert::ToShort ( char * integerString, short * result ) {
        bool converted = false;
        if ( integerString != nullptr ) {
          *result = ( short ) lib::Stdlib::Atoi ( integerString );
          converted = true;
        }
        return converted;
      }

      char * Convert::ToCString ( short shortInteger ) {
        char buffer [5];
        lib::Stdio::SnPrintF ( buffer, 5, "%.4X", shortInteger );
        return org::tools::misc::CString::GetCopyOfCString ( buffer );
      }

      const char * Convert::ToCString ( int value ) {
        return ToCString <int> ( value, 10, "%d" );
      }

      const char * Convert::ToCString ( int * value ) {
        return ToCString <int *> ( value, 16, "%p" );
      }

      const char * Convert::ToCString ( void * value ) {
        return ToCString <void *> ( value, 16, "%p" );
      }

      const char * Convert::ToCString ( char value ) {
        return ToCString <char> ( value, 2, "%c" );
      }

      const char * Convert::ToCString ( const char * value ) {
        return org::tools::misc::CString::GetCopyOfCString ( value );
      }

      const char * Convert::ToCString ( char * value ) {
        return ToCString <char *> ( value, 16, "%p" );
      }

      const char * Convert::ToCString ( double floating ) {
        char buffer [10];
        lib::Stdio::SnPrintF ( buffer, 10, "%f", floating );
        return org::tools::misc::CString::GetCopyOfCString ( buffer );
      }

      const char * Convert::ToCString ( const char * format, double floating ) {
        char buffer [10];
        lib::Stdio::SnPrintF ( buffer, 10, format, floating );
        return org::tools::misc::CString::GetCopyOfCString ( buffer );
      }
    }
  }
}
