


#include <tools/misc/fileextension.hpp>

#include <tools/misc/stringextension.hpp>
#include <abstraction/file.hpp>

namespace org {
  namespace tools {
    namespace misc {
      int FileExtension::CountLines ( const char * filePath ) {
        return org::abstraction::File::CountLines ( filePath );
      }

      const char ** FileExtension::ReadAllLines ( const char * filePath, int* count ) {
        return org::abstraction::File::ReadAllLines ( filePath, count );
      }

      char * FileExtension::ReadFile ( const char * filePath ) {
        int count = 0;
        const char  ** lines = ReadAllLines ( filePath, &count );
        char * finalString = org::tools::misc::CString::Concatenate ( lines, count, "\n" );
        org::tools::misc::CString::FreeStringArray ( ( char ** ) lines, count );
        return finalString;
      }
      
      unsigned char * FileExtension::ReadBinaryFile ( const char * filePath, int * size ) {
        return org::abstraction::File::ReadBinaryFile ( filePath, size );
      }

      const char ** FileExtension::ReadAllLinesAndClean ( const char * filePath, int* count ) {
        const char ** file = FileExtension::ReadAllLines ( filePath, count );
        int fileCount = *count;
        const char ** clean =  ( const char  ** ) org::tools::misc::CString::RemoveEmptyEntries ( ( char ** ) file, *count, count );
        char* trimmed = nullptr;
        for ( int i = 0; i < *count; i ++ ) {
          trimmed = org::tools::misc::CString::OldTrim ( ( char * ) clean [i] );
          org::tools::misc::CString::FreeCharArray ( ( char * ) clean [i] );
          clean [i] = trimmed;
        }
        org::tools::misc::CString::FreeStringArray ( ( char ** ) file, fileCount );
        return clean;
      }
    }
  }
}
