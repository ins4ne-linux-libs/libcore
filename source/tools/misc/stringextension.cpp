
#include <tools/misc/stringextension.hpp>

#include <abstraction/time.hpp>
#include <process/memorymanager.hpp>
#include <constants/libcore.hpp>
#include <lib/stdlib.hpp>
#include <abstraction/memory.hpp>

namespace org {
  namespace tools {
    namespace misc {
      char * CString::GetCTime ( char* timeToFill, const char * format ) {
        return abstraction::Time::Now ( timeToFill, format, true );
      }

      char* CString::GetCharArray ( int size ) {
        char* ans = nullptr;
        if ( size > 0 )
          ans = ( char * ) org::abstraction::Memory::Allocate ( size * sizeof ( char ) );
        return ans;
      }

      char** CString::GetStringArray ( int size ) {
        return ( char ** ) org::abstraction::Memory::Allocate ( size * sizeof ( char * ) );
      }

      void CString::FreeCharArray ( char* array ) {
        org::process::MemoryManager::FreeM ( ( void* ) array );
      }

      void CString::FreeStringArray ( char** array, int size ) {
        org::process::MemoryManager::FreeMA ( ( void** ) array, size );
      }

      char* CString::GetStringFrom ( const char* array, int size ) {
        char* newString = GetCharArray ( size + 1 );
        CString::CopyStringTo ( array, newString, size );
        return newString;
      }

      void CString::CopyStringToMax ( const char * source, char * destination, int max ) {
        int sourceSize = CString::GetCCharArrayLength ( source );
        CString::CopyStringTo ( source, destination, sourceSize > max ? max : sourceSize );
      }

      void CString::CopyStringTo ( const char * source, char * destination ) {
        CString::CopyStringTo ( source, destination, CString::GetCCharArrayLength ( source ) );
      }

      void CString::CopyStringTo ( const char * source, char * destination, int size ) {
        if ( source != nullptr && destination != nullptr )
          for ( int i = 0; i < size; i ++ )
            destination [i] = source [i];
      }

      char* CString::GetCopyOfCString ( const char* cString ) {
        return GetStringFrom ( cString, GetCCharArrayLength ( cString ) );
      }

      char** CString::GetCopyOfStringArray ( char** cStrings, int stringCount ) {
        char** newArray = GetStringArray ( stringCount );
        for ( int i = 0; i < stringCount; i ++ )
          newArray [i] = GetCopyOfCString ( cStrings [i] );
        return newArray;
      }

      int CString::GetCCharArrayLength ( const char* cString ) {
        int ans = 0;
        if ( cString != nullptr )
          while ( cString [ans++] != 0 ) ;
        if ( ans <= 0 )
          ans = 1;
        return ans - 1;
      }

      char* CString::Concatenate ( const char* cString1, const char* cString2 ) {
        const char* strings [2] = { cString1, cString2 };
        return Concatenate ( strings, 2 );
      }

      char* CString::Concatenate ( const char* cString1, const char* cString2, char separator ) {
        char* sepString = GetCharArray ( 2 );
        sepString [0] = separator;
        const char* strings [3] = { cString1, sepString, cString2 };
        char* finalString = Concatenate ( strings, 3 );
        org::process::MemoryManager::FreeM ( ( void* ) sepString );
        return finalString;
      }

      char* CString::Concatenate ( const char** cStrings, int number ) {
        int total = 0;
        int* sizes = ( int* ) org::process::MemoryManager::Malloc ( number, sizeof ( int ) );
        for ( int i = 0; i < number; i ++ )
          total += sizes [i] = GetCCharArrayLength ( cStrings [i] );
        char* fullCString = GetCharArray ( total + 1 );
        int current = 0;
        for ( int i = 0; i < number; current += sizes [i++] )
          PutIn ( cStrings [i], fullCString, 0, current, sizes [i] );
        org::process::MemoryManager::FreeM ( ( void* ) sizes );
        return fullCString;
      }

      char * CString::Concatenate ( const char** cStrings, int number, const char * separator ) {
        int total = 0;
        int* sizes = ( int* ) org::process::MemoryManager::Malloc ( number, sizeof ( int ) );
        int separatorLength = GetCCharArrayLength ( separator );
        for ( int i = 0; i < number; i ++ )
          total += sizes [i] = GetCCharArrayLength ( cStrings [i] );
        char* fullCString = GetCharArray ( total + 1 + ( separatorLength * number ) );
        int current = 0;
        for ( int i = 0; i < number; current += ( sizes [i++] + separatorLength ) ) {
          PutIn ( cStrings [i], fullCString, 0, current, sizes [i] );
          if ( separatorLength > 0 && i < number - 1 ) {
            PutIn ( separator, fullCString, 0, current + sizes [i], separatorLength );
          }
        }
        org::process::MemoryManager::FreeM ( ( void * ) sizes );
        return fullCString;
      }

      void CString::PutIn ( const char * from, char* to, int fromHere, int toHere, int forAsMuch ) {
        if ( from != nullptr && to != nullptr )
          for ( int i = 0; i < forAsMuch; i ++ )
            to [toHere + i] = from [fromHere + i];
      }

      int CString::IsPartiallyEqual ( const char * string1, const char * string2 ) {
        int difference = 1;
        int i = 0;
        if ( string1 != nullptr && string2 != nullptr )
          for ( ; string1 [i] != 0 && string2 [i] != 0 && difference == 1; i ++ )
            difference = string1 [i] != string2 [i] ? 0 : 1;
        return difference;
      }

      bool CString::IsEqual ( const char * string1, const char * string2 ) {
        bool difference = string1 != nullptr && string2 != nullptr;
        int i = 0;
        if ( difference ) {
          for ( ; string1 [i] != 0 && string2 [i] != 0 && difference == 1; i ++ )
            difference = string1 [i] != string2 [i] ? false : true;
          difference = difference & ( string1 [i] == string2 [i] );
        }
        return difference;
      }

      bool CString::IsEmpty ( const char * string1 ) {
        return string1 != nullptr ? CString::IsEqual ( string1, CONSTANT_EMPTY_STRING ) : true;
      }

      char* CString::Filter ( char* toFilter, char toHide ) {
        int length = GetCCharArrayLength ( toFilter );
        int occurences = CountOccurences ( toFilter, toHide );
        char* filtered = GetCharArray ( length - occurences + 1 );
        for ( int i = 0, j = 0; i < length; i ++ )
          if ( toFilter [i] != toHide )
            filtered [j ++] = toFilter [i];
        return filtered;
      }

      char** CString::Split ( const char * toSplit, char splitting, int * count, int maxParts ) {
        int partsCount = CountOccurences ( toSplit, splitting ) + 1;
        if ( partsCount > maxParts && maxParts > 0 )
          partsCount = maxParts;
        char** parts = GetStringArray ( partsCount );
        int firstIndex = FindFirstIndex ( toSplit, splitting );
        int partCursor = 0;
        while ( firstIndex != -1 ) {
          parts [partCursor ++] = GetStringFrom ( toSplit, firstIndex );
          toSplit = &( toSplit [firstIndex + 1] );
          if ( maxParts > 0 && ( maxParts - 1 ) >= partCursor )
            break;
          firstIndex = FindFirstIndex ( toSplit, splitting );
        }
        parts [partCursor] = GetCopyOfCString ( toSplit );
        if ( count != nullptr )
          *count = partsCount;
        return parts;
      }

      char** CString::SpaceSplit ( const char * toSplit, int* count ) {
        return Split ( toSplit, LITERAL_SPACE, count, -1, true, true );
      }

      char** CString::Split ( const char * toSplit, char splitting, int* count ) {
        return Split ( toSplit, splitting, count, -1, true, true );
      }
      
      char** CString::Split ( const char * toSplit, char splitting, int* count, int maxParts, bool trim, bool removeEmptyEntries ) {
        char** finalSplit = CString::Split ( toSplit, splitting, count, maxParts );
        if ( trim ) {
          char** trimmed = ( char** ) org::process::MemoryManager::Malloc ( *count, sizeof ( char* ) );
          for ( int i = 0; i < *count; i ++ ) {
            trimmed [i] = CString::Trim ( finalSplit [i] );
            org::process::MemoryManager::FreeM ( ( void* ) finalSplit [i] );
          }
          org::process::MemoryManager::FreeM ( ( void* ) finalSplit );
          finalSplit = trimmed;
        }
        if ( removeEmptyEntries ) {
          int newCount = *count;
          char** cleaned = CString::RemoveEmptyEntries ( finalSplit, *count, &newCount );
          for ( int i = 0; i < *count; i ++ )
            org::process::MemoryManager::FreeM ( ( void* ) finalSplit [i] );
          org::process::MemoryManager::FreeM ( ( void* ) finalSplit );
          finalSplit = cleaned;
          *count = newCount;
        }
        return finalSplit;
      }

      char** CString::SplitAndTrim ( const char * toSplit, char splitting, int* count, int maxParts ) {
        char** splited = CString::Split ( toSplit, splitting, count, maxParts );
        char** trimmed = ( char** ) org::process::MemoryManager::Malloc ( *count, sizeof ( char* ) );
        for ( int i = 0; i < *count; i ++ ) {
          trimmed [i] = CString::Trim ( splited [i] );
          org::process::MemoryManager::FreeM ( ( void* ) splited [i] );
        }
        org::process::MemoryManager::FreeM ( ( void* ) splited );
        return trimmed;
      }

      char** CString::SplitAndRemoveEmptyEntries ( const char * toSplit, char splitting, int* count, int maxParts ) {
        char** split = Split ( toSplit, splitting, count, maxParts );
        int splitCount = *count;
        char** clean = RemoveEmptyEntries ( split, splitCount, count );
        FreeStringArray ( split, splitCount );
        return clean;
      }

      char** CString::RemoveEmptyEntries ( char** stringArray, int size, int* newSize ) {
        *newSize = size;
        for ( int i = 0; i < size; i ++ )
          if ( !( GetCCharArrayLength ( stringArray [i] ) > 0 ) )
            ( *newSize ) --;
        char** newArray = GetStringArray ( *newSize );
        for ( int i = 0, j = 0; i < size; i ++ )
          if ( GetCCharArrayLength ( stringArray [i] ) > 0 )
            newArray [j ++] = GetCopyOfCString ( stringArray [i] );
        return newArray;
      }

      int CString::CountInRange ( const char * cString, char lowerBound, char upperBound ) {
        int occurences = 0;
        int i = 0;
        if ( cString != nullptr )
          while ( cString [i] != 0 ) {
            if ( cString [i] > lowerBound && cString [i] < upperBound )
              occurences ++;
            i ++;
          }
        return occurences;
      }

      int CString::CountOccurences ( const char * cString, char value ) {
        int occurences = 0;
        int i = 0;
        while ( cString [i] != 0 )
          if ( cString [i ++] == value )
            occurences ++;
        return occurences;
      }

      char* CString::OldTrim ( char* toTrim ) {
        int length = GetCCharArrayLength ( toTrim );
        int occurences = CountInRange ( toTrim, 31, 127 );
        char* filtered = GetCharArray ( occurences + 1 );
        for ( int i = 0, j = 0; i < length; i ++ )
          if ( toTrim [i] > 31 && toTrim [i] < 127 )
            filtered [j ++] = toTrim [i];
        return filtered;
      }

      char* CString::Trim ( const char * toTrim ) {
        char* trimmed = nullptr;
        if ( toTrim != nullptr ) {
          trimmed = CString::TrimStart ( toTrim );
          char* tmp = trimmed;
          trimmed = CString::TrimEnd ( trimmed );
          org::process::MemoryManager::FreeM ( ( void* ) tmp );
        }
        return trimmed;
      }

      char* CString::TrimStart ( const char * toTrim ) {
        char* trimmed = nullptr;
        int endIndex = CString::FindInRange ( toTrim, 32, 127, 0, -1, 1 );
        char* trimmedStart = CString::TrimRangeZone ( toTrim, 0, 33, 0, endIndex );
        trimmed = trimmedStart;
        trimmed = CString::TrimRangeZone ( trimmed, 126, 255, 0, endIndex );
        org::process::MemoryManager::FreeM ( ( void* ) trimmedStart );
        return trimmed;
      }

      char* CString::TrimEnd ( const char * toTrim ) {
        char* trimmed = nullptr;
        int startIndex = CString::FindInRange ( toTrim, 32, 127, 0, -1, -1 );
        char* trimmedStart = CString::TrimRangeZone ( toTrim, 0, 33, startIndex, -1 );
        trimmed = trimmedStart;
        trimmed = CString::TrimRangeZone ( trimmed, 126, 255, startIndex, -1 );
        org::process::MemoryManager::FreeM ( ( void* ) trimmedStart );
        return trimmed;
      }
      
      char* CString::TrimRangeZone ( const char * toTrim, char lowerBound, char upperBound ) {
        return CString::TrimRangeZone ( toTrim, lowerBound, upperBound, 0, -1 );
      }

      char* CString::TrimRangeZone ( const char * toTrim, char lowerBound, char upperBound, int start, int end ) {
        if ( end == -1 )
          end = CString::GetCCharArrayLength ( toTrim );
        int length = GetCCharArrayLength ( toTrim );
        int occurences = CountInRange ( toTrim, lowerBound, upperBound, start, end );
        char* filtered = GetCharArray ( length - occurences + 1 );
        for ( int i = 0, j = 0; i < length; i ++ )
          if ( i < start || i >= end )
            filtered [j ++] = toTrim [i];
          else if ( !( toTrim [i] > lowerBound && toTrim [i] < upperBound ) )
            filtered [j ++] = toTrim [i];
        return filtered;
      }

      int CString::CountInRange ( const char * cString, char lowerBound, char upperBound, int start, int end ) {
        int count = 0;
        int length = CString::GetCCharArrayLength ( cString );
        if ( end >= length )
          end = length - 1;
        if ( cString != nullptr )
          for ( int i = start >= 0 ? start : 0; i < end; i ++ )
            if ( cString [i] > lowerBound && cString [i] < upperBound )
              count ++;
        return count;
      }

      int CString::FindFirstIndex ( const char * cString, char value ) {
        int index = -1;
        int i = 0;
        while ( cString [i] != 0 && index == -1 )
          if ( cString [i ++] == value )
            index = i - 1;
        return index;
      }

      int CString::FindLastIndex ( char* cString, char value ) {
        return FindLastIndex ( cString, value, -1 );
      }

      int CString::FindLastIndex ( char* cString, char value, int beforeIndex ) {
        int index = -1;
        int i = 0;
        while ( cString [i] != 0 && ( i < beforeIndex || beforeIndex == -1 ) )
          if ( cString [i ++] == value )
            index = i - 1;
        return index;
      }

      int CString::FindInRange ( const char * cString, char lowerBound, char upperBound, int start, int beforeIndex, int maximumTimes ) {
        int index = -1;
        int i = start;
        int times = 0;
        int length = CString::GetCCharArrayLength ( cString );
        if ( cString != nullptr )
          while ( i < length && cString [i] != 0 && ( i < beforeIndex || beforeIndex == -1 ) && ( times < maximumTimes || maximumTimes == -1 ) )
            if ( cString [i ++] > lowerBound && cString [i ++] < upperBound ) {
              index = i - 1;
              times ++;
            }
        return index;
      }

      int CString::Find ( const char * cString, char value, int start, int beforeIndex, int maximumTimes ) {
        int index = -1;
        int i = start;
        int times = 0;
        while ( cString [i] != 0 && ( i < beforeIndex || beforeIndex == -1 ) && ( times < maximumTimes || maximumTimes == -1 ) )
          if ( cString [i ++] == value ) {
            index = i - 1;
            times ++;
          }
        return index;
      }

      int CString::Find ( const char * cString, const char * value, int start, int beforeIndex, int maximumTimes ) {
        int index = -1;
        int i = start;
        int times = 0;
        while ( cString [i] != 0 && ( i < beforeIndex || beforeIndex == -1 ) && ( times < maximumTimes || maximumTimes == -1 ) )
          if ( CString::IsPartiallyEqual ( &( cString [i ++] ), value ) ) {
            index = i - 1;
            times ++;
          }
        return index;
      }

      bool CString::IsMasked ( const char * cString, const char * mask ) {
        bool contains = cString != nullptr && mask != nullptr;
        int i = 0;
        while ( mask [i] != 0 && contains ) {
          if ( cString [i] != 0 ) {
            if ( cString [i] != mask [i] ) {
              contains = false;
            }
          } else
            contains = false;
          i ++;
        }
        return contains == 0 ? false : true;
      }
      
      char* CString::Replace ( const char * cString, int startIndex, int endIndex, const char * replacement ) {
        int finalLength = GetCCharArrayLength ( cString );
        finalLength = finalLength - endIndex + startIndex;
        finalLength += GetCCharArrayLength ( replacement ) + 1;
        char* newString = GetCharArray ( finalLength );
        int i = 0;
        int old = 0;
        int adding = 0;
        while ( cString [old] != 0 )
          if ( old == startIndex ) {
            while ( replacement [adding] != 0 )
              newString [i ++] = replacement [adding ++];
            old = endIndex;
          } else
            newString [i ++] = cString [old ++];
        return newString;
      }

      char* CString::Replace ( const char * cString, const char * toReplace, const char * replacement ) {
        char* finalString = GetCopyOfCString ( cString );
        char* tmpString = finalString;
        int length = CString::GetCCharArrayLength ( toReplace );
        int index = CString::Find ( finalString, toReplace, 0, -1, 1 );
        while ( index != -1 ) {
          finalString = CString::Replace ( tmpString, index, index + length, replacement );
          org::process::MemoryManager::FreeM ( ( void* ) tmpString );
          tmpString = finalString;
          index = CString::Find ( tmpString, toReplace, index + CString::GetCCharArrayLength ( replacement ), -1, 1 );
        }
        return finalString;
      }

      char* CString::CutAtValue ( char* toCut, char value ) {
        int index = FindFirstIndex ( toCut, value );
        char* cut = nullptr;
        if ( index != -1 ) {
          cut = GetStringFrom ( toCut, index );
        }
        return cut;
      }

      int CString::ToInt ( const char * integerString ) {
        int asInt = 0;
        if ( integerString != nullptr )
          asInt = lib::Stdlib::Atoi ( integerString );
        return asInt;
      }

      int CString::FindAllArgsInFormat (const char* format, char*** argsFormatArray, int maxNumber) {
        int argCount = 0;
        int i = 0;
        char* newFormat = nullptr;
        *argsFormatArray = (char**) org::process::MemoryManager::Malloc ( maxNumber, sizeof ( char * ) );
        for ( i = 0; i < maxNumber; i ++ )
          ( *argsFormatArray ) [i] = nullptr;
        i = 0;
        while ( format [i] != 0 ) {
          if ( format [i] == LITERAL_PERCENT ) {
            newFormat = FindFormat (&format [i]);
            if ( newFormat != nullptr ) {
              ( *argsFormatArray ) [argCount ++] = newFormat;
            }
          }
          i++;
        }
        return argCount;
      }

      bool CString::IsInRange ( char character, int lowerBoung, int upperBound ) {
        return ( character >= lowerBoung && character <= upperBound ) ? true : false;
      }

      bool CString::IsCharacter ( char character ) {
        return CString::IsUpperCharacter ( character ) || CString::IsLowerCharacter ( character );
      }

      bool CString::IsUpperCharacter ( char character ) {
        return ( character > 64 && character < 91 ) ? true : false;
      }

      bool CString::IsLowerCharacter ( char character ) {
        return ( character > 96 && character < 123 ) ? true : false;
      }

      bool CString::IsNumberCharacter ( char character ) {
        return ( character >= 48 && character <= 57 ) ? true : false;
      }

      bool CString::IsPonctuationCharacter ( char character ) {
        return  CString::IsInRange ( character, 32, 47 ) || CString::IsInRange ( character, 58, 64 ) ||
                CString::IsInRange ( character, 91, 96 ) || CString::IsInRange ( character, 123, 126 );
      }

      bool CString::IsPrintableCharacter ( char character ) {
        return CString::IsCharacter ( character ) || CString::IsNumberCharacter ( character ) || CString::IsPonctuationCharacter ( character );
      }

      bool CString::CharacterIsInArray (char character, const char* array, int size) {
        bool found = 0;
        int i = 0;
        for (i = 0; i < size && !found; i++)
          found = array [i] == character;
        return found;
      }

      char* CString::FindFormat (const char* fromHere) {
        char* ans = nullptr;
        bool canContinue = true;
        int cursor = 1;
        if (fromHere [0] == LITERAL_PERCENT)
          while (fromHere [cursor] != 0 && canContinue) {
            if (IsCharacter (fromHere [cursor])) {
              canContinue = false; 
              if (CharacterIsInArray (fromHere [cursor], CONSTANT_FROMAT_LENGTH_CHARACTERS, 6)) {
                if ((fromHere [cursor] == LITERAL_H && fromHere [cursor + 1] == LITERAL_H) || (fromHere [cursor] == LITERAL_L && fromHere [cursor + 1] == LITERAL_L))
                  cursor ++;
                cursor ++;
              }
              if (CharacterIsInArray (fromHere [cursor], CONSTANT_STRING_FROMAT_CHARACTERS, 17)) {
                ans = GetStringFrom (fromHere, cursor + 1);
              }
            }
            cursor ++;
          }
        return ans;
      }

      char** CString::CreateArgStrings (char** formats, void** args, int argCount) {
        char** argStrings = (char**) org::process::MemoryManager::Malloc (argCount, sizeof (char*));
        int i = 0;
        for (i = 0; i < argCount; i ++)
          argStrings [i] = CreateArgString (formats [i], args [i]);
        return argStrings;
      }

      char* CString::CreateArgString (char* format, void* arg) {
        char* buffer = (char*) org::process::MemoryManager::Malloc (30, sizeof(char));
        char* str = nullptr;
        char argType = GetArgTypeFromFormat (format);
        if (argType == LITERAL_S) {
          org::process::MemoryManager::FreeM (buffer);
          buffer = GetCopyOfCString ((char*) arg);
        } else
          lib::Stdio::SPrintF (buffer, format, ((char*)arg));
        str = GetCopyOfCString (buffer);
        org::process::MemoryManager::FreeM (buffer);
        return str;
      }

      char CString::GetArgTypeFromFormat (char* format) {
        return format [CString::GetCCharArrayLength (format) - 1];
      }

      char* CString::FormatString (const char * format, int argc, void** args) {
        char** argsFormats = nullptr;
        char** argStrings = nullptr;
        char* finalString = nullptr;
        int i = 0;
        int argCursor = 0;
        int argValueCursor = 0;
        int formatCursor = 0;
        char * currentFormat = nullptr;
        int formatLength = 0;
        int totalLength = GetCCharArrayLength (format);
        int argNumber = FindAllArgsInFormat (format, &argsFormats, argc);
        
        argStrings = CreateArgStrings (argsFormats, args, argc);
        for (i = 0; i < argNumber; i ++)
          totalLength += GetCCharArrayLength (argStrings [i]) - GetCCharArrayLength (argsFormats [i]);
        
        finalString = GetCharArray (totalLength + 1);

        i = 0;
        formatCursor = 0;
        while (format [formatCursor] != 0) {
          if (format [formatCursor] == LITERAL_PERCENT) {
            currentFormat = CString::FindFormat ( &format [formatCursor] );
            formatLength = currentFormat != nullptr ? CString::GetCCharArrayLength ( currentFormat ) : 0;
            org::process::MemoryManager::FreeM ( currentFormat );
            if (formatLength > 0) {
              argValueCursor = 0;
              while (argStrings [argCursor] [argValueCursor] != 0)
                finalString [i++] = argStrings [argCursor] [argValueCursor++];
              formatCursor += GetCCharArrayLength (argsFormats [argCursor]) - 1;
              argCursor ++;
            } else
              finalString [i++] = format [formatCursor];
          } else
            finalString [i++] = format [formatCursor];
          formatCursor ++;
        }
        finalString [totalLength] = 0;

        org::process::MemoryManager::FreeMA ((void**) argsFormats, argNumber);
        org::process::MemoryManager::FreeMA ((void**) argStrings, argNumber);
        return finalString;
      }

      char * CString::PutBetween ( const char * cString, char surroundings ) {
        return PutBetween ( cString, surroundings, surroundings );
      }

      char * CString::PutBetween ( const char * cString, char prefix, char suffix ) {
        int length = CString::GetCCharArrayLength ( cString );
        char* fullCString = GetCharArray ( length + 3 );
        fullCString [0] = prefix;
        fullCString [length + 1] = suffix;
        for ( int i = 0; i < length; i ++ )
          fullCString [i + 1] = cString [i];
        return fullCString;
      }

      char * CString::PutBetween ( const char * cString, const char * prefix, const char * suffix ) {
        int length = CString::GetCCharArrayLength ( cString );
        int preLength = CString::GetCCharArrayLength ( prefix );
        int sufLength = CString::GetCCharArrayLength ( suffix );
        char* fullCString = GetCharArray ( length + preLength + sufLength + 1 );
        PutIn ( prefix, fullCString, 0, 0, preLength );
        PutIn ( cString, fullCString, 0, preLength, length );
        PutIn ( suffix, fullCString, 0, preLength + length, sufLength );
        return fullCString;
      }

      char CString::ToLower ( char character ) {
        return CString::IsUpperCharacter ( character ) ? character + 32 : character;
      }

      char CString::ToUpper ( char character ) {
        return CString::IsLowerCharacter ( character ) ? character - 32 : character;
      }

      char * CString::ToLower ( const char * toLower ) {
        char * ans = CString::GetCopyOfCString ( toLower );
        int i = 0;
        while ( ans [i] != 0 ) {
          ans [i] = CString::ToLower ( ans [i] );
          i ++;
        }
        return ans;
      }

      char * CString::ToUpper ( const char * toUpper ) {
        char * ans = CString::GetCopyOfCString ( toUpper );
        int i = 0;
        while ( ans [i] != 0 ) {
          ans [i] = CString::ToUpper ( ans [i] );
          i ++;
        }
        return ans;
      }

      bool CString::Contains ( const char * toLookIn, char toLookFor ) {
        bool answer = toLookIn != nullptr;
        if ( answer ) {
          int index = 0;
          while ( toLookIn [index] != toLookFor && toLookIn [index] != 0 ) {
            index ++;
          }
          answer = toLookIn [index] != 0;
        }
        return answer;
      }

      char * CString::Remove ( const char * cString, int from, int to ) {
        char * newString = nullptr;
        int length = CString::GetCCharArrayLength ( cString );
        if ( to > length || to == -1 )
          to = length;
        if ( from < 0 )
          from = 0;
        if ( from < to ) {
          int difference = to - from;
          int size = CString::GetCCharArrayLength ( cString ) - difference + 1;
          int offset = 0;
          newString = CString::GetCharArray ( size );
          for ( int i = 0; i < size; i ++ ) {
            if ( i >= from && offset == 0 )
              offset = difference;
            newString [i] = cString [i + offset];
          }
          newString [size - 1] = '\0';
        }
        return newString;
      }

      int CString::FindClosing ( const char * str, int start, int end, char closing, const char * openings, const char * closings, const char * inhibiting, const char * closingInhibiting, const char * escapeCharacters ) {
        int found = -1;
        int cursor = start;
        bool inhibited = false;
        bool escaping = false;
        int currentLevel = 0;
        if ( start < CString::GetCCharArrayLength ( str ) )
          while ( str [cursor] != '\0' && cursor < end ) {
            if ( str [cursor] == closing && ! inhibited && currentLevel == 0 ) {
              found = cursor;
              break;
            }
            
            if ( inhibited && CString::FindFirstIndex ( escapeCharacters, str [cursor] ) >= 0 ) {
              escaping = true;
              goto next;
            } else if ( CString::FindFirstIndex ( inhibiting, str [cursor] ) >= 0 ) {
              if ( ! escaping )
                inhibited = ! inhibited;
            } else if ( CString::FindFirstIndex ( openings, str [cursor] ) >= 0 && ! inhibited ) {
              currentLevel ++;
            } else if ( CString::FindFirstIndex ( closings, str [cursor] ) >= 0 && ! inhibited ) {
              currentLevel --;
            }
            
            escaping = false;
            
          next:
            cursor ++;
          }

        return found;
      }

      int CString::FindOccurence ( const char * str, int start, int end, char character, int occurence, const char * inhibiting, const char * escapeCharacters ) {
        int found = -1;
        int cursor = start;
        bool inhibited = false;
        bool escaping = false;
        int occurenceCount = 0;
        if ( start < CString::GetCCharArrayLength ( str ) )
          while ( str [cursor] != '\0' && cursor < end ) {
            if ( str [cursor] == character && ! inhibited ) {
              occurenceCount ++;
              if ( occurenceCount == occurence ) {
                found = cursor;
                break;
              }
            }
            
            if ( inhibited && CString::FindFirstIndex ( escapeCharacters, str [cursor] ) >= 0 ) {
              escaping = true;
              goto next;
            } else if ( CString::FindFirstIndex ( inhibiting, str [cursor] ) >= 0 ) {
              if ( ! escaping )
                inhibited = ! inhibited;
            }
            
            escaping = false;
            
          next:
            cursor ++;
          }

        return found;
      }

      const char * CString::Substring ( const char * str, int start, int end ) {
        const char * subString = nullptr;
        int length = CString::GetCCharArrayLength ( str );
        if ( start < length ) {
          if ( end > length )
            end = length - 1;
          subString = CString::GetStringFrom ( & str [start], end - start );
        }
        return subString;
      }
    }
  }
}
