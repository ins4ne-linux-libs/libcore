


#include <tools/misc/wait.hpp>

#include <lib/freax/unistd.hpp>

namespace org {
  namespace tools {
    namespace misc {
      void Wait::Seconds ( int seconds ) {
        Wait::Milliseconds ( seconds * 1000 );
      }

      void Wait::Milliseconds ( int milliseconds ) {
        Wait::Microseconds ( milliseconds * 1000 );
      }

      void Wait::Microseconds ( int microseconds ) {
        lib::freax::Unistd::USleep ( microseconds );
      }
    }
  }
}
