
#include <types/cstring.hpp>

#include <tools/misc/stringextension.hpp>
#include <lib/stdlib.hpp>
#include <abstraction/memory.hpp>

namespace org {
  namespace types {
    const char * CString::ConversionConstChar ( void ) const {
      return this -> _string;
    }
    
    CString& CString::OperatorAssignCString ( const CString& cString ) {
      this -> SaveString ( cString.c_str ( ) );
      return * this;
    }
    
    CString& CString::OperatorAssignConstChar ( const char * cString ) {
      this -> SaveString ( cString );
      return * this;
    }
    
    CString& CString::OperatorAddConstChar ( const char * cString ) {
      this -> Add ( cString );
      return * this;
    }

    CString& CString::OperatorAddChar ( char character ) {
      char toAdd [2] = { character, '\0' };
      this -> Add ( toAdd );
      return * this;
    }

    CString& CString::OperatorAddCString ( CString& cString ) {
      this -> Add ( cString.c_str ( ) );
      return * this;
    }
    
    bool CString::OperatorEqual ( const CString &b ) const {
      return org::tools::misc::CString::IsEqual ( this -> _string, b._string );
    }

    bool CString::OperatorNotEqual ( const CString &b ) const {
      return ! org::tools::misc::CString::IsEqual ( this -> _string, b._string );
    }
    
    bool CString::OperatorEqual ( const char * b ) const {
      return org::tools::misc::CString::IsEqual ( this -> _string, b );
    }

    bool CString::OperatorNotEqual ( const char * b ) const {
      return ! org::tools::misc::CString::IsEqual ( this -> _string, b );
    }
    
    bool CString::OperatorLess ( const CString &b ) const {
      bool isLess = false;
      bool isEqual = true;
      int i = 0;
      int minimum = this -> _length > b._length ? b._length : this -> _length;
      bool isLonger = this -> _length > b._length;
      for ( ; i < minimum && isEqual; i ++ )
        isEqual = this -> _string [i] == b._string [i];
      if ( isEqual )
        isLess = isLonger;
      else
        isLess = this -> _string [i - 1] > b._string [i - 1];

      return isLess;
    }

    void CString::InitializeCString ( void ) {
      this -> _string = nullptr;
      this -> _length = 0;
      this -> _hasSavedString = false;
      this -> _automaticAllocation = false;
    }

    void CString::SaveString ( const char * cString ) {
      this -> SaveString ( cString, false );
    }
    
    void CString::SaveString ( const char * cString, bool useString ) {
      char * old = this -> _string;
      this -> _hasSavedString = ! useString;
      this -> _string = useString ? ( char * ) cString : org::tools::misc::CString::GetCopyOfCString ( cString );
      this -> _length = org::tools::misc::CString::GetCCharArrayLength ( this -> _string );
      lib::Stdlib::Free ( old );
    }

    CString::CString ( void ) {
      this -> InitializeCString ( );
    }
    
    CString::CString ( const char * cString ) {
      this -> InitializeCString ( );
      this -> SaveString ( cString );
    }
    
    CString::CString ( const char * cString, bool useString ) {
      this -> InitializeCString ( );
      this -> SaveString ( cString, useString );
    }
    
    CString::CString ( const CString &obj ) {
      this -> InitializeCString ( );
      this -> SaveString ( obj.c_str ( ) );
    }

    CString::~CString ( void ) {
      if ( this -> _hasSavedString )
        // if ( ! this -> _automaticAllocation )
          lib::Stdlib::Free ( ( void * ) this -> _string );
    }

    int CString::length ( void ) {
      return this -> _length;
    }

    char CString::back ( void ) {
      return this -> _string [this -> _length - 1];
    }

    char CString::at ( int index ) {
      return this -> _string [index];
    }

    void CString::append ( const char * toAppend ) {
      this -> Add ( toAppend );
    }

    const char * CString::c_str ( void ) const {
      return this -> _string;
    }
    
    void CString::InsertAt ( const char * toInsert, int index ) {
      CString start = this -> c_str ( );
      start.Cut ( index );
      CString end = this -> c_str ( );
      end.Cut ( 0, index );
      CString newStr = start;
      newStr += toInsert;
      newStr += end;

      this -> SaveString ( newStr.c_str ( ) );
    }
    
    void CString::InsertBefore ( const char * toInsert ) {
      CString newStr = toInsert;
      newStr += *this;
      this -> SaveString ( newStr.c_str ( ) );
    }

    void CString::Add ( const char * atTheEnd ) {
      char * concat = org::tools::misc::CString::Concatenate ( this -> _string, atTheEnd );
      this -> SaveString ( concat );
      lib::Stdlib::Free ( concat );
    }
    
    void CString::Cut ( int from, int to ) {
      this -> SaveString ( org::tools::misc::CString::Remove ( this -> _string, from, to ) );
    }
    
    void CString::Cut ( int from ) {
      this -> Cut ( from, this -> _length );
    }

    void CString::Trim ( void ) {
      this -> SaveString ( org::tools::misc::CString::Trim ( this -> _string ) );
    }

    int CString::FindClosing ( int start, int end, char closing, const char * openings, const char * closings, const char * inhibiting, const char * closingInhibiting, const char * escapeCharacters ) {
      return org::tools::misc::CString::FindClosing ( this -> _string, start, end, closing, openings, closings, inhibiting, closingInhibiting, escapeCharacters );
    }

    int CString::FindClosing ( int start, char closing, const char * openings, const char * closings, const char * inhibiting, const char * closingInhibiting, const char * escapeCharacters ) {
      return this -> FindClosing ( start, this -> _length, closing, openings, closings, inhibiting, closingInhibiting, escapeCharacters );
    }

    int CString::FindClosing ( char closing, const char * openings, const char * closings, const char * inhibiting, const char * closingInhibiting, const char * escapeCharacters ) {
      return this -> FindClosing ( 0, closing, openings, closings, inhibiting, closingInhibiting, escapeCharacters );
    }

    int CString::FindOccurence ( int start, int end, char character, int occurence, const char * inhibiting, const char * escapeCharacters ) {
      return org::tools::misc::CString::FindOccurence ( this -> _string, start, end, character, occurence, inhibiting, escapeCharacters );
    }

    int CString::FindOccurence ( char character, int occurence, const char * inhibiting, const char * escapeCharacters ) {
      return this -> FindOccurence ( 0, this -> _length, character, occurence, inhibiting, escapeCharacters );
    }
    
    int CString::Find ( const char * value, int start, int beforeIndex, int maximumTimes ) {
      return org::tools::misc::CString::Find ( this -> _string, value, start, beforeIndex, maximumTimes );
    }

    void CString::Replace ( const char * toReplace, const char * replacement ) {
      this -> SaveString ( org::tools::misc::CString::Replace ( this -> _string, toReplace, replacement ) );
    }

    CString * CString::SubString ( int start, int end ) {
      return new CString ( org::tools::misc::CString::Substring ( this -> _string, start, end ) );
    }

    CString * CString::SubString ( int start ) {
      return this -> SubString ( start, this -> _length );
    }

    CString ** CString::Split ( char splitting, int * count, int maxParts, bool trim, bool removeEmptyEntries ) {
      char ** splited = org::tools::misc::CString::Split ( this -> _string, splitting, count, maxParts, trim, removeEmptyEntries );
      CString ** ans = ( CString ** ) org::abstraction::Memory::Allocate ( *count * sizeof ( CString * ) );
      for ( int i = 0; i < *count; i ++ )
        ( ans [i] = new CString ( splited [i], true ) ) -> Trim ( );
      lib::Stdlib::Free ( ( void * ) splited );
      return ans;
    }
    
    CString * CString::Copy ( void ) {
      return new CString ( this -> _string );
    }

    const char * CString::ObjectType ( void ) const {
      return "org::types::CString";
    }
  }
}